/*!
 * jQuery JavaScript Library v2.1.3
 * http://jquery.com/
 *
 * Includes Sizzle.js
 * http://sizzlejs.com/
 *
 * Copyright 2005, 2014 jQuery Foundation, Inc. and other contributors
 * Released under the MIT license
 * http://jquery.org/license
 *
 * Date: 2014-12-18T15:11Z
 */

(function( global, factory ) {

	if ( typeof module === "object" && typeof module.exports === "object" ) {
		// For CommonJS and CommonJS-like environments where a proper `window`
		// is present, execute the factory and get jQuery.
		// For environments that do not have a `window` with a `document`
		// (such as Node.js), expose a factory as module.exports.
		// This accentuates the need for the creation of a real `window`.
		// e.g. var jQuery = require("jquery")(window);
		// See ticket #14549 for more info.
		module.exports = global.document ?
			factory( global, true ) :
			function( w ) {
				if ( !w.document ) {
					throw new Error( "jQuery requires a window with a document" );
				}
				return factory( w );
			};
	} else {
		factory( global );
	}

// Pass this if window is not defined yet
}(typeof window !== "undefined" ? window : this, function( window, noGlobal ) {

// Support: Firefox 18+
// Can't be in strict mode, several libs including ASP.NET trace
// the stack via arguments.caller.callee and Firefox dies if
// you try to trace through "use strict" call chains. (#13335)
//

var arr = [];

var slice = arr.slice;

var concat = arr.concat;

var push = arr.push;

var indexOf = arr.indexOf;

var class2type = {};

var toString = class2type.toString;

var hasOwn = class2type.hasOwnProperty;

var support = {};



var
	// Use the correct document accordingly with window argument (sandbox)
	document = window.document,

	version = "2.1.3",

	// Define a local copy of jQuery
	jQuery = function( selector, context ) {
		// The jQuery object is actually just the init constructor 'enhanced'
		// Need init if jQuery is called (just allow error to be thrown if not included)
		return new jQuery.fn.init( selector, context );
	},

	// Support: Android<4.1
	// Make sure we trim BOM and NBSP
	rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,

	// Matches dashed string for camelizing
	rmsPrefix = /^-ms-/,
	rdashAlpha = /-([\da-z])/gi,

	// Used by jQuery.camelCase as callback to replace()
	fcamelCase = function( all, letter ) {
		return letter.toUpperCase();
	};

jQuery.fn = jQuery.prototype = {
	// The current version of jQuery being used
	jquery: version,

	constructor: jQuery,

	// Start with an empty selector
	selector: "",

	// The default length of a jQuery object is 0
	length: 0,

	toArray: function() {
		return slice.call( this );
	},

	// Get the Nth element in the matched element set OR
	// Get the whole matched element set as a clean array
	get: function( num ) {
		return num != null ?

			// Return just the one element from the set
			( num < 0 ? this[ num + this.length ] : this[ num ] ) :

			// Return all the elements in a clean array
			slice.call( this );
	},

	// Take an array of elements and push it onto the stack
	// (returning the new matched element set)
	pushStack: function( elems ) {

		// Build a new jQuery matched element set
		var ret = jQuery.merge( this.constructor(), elems );

		// Add the old object onto the stack (as a reference)
		ret.prevObject = this;
		ret.context = this.context;

		// Return the newly-formed element set
		return ret;
	},

	// Execute a callback for every element in the matched set.
	// (You can seed the arguments with an array of args, but this is
	// only used internally.)
	each: function( callback, args ) {
		return jQuery.each( this, callback, args );
	},

	map: function( callback ) {
		return this.pushStack( jQuery.map(this, function( elem, i ) {
			return callback.call( elem, i, elem );
		}));
	},

	slice: function() {
		return this.pushStack( slice.apply( this, arguments ) );
	},

	first: function() {
		return this.eq( 0 );
	},

	last: function() {
		return this.eq( -1 );
	},

	eq: function( i ) {
		var len = this.length,
			j = +i + ( i < 0 ? len : 0 );
		return this.pushStack( j >= 0 && j < len ? [ this[j] ] : [] );
	},

	end: function() {
		return this.prevObject || this.constructor(null);
	},

	// For internal use only.
	// Behaves like an Array's method, not like a jQuery method.
	push: push,
	sort: arr.sort,
	splice: arr.splice
};

jQuery.extend = jQuery.fn.extend = function() {
	var options, name, src, copy, copyIsArray, clone,
		target = arguments[0] || {},
		i = 1,
		length = arguments.length,
		deep = false;

	// Handle a deep copy situation
	if ( typeof target === "boolean" ) {
		deep = target;

		// Skip the boolean and the target
		target = arguments[ i ] || {};
		i++;
	}

	// Handle case when target is a string or something (possible in deep copy)
	if ( typeof target !== "object" && !jQuery.isFunction(target) ) {
		target = {};
	}

	// Extend jQuery itself if only one argument is passed
	if ( i === length ) {
		target = this;
		i--;
	}

	for ( ; i < length; i++ ) {
		// Only deal with non-null/undefined values
		if ( (options = arguments[ i ]) != null ) {
			// Extend the base object
			for ( name in options ) {
				src = target[ name ];
				copy = options[ name ];

				// Prevent never-ending loop
				if ( target === copy ) {
					continue;
				}

				// Recurse if we're merging plain objects or arrays
				if ( deep && copy && ( jQuery.isPlainObject(copy) || (copyIsArray = jQuery.isArray(copy)) ) ) {
					if ( copyIsArray ) {
						copyIsArray = false;
						clone = src && jQuery.isArray(src) ? src : [];

					} else {
						clone = src && jQuery.isPlainObject(src) ? src : {};
					}

					// Never move original objects, clone them
					target[ name ] = jQuery.extend( deep, clone, copy );

				// Don't bring in undefined values
				} else if ( copy !== undefined ) {
					target[ name ] = copy;
				}
			}
		}
	}

	// Return the modified object
	return target;
};

jQuery.extend({
	// Unique for each copy of jQuery on the page
	expando: "jQuery" + ( version + Math.random() ).replace( /\D/g, "" ),

	// Assume jQuery is ready without the ready module
	isReady: true,

	error: function( msg ) {
		throw new Error( msg );
	},

	noop: function() {},

	isFunction: function( obj ) {
		return jQuery.type(obj) === "function";
	},

	isArray: Array.isArray,

	isWindow: function( obj ) {
		return obj != null && obj === obj.window;
	},

	isNumeric: function( obj ) {
		// parseFloat NaNs numeric-cast false positives (null|true|false|"")
		// ...but misinterprets leading-number strings, particularly hex literals ("0x...")
		// subtraction forces infinities to NaN
		// adding 1 corrects loss of precision from parseFloat (#15100)
		return !jQuery.isArray( obj ) && (obj - parseFloat( obj ) + 1) >= 0;
	},

	isPlainObject: function( obj ) {
		// Not plain objects:
		// - Any object or value whose internal [[Class]] property is not "[object Object]"
		// - DOM nodes
		// - window
		if ( jQuery.type( obj ) !== "object" || obj.nodeType || jQuery.isWindow( obj ) ) {
			return false;
		}

		if ( obj.constructor &&
				!hasOwn.call( obj.constructor.prototype, "isPrototypeOf" ) ) {
			return false;
		}

		// If the function hasn't returned already, we're confident that
		// |obj| is a plain object, created by {} or constructed with new Object
		return true;
	},

	isEmptyObject: function( obj ) {
		var name;
		for ( name in obj ) {
			return false;
		}
		return true;
	},

	type: function( obj ) {
		if ( obj == null ) {
			return obj + "";
		}
		// Support: Android<4.0, iOS<6 (functionish RegExp)
		return typeof obj === "object" || typeof obj === "function" ?
			class2type[ toString.call(obj) ] || "object" :
			typeof obj;
	},

	// Evaluates a script in a global context
	globalEval: function( code ) {
		var script,
			indirect = eval;

		code = jQuery.trim( code );

		if ( code ) {
			// If the code includes a valid, prologue position
			// strict mode pragma, execute code by injecting a
			// script tag into the document.
			if ( code.indexOf("use strict") === 1 ) {
				script = document.createElement("script");
				script.text = code;
				document.head.appendChild( script ).parentNode.removeChild( script );
			} else {
			// Otherwise, avoid the DOM node creation, insertion
			// and removal by using an indirect global eval
				indirect( code );
			}
		}
	},

	// Convert dashed to camelCase; used by the css and data modules
	// Support: IE9-11+
	// Microsoft forgot to hump their vendor prefix (#9572)
	camelCase: function( string ) {
		return string.replace( rmsPrefix, "ms-" ).replace( rdashAlpha, fcamelCase );
	},

	nodeName: function( elem, name ) {
		return elem.nodeName && elem.nodeName.toLowerCase() === name.toLowerCase();
	},

	// args is for internal usage only
	each: function( obj, callback, args ) {
		var value,
			i = 0,
			length = obj.length,
			isArray = isArraylike( obj );

		if ( args ) {
			if ( isArray ) {
				for ( ; i < length; i++ ) {
					value = callback.apply( obj[ i ], args );

					if ( value === false ) {
						break;
					}
				}
			} else {
				for ( i in obj ) {
					value = callback.apply( obj[ i ], args );

					if ( value === false ) {
						break;
					}
				}
			}

		// A special, fast, case for the most common use of each
		} else {
			if ( isArray ) {
				for ( ; i < length; i++ ) {
					value = callback.call( obj[ i ], i, obj[ i ] );

					if ( value === false ) {
						break;
					}
				}
			} else {
				for ( i in obj ) {
					value = callback.call( obj[ i ], i, obj[ i ] );

					if ( value === false ) {
						break;
					}
				}
			}
		}

		return obj;
	},

	// Support: Android<4.1
	trim: function( text ) {
		return text == null ?
			"" :
			( text + "" ).replace( rtrim, "" );
	},

	// results is for internal usage only
	makeArray: function( arr, results ) {
		var ret = results || [];

		if ( arr != null ) {
			if ( isArraylike( Object(arr) ) ) {
				jQuery.merge( ret,
					typeof arr === "string" ?
					[ arr ] : arr
				);
			} else {
				push.call( ret, arr );
			}
		}

		return ret;
	},

	inArray: function( elem, arr, i ) {
		return arr == null ? -1 : indexOf.call( arr, elem, i );
	},

	merge: function( first, second ) {
		var len = +second.length,
			j = 0,
			i = first.length;

		for ( ; j < len; j++ ) {
			first[ i++ ] = second[ j ];
		}

		first.length = i;

		return first;
	},

	grep: function( elems, callback, invert ) {
		var callbackInverse,
			matches = [],
			i = 0,
			length = elems.length,
			callbackExpect = !invert;

		// Go through the array, only saving the items
		// that pass the validator function
		for ( ; i < length; i++ ) {
			callbackInverse = !callback( elems[ i ], i );
			if ( callbackInverse !== callbackExpect ) {
				matches.push( elems[ i ] );
			}
		}

		return matches;
	},

	// arg is for internal usage only
	map: function( elems, callback, arg ) {
		var value,
			i = 0,
			length = elems.length,
			isArray = isArraylike( elems ),
			ret = [];

		// Go through the array, translating each of the items to their new values
		if ( isArray ) {
			for ( ; i < length; i++ ) {
				value = callback( elems[ i ], i, arg );

				if ( value != null ) {
					ret.push( value );
				}
			}

		// Go through every key on the object,
		} else {
			for ( i in elems ) {
				value = callback( elems[ i ], i, arg );

				if ( value != null ) {
					ret.push( value );
				}
			}
		}

		// Flatten any nested arrays
		return concat.apply( [], ret );
	},

	// A global GUID counter for objects
	guid: 1,

	// Bind a function to a context, optionally partially applying any
	// arguments.
	proxy: function( fn, context ) {
		var tmp, args, proxy;

		if ( typeof context === "string" ) {
			tmp = fn[ context ];
			context = fn;
			fn = tmp;
		}

		// Quick check to determine if target is callable, in the spec
		// this throws a TypeError, but we will just return undefined.
		if ( !jQuery.isFunction( fn ) ) {
			return undefined;
		}

		// Simulated bind
		args = slice.call( arguments, 2 );
		proxy = function() {
			return fn.apply( context || this, args.concat( slice.call( arguments ) ) );
		};

		// Set the guid of unique handler to the same of original handler, so it can be removed
		proxy.guid = fn.guid = fn.guid || jQuery.guid++;

		return proxy;
	},

	now: Date.now,

	// jQuery.support is not used in Core but other projects attach their
	// properties to it so it needs to exist.
	support: support
});

// Populate the class2type map
jQuery.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function(i, name) {
	class2type[ "[object " + name + "]" ] = name.toLowerCase();
});

function isArraylike( obj ) {
	var length = obj.length,
		type = jQuery.type( obj );

	if ( type === "function" || jQuery.isWindow( obj ) ) {
		return false;
	}

	if ( obj.nodeType === 1 && length ) {
		return true;
	}

	return type === "array" || length === 0 ||
		typeof length === "number" && length > 0 && ( length - 1 ) in obj;
}
var Sizzle =
/*!
 * Sizzle CSS Selector Engine v2.2.0-pre
 * http://sizzlejs.com/
 *
 * Copyright 2008, 2014 jQuery Foundation, Inc. and other contributors
 * Released under the MIT license
 * http://jquery.org/license
 *
 * Date: 2014-12-16
 */
(function( window ) {

var i,
	support,
	Expr,
	getText,
	isXML,
	tokenize,
	compile,
	select,
	outermostContext,
	sortInput,
	hasDuplicate,

	// Local document vars
	setDocument,
	document,
	docElem,
	documentIsHTML,
	rbuggyQSA,
	rbuggyMatches,
	matches,
	contains,

	// Instance-specific data
	expando = "sizzle" + 1 * new Date(),
	preferredDoc = window.document,
	dirruns = 0,
	done = 0,
	classCache = createCache(),
	tokenCache = createCache(),
	compilerCache = createCache(),
	sortOrder = function( a, b ) {
		if ( a === b ) {
			hasDuplicate = true;
		}
		return 0;
	},

	// General-purpose constants
	MAX_NEGATIVE = 1 << 31,

	// Instance methods
	hasOwn = ({}).hasOwnProperty,
	arr = [],
	pop = arr.pop,
	push_native = arr.push,
	push = arr.push,
	slice = arr.slice,
	// Use a stripped-down indexOf as it's faster than native
	// http://jsperf.com/thor-indexof-vs-for/5
	indexOf = function( list, elem ) {
		var i = 0,
			len = list.length;
		for ( ; i < len; i++ ) {
			if ( list[i] === elem ) {
				return i;
			}
		}
		return -1;
	},

	booleans = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",

	// Regular expressions

	// Whitespace characters http://www.w3.org/TR/css3-selectors/#whitespace
	whitespace = "[\\x20\\t\\r\\n\\f]",
	// http://www.w3.org/TR/css3-syntax/#characters
	characterEncoding = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",

	// Loosely modeled on CSS identifier characters
	// An unquoted value should be a CSS identifier http://www.w3.org/TR/css3-selectors/#attribute-selectors
	// Proper syntax: http://www.w3.org/TR/CSS21/syndata.html#value-def-identifier
	identifier = characterEncoding.replace( "w", "w#" ),

	// Attribute selectors: http://www.w3.org/TR/selectors/#attribute-selectors
	attributes = "\\[" + whitespace + "*(" + characterEncoding + ")(?:" + whitespace +
		// Operator (capture 2)
		"*([*^$|!~]?=)" + whitespace +
		// "Attribute values must be CSS identifiers [capture 5] or strings [capture 3 or capture 4]"
		"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + identifier + "))|)" + whitespace +
		"*\\]",

	pseudos = ":(" + characterEncoding + ")(?:\\((" +
		// To reduce the number of selectors needing tokenize in the preFilter, prefer arguments:
		// 1. quoted (capture 3; capture 4 or capture 5)
		"('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|" +
		// 2. simple (capture 6)
		"((?:\\\\.|[^\\\\()[\\]]|" + attributes + ")*)|" +
		// 3. anything else (capture 2)
		".*" +
		")\\)|)",

	// Leading and non-escaped trailing whitespace, capturing some non-whitespace characters preceding the latter
	rwhitespace = new RegExp( whitespace + "+", "g" ),
	rtrim = new RegExp( "^" + whitespace + "+|((?:^|[^\\\\])(?:\\\\.)*)" + whitespace + "+$", "g" ),

	rcomma = new RegExp( "^" + whitespace + "*," + whitespace + "*" ),
	rcombinators = new RegExp( "^" + whitespace + "*([>+~]|" + whitespace + ")" + whitespace + "*" ),

	rattributeQuotes = new RegExp( "=" + whitespace + "*([^\\]'\"]*?)" + whitespace + "*\\]", "g" ),

	rpseudo = new RegExp( pseudos ),
	ridentifier = new RegExp( "^" + identifier + "$" ),

	matchExpr = {
		"ID": new RegExp( "^#(" + characterEncoding + ")" ),
		"CLASS": new RegExp( "^\\.(" + characterEncoding + ")" ),
		"TAG": new RegExp( "^(" + characterEncoding.replace( "w", "w*" ) + ")" ),
		"ATTR": new RegExp( "^" + attributes ),
		"PSEUDO": new RegExp( "^" + pseudos ),
		"CHILD": new RegExp( "^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + whitespace +
			"*(even|odd|(([+-]|)(\\d*)n|)" + whitespace + "*(?:([+-]|)" + whitespace +
			"*(\\d+)|))" + whitespace + "*\\)|)", "i" ),
		"bool": new RegExp( "^(?:" + booleans + ")$", "i" ),
		// For use in libraries implementing .is()
		// We use this for POS matching in `select`
		"needsContext": new RegExp( "^" + whitespace + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" +
			whitespace + "*((?:-\\d)?\\d*)" + whitespace + "*\\)|)(?=[^-]|$)", "i" )
	},

	rinputs = /^(?:input|select|textarea|button)$/i,
	rheader = /^h\d$/i,

	rnative = /^[^{]+\{\s*\[native \w/,

	// Easily-parseable/retrievable ID or TAG or CLASS selectors
	rquickExpr = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,

	rsibling = /[+~]/,
	rescape = /'|\\/g,

	// CSS escapes http://www.w3.org/TR/CSS21/syndata.html#escaped-characters
	runescape = new RegExp( "\\\\([\\da-f]{1,6}" + whitespace + "?|(" + whitespace + ")|.)", "ig" ),
	funescape = function( _, escaped, escapedWhitespace ) {
		var high = "0x" + escaped - 0x10000;
		// NaN means non-codepoint
		// Support: Firefox<24
		// Workaround erroneous numeric interpretation of +"0x"
		return high !== high || escapedWhitespace ?
			escaped :
			high < 0 ?
				// BMP codepoint
				String.fromCharCode( high + 0x10000 ) :
				// Supplemental Plane codepoint (surrogate pair)
				String.fromCharCode( high >> 10 | 0xD800, high & 0x3FF | 0xDC00 );
	},

	// Used for iframes
	// See setDocument()
	// Removing the function wrapper causes a "Permission Denied"
	// error in IE
	unloadHandler = function() {
		setDocument();
	};

// Optimize for push.apply( _, NodeList )
try {
	push.apply(
		(arr = slice.call( preferredDoc.childNodes )),
		preferredDoc.childNodes
	);
	// Support: Android<4.0
	// Detect silently failing push.apply
	arr[ preferredDoc.childNodes.length ].nodeType;
} catch ( e ) {
	push = { apply: arr.length ?

		// Leverage slice if possible
		function( target, els ) {
			push_native.apply( target, slice.call(els) );
		} :

		// Support: IE<9
		// Otherwise append directly
		function( target, els ) {
			var j = target.length,
				i = 0;
			// Can't trust NodeList.length
			while ( (target[j++] = els[i++]) ) {}
			target.length = j - 1;
		}
	};
}

function Sizzle( selector, context, results, seed ) {
	var match, elem, m, nodeType,
		// QSA vars
		i, groups, old, nid, newContext, newSelector;

	if ( ( context ? context.ownerDocument || context : preferredDoc ) !== document ) {
		setDocument( context );
	}

	context = context || document;
	results = results || [];
	nodeType = context.nodeType;

	if ( typeof selector !== "string" || !selector ||
		nodeType !== 1 && nodeType !== 9 && nodeType !== 11 ) {

		return results;
	}

	if ( !seed && documentIsHTML ) {

		// Try to shortcut find operations when possible (e.g., not under DocumentFragment)
		if ( nodeType !== 11 && (match = rquickExpr.exec( selector )) ) {
			// Speed-up: Sizzle("#ID")
			if ( (m = match[1]) ) {
				if ( nodeType === 9 ) {
					elem = context.getElementById( m );
					// Check parentNode to catch when Blackberry 4.6 returns
					// nodes that are no longer in the document (jQuery #6963)
					if ( elem && elem.parentNode ) {
						// Handle the case where IE, Opera, and Webkit return items
						// by name instead of ID
						if ( elem.id === m ) {
							results.push( elem );
							return results;
						}
					} else {
						return results;
					}
				} else {
					// Context is not a document
					if ( context.ownerDocument && (elem = context.ownerDocument.getElementById( m )) &&
						contains( context, elem ) && elem.id === m ) {
						results.push( elem );
						return results;
					}
				}

			// Speed-up: Sizzle("TAG")
			} else if ( match[2] ) {
				push.apply( results, context.getElementsByTagName( selector ) );
				return results;

			// Speed-up: Sizzle(".CLASS")
			} else if ( (m = match[3]) && support.getElementsByClassName ) {
				push.apply( results, context.getElementsByClassName( m ) );
				return results;
			}
		}

		// QSA path
		if ( support.qsa && (!rbuggyQSA || !rbuggyQSA.test( selector )) ) {
			nid = old = expando;
			newContext = context;
			newSelector = nodeType !== 1 && selector;

			// qSA works strangely on Element-rooted queries
			// We can work around this by specifying an extra ID on the root
			// and working up from there (Thanks to Andrew Dupont for the technique)
			// IE 8 doesn't work on object elements
			if ( nodeType === 1 && context.nodeName.toLowerCase() !== "object" ) {
				groups = tokenize( selector );

				if ( (old = context.getAttribute("id")) ) {
					nid = old.replace( rescape, "\\$&" );
				} else {
					context.setAttribute( "id", nid );
				}
				nid = "[id='" + nid + "'] ";

				i = groups.length;
				while ( i-- ) {
					groups[i] = nid + toSelector( groups[i] );
				}
				newContext = rsibling.test( selector ) && testContext( context.parentNode ) || context;
				newSelector = groups.join(",");
			}

			if ( newSelector ) {
				try {
					push.apply( results,
						newContext.querySelectorAll( newSelector )
					);
					return results;
				} catch(qsaError) {
				} finally {
					if ( !old ) {
						context.removeAttribute("id");
					}
				}
			}
		}
	}

	// All others
	return select( selector.replace( rtrim, "$1" ), context, results, seed );
}

/**
 * Create key-value caches of limited size
 * @returns {Function(string, Object)} Returns the Object data after storing it on itself with
 *	property name the (space-suffixed) string and (if the cache is larger than Expr.cacheLength)
 *	deleting the oldest entry
 */
function createCache() {
	var keys = [];

	function cache( key, value ) {
		// Use (key + " ") to avoid collision with native prototype properties (see Issue #157)
		if ( keys.push( key + " " ) > Expr.cacheLength ) {
			// Only keep the most recent entries
			delete cache[ keys.shift() ];
		}
		return (cache[ key + " " ] = value);
	}
	return cache;
}

/**
 * Mark a function for special use by Sizzle
 * @param {Function} fn The function to mark
 */
function markFunction( fn ) {
	fn[ expando ] = true;
	return fn;
}

/**
 * Support testing using an element
 * @param {Function} fn Passed the created div and expects a boolean result
 */
function assert( fn ) {
	var div = document.createElement("div");

	try {
		return !!fn( div );
	} catch (e) {
		return false;
	} finally {
		// Remove from its parent by default
		if ( div.parentNode ) {
			div.parentNode.removeChild( div );
		}
		// release memory in IE
		div = null;
	}
}

/**
 * Adds the same handler for all of the specified attrs
 * @param {String} attrs Pipe-separated list of attributes
 * @param {Function} handler The method that will be applied
 */
function addHandle( attrs, handler ) {
	var arr = attrs.split("|"),
		i = attrs.length;

	while ( i-- ) {
		Expr.attrHandle[ arr[i] ] = handler;
	}
}

/**
 * Checks document order of two siblings
 * @param {Element} a
 * @param {Element} b
 * @returns {Number} Returns less than 0 if a precedes b, greater than 0 if a follows b
 */
function siblingCheck( a, b ) {
	var cur = b && a,
		diff = cur && a.nodeType === 1 && b.nodeType === 1 &&
			( ~b.sourceIndex || MAX_NEGATIVE ) -
			( ~a.sourceIndex || MAX_NEGATIVE );

	// Use IE sourceIndex if available on both nodes
	if ( diff ) {
		return diff;
	}

	// Check if b follows a
	if ( cur ) {
		while ( (cur = cur.nextSibling) ) {
			if ( cur === b ) {
				return -1;
			}
		}
	}

	return a ? 1 : -1;
}

/**
 * Returns a function to use in pseudos for input types
 * @param {String} type
 */
function createInputPseudo( type ) {
	return function( elem ) {
		var name = elem.nodeName.toLowerCase();
		return name === "input" && elem.type === type;
	};
}

/**
 * Returns a function to use in pseudos for buttons
 * @param {String} type
 */
function createButtonPseudo( type ) {
	return function( elem ) {
		var name = elem.nodeName.toLowerCase();
		return (name === "input" || name === "button") && elem.type === type;
	};
}

/**
 * Returns a function to use in pseudos for positionals
 * @param {Function} fn
 */
function createPositionalPseudo( fn ) {
	return markFunction(function( argument ) {
		argument = +argument;
		return markFunction(function( seed, matches ) {
			var j,
				matchIndexes = fn( [], seed.length, argument ),
				i = matchIndexes.length;

			// Match elements found at the specified indexes
			while ( i-- ) {
				if ( seed[ (j = matchIndexes[i]) ] ) {
					seed[j] = !(matches[j] = seed[j]);
				}
			}
		});
	});
}

/**
 * Checks a node for validity as a Sizzle context
 * @param {Element|Object=} context
 * @returns {Element|Object|Boolean} The input node if acceptable, otherwise a falsy value
 */
function testContext( context ) {
	return context && typeof context.getElementsByTagName !== "undefined" && context;
}

// Expose support vars for convenience
support = Sizzle.support = {};

/**
 * Detects XML nodes
 * @param {Element|Object} elem An element or a document
 * @returns {Boolean} True iff elem is a non-HTML XML node
 */
isXML = Sizzle.isXML = function( elem ) {
	// documentElement is verified for cases where it doesn't yet exist
	// (such as loading iframes in IE - #4833)
	var documentElement = elem && (elem.ownerDocument || elem).documentElement;
	return documentElement ? documentElement.nodeName !== "HTML" : false;
};

/**
 * Sets document-related variables once based on the current document
 * @param {Element|Object} [doc] An element or document object to use to set the document
 * @returns {Object} Returns the current document
 */
setDocument = Sizzle.setDocument = function( node ) {
	var hasCompare, parent,
		doc = node ? node.ownerDocument || node : preferredDoc;

	// If no document and documentElement is available, return
	if ( doc === document || doc.nodeType !== 9 || !doc.documentElement ) {
		return document;
	}

	// Set our document
	document = doc;
	docElem = doc.documentElement;
	parent = doc.defaultView;

	// Support: IE>8
	// If iframe document is assigned to "document" variable and if iframe has been reloaded,
	// IE will throw "permission denied" error when accessing "document" variable, see jQuery #13936
	// IE6-8 do not support the defaultView property so parent will be undefined
	if ( parent && parent !== parent.top ) {
		// IE11 does not have attachEvent, so all must suffer
		if ( parent.addEventListener ) {
			parent.addEventListener( "unload", unloadHandler, false );
		} else if ( parent.attachEvent ) {
			parent.attachEvent( "onunload", unloadHandler );
		}
	}

	/* Support tests
	---------------------------------------------------------------------- */
	documentIsHTML = !isXML( doc );

	/* Attributes
	---------------------------------------------------------------------- */

	// Support: IE<8
	// Verify that getAttribute really returns attributes and not properties
	// (excepting IE8 booleans)
	support.attributes = assert(function( div ) {
		div.className = "i";
		return !div.getAttribute("className");
	});

	/* getElement(s)By*
	---------------------------------------------------------------------- */

	// Check if getElementsByTagName("*") returns only elements
	support.getElementsByTagName = assert(function( div ) {
		div.appendChild( doc.createComment("") );
		return !div.getElementsByTagName("*").length;
	});

	// Support: IE<9
	support.getElementsByClassName = rnative.test( doc.getElementsByClassName );

	// Support: IE<10
	// Check if getElementById returns elements by name
	// The broken getElementById methods don't pick up programatically-set names,
	// so use a roundabout getElementsByName test
	support.getById = assert(function( div ) {
		docElem.appendChild( div ).id = expando;
		return !doc.getElementsByName || !doc.getElementsByName( expando ).length;
	});

	// ID find and filter
	if ( support.getById ) {
		Expr.find["ID"] = function( id, context ) {
			if ( typeof context.getElementById !== "undefined" && documentIsHTML ) {
				var m = context.getElementById( id );
				// Check parentNode to catch when Blackberry 4.6 returns
				// nodes that are no longer in the document #6963
				return m && m.parentNode ? [ m ] : [];
			}
		};
		Expr.filter["ID"] = function( id ) {
			var attrId = id.replace( runescape, funescape );
			return function( elem ) {
				return elem.getAttribute("id") === attrId;
			};
		};
	} else {
		// Support: IE6/7
		// getElementById is not reliable as a find shortcut
		delete Expr.find["ID"];

		Expr.filter["ID"] =  function( id ) {
			var attrId = id.replace( runescape, funescape );
			return function( elem ) {
				var node = typeof elem.getAttributeNode !== "undefined" && elem.getAttributeNode("id");
				return node && node.value === attrId;
			};
		};
	}

	// Tag
	Expr.find["TAG"] = support.getElementsByTagName ?
		function( tag, context ) {
			if ( typeof context.getElementsByTagName !== "undefined" ) {
				return context.getElementsByTagName( tag );

			// DocumentFragment nodes don't have gEBTN
			} else if ( support.qsa ) {
				return context.querySelectorAll( tag );
			}
		} :

		function( tag, context ) {
			var elem,
				tmp = [],
				i = 0,
				// By happy coincidence, a (broken) gEBTN appears on DocumentFragment nodes too
				results = context.getElementsByTagName( tag );

			// Filter out possible comments
			if ( tag === "*" ) {
				while ( (elem = results[i++]) ) {
					if ( elem.nodeType === 1 ) {
						tmp.push( elem );
					}
				}

				return tmp;
			}
			return results;
		};

	// Class
	Expr.find["CLASS"] = support.getElementsByClassName && function( className, context ) {
		if ( documentIsHTML ) {
			return context.getElementsByClassName( className );
		}
	};

	/* QSA/matchesSelector
	---------------------------------------------------------------------- */

	// QSA and matchesSelector support

	// matchesSelector(:active) reports false when true (IE9/Opera 11.5)
	rbuggyMatches = [];

	// qSa(:focus) reports false when true (Chrome 21)
	// We allow this because of a bug in IE8/9 that throws an error
	// whenever `document.activeElement` is accessed on an iframe
	// So, we allow :focus to pass through QSA all the time to avoid the IE error
	// See http://bugs.jquery.com/ticket/13378
	rbuggyQSA = [];

	if ( (support.qsa = rnative.test( doc.querySelectorAll )) ) {
		// Build QSA regex
		// Regex strategy adopted from Diego Perini
		assert(function( div ) {
			// Select is set to empty string on purpose
			// This is to test IE's treatment of not explicitly
			// setting a boolean content attribute,
			// since its presence should be enough
			// http://bugs.jquery.com/ticket/12359
			docElem.appendChild( div ).innerHTML = "<a id='" + expando + "'></a>" +
				"<select id='" + expando + "-\f]' msallowcapture=''>" +
				"<option selected=''></option></select>";

			// Support: IE8, Opera 11-12.16
			// Nothing should be selected when empty strings follow ^= or $= or *=
			// The test attribute must be unknown in Opera but "safe" for WinRT
			// http://msdn.microsoft.com/en-us/library/ie/hh465388.aspx#attribute_section
			if ( div.querySelectorAll("[msallowcapture^='']").length ) {
				rbuggyQSA.push( "[*^$]=" + whitespace + "*(?:''|\"\")" );
			}

			// Support: IE8
			// Boolean attributes and "value" are not treated correctly
			if ( !div.querySelectorAll("[selected]").length ) {
				rbuggyQSA.push( "\\[" + whitespace + "*(?:value|" + booleans + ")" );
			}

			// Support: Chrome<29, Android<4.2+, Safari<7.0+, iOS<7.0+, PhantomJS<1.9.7+
			if ( !div.querySelectorAll( "[id~=" + expando + "-]" ).length ) {
				rbuggyQSA.push("~=");
			}

			// Webkit/Opera - :checked should return selected option elements
			// http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
			// IE8 throws error here and will not see later tests
			if ( !div.querySelectorAll(":checked").length ) {
				rbuggyQSA.push(":checked");
			}

			// Support: Safari 8+, iOS 8+
			// https://bugs.webkit.org/show_bug.cgi?id=136851
			// In-page `selector#id sibing-combinator selector` fails
			if ( !div.querySelectorAll( "a#" + expando + "+*" ).length ) {
				rbuggyQSA.push(".#.+[+~]");
			}
		});

		assert(function( div ) {
			// Support: Windows 8 Native Apps
			// The type and name attributes are restricted during .innerHTML assignment
			var input = doc.createElement("input");
			input.setAttribute( "type", "hidden" );
			div.appendChild( input ).setAttribute( "name", "D" );

			// Support: IE8
			// Enforce case-sensitivity of name attribute
			if ( div.querySelectorAll("[name=d]").length ) {
				rbuggyQSA.push( "name" + whitespace + "*[*^$|!~]?=" );
			}

			// FF 3.5 - :enabled/:disabled and hidden elements (hidden elements are still enabled)
			// IE8 throws error here and will not see later tests
			if ( !div.querySelectorAll(":enabled").length ) {
				rbuggyQSA.push( ":enabled", ":disabled" );
			}

			// Opera 10-11 does not throw on post-comma invalid pseudos
			div.querySelectorAll("*,:x");
			rbuggyQSA.push(",.*:");
		});
	}

	if ( (support.matchesSelector = rnative.test( (matches = docElem.matches ||
		docElem.webkitMatchesSelector ||
		docElem.mozMatchesSelector ||
		docElem.oMatchesSelector ||
		docElem.msMatchesSelector) )) ) {

		assert(function( div ) {
			// Check to see if it's possible to do matchesSelector
			// on a disconnected node (IE 9)
			support.disconnectedMatch = matches.call( div, "div" );

			// This should fail with an exception
			// Gecko does not error, returns false instead
			matches.call( div, "[s!='']:x" );
			rbuggyMatches.push( "!=", pseudos );
		});
	}

	rbuggyQSA = rbuggyQSA.length && new RegExp( rbuggyQSA.join("|") );
	rbuggyMatches = rbuggyMatches.length && new RegExp( rbuggyMatches.join("|") );

	/* Contains
	---------------------------------------------------------------------- */
	hasCompare = rnative.test( docElem.compareDocumentPosition );

	// Element contains another
	// Purposefully does not implement inclusive descendent
	// As in, an element does not contain itself
	contains = hasCompare || rnative.test( docElem.contains ) ?
		function( a, b ) {
			var adown = a.nodeType === 9 ? a.documentElement : a,
				bup = b && b.parentNode;
			return a === bup || !!( bup && bup.nodeType === 1 && (
				adown.contains ?
					adown.contains( bup ) :
					a.compareDocumentPosition && a.compareDocumentPosition( bup ) & 16
			));
		} :
		function( a, b ) {
			if ( b ) {
				while ( (b = b.parentNode) ) {
					if ( b === a ) {
						return true;
					}
				}
			}
			return false;
		};

	/* Sorting
	---------------------------------------------------------------------- */

	// Document order sorting
	sortOrder = hasCompare ?
	function( a, b ) {

		// Flag for duplicate removal
		if ( a === b ) {
			hasDuplicate = true;
			return 0;
		}

		// Sort on method existence if only one input has compareDocumentPosition
		var compare = !a.compareDocumentPosition - !b.compareDocumentPosition;
		if ( compare ) {
			return compare;
		}

		// Calculate position if both inputs belong to the same document
		compare = ( a.ownerDocument || a ) === ( b.ownerDocument || b ) ?
			a.compareDocumentPosition( b ) :

			// Otherwise we know they are disconnected
			1;

		// Disconnected nodes
		if ( compare & 1 ||
			(!support.sortDetached && b.compareDocumentPosition( a ) === compare) ) {

			// Choose the first element that is related to our preferred document
			if ( a === doc || a.ownerDocument === preferredDoc && contains(preferredDoc, a) ) {
				return -1;
			}
			if ( b === doc || b.ownerDocument === preferredDoc && contains(preferredDoc, b) ) {
				return 1;
			}

			// Maintain original order
			return sortInput ?
				( indexOf( sortInput, a ) - indexOf( sortInput, b ) ) :
				0;
		}

		return compare & 4 ? -1 : 1;
	} :
	function( a, b ) {
		// Exit early if the nodes are identical
		if ( a === b ) {
			hasDuplicate = true;
			return 0;
		}

		var cur,
			i = 0,
			aup = a.parentNode,
			bup = b.parentNode,
			ap = [ a ],
			bp = [ b ];

		// Parentless nodes are either documents or disconnected
		if ( !aup || !bup ) {
			return a === doc ? -1 :
				b === doc ? 1 :
				aup ? -1 :
				bup ? 1 :
				sortInput ?
				( indexOf( sortInput, a ) - indexOf( sortInput, b ) ) :
				0;

		// If the nodes are siblings, we can do a quick check
		} else if ( aup === bup ) {
			return siblingCheck( a, b );
		}

		// Otherwise we need full lists of their ancestors for comparison
		cur = a;
		while ( (cur = cur.parentNode) ) {
			ap.unshift( cur );
		}
		cur = b;
		while ( (cur = cur.parentNode) ) {
			bp.unshift( cur );
		}

		// Walk down the tree looking for a discrepancy
		while ( ap[i] === bp[i] ) {
			i++;
		}

		return i ?
			// Do a sibling check if the nodes have a common ancestor
			siblingCheck( ap[i], bp[i] ) :

			// Otherwise nodes in our document sort first
			ap[i] === preferredDoc ? -1 :
			bp[i] === preferredDoc ? 1 :
			0;
	};

	return doc;
};

Sizzle.matches = function( expr, elements ) {
	return Sizzle( expr, null, null, elements );
};

Sizzle.matchesSelector = function( elem, expr ) {
	// Set document vars if needed
	if ( ( elem.ownerDocument || elem ) !== document ) {
		setDocument( elem );
	}

	// Make sure that attribute selectors are quoted
	expr = expr.replace( rattributeQuotes, "='$1']" );

	if ( support.matchesSelector && documentIsHTML &&
		( !rbuggyMatches || !rbuggyMatches.test( expr ) ) &&
		( !rbuggyQSA     || !rbuggyQSA.test( expr ) ) ) {

		try {
			var ret = matches.call( elem, expr );

			// IE 9's matchesSelector returns false on disconnected nodes
			if ( ret || support.disconnectedMatch ||
					// As well, disconnected nodes are said to be in a document
					// fragment in IE 9
					elem.document && elem.document.nodeType !== 11 ) {
				return ret;
			}
		} catch (e) {}
	}

	return Sizzle( expr, document, null, [ elem ] ).length > 0;
};

Sizzle.contains = function( context, elem ) {
	// Set document vars if needed
	if ( ( context.ownerDocument || context ) !== document ) {
		setDocument( context );
	}
	return contains( context, elem );
};

Sizzle.attr = function( elem, name ) {
	// Set document vars if needed
	if ( ( elem.ownerDocument || elem ) !== document ) {
		setDocument( elem );
	}

	var fn = Expr.attrHandle[ name.toLowerCase() ],
		// Don't get fooled by Object.prototype properties (jQuery #13807)
		val = fn && hasOwn.call( Expr.attrHandle, name.toLowerCase() ) ?
			fn( elem, name, !documentIsHTML ) :
			undefined;

	return val !== undefined ?
		val :
		support.attributes || !documentIsHTML ?
			elem.getAttribute( name ) :
			(val = elem.getAttributeNode(name)) && val.specified ?
				val.value :
				null;
};

Sizzle.error = function( msg ) {
	throw new Error( "Syntax error, unrecognized expression: " + msg );
};

/**
 * Document sorting and removing duplicates
 * @param {ArrayLike} results
 */
Sizzle.uniqueSort = function( results ) {
	var elem,
		duplicates = [],
		j = 0,
		i = 0;

	// Unless we *know* we can detect duplicates, assume their presence
	hasDuplicate = !support.detectDuplicates;
	sortInput = !support.sortStable && results.slice( 0 );
	results.sort( sortOrder );

	if ( hasDuplicate ) {
		while ( (elem = results[i++]) ) {
			if ( elem === results[ i ] ) {
				j = duplicates.push( i );
			}
		}
		while ( j-- ) {
			results.splice( duplicates[ j ], 1 );
		}
	}

	// Clear input after sorting to release objects
	// See https://github.com/jquery/sizzle/pull/225
	sortInput = null;

	return results;
};

/**
 * Utility function for retrieving the text value of an array of DOM nodes
 * @param {Array|Element} elem
 */
getText = Sizzle.getText = function( elem ) {
	var node,
		ret = "",
		i = 0,
		nodeType = elem.nodeType;

	if ( !nodeType ) {
		// If no nodeType, this is expected to be an array
		while ( (node = elem[i++]) ) {
			// Do not traverse comment nodes
			ret += getText( node );
		}
	} else if ( nodeType === 1 || nodeType === 9 || nodeType === 11 ) {
		// Use textContent for elements
		// innerText usage removed for consistency of new lines (jQuery #11153)
		if ( typeof elem.textContent === "string" ) {
			return elem.textContent;
		} else {
			// Traverse its children
			for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {
				ret += getText( elem );
			}
		}
	} else if ( nodeType === 3 || nodeType === 4 ) {
		return elem.nodeValue;
	}
	// Do not include comment or processing instruction nodes

	return ret;
};

Expr = Sizzle.selectors = {

	// Can be adjusted by the user
	cacheLength: 50,

	createPseudo: markFunction,

	match: matchExpr,

	attrHandle: {},

	find: {},

	relative: {
		">": { dir: "parentNode", first: true },
		" ": { dir: "parentNode" },
		"+": { dir: "previousSibling", first: true },
		"~": { dir: "previousSibling" }
	},

	preFilter: {
		"ATTR": function( match ) {
			match[1] = match[1].replace( runescape, funescape );

			// Move the given value to match[3] whether quoted or unquoted
			match[3] = ( match[3] || match[4] || match[5] || "" ).replace( runescape, funescape );

			if ( match[2] === "~=" ) {
				match[3] = " " + match[3] + " ";
			}

			return match.slice( 0, 4 );
		},

		"CHILD": function( match ) {
			/* matches from matchExpr["CHILD"]
				1 type (only|nth|...)
				2 what (child|of-type)
				3 argument (even|odd|\d*|\d*n([+-]\d+)?|...)
				4 xn-component of xn+y argument ([+-]?\d*n|)
				5 sign of xn-component
				6 x of xn-component
				7 sign of y-component
				8 y of y-component
			*/
			match[1] = match[1].toLowerCase();

			if ( match[1].slice( 0, 3 ) === "nth" ) {
				// nth-* requires argument
				if ( !match[3] ) {
					Sizzle.error( match[0] );
				}

				// numeric x and y parameters for Expr.filter.CHILD
				// remember that false/true cast respectively to 0/1
				match[4] = +( match[4] ? match[5] + (match[6] || 1) : 2 * ( match[3] === "even" || match[3] === "odd" ) );
				match[5] = +( ( match[7] + match[8] ) || match[3] === "odd" );

			// other types prohibit arguments
			} else if ( match[3] ) {
				Sizzle.error( match[0] );
			}

			return match;
		},

		"PSEUDO": function( match ) {
			var excess,
				unquoted = !match[6] && match[2];

			if ( matchExpr["CHILD"].test( match[0] ) ) {
				return null;
			}

			// Accept quoted arguments as-is
			if ( match[3] ) {
				match[2] = match[4] || match[5] || "";

			// Strip excess characters from unquoted arguments
			} else if ( unquoted && rpseudo.test( unquoted ) &&
				// Get excess from tokenize (recursively)
				(excess = tokenize( unquoted, true )) &&
				// advance to the next closing parenthesis
				(excess = unquoted.indexOf( ")", unquoted.length - excess ) - unquoted.length) ) {

				// excess is a negative index
				match[0] = match[0].slice( 0, excess );
				match[2] = unquoted.slice( 0, excess );
			}

			// Return only captures needed by the pseudo filter method (type and argument)
			return match.slice( 0, 3 );
		}
	},

	filter: {

		"TAG": function( nodeNameSelector ) {
			var nodeName = nodeNameSelector.replace( runescape, funescape ).toLowerCase();
			return nodeNameSelector === "*" ?
				function() { return true; } :
				function( elem ) {
					return elem.nodeName && elem.nodeName.toLowerCase() === nodeName;
				};
		},

		"CLASS": function( className ) {
			var pattern = classCache[ className + " " ];

			return pattern ||
				(pattern = new RegExp( "(^|" + whitespace + ")" + className + "(" + whitespace + "|$)" )) &&
				classCache( className, function( elem ) {
					return pattern.test( typeof elem.className === "string" && elem.className || typeof elem.getAttribute !== "undefined" && elem.getAttribute("class") || "" );
				});
		},

		"ATTR": function( name, operator, check ) {
			return function( elem ) {
				var result = Sizzle.attr( elem, name );

				if ( result == null ) {
					return operator === "!=";
				}
				if ( !operator ) {
					return true;
				}

				result += "";

				return operator === "=" ? result === check :
					operator === "!=" ? result !== check :
					operator === "^=" ? check && result.indexOf( check ) === 0 :
					operator === "*=" ? check && result.indexOf( check ) > -1 :
					operator === "$=" ? check && result.slice( -check.length ) === check :
					operator === "~=" ? ( " " + result.replace( rwhitespace, " " ) + " " ).indexOf( check ) > -1 :
					operator === "|=" ? result === check || result.slice( 0, check.length + 1 ) === check + "-" :
					false;
			};
		},

		"CHILD": function( type, what, argument, first, last ) {
			var simple = type.slice( 0, 3 ) !== "nth",
				forward = type.slice( -4 ) !== "last",
				ofType = what === "of-type";

			return first === 1 && last === 0 ?

				// Shortcut for :nth-*(n)
				function( elem ) {
					return !!elem.parentNode;
				} :

				function( elem, context, xml ) {
					var cache, outerCache, node, diff, nodeIndex, start,
						dir = simple !== forward ? "nextSibling" : "previousSibling",
						parent = elem.parentNode,
						name = ofType && elem.nodeName.toLowerCase(),
						useCache = !xml && !ofType;

					if ( parent ) {

						// :(first|last|only)-(child|of-type)
						if ( simple ) {
							while ( dir ) {
								node = elem;
								while ( (node = node[ dir ]) ) {
									if ( ofType ? node.nodeName.toLowerCase() === name : node.nodeType === 1 ) {
										return false;
									}
								}
								// Reverse direction for :only-* (if we haven't yet done so)
								start = dir = type === "only" && !start && "nextSibling";
							}
							return true;
						}

						start = [ forward ? parent.firstChild : parent.lastChild ];

						// non-xml :nth-child(...) stores cache data on `parent`
						if ( forward && useCache ) {
							// Seek `elem` from a previously-cached index
							outerCache = parent[ expando ] || (parent[ expando ] = {});
							cache = outerCache[ type ] || [];
							nodeIndex = cache[0] === dirruns && cache[1];
							diff = cache[0] === dirruns && cache[2];
							node = nodeIndex && parent.childNodes[ nodeIndex ];

							while ( (node = ++nodeIndex && node && node[ dir ] ||

								// Fallback to seeking `elem` from the start
								(diff = nodeIndex = 0) || start.pop()) ) {

								// When found, cache indexes on `parent` and break
								if ( node.nodeType === 1 && ++diff && node === elem ) {
									outerCache[ type ] = [ dirruns, nodeIndex, diff ];
									break;
								}
							}

						// Use previously-cached element index if available
						} else if ( useCache && (cache = (elem[ expando ] || (elem[ expando ] = {}))[ type ]) && cache[0] === dirruns ) {
							diff = cache[1];

						// xml :nth-child(...) or :nth-last-child(...) or :nth(-last)?-of-type(...)
						} else {
							// Use the same loop as above to seek `elem` from the start
							while ( (node = ++nodeIndex && node && node[ dir ] ||
								(diff = nodeIndex = 0) || start.pop()) ) {

								if ( ( ofType ? node.nodeName.toLowerCase() === name : node.nodeType === 1 ) && ++diff ) {
									// Cache the index of each encountered element
									if ( useCache ) {
										(node[ expando ] || (node[ expando ] = {}))[ type ] = [ dirruns, diff ];
									}

									if ( node === elem ) {
										break;
									}
								}
							}
						}

						// Incorporate the offset, then check against cycle size
						diff -= last;
						return diff === first || ( diff % first === 0 && diff / first >= 0 );
					}
				};
		},

		"PSEUDO": function( pseudo, argument ) {
			// pseudo-class names are case-insensitive
			// http://www.w3.org/TR/selectors/#pseudo-classes
			// Prioritize by case sensitivity in case custom pseudos are added with uppercase letters
			// Remember that setFilters inherits from pseudos
			var args,
				fn = Expr.pseudos[ pseudo ] || Expr.setFilters[ pseudo.toLowerCase() ] ||
					Sizzle.error( "unsupported pseudo: " + pseudo );

			// The user may use createPseudo to indicate that
			// arguments are needed to create the filter function
			// just as Sizzle does
			if ( fn[ expando ] ) {
				return fn( argument );
			}

			// But maintain support for old signatures
			if ( fn.length > 1 ) {
				args = [ pseudo, pseudo, "", argument ];
				return Expr.setFilters.hasOwnProperty( pseudo.toLowerCase() ) ?
					markFunction(function( seed, matches ) {
						var idx,
							matched = fn( seed, argument ),
							i = matched.length;
						while ( i-- ) {
							idx = indexOf( seed, matched[i] );
							seed[ idx ] = !( matches[ idx ] = matched[i] );
						}
					}) :
					function( elem ) {
						return fn( elem, 0, args );
					};
			}

			return fn;
		}
	},

	pseudos: {
		// Potentially complex pseudos
		"not": markFunction(function( selector ) {
			// Trim the selector passed to compile
			// to avoid treating leading and trailing
			// spaces as combinators
			var input = [],
				results = [],
				matcher = compile( selector.replace( rtrim, "$1" ) );

			return matcher[ expando ] ?
				markFunction(function( seed, matches, context, xml ) {
					var elem,
						unmatched = matcher( seed, null, xml, [] ),
						i = seed.length;

					// Match elements unmatched by `matcher`
					while ( i-- ) {
						if ( (elem = unmatched[i]) ) {
							seed[i] = !(matches[i] = elem);
						}
					}
				}) :
				function( elem, context, xml ) {
					input[0] = elem;
					matcher( input, null, xml, results );
					// Don't keep the element (issue #299)
					input[0] = null;
					return !results.pop();
				};
		}),

		"has": markFunction(function( selector ) {
			return function( elem ) {
				return Sizzle( selector, elem ).length > 0;
			};
		}),

		"contains": markFunction(function( text ) {
			text = text.replace( runescape, funescape );
			return function( elem ) {
				return ( elem.textContent || elem.innerText || getText( elem ) ).indexOf( text ) > -1;
			};
		}),

		// "Whether an element is represented by a :lang() selector
		// is based solely on the element's language value
		// being equal to the identifier C,
		// or beginning with the identifier C immediately followed by "-".
		// The matching of C against the element's language value is performed case-insensitively.
		// The identifier C does not have to be a valid language name."
		// http://www.w3.org/TR/selectors/#lang-pseudo
		"lang": markFunction( function( lang ) {
			// lang value must be a valid identifier
			if ( !ridentifier.test(lang || "") ) {
				Sizzle.error( "unsupported lang: " + lang );
			}
			lang = lang.replace( runescape, funescape ).toLowerCase();
			return function( elem ) {
				var elemLang;
				do {
					if ( (elemLang = documentIsHTML ?
						elem.lang :
						elem.getAttribute("xml:lang") || elem.getAttribute("lang")) ) {

						elemLang = elemLang.toLowerCase();
						return elemLang === lang || elemLang.indexOf( lang + "-" ) === 0;
					}
				} while ( (elem = elem.parentNode) && elem.nodeType === 1 );
				return false;
			};
		}),

		// Miscellaneous
		"target": function( elem ) {
			var hash = window.location && window.location.hash;
			return hash && hash.slice( 1 ) === elem.id;
		},

		"root": function( elem ) {
			return elem === docElem;
		},

		"focus": function( elem ) {
			return elem === document.activeElement && (!document.hasFocus || document.hasFocus()) && !!(elem.type || elem.href || ~elem.tabIndex);
		},

		// Boolean properties
		"enabled": function( elem ) {
			return elem.disabled === false;
		},

		"disabled": function( elem ) {
			return elem.disabled === true;
		},

		"checked": function( elem ) {
			// In CSS3, :checked should return both checked and selected elements
			// http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
			var nodeName = elem.nodeName.toLowerCase();
			return (nodeName === "input" && !!elem.checked) || (nodeName === "option" && !!elem.selected);
		},

		"selected": function( elem ) {
			// Accessing this property makes selected-by-default
			// options in Safari work properly
			if ( elem.parentNode ) {
				elem.parentNode.selectedIndex;
			}

			return elem.selected === true;
		},

		// Contents
		"empty": function( elem ) {
			// http://www.w3.org/TR/selectors/#empty-pseudo
			// :empty is negated by element (1) or content nodes (text: 3; cdata: 4; entity ref: 5),
			//   but not by others (comment: 8; processing instruction: 7; etc.)
			// nodeType < 6 works because attributes (2) do not appear as children
			for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {
				if ( elem.nodeType < 6 ) {
					return false;
				}
			}
			return true;
		},

		"parent": function( elem ) {
			return !Expr.pseudos["empty"]( elem );
		},

		// Element/input types
		"header": function( elem ) {
			return rheader.test( elem.nodeName );
		},

		"input": function( elem ) {
			return rinputs.test( elem.nodeName );
		},

		"button": function( elem ) {
			var name = elem.nodeName.toLowerCase();
			return name === "input" && elem.type === "button" || name === "button";
		},

		"text": function( elem ) {
			var attr;
			return elem.nodeName.toLowerCase() === "input" &&
				elem.type === "text" &&

				// Support: IE<8
				// New HTML5 attribute values (e.g., "search") appear with elem.type === "text"
				( (attr = elem.getAttribute("type")) == null || attr.toLowerCase() === "text" );
		},

		// Position-in-collection
		"first": createPositionalPseudo(function() {
			return [ 0 ];
		}),

		"last": createPositionalPseudo(function( matchIndexes, length ) {
			return [ length - 1 ];
		}),

		"eq": createPositionalPseudo(function( matchIndexes, length, argument ) {
			return [ argument < 0 ? argument + length : argument ];
		}),

		"even": createPositionalPseudo(function( matchIndexes, length ) {
			var i = 0;
			for ( ; i < length; i += 2 ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		}),

		"odd": createPositionalPseudo(function( matchIndexes, length ) {
			var i = 1;
			for ( ; i < length; i += 2 ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		}),

		"lt": createPositionalPseudo(function( matchIndexes, length, argument ) {
			var i = argument < 0 ? argument + length : argument;
			for ( ; --i >= 0; ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		}),

		"gt": createPositionalPseudo(function( matchIndexes, length, argument ) {
			var i = argument < 0 ? argument + length : argument;
			for ( ; ++i < length; ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		})
	}
};

Expr.pseudos["nth"] = Expr.pseudos["eq"];

// Add button/input type pseudos
for ( i in { radio: true, checkbox: true, file: true, password: true, image: true } ) {
	Expr.pseudos[ i ] = createInputPseudo( i );
}
for ( i in { submit: true, reset: true } ) {
	Expr.pseudos[ i ] = createButtonPseudo( i );
}

// Easy API for creating new setFilters
function setFilters() {}
setFilters.prototype = Expr.filters = Expr.pseudos;
Expr.setFilters = new setFilters();

tokenize = Sizzle.tokenize = function( selector, parseOnly ) {
	var matched, match, tokens, type,
		soFar, groups, preFilters,
		cached = tokenCache[ selector + " " ];

	if ( cached ) {
		return parseOnly ? 0 : cached.slice( 0 );
	}

	soFar = selector;
	groups = [];
	preFilters = Expr.preFilter;

	while ( soFar ) {

		// Comma and first run
		if ( !matched || (match = rcomma.exec( soFar )) ) {
			if ( match ) {
				// Don't consume trailing commas as valid
				soFar = soFar.slice( match[0].length ) || soFar;
			}
			groups.push( (tokens = []) );
		}

		matched = false;

		// Combinators
		if ( (match = rcombinators.exec( soFar )) ) {
			matched = match.shift();
			tokens.push({
				value: matched,
				// Cast descendant combinators to space
				type: match[0].replace( rtrim, " " )
			});
			soFar = soFar.slice( matched.length );
		}

		// Filters
		for ( type in Expr.filter ) {
			if ( (match = matchExpr[ type ].exec( soFar )) && (!preFilters[ type ] ||
				(match = preFilters[ type ]( match ))) ) {
				matched = match.shift();
				tokens.push({
					value: matched,
					type: type,
					matches: match
				});
				soFar = soFar.slice( matched.length );
			}
		}

		if ( !matched ) {
			break;
		}
	}

	// Return the length of the invalid excess
	// if we're just parsing
	// Otherwise, throw an error or return tokens
	return parseOnly ?
		soFar.length :
		soFar ?
			Sizzle.error( selector ) :
			// Cache the tokens
			tokenCache( selector, groups ).slice( 0 );
};

function toSelector( tokens ) {
	var i = 0,
		len = tokens.length,
		selector = "";
	for ( ; i < len; i++ ) {
		selector += tokens[i].value;
	}
	return selector;
}

function addCombinator( matcher, combinator, base ) {
	var dir = combinator.dir,
		checkNonElements = base && dir === "parentNode",
		doneName = done++;

	return combinator.first ?
		// Check against closest ancestor/preceding element
		function( elem, context, xml ) {
			while ( (elem = elem[ dir ]) ) {
				if ( elem.nodeType === 1 || checkNonElements ) {
					return matcher( elem, context, xml );
				}
			}
		} :

		// Check against all ancestor/preceding elements
		function( elem, context, xml ) {
			var oldCache, outerCache,
				newCache = [ dirruns, doneName ];

			// We can't set arbitrary data on XML nodes, so they don't benefit from dir caching
			if ( xml ) {
				while ( (elem = elem[ dir ]) ) {
					if ( elem.nodeType === 1 || checkNonElements ) {
						if ( matcher( elem, context, xml ) ) {
							return true;
						}
					}
				}
			} else {
				while ( (elem = elem[ dir ]) ) {
					if ( elem.nodeType === 1 || checkNonElements ) {
						outerCache = elem[ expando ] || (elem[ expando ] = {});
						if ( (oldCache = outerCache[ dir ]) &&
							oldCache[ 0 ] === dirruns && oldCache[ 1 ] === doneName ) {

							// Assign to newCache so results back-propagate to previous elements
							return (newCache[ 2 ] = oldCache[ 2 ]);
						} else {
							// Reuse newcache so results back-propagate to previous elements
							outerCache[ dir ] = newCache;

							// A match means we're done; a fail means we have to keep checking
							if ( (newCache[ 2 ] = matcher( elem, context, xml )) ) {
								return true;
							}
						}
					}
				}
			}
		};
}

function elementMatcher( matchers ) {
	return matchers.length > 1 ?
		function( elem, context, xml ) {
			var i = matchers.length;
			while ( i-- ) {
				if ( !matchers[i]( elem, context, xml ) ) {
					return false;
				}
			}
			return true;
		} :
		matchers[0];
}

function multipleContexts( selector, contexts, results ) {
	var i = 0,
		len = contexts.length;
	for ( ; i < len; i++ ) {
		Sizzle( selector, contexts[i], results );
	}
	return results;
}

function condense( unmatched, map, filter, context, xml ) {
	var elem,
		newUnmatched = [],
		i = 0,
		len = unmatched.length,
		mapped = map != null;

	for ( ; i < len; i++ ) {
		if ( (elem = unmatched[i]) ) {
			if ( !filter || filter( elem, context, xml ) ) {
				newUnmatched.push( elem );
				if ( mapped ) {
					map.push( i );
				}
			}
		}
	}

	return newUnmatched;
}

function setMatcher( preFilter, selector, matcher, postFilter, postFinder, postSelector ) {
	if ( postFilter && !postFilter[ expando ] ) {
		postFilter = setMatcher( postFilter );
	}
	if ( postFinder && !postFinder[ expando ] ) {
		postFinder = setMatcher( postFinder, postSelector );
	}
	return markFunction(function( seed, results, context, xml ) {
		var temp, i, elem,
			preMap = [],
			postMap = [],
			preexisting = results.length,

			// Get initial elements from seed or context
			elems = seed || multipleContexts( selector || "*", context.nodeType ? [ context ] : context, [] ),

			// Prefilter to get matcher input, preserving a map for seed-results synchronization
			matcherIn = preFilter && ( seed || !selector ) ?
				condense( elems, preMap, preFilter, context, xml ) :
				elems,

			matcherOut = matcher ?
				// If we have a postFinder, or filtered seed, or non-seed postFilter or preexisting results,
				postFinder || ( seed ? preFilter : preexisting || postFilter ) ?

					// ...intermediate processing is necessary
					[] :

					// ...otherwise use results directly
					results :
				matcherIn;

		// Find primary matches
		if ( matcher ) {
			matcher( matcherIn, matcherOut, context, xml );
		}

		// Apply postFilter
		if ( postFilter ) {
			temp = condense( matcherOut, postMap );
			postFilter( temp, [], context, xml );

			// Un-match failing elements by moving them back to matcherIn
			i = temp.length;
			while ( i-- ) {
				if ( (elem = temp[i]) ) {
					matcherOut[ postMap[i] ] = !(matcherIn[ postMap[i] ] = elem);
				}
			}
		}

		if ( seed ) {
			if ( postFinder || preFilter ) {
				if ( postFinder ) {
					// Get the final matcherOut by condensing this intermediate into postFinder contexts
					temp = [];
					i = matcherOut.length;
					while ( i-- ) {
						if ( (elem = matcherOut[i]) ) {
							// Restore matcherIn since elem is not yet a final match
							temp.push( (matcherIn[i] = elem) );
						}
					}
					postFinder( null, (matcherOut = []), temp, xml );
				}

				// Move matched elements from seed to results to keep them synchronized
				i = matcherOut.length;
				while ( i-- ) {
					if ( (elem = matcherOut[i]) &&
						(temp = postFinder ? indexOf( seed, elem ) : preMap[i]) > -1 ) {

						seed[temp] = !(results[temp] = elem);
					}
				}
			}

		// Add elements to results, through postFinder if defined
		} else {
			matcherOut = condense(
				matcherOut === results ?
					matcherOut.splice( preexisting, matcherOut.length ) :
					matcherOut
			);
			if ( postFinder ) {
				postFinder( null, results, matcherOut, xml );
			} else {
				push.apply( results, matcherOut );
			}
		}
	});
}

function matcherFromTokens( tokens ) {
	var checkContext, matcher, j,
		len = tokens.length,
		leadingRelative = Expr.relative[ tokens[0].type ],
		implicitRelative = leadingRelative || Expr.relative[" "],
		i = leadingRelative ? 1 : 0,

		// The foundational matcher ensures that elements are reachable from top-level context(s)
		matchContext = addCombinator( function( elem ) {
			return elem === checkContext;
		}, implicitRelative, true ),
		matchAnyContext = addCombinator( function( elem ) {
			return indexOf( checkContext, elem ) > -1;
		}, implicitRelative, true ),
		matchers = [ function( elem, context, xml ) {
			var ret = ( !leadingRelative && ( xml || context !== outermostContext ) ) || (
				(checkContext = context).nodeType ?
					matchContext( elem, context, xml ) :
					matchAnyContext( elem, context, xml ) );
			// Avoid hanging onto element (issue #299)
			checkContext = null;
			return ret;
		} ];

	for ( ; i < len; i++ ) {
		if ( (matcher = Expr.relative[ tokens[i].type ]) ) {
			matchers = [ addCombinator(elementMatcher( matchers ), matcher) ];
		} else {
			matcher = Expr.filter[ tokens[i].type ].apply( null, tokens[i].matches );

			// Return special upon seeing a positional matcher
			if ( matcher[ expando ] ) {
				// Find the next relative operator (if any) for proper handling
				j = ++i;
				for ( ; j < len; j++ ) {
					if ( Expr.relative[ tokens[j].type ] ) {
						break;
					}
				}
				return setMatcher(
					i > 1 && elementMatcher( matchers ),
					i > 1 && toSelector(
						// If the preceding token was a descendant combinator, insert an implicit any-element `*`
						tokens.slice( 0, i - 1 ).concat({ value: tokens[ i - 2 ].type === " " ? "*" : "" })
					).replace( rtrim, "$1" ),
					matcher,
					i < j && matcherFromTokens( tokens.slice( i, j ) ),
					j < len && matcherFromTokens( (tokens = tokens.slice( j )) ),
					j < len && toSelector( tokens )
				);
			}
			matchers.push( matcher );
		}
	}

	return elementMatcher( matchers );
}

function matcherFromGroupMatchers( elementMatchers, setMatchers ) {
	var bySet = setMatchers.length > 0,
		byElement = elementMatchers.length > 0,
		superMatcher = function( seed, context, xml, results, outermost ) {
			var elem, j, matcher,
				matchedCount = 0,
				i = "0",
				unmatched = seed && [],
				setMatched = [],
				contextBackup = outermostContext,
				// We must always have either seed elements or outermost context
				elems = seed || byElement && Expr.find["TAG"]( "*", outermost ),
				// Use integer dirruns iff this is the outermost matcher
				dirrunsUnique = (dirruns += contextBackup == null ? 1 : Math.random() || 0.1),
				len = elems.length;

			if ( outermost ) {
				outermostContext = context !== document && context;
			}

			// Add elements passing elementMatchers directly to results
			// Keep `i` a string if there are no elements so `matchedCount` will be "00" below
			// Support: IE<9, Safari
			// Tolerate NodeList properties (IE: "length"; Safari: <number>) matching elements by id
			for ( ; i !== len && (elem = elems[i]) != null; i++ ) {
				if ( byElement && elem ) {
					j = 0;
					while ( (matcher = elementMatchers[j++]) ) {
						if ( matcher( elem, context, xml ) ) {
							results.push( elem );
							break;
						}
					}
					if ( outermost ) {
						dirruns = dirrunsUnique;
					}
				}

				// Track unmatched elements for set filters
				if ( bySet ) {
					// They will have gone through all possible matchers
					if ( (elem = !matcher && elem) ) {
						matchedCount--;
					}

					// Lengthen the array for every element, matched or not
					if ( seed ) {
						unmatched.push( elem );
					}
				}
			}

			// Apply set filters to unmatched elements
			matchedCount += i;
			if ( bySet && i !== matchedCount ) {
				j = 0;
				while ( (matcher = setMatchers[j++]) ) {
					matcher( unmatched, setMatched, context, xml );
				}

				if ( seed ) {
					// Reintegrate element matches to eliminate the need for sorting
					if ( matchedCount > 0 ) {
						while ( i-- ) {
							if ( !(unmatched[i] || setMatched[i]) ) {
								setMatched[i] = pop.call( results );
							}
						}
					}

					// Discard index placeholder values to get only actual matches
					setMatched = condense( setMatched );
				}

				// Add matches to results
				push.apply( results, setMatched );

				// Seedless set matches succeeding multiple successful matchers stipulate sorting
				if ( outermost && !seed && setMatched.length > 0 &&
					( matchedCount + setMatchers.length ) > 1 ) {

					Sizzle.uniqueSort( results );
				}
			}

			// Override manipulation of globals by nested matchers
			if ( outermost ) {
				dirruns = dirrunsUnique;
				outermostContext = contextBackup;
			}

			return unmatched;
		};

	return bySet ?
		markFunction( superMatcher ) :
		superMatcher;
}

compile = Sizzle.compile = function( selector, match /* Internal Use Only */ ) {
	var i,
		setMatchers = [],
		elementMatchers = [],
		cached = compilerCache[ selector + " " ];

	if ( !cached ) {
		// Generate a function of recursive functions that can be used to check each element
		if ( !match ) {
			match = tokenize( selector );
		}
		i = match.length;
		while ( i-- ) {
			cached = matcherFromTokens( match[i] );
			if ( cached[ expando ] ) {
				setMatchers.push( cached );
			} else {
				elementMatchers.push( cached );
			}
		}

		// Cache the compiled function
		cached = compilerCache( selector, matcherFromGroupMatchers( elementMatchers, setMatchers ) );

		// Save selector and tokenization
		cached.selector = selector;
	}
	return cached;
};

/**
 * A low-level selection function that works with Sizzle's compiled
 *  selector functions
 * @param {String|Function} selector A selector or a pre-compiled
 *  selector function built with Sizzle.compile
 * @param {Element} context
 * @param {Array} [results]
 * @param {Array} [seed] A set of elements to match against
 */
select = Sizzle.select = function( selector, context, results, seed ) {
	var i, tokens, token, type, find,
		compiled = typeof selector === "function" && selector,
		match = !seed && tokenize( (selector = compiled.selector || selector) );

	results = results || [];

	// Try to minimize operations if there is no seed and only one group
	if ( match.length === 1 ) {

		// Take a shortcut and set the context if the root selector is an ID
		tokens = match[0] = match[0].slice( 0 );
		if ( tokens.length > 2 && (token = tokens[0]).type === "ID" &&
				support.getById && context.nodeType === 9 && documentIsHTML &&
				Expr.relative[ tokens[1].type ] ) {

			context = ( Expr.find["ID"]( token.matches[0].replace(runescape, funescape), context ) || [] )[0];
			if ( !context ) {
				return results;

			// Precompiled matchers will still verify ancestry, so step up a level
			} else if ( compiled ) {
				context = context.parentNode;
			}

			selector = selector.slice( tokens.shift().value.length );
		}

		// Fetch a seed set for right-to-left matching
		i = matchExpr["needsContext"].test( selector ) ? 0 : tokens.length;
		while ( i-- ) {
			token = tokens[i];

			// Abort if we hit a combinator
			if ( Expr.relative[ (type = token.type) ] ) {
				break;
			}
			if ( (find = Expr.find[ type ]) ) {
				// Search, expanding context for leading sibling combinators
				if ( (seed = find(
					token.matches[0].replace( runescape, funescape ),
					rsibling.test( tokens[0].type ) && testContext( context.parentNode ) || context
				)) ) {

					// If seed is empty or no tokens remain, we can return early
					tokens.splice( i, 1 );
					selector = seed.length && toSelector( tokens );
					if ( !selector ) {
						push.apply( results, seed );
						return results;
					}

					break;
				}
			}
		}
	}

	// Compile and execute a filtering function if one is not provided
	// Provide `match` to avoid retokenization if we modified the selector above
	( compiled || compile( selector, match ) )(
		seed,
		context,
		!documentIsHTML,
		results,
		rsibling.test( selector ) && testContext( context.parentNode ) || context
	);
	return results;
};

// One-time assignments

// Sort stability
support.sortStable = expando.split("").sort( sortOrder ).join("") === expando;

// Support: Chrome 14-35+
// Always assume duplicates if they aren't passed to the comparison function
support.detectDuplicates = !!hasDuplicate;

// Initialize against the default document
setDocument();

// Support: Webkit<537.32 - Safari 6.0.3/Chrome 25 (fixed in Chrome 27)
// Detached nodes confoundingly follow *each other*
support.sortDetached = assert(function( div1 ) {
	// Should return 1, but returns 4 (following)
	return div1.compareDocumentPosition( document.createElement("div") ) & 1;
});

// Support: IE<8
// Prevent attribute/property "interpolation"
// http://msdn.microsoft.com/en-us/library/ms536429%28VS.85%29.aspx
if ( !assert(function( div ) {
	div.innerHTML = "<a href='#'></a>";
	return div.firstChild.getAttribute("href") === "#" ;
}) ) {
	addHandle( "type|href|height|width", function( elem, name, isXML ) {
		if ( !isXML ) {
			return elem.getAttribute( name, name.toLowerCase() === "type" ? 1 : 2 );
		}
	});
}

// Support: IE<9
// Use defaultValue in place of getAttribute("value")
if ( !support.attributes || !assert(function( div ) {
	div.innerHTML = "<input/>";
	div.firstChild.setAttribute( "value", "" );
	return div.firstChild.getAttribute( "value" ) === "";
}) ) {
	addHandle( "value", function( elem, name, isXML ) {
		if ( !isXML && elem.nodeName.toLowerCase() === "input" ) {
			return elem.defaultValue;
		}
	});
}

// Support: IE<9
// Use getAttributeNode to fetch booleans when getAttribute lies
if ( !assert(function( div ) {
	return div.getAttribute("disabled") == null;
}) ) {
	addHandle( booleans, function( elem, name, isXML ) {
		var val;
		if ( !isXML ) {
			return elem[ name ] === true ? name.toLowerCase() :
					(val = elem.getAttributeNode( name )) && val.specified ?
					val.value :
				null;
		}
	});
}

return Sizzle;

})( window );



jQuery.find = Sizzle;
jQuery.expr = Sizzle.selectors;
jQuery.expr[":"] = jQuery.expr.pseudos;
jQuery.unique = Sizzle.uniqueSort;
jQuery.text = Sizzle.getText;
jQuery.isXMLDoc = Sizzle.isXML;
jQuery.contains = Sizzle.contains;



var rneedsContext = jQuery.expr.match.needsContext;

var rsingleTag = (/^<(\w+)\s*\/?>(?:<\/\1>|)$/);



var risSimple = /^.[^:#\[\.,]*$/;

// Implement the identical functionality for filter and not
function winnow( elements, qualifier, not ) {
	if ( jQuery.isFunction( qualifier ) ) {
		return jQuery.grep( elements, function( elem, i ) {
			/* jshint -W018 */
			return !!qualifier.call( elem, i, elem ) !== not;
		});

	}

	if ( qualifier.nodeType ) {
		return jQuery.grep( elements, function( elem ) {
			return ( elem === qualifier ) !== not;
		});

	}

	if ( typeof qualifier === "string" ) {
		if ( risSimple.test( qualifier ) ) {
			return jQuery.filter( qualifier, elements, not );
		}

		qualifier = jQuery.filter( qualifier, elements );
	}

	return jQuery.grep( elements, function( elem ) {
		return ( indexOf.call( qualifier, elem ) >= 0 ) !== not;
	});
}

jQuery.filter = function( expr, elems, not ) {
	var elem = elems[ 0 ];

	if ( not ) {
		expr = ":not(" + expr + ")";
	}

	return elems.length === 1 && elem.nodeType === 1 ?
		jQuery.find.matchesSelector( elem, expr ) ? [ elem ] : [] :
		jQuery.find.matches( expr, jQuery.grep( elems, function( elem ) {
			return elem.nodeType === 1;
		}));
};

jQuery.fn.extend({
	find: function( selector ) {
		var i,
			len = this.length,
			ret = [],
			self = this;

		if ( typeof selector !== "string" ) {
			return this.pushStack( jQuery( selector ).filter(function() {
				for ( i = 0; i < len; i++ ) {
					if ( jQuery.contains( self[ i ], this ) ) {
						return true;
					}
				}
			}) );
		}

		for ( i = 0; i < len; i++ ) {
			jQuery.find( selector, self[ i ], ret );
		}

		// Needed because $( selector, context ) becomes $( context ).find( selector )
		ret = this.pushStack( len > 1 ? jQuery.unique( ret ) : ret );
		ret.selector = this.selector ? this.selector + " " + selector : selector;
		return ret;
	},
	filter: function( selector ) {
		return this.pushStack( winnow(this, selector || [], false) );
	},
	not: function( selector ) {
		return this.pushStack( winnow(this, selector || [], true) );
	},
	is: function( selector ) {
		return !!winnow(
			this,

			// If this is a positional/relative selector, check membership in the returned set
			// so $("p:first").is("p:last") won't return true for a doc with two "p".
			typeof selector === "string" && rneedsContext.test( selector ) ?
				jQuery( selector ) :
				selector || [],
			false
		).length;
	}
});


// Initialize a jQuery object


// A central reference to the root jQuery(document)
var rootjQuery,

	// A simple way to check for HTML strings
	// Prioritize #id over <tag> to avoid XSS via location.hash (#9521)
	// Strict HTML recognition (#11290: must start with <)
	rquickExpr = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,

	init = jQuery.fn.init = function( selector, context ) {
		var match, elem;

		// HANDLE: $(""), $(null), $(undefined), $(false)
		if ( !selector ) {
			return this;
		}

		// Handle HTML strings
		if ( typeof selector === "string" ) {
			if ( selector[0] === "<" && selector[ selector.length - 1 ] === ">" && selector.length >= 3 ) {
				// Assume that strings that start and end with <> are HTML and skip the regex check
				match = [ null, selector, null ];

			} else {
				match = rquickExpr.exec( selector );
			}

			// Match html or make sure no context is specified for #id
			if ( match && (match[1] || !context) ) {

				// HANDLE: $(html) -> $(array)
				if ( match[1] ) {
					context = context instanceof jQuery ? context[0] : context;

					// Option to run scripts is true for back-compat
					// Intentionally let the error be thrown if parseHTML is not present
					jQuery.merge( this, jQuery.parseHTML(
						match[1],
						context && context.nodeType ? context.ownerDocument || context : document,
						true
					) );

					// HANDLE: $(html, props)
					if ( rsingleTag.test( match[1] ) && jQuery.isPlainObject( context ) ) {
						for ( match in context ) {
							// Properties of context are called as methods if possible
							if ( jQuery.isFunction( this[ match ] ) ) {
								this[ match ]( context[ match ] );

							// ...and otherwise set as attributes
							} else {
								this.attr( match, context[ match ] );
							}
						}
					}

					return this;

				// HANDLE: $(#id)
				} else {
					elem = document.getElementById( match[2] );

					// Support: Blackberry 4.6
					// gEBID returns nodes no longer in the document (#6963)
					if ( elem && elem.parentNode ) {
						// Inject the element directly into the jQuery object
						this.length = 1;
						this[0] = elem;
					}

					this.context = document;
					this.selector = selector;
					return this;
				}

			// HANDLE: $(expr, $(...))
			} else if ( !context || context.jquery ) {
				return ( context || rootjQuery ).find( selector );

			// HANDLE: $(expr, context)
			// (which is just equivalent to: $(context).find(expr)
			} else {
				return this.constructor( context ).find( selector );
			}

		// HANDLE: $(DOMElement)
		} else if ( selector.nodeType ) {
			this.context = this[0] = selector;
			this.length = 1;
			return this;

		// HANDLE: $(function)
		// Shortcut for document ready
		} else if ( jQuery.isFunction( selector ) ) {
			return typeof rootjQuery.ready !== "undefined" ?
				rootjQuery.ready( selector ) :
				// Execute immediately if ready is not present
				selector( jQuery );
		}

		if ( selector.selector !== undefined ) {
			this.selector = selector.selector;
			this.context = selector.context;
		}

		return jQuery.makeArray( selector, this );
	};

// Give the init function the jQuery prototype for later instantiation
init.prototype = jQuery.fn;

// Initialize central reference
rootjQuery = jQuery( document );


var rparentsprev = /^(?:parents|prev(?:Until|All))/,
	// Methods guaranteed to produce a unique set when starting from a unique set
	guaranteedUnique = {
		children: true,
		contents: true,
		next: true,
		prev: true
	};

jQuery.extend({
	dir: function( elem, dir, until ) {
		var matched = [],
			truncate = until !== undefined;

		while ( (elem = elem[ dir ]) && elem.nodeType !== 9 ) {
			if ( elem.nodeType === 1 ) {
				if ( truncate && jQuery( elem ).is( until ) ) {
					break;
				}
				matched.push( elem );
			}
		}
		return matched;
	},

	sibling: function( n, elem ) {
		var matched = [];

		for ( ; n; n = n.nextSibling ) {
			if ( n.nodeType === 1 && n !== elem ) {
				matched.push( n );
			}
		}

		return matched;
	}
});

jQuery.fn.extend({
	has: function( target ) {
		var targets = jQuery( target, this ),
			l = targets.length;

		return this.filter(function() {
			var i = 0;
			for ( ; i < l; i++ ) {
				if ( jQuery.contains( this, targets[i] ) ) {
					return true;
				}
			}
		});
	},

	closest: function( selectors, context ) {
		var cur,
			i = 0,
			l = this.length,
			matched = [],
			pos = rneedsContext.test( selectors ) || typeof selectors !== "string" ?
				jQuery( selectors, context || this.context ) :
				0;

		for ( ; i < l; i++ ) {
			for ( cur = this[i]; cur && cur !== context; cur = cur.parentNode ) {
				// Always skip document fragments
				if ( cur.nodeType < 11 && (pos ?
					pos.index(cur) > -1 :

					// Don't pass non-elements to Sizzle
					cur.nodeType === 1 &&
						jQuery.find.matchesSelector(cur, selectors)) ) {

					matched.push( cur );
					break;
				}
			}
		}

		return this.pushStack( matched.length > 1 ? jQuery.unique( matched ) : matched );
	},

	// Determine the position of an element within the set
	index: function( elem ) {

		// No argument, return index in parent
		if ( !elem ) {
			return ( this[ 0 ] && this[ 0 ].parentNode ) ? this.first().prevAll().length : -1;
		}

		// Index in selector
		if ( typeof elem === "string" ) {
			return indexOf.call( jQuery( elem ), this[ 0 ] );
		}

		// Locate the position of the desired element
		return indexOf.call( this,

			// If it receives a jQuery object, the first element is used
			elem.jquery ? elem[ 0 ] : elem
		);
	},

	add: function( selector, context ) {
		return this.pushStack(
			jQuery.unique(
				jQuery.merge( this.get(), jQuery( selector, context ) )
			)
		);
	},

	addBack: function( selector ) {
		return this.add( selector == null ?
			this.prevObject : this.prevObject.filter(selector)
		);
	}
});

function sibling( cur, dir ) {
	while ( (cur = cur[dir]) && cur.nodeType !== 1 ) {}
	return cur;
}

jQuery.each({
	parent: function( elem ) {
		var parent = elem.parentNode;
		return parent && parent.nodeType !== 11 ? parent : null;
	},
	parents: function( elem ) {
		return jQuery.dir( elem, "parentNode" );
	},
	parentsUntil: function( elem, i, until ) {
		return jQuery.dir( elem, "parentNode", until );
	},
	next: function( elem ) {
		return sibling( elem, "nextSibling" );
	},
	prev: function( elem ) {
		return sibling( elem, "previousSibling" );
	},
	nextAll: function( elem ) {
		return jQuery.dir( elem, "nextSibling" );
	},
	prevAll: function( elem ) {
		return jQuery.dir( elem, "previousSibling" );
	},
	nextUntil: function( elem, i, until ) {
		return jQuery.dir( elem, "nextSibling", until );
	},
	prevUntil: function( elem, i, until ) {
		return jQuery.dir( elem, "previousSibling", until );
	},
	siblings: function( elem ) {
		return jQuery.sibling( ( elem.parentNode || {} ).firstChild, elem );
	},
	children: function( elem ) {
		return jQuery.sibling( elem.firstChild );
	},
	contents: function( elem ) {
		return elem.contentDocument || jQuery.merge( [], elem.childNodes );
	}
}, function( name, fn ) {
	jQuery.fn[ name ] = function( until, selector ) {
		var matched = jQuery.map( this, fn, until );

		if ( name.slice( -5 ) !== "Until" ) {
			selector = until;
		}

		if ( selector && typeof selector === "string" ) {
			matched = jQuery.filter( selector, matched );
		}

		if ( this.length > 1 ) {
			// Remove duplicates
			if ( !guaranteedUnique[ name ] ) {
				jQuery.unique( matched );
			}

			// Reverse order for parents* and prev-derivatives
			if ( rparentsprev.test( name ) ) {
				matched.reverse();
			}
		}

		return this.pushStack( matched );
	};
});
var rnotwhite = (/\S+/g);



// String to Object options format cache
var optionsCache = {};

// Convert String-formatted options into Object-formatted ones and store in cache
function createOptions( options ) {
	var object = optionsCache[ options ] = {};
	jQuery.each( options.match( rnotwhite ) || [], function( _, flag ) {
		object[ flag ] = true;
	});
	return object;
}

/*
 * Create a callback list using the following parameters:
 *
 *	options: an optional list of space-separated options that will change how
 *			the callback list behaves or a more traditional option object
 *
 * By default a callback list will act like an event callback list and can be
 * "fired" multiple times.
 *
 * Possible options:
 *
 *	once:			will ensure the callback list can only be fired once (like a Deferred)
 *
 *	memory:			will keep track of previous values and will call any callback added
 *					after the list has been fired right away with the latest "memorized"
 *					values (like a Deferred)
 *
 *	unique:			will ensure a callback can only be added once (no duplicate in the list)
 *
 *	stopOnFalse:	interrupt callings when a callback returns false
 *
 */
jQuery.Callbacks = function( options ) {

	// Convert options from String-formatted to Object-formatted if needed
	// (we check in cache first)
	options = typeof options === "string" ?
		( optionsCache[ options ] || createOptions( options ) ) :
		jQuery.extend( {}, options );

	var // Last fire value (for non-forgettable lists)
		memory,
		// Flag to know if list was already fired
		fired,
		// Flag to know if list is currently firing
		firing,
		// First callback to fire (used internally by add and fireWith)
		firingStart,
		// End of the loop when firing
		firingLength,
		// Index of currently firing callback (modified by remove if needed)
		firingIndex,
		// Actual callback list
		list = [],
		// Stack of fire calls for repeatable lists
		stack = !options.once && [],
		// Fire callbacks
		fire = function( data ) {
			memory = options.memory && data;
			fired = true;
			firingIndex = firingStart || 0;
			firingStart = 0;
			firingLength = list.length;
			firing = true;
			for ( ; list && firingIndex < firingLength; firingIndex++ ) {
				if ( list[ firingIndex ].apply( data[ 0 ], data[ 1 ] ) === false && options.stopOnFalse ) {
					memory = false; // To prevent further calls using add
					break;
				}
			}
			firing = false;
			if ( list ) {
				if ( stack ) {
					if ( stack.length ) {
						fire( stack.shift() );
					}
				} else if ( memory ) {
					list = [];
				} else {
					self.disable();
				}
			}
		},
		// Actual Callbacks object
		self = {
			// Add a callback or a collection of callbacks to the list
			add: function() {
				if ( list ) {
					// First, we save the current length
					var start = list.length;
					(function add( args ) {
						jQuery.each( args, function( _, arg ) {
							var type = jQuery.type( arg );
							if ( type === "function" ) {
								if ( !options.unique || !self.has( arg ) ) {
									list.push( arg );
								}
							} else if ( arg && arg.length && type !== "string" ) {
								// Inspect recursively
								add( arg );
							}
						});
					})( arguments );
					// Do we need to add the callbacks to the
					// current firing batch?
					if ( firing ) {
						firingLength = list.length;
					// With memory, if we're not firing then
					// we should call right away
					} else if ( memory ) {
						firingStart = start;
						fire( memory );
					}
				}
				return this;
			},
			// Remove a callback from the list
			remove: function() {
				if ( list ) {
					jQuery.each( arguments, function( _, arg ) {
						var index;
						while ( ( index = jQuery.inArray( arg, list, index ) ) > -1 ) {
							list.splice( index, 1 );
							// Handle firing indexes
							if ( firing ) {
								if ( index <= firingLength ) {
									firingLength--;
								}
								if ( index <= firingIndex ) {
									firingIndex--;
								}
							}
						}
					});
				}
				return this;
			},
			// Check if a given callback is in the list.
			// If no argument is given, return whether or not list has callbacks attached.
			has: function( fn ) {
				return fn ? jQuery.inArray( fn, list ) > -1 : !!( list && list.length );
			},
			// Remove all callbacks from the list
			empty: function() {
				list = [];
				firingLength = 0;
				return this;
			},
			// Have the list do nothing anymore
			disable: function() {
				list = stack = memory = undefined;
				return this;
			},
			// Is it disabled?
			disabled: function() {
				return !list;
			},
			// Lock the list in its current state
			lock: function() {
				stack = undefined;
				if ( !memory ) {
					self.disable();
				}
				return this;
			},
			// Is it locked?
			locked: function() {
				return !stack;
			},
			// Call all callbacks with the given context and arguments
			fireWith: function( context, args ) {
				if ( list && ( !fired || stack ) ) {
					args = args || [];
					args = [ context, args.slice ? args.slice() : args ];
					if ( firing ) {
						stack.push( args );
					} else {
						fire( args );
					}
				}
				return this;
			},
			// Call all the callbacks with the given arguments
			fire: function() {
				self.fireWith( this, arguments );
				return this;
			},
			// To know if the callbacks have already been called at least once
			fired: function() {
				return !!fired;
			}
		};

	return self;
};


jQuery.extend({

	Deferred: function( func ) {
		var tuples = [
				// action, add listener, listener list, final state
				[ "resolve", "done", jQuery.Callbacks("once memory"), "resolved" ],
				[ "reject", "fail", jQuery.Callbacks("once memory"), "rejected" ],
				[ "notify", "progress", jQuery.Callbacks("memory") ]
			],
			state = "pending",
			promise = {
				state: function() {
					return state;
				},
				always: function() {
					deferred.done( arguments ).fail( arguments );
					return this;
				},
				then: function( /* fnDone, fnFail, fnProgress */ ) {
					var fns = arguments;
					return jQuery.Deferred(function( newDefer ) {
						jQuery.each( tuples, function( i, tuple ) {
							var fn = jQuery.isFunction( fns[ i ] ) && fns[ i ];
							// deferred[ done | fail | progress ] for forwarding actions to newDefer
							deferred[ tuple[1] ](function() {
								var returned = fn && fn.apply( this, arguments );
								if ( returned && jQuery.isFunction( returned.promise ) ) {
									returned.promise()
										.done( newDefer.resolve )
										.fail( newDefer.reject )
										.progress( newDefer.notify );
								} else {
									newDefer[ tuple[ 0 ] + "With" ]( this === promise ? newDefer.promise() : this, fn ? [ returned ] : arguments );
								}
							});
						});
						fns = null;
					}).promise();
				},
				// Get a promise for this deferred
				// If obj is provided, the promise aspect is added to the object
				promise: function( obj ) {
					return obj != null ? jQuery.extend( obj, promise ) : promise;
				}
			},
			deferred = {};

		// Keep pipe for back-compat
		promise.pipe = promise.then;

		// Add list-specific methods
		jQuery.each( tuples, function( i, tuple ) {
			var list = tuple[ 2 ],
				stateString = tuple[ 3 ];

			// promise[ done | fail | progress ] = list.add
			promise[ tuple[1] ] = list.add;

			// Handle state
			if ( stateString ) {
				list.add(function() {
					// state = [ resolved | rejected ]
					state = stateString;

				// [ reject_list | resolve_list ].disable; progress_list.lock
				}, tuples[ i ^ 1 ][ 2 ].disable, tuples[ 2 ][ 2 ].lock );
			}

			// deferred[ resolve | reject | notify ]
			deferred[ tuple[0] ] = function() {
				deferred[ tuple[0] + "With" ]( this === deferred ? promise : this, arguments );
				return this;
			};
			deferred[ tuple[0] + "With" ] = list.fireWith;
		});

		// Make the deferred a promise
		promise.promise( deferred );

		// Call given func if any
		if ( func ) {
			func.call( deferred, deferred );
		}

		// All done!
		return deferred;
	},

	// Deferred helper
	when: function( subordinate /* , ..., subordinateN */ ) {
		var i = 0,
			resolveValues = slice.call( arguments ),
			length = resolveValues.length,

			// the count of uncompleted subordinates
			remaining = length !== 1 || ( subordinate && jQuery.isFunction( subordinate.promise ) ) ? length : 0,

			// the master Deferred. If resolveValues consist of only a single Deferred, just use that.
			deferred = remaining === 1 ? subordinate : jQuery.Deferred(),

			// Update function for both resolve and progress values
			updateFunc = function( i, contexts, values ) {
				return function( value ) {
					contexts[ i ] = this;
					values[ i ] = arguments.length > 1 ? slice.call( arguments ) : value;
					if ( values === progressValues ) {
						deferred.notifyWith( contexts, values );
					} else if ( !( --remaining ) ) {
						deferred.resolveWith( contexts, values );
					}
				};
			},

			progressValues, progressContexts, resolveContexts;

		// Add listeners to Deferred subordinates; treat others as resolved
		if ( length > 1 ) {
			progressValues = new Array( length );
			progressContexts = new Array( length );
			resolveContexts = new Array( length );
			for ( ; i < length; i++ ) {
				if ( resolveValues[ i ] && jQuery.isFunction( resolveValues[ i ].promise ) ) {
					resolveValues[ i ].promise()
						.done( updateFunc( i, resolveContexts, resolveValues ) )
						.fail( deferred.reject )
						.progress( updateFunc( i, progressContexts, progressValues ) );
				} else {
					--remaining;
				}
			}
		}

		// If we're not waiting on anything, resolve the master
		if ( !remaining ) {
			deferred.resolveWith( resolveContexts, resolveValues );
		}

		return deferred.promise();
	}
});


// The deferred used on DOM ready
var readyList;

jQuery.fn.ready = function( fn ) {
	// Add the callback
	jQuery.ready.promise().done( fn );

	return this;
};

jQuery.extend({
	// Is the DOM ready to be used? Set to true once it occurs.
	isReady: false,

	// A counter to track how many items to wait for before
	// the ready event fires. See #6781
	readyWait: 1,

	// Hold (or release) the ready event
	holdReady: function( hold ) {
		if ( hold ) {
			jQuery.readyWait++;
		} else {
			jQuery.ready( true );
		}
	},

	// Handle when the DOM is ready
	ready: function( wait ) {

		// Abort if there are pending holds or we're already ready
		if ( wait === true ? --jQuery.readyWait : jQuery.isReady ) {
			return;
		}

		// Remember that the DOM is ready
		jQuery.isReady = true;

		// If a normal DOM Ready event fired, decrement, and wait if need be
		if ( wait !== true && --jQuery.readyWait > 0 ) {
			return;
		}

		// If there are functions bound, to execute
		readyList.resolveWith( document, [ jQuery ] );

		// Trigger any bound ready events
		if ( jQuery.fn.triggerHandler ) {
			jQuery( document ).triggerHandler( "ready" );
			jQuery( document ).off( "ready" );
		}
	}
});

/**
 * The ready event handler and self cleanup method
 */
function completed() {
	document.removeEventListener( "DOMContentLoaded", completed, false );
	window.removeEventListener( "load", completed, false );
	jQuery.ready();
}

jQuery.ready.promise = function( obj ) {
	if ( !readyList ) {

		readyList = jQuery.Deferred();

		// Catch cases where $(document).ready() is called after the browser event has already occurred.
		// We once tried to use readyState "interactive" here, but it caused issues like the one
		// discovered by ChrisS here: http://bugs.jquery.com/ticket/12282#comment:15
		if ( document.readyState === "complete" ) {
			// Handle it asynchronously to allow scripts the opportunity to delay ready
			setTimeout( jQuery.ready );

		} else {

			// Use the handy event callback
			document.addEventListener( "DOMContentLoaded", completed, false );

			// A fallback to window.onload, that will always work
			window.addEventListener( "load", completed, false );
		}
	}
	return readyList.promise( obj );
};

// Kick off the DOM ready check even if the user does not
jQuery.ready.promise();




// Multifunctional method to get and set values of a collection
// The value/s can optionally be executed if it's a function
var access = jQuery.access = function( elems, fn, key, value, chainable, emptyGet, raw ) {
	var i = 0,
		len = elems.length,
		bulk = key == null;

	// Sets many values
	if ( jQuery.type( key ) === "object" ) {
		chainable = true;
		for ( i in key ) {
			jQuery.access( elems, fn, i, key[i], true, emptyGet, raw );
		}

	// Sets one value
	} else if ( value !== undefined ) {
		chainable = true;

		if ( !jQuery.isFunction( value ) ) {
			raw = true;
		}

		if ( bulk ) {
			// Bulk operations run against the entire set
			if ( raw ) {
				fn.call( elems, value );
				fn = null;

			// ...except when executing function values
			} else {
				bulk = fn;
				fn = function( elem, key, value ) {
					return bulk.call( jQuery( elem ), value );
				};
			}
		}

		if ( fn ) {
			for ( ; i < len; i++ ) {
				fn( elems[i], key, raw ? value : value.call( elems[i], i, fn( elems[i], key ) ) );
			}
		}
	}

	return chainable ?
		elems :

		// Gets
		bulk ?
			fn.call( elems ) :
			len ? fn( elems[0], key ) : emptyGet;
};


/**
 * Determines whether an object can have data
 */
jQuery.acceptData = function( owner ) {
	// Accepts only:
	//  - Node
	//    - Node.ELEMENT_NODE
	//    - Node.DOCUMENT_NODE
	//  - Object
	//    - Any
	/* jshint -W018 */
	return owner.nodeType === 1 || owner.nodeType === 9 || !( +owner.nodeType );
};


function Data() {
	// Support: Android<4,
	// Old WebKit does not have Object.preventExtensions/freeze method,
	// return new empty object instead with no [[set]] accessor
	Object.defineProperty( this.cache = {}, 0, {
		get: function() {
			return {};
		}
	});

	this.expando = jQuery.expando + Data.uid++;
}

Data.uid = 1;
Data.accepts = jQuery.acceptData;

Data.prototype = {
	key: function( owner ) {
		// We can accept data for non-element nodes in modern browsers,
		// but we should not, see #8335.
		// Always return the key for a frozen object.
		if ( !Data.accepts( owner ) ) {
			return 0;
		}

		var descriptor = {},
			// Check if the owner object already has a cache key
			unlock = owner[ this.expando ];

		// If not, create one
		if ( !unlock ) {
			unlock = Data.uid++;

			// Secure it in a non-enumerable, non-writable property
			try {
				descriptor[ this.expando ] = { value: unlock };
				Object.defineProperties( owner, descriptor );

			// Support: Android<4
			// Fallback to a less secure definition
			} catch ( e ) {
				descriptor[ this.expando ] = unlock;
				jQuery.extend( owner, descriptor );
			}
		}

		// Ensure the cache object
		if ( !this.cache[ unlock ] ) {
			this.cache[ unlock ] = {};
		}

		return unlock;
	},
	set: function( owner, data, value ) {
		var prop,
			// There may be an unlock assigned to this node,
			// if there is no entry for this "owner", create one inline
			// and set the unlock as though an owner entry had always existed
			unlock = this.key( owner ),
			cache = this.cache[ unlock ];

		// Handle: [ owner, key, value ] args
		if ( typeof data === "string" ) {
			cache[ data ] = value;

		// Handle: [ owner, { properties } ] args
		} else {
			// Fresh assignments by object are shallow copied
			if ( jQuery.isEmptyObject( cache ) ) {
				jQuery.extend( this.cache[ unlock ], data );
			// Otherwise, copy the properties one-by-one to the cache object
			} else {
				for ( prop in data ) {
					cache[ prop ] = data[ prop ];
				}
			}
		}
		return cache;
	},
	get: function( owner, key ) {
		// Either a valid cache is found, or will be created.
		// New caches will be created and the unlock returned,
		// allowing direct access to the newly created
		// empty data object. A valid owner object must be provided.
		var cache = this.cache[ this.key( owner ) ];

		return key === undefined ?
			cache : cache[ key ];
	},
	access: function( owner, key, value ) {
		var stored;
		// In cases where either:
		//
		//   1. No key was specified
		//   2. A string key was specified, but no value provided
		//
		// Take the "read" path and allow the get method to determine
		// which value to return, respectively either:
		//
		//   1. The entire cache object
		//   2. The data stored at the key
		//
		if ( key === undefined ||
				((key && typeof key === "string") && value === undefined) ) {

			stored = this.get( owner, key );

			return stored !== undefined ?
				stored : this.get( owner, jQuery.camelCase(key) );
		}

		// [*]When the key is not a string, or both a key and value
		// are specified, set or extend (existing objects) with either:
		//
		//   1. An object of properties
		//   2. A key and value
		//
		this.set( owner, key, value );

		// Since the "set" path can have two possible entry points
		// return the expected data based on which path was taken[*]
		return value !== undefined ? value : key;
	},
	remove: function( owner, key ) {
		var i, name, camel,
			unlock = this.key( owner ),
			cache = this.cache[ unlock ];

		if ( key === undefined ) {
			this.cache[ unlock ] = {};

		} else {
			// Support array or space separated string of keys
			if ( jQuery.isArray( key ) ) {
				// If "name" is an array of keys...
				// When data is initially created, via ("key", "val") signature,
				// keys will be converted to camelCase.
				// Since there is no way to tell _how_ a key was added, remove
				// both plain key and camelCase key. #12786
				// This will only penalize the array argument path.
				name = key.concat( key.map( jQuery.camelCase ) );
			} else {
				camel = jQuery.camelCase( key );
				// Try the string as a key before any manipulation
				if ( key in cache ) {
					name = [ key, camel ];
				} else {
					// If a key with the spaces exists, use it.
					// Otherwise, create an array by matching non-whitespace
					name = camel;
					name = name in cache ?
						[ name ] : ( name.match( rnotwhite ) || [] );
				}
			}

			i = name.length;
			while ( i-- ) {
				delete cache[ name[ i ] ];
			}
		}
	},
	hasData: function( owner ) {
		return !jQuery.isEmptyObject(
			this.cache[ owner[ this.expando ] ] || {}
		);
	},
	discard: function( owner ) {
		if ( owner[ this.expando ] ) {
			delete this.cache[ owner[ this.expando ] ];
		}
	}
};
var data_priv = new Data();

var data_user = new Data();



//	Implementation Summary
//
//	1. Enforce API surface and semantic compatibility with 1.9.x branch
//	2. Improve the module's maintainability by reducing the storage
//		paths to a single mechanism.
//	3. Use the same single mechanism to support "private" and "user" data.
//	4. _Never_ expose "private" data to user code (TODO: Drop _data, _removeData)
//	5. Avoid exposing implementation details on user objects (eg. expando properties)
//	6. Provide a clear path for implementation upgrade to WeakMap in 2014

var rbrace = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
	rmultiDash = /([A-Z])/g;

function dataAttr( elem, key, data ) {
	var name;

	// If nothing was found internally, try to fetch any
	// data from the HTML5 data-* attribute
	if ( data === undefined && elem.nodeType === 1 ) {
		name = "data-" + key.replace( rmultiDash, "-$1" ).toLowerCase();
		data = elem.getAttribute( name );

		if ( typeof data === "string" ) {
			try {
				data = data === "true" ? true :
					data === "false" ? false :
					data === "null" ? null :
					// Only convert to a number if it doesn't change the string
					+data + "" === data ? +data :
					rbrace.test( data ) ? jQuery.parseJSON( data ) :
					data;
			} catch( e ) {}

			// Make sure we set the data so it isn't changed later
			data_user.set( elem, key, data );
		} else {
			data = undefined;
		}
	}
	return data;
}

jQuery.extend({
	hasData: function( elem ) {
		return data_user.hasData( elem ) || data_priv.hasData( elem );
	},

	data: function( elem, name, data ) {
		return data_user.access( elem, name, data );
	},

	removeData: function( elem, name ) {
		data_user.remove( elem, name );
	},

	// TODO: Now that all calls to _data and _removeData have been replaced
	// with direct calls to data_priv methods, these can be deprecated.
	_data: function( elem, name, data ) {
		return data_priv.access( elem, name, data );
	},

	_removeData: function( elem, name ) {
		data_priv.remove( elem, name );
	}
});

jQuery.fn.extend({
	data: function( key, value ) {
		var i, name, data,
			elem = this[ 0 ],
			attrs = elem && elem.attributes;

		// Gets all values
		if ( key === undefined ) {
			if ( this.length ) {
				data = data_user.get( elem );

				if ( elem.nodeType === 1 && !data_priv.get( elem, "hasDataAttrs" ) ) {
					i = attrs.length;
					while ( i-- ) {

						// Support: IE11+
						// The attrs elements can be null (#14894)
						if ( attrs[ i ] ) {
							name = attrs[ i ].name;
							if ( name.indexOf( "data-" ) === 0 ) {
								name = jQuery.camelCase( name.slice(5) );
								dataAttr( elem, name, data[ name ] );
							}
						}
					}
					data_priv.set( elem, "hasDataAttrs", true );
				}
			}

			return data;
		}

		// Sets multiple values
		if ( typeof key === "object" ) {
			return this.each(function() {
				data_user.set( this, key );
			});
		}

		return access( this, function( value ) {
			var data,
				camelKey = jQuery.camelCase( key );

			// The calling jQuery object (element matches) is not empty
			// (and therefore has an element appears at this[ 0 ]) and the
			// `value` parameter was not undefined. An empty jQuery object
			// will result in `undefined` for elem = this[ 0 ] which will
			// throw an exception if an attempt to read a data cache is made.
			if ( elem && value === undefined ) {
				// Attempt to get data from the cache
				// with the key as-is
				data = data_user.get( elem, key );
				if ( data !== undefined ) {
					return data;
				}

				// Attempt to get data from the cache
				// with the key camelized
				data = data_user.get( elem, camelKey );
				if ( data !== undefined ) {
					return data;
				}

				// Attempt to "discover" the data in
				// HTML5 custom data-* attrs
				data = dataAttr( elem, camelKey, undefined );
				if ( data !== undefined ) {
					return data;
				}

				// We tried really hard, but the data doesn't exist.
				return;
			}

			// Set the data...
			this.each(function() {
				// First, attempt to store a copy or reference of any
				// data that might've been store with a camelCased key.
				var data = data_user.get( this, camelKey );

				// For HTML5 data-* attribute interop, we have to
				// store property names with dashes in a camelCase form.
				// This might not apply to all properties...*
				data_user.set( this, camelKey, value );

				// *... In the case of properties that might _actually_
				// have dashes, we need to also store a copy of that
				// unchanged property.
				if ( key.indexOf("-") !== -1 && data !== undefined ) {
					data_user.set( this, key, value );
				}
			});
		}, null, value, arguments.length > 1, null, true );
	},

	removeData: function( key ) {
		return this.each(function() {
			data_user.remove( this, key );
		});
	}
});


jQuery.extend({
	queue: function( elem, type, data ) {
		var queue;

		if ( elem ) {
			type = ( type || "fx" ) + "queue";
			queue = data_priv.get( elem, type );

			// Speed up dequeue by getting out quickly if this is just a lookup
			if ( data ) {
				if ( !queue || jQuery.isArray( data ) ) {
					queue = data_priv.access( elem, type, jQuery.makeArray(data) );
				} else {
					queue.push( data );
				}
			}
			return queue || [];
		}
	},

	dequeue: function( elem, type ) {
		type = type || "fx";

		var queue = jQuery.queue( elem, type ),
			startLength = queue.length,
			fn = queue.shift(),
			hooks = jQuery._queueHooks( elem, type ),
			next = function() {
				jQuery.dequeue( elem, type );
			};

		// If the fx queue is dequeued, always remove the progress sentinel
		if ( fn === "inprogress" ) {
			fn = queue.shift();
			startLength--;
		}

		if ( fn ) {

			// Add a progress sentinel to prevent the fx queue from being
			// automatically dequeued
			if ( type === "fx" ) {
				queue.unshift( "inprogress" );
			}

			// Clear up the last queue stop function
			delete hooks.stop;
			fn.call( elem, next, hooks );
		}

		if ( !startLength && hooks ) {
			hooks.empty.fire();
		}
	},

	// Not public - generate a queueHooks object, or return the current one
	_queueHooks: function( elem, type ) {
		var key = type + "queueHooks";
		return data_priv.get( elem, key ) || data_priv.access( elem, key, {
			empty: jQuery.Callbacks("once memory").add(function() {
				data_priv.remove( elem, [ type + "queue", key ] );
			})
		});
	}
});

jQuery.fn.extend({
	queue: function( type, data ) {
		var setter = 2;

		if ( typeof type !== "string" ) {
			data = type;
			type = "fx";
			setter--;
		}

		if ( arguments.length < setter ) {
			return jQuery.queue( this[0], type );
		}

		return data === undefined ?
			this :
			this.each(function() {
				var queue = jQuery.queue( this, type, data );

				// Ensure a hooks for this queue
				jQuery._queueHooks( this, type );

				if ( type === "fx" && queue[0] !== "inprogress" ) {
					jQuery.dequeue( this, type );
				}
			});
	},
	dequeue: function( type ) {
		return this.each(function() {
			jQuery.dequeue( this, type );
		});
	},
	clearQueue: function( type ) {
		return this.queue( type || "fx", [] );
	},
	// Get a promise resolved when queues of a certain type
	// are emptied (fx is the type by default)
	promise: function( type, obj ) {
		var tmp,
			count = 1,
			defer = jQuery.Deferred(),
			elements = this,
			i = this.length,
			resolve = function() {
				if ( !( --count ) ) {
					defer.resolveWith( elements, [ elements ] );
				}
			};

		if ( typeof type !== "string" ) {
			obj = type;
			type = undefined;
		}
		type = type || "fx";

		while ( i-- ) {
			tmp = data_priv.get( elements[ i ], type + "queueHooks" );
			if ( tmp && tmp.empty ) {
				count++;
				tmp.empty.add( resolve );
			}
		}
		resolve();
		return defer.promise( obj );
	}
});
var pnum = (/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/).source;

var cssExpand = [ "Top", "Right", "Bottom", "Left" ];

var isHidden = function( elem, el ) {
		// isHidden might be called from jQuery#filter function;
		// in that case, element will be second argument
		elem = el || elem;
		return jQuery.css( elem, "display" ) === "none" || !jQuery.contains( elem.ownerDocument, elem );
	};

var rcheckableType = (/^(?:checkbox|radio)$/i);



(function() {
	var fragment = document.createDocumentFragment(),
		div = fragment.appendChild( document.createElement( "div" ) ),
		input = document.createElement( "input" );

	// Support: Safari<=5.1
	// Check state lost if the name is set (#11217)
	// Support: Windows Web Apps (WWA)
	// `name` and `type` must use .setAttribute for WWA (#14901)
	input.setAttribute( "type", "radio" );
	input.setAttribute( "checked", "checked" );
	input.setAttribute( "name", "t" );

	div.appendChild( input );

	// Support: Safari<=5.1, Android<4.2
	// Older WebKit doesn't clone checked state correctly in fragments
	support.checkClone = div.cloneNode( true ).cloneNode( true ).lastChild.checked;

	// Support: IE<=11+
	// Make sure textarea (and checkbox) defaultValue is properly cloned
	div.innerHTML = "<textarea>x</textarea>";
	support.noCloneChecked = !!div.cloneNode( true ).lastChild.defaultValue;
})();
var strundefined = typeof undefined;



support.focusinBubbles = "onfocusin" in window;


var
	rkeyEvent = /^key/,
	rmouseEvent = /^(?:mouse|pointer|contextmenu)|click/,
	rfocusMorph = /^(?:focusinfocus|focusoutblur)$/,
	rtypenamespace = /^([^.]*)(?:\.(.+)|)$/;

function returnTrue() {
	return true;
}

function returnFalse() {
	return false;
}

function safeActiveElement() {
	try {
		return document.activeElement;
	} catch ( err ) { }
}

/*
 * Helper functions for managing events -- not part of the public interface.
 * Props to Dean Edwards' addEvent library for many of the ideas.
 */
jQuery.event = {

	global: {},

	add: function( elem, types, handler, data, selector ) {

		var handleObjIn, eventHandle, tmp,
			events, t, handleObj,
			special, handlers, type, namespaces, origType,
			elemData = data_priv.get( elem );

		// Don't attach events to noData or text/comment nodes (but allow plain objects)
		if ( !elemData ) {
			return;
		}

		// Caller can pass in an object of custom data in lieu of the handler
		if ( handler.handler ) {
			handleObjIn = handler;
			handler = handleObjIn.handler;
			selector = handleObjIn.selector;
		}

		// Make sure that the handler has a unique ID, used to find/remove it later
		if ( !handler.guid ) {
			handler.guid = jQuery.guid++;
		}

		// Init the element's event structure and main handler, if this is the first
		if ( !(events = elemData.events) ) {
			events = elemData.events = {};
		}
		if ( !(eventHandle = elemData.handle) ) {
			eventHandle = elemData.handle = function( e ) {
				// Discard the second event of a jQuery.event.trigger() and
				// when an event is called after a page has unloaded
				return typeof jQuery !== strundefined && jQuery.event.triggered !== e.type ?
					jQuery.event.dispatch.apply( elem, arguments ) : undefined;
			};
		}

		// Handle multiple events separated by a space
		types = ( types || "" ).match( rnotwhite ) || [ "" ];
		t = types.length;
		while ( t-- ) {
			tmp = rtypenamespace.exec( types[t] ) || [];
			type = origType = tmp[1];
			namespaces = ( tmp[2] || "" ).split( "." ).sort();

			// There *must* be a type, no attaching namespace-only handlers
			if ( !type ) {
				continue;
			}

			// If event changes its type, use the special event handlers for the changed type
			special = jQuery.event.special[ type ] || {};

			// If selector defined, determine special event api type, otherwise given type
			type = ( selector ? special.delegateType : special.bindType ) || type;

			// Update special based on newly reset type
			special = jQuery.event.special[ type ] || {};

			// handleObj is passed to all event handlers
			handleObj = jQuery.extend({
				type: type,
				origType: origType,
				data: data,
				handler: handler,
				guid: handler.guid,
				selector: selector,
				needsContext: selector && jQuery.expr.match.needsContext.test( selector ),
				namespace: namespaces.join(".")
			}, handleObjIn );

			// Init the event handler queue if we're the first
			if ( !(handlers = events[ type ]) ) {
				handlers = events[ type ] = [];
				handlers.delegateCount = 0;

				// Only use addEventListener if the special events handler returns false
				if ( !special.setup || special.setup.call( elem, data, namespaces, eventHandle ) === false ) {
					if ( elem.addEventListener ) {
						elem.addEventListener( type, eventHandle, false );
					}
				}
			}

			if ( special.add ) {
				special.add.call( elem, handleObj );

				if ( !handleObj.handler.guid ) {
					handleObj.handler.guid = handler.guid;
				}
			}

			// Add to the element's handler list, delegates in front
			if ( selector ) {
				handlers.splice( handlers.delegateCount++, 0, handleObj );
			} else {
				handlers.push( handleObj );
			}

			// Keep track of which events have ever been used, for event optimization
			jQuery.event.global[ type ] = true;
		}

	},

	// Detach an event or set of events from an element
	remove: function( elem, types, handler, selector, mappedTypes ) {

		var j, origCount, tmp,
			events, t, handleObj,
			special, handlers, type, namespaces, origType,
			elemData = data_priv.hasData( elem ) && data_priv.get( elem );

		if ( !elemData || !(events = elemData.events) ) {
			return;
		}

		// Once for each type.namespace in types; type may be omitted
		types = ( types || "" ).match( rnotwhite ) || [ "" ];
		t = types.length;
		while ( t-- ) {
			tmp = rtypenamespace.exec( types[t] ) || [];
			type = origType = tmp[1];
			namespaces = ( tmp[2] || "" ).split( "." ).sort();

			// Unbind all events (on this namespace, if provided) for the element
			if ( !type ) {
				for ( type in events ) {
					jQuery.event.remove( elem, type + types[ t ], handler, selector, true );
				}
				continue;
			}

			special = jQuery.event.special[ type ] || {};
			type = ( selector ? special.delegateType : special.bindType ) || type;
			handlers = events[ type ] || [];
			tmp = tmp[2] && new RegExp( "(^|\\.)" + namespaces.join("\\.(?:.*\\.|)") + "(\\.|$)" );

			// Remove matching events
			origCount = j = handlers.length;
			while ( j-- ) {
				handleObj = handlers[ j ];

				if ( ( mappedTypes || origType === handleObj.origType ) &&
					( !handler || handler.guid === handleObj.guid ) &&
					( !tmp || tmp.test( handleObj.namespace ) ) &&
					( !selector || selector === handleObj.selector || selector === "**" && handleObj.selector ) ) {
					handlers.splice( j, 1 );

					if ( handleObj.selector ) {
						handlers.delegateCount--;
					}
					if ( special.remove ) {
						special.remove.call( elem, handleObj );
					}
				}
			}

			// Remove generic event handler if we removed something and no more handlers exist
			// (avoids potential for endless recursion during removal of special event handlers)
			if ( origCount && !handlers.length ) {
				if ( !special.teardown || special.teardown.call( elem, namespaces, elemData.handle ) === false ) {
					jQuery.removeEvent( elem, type, elemData.handle );
				}

				delete events[ type ];
			}
		}

		// Remove the expando if it's no longer used
		if ( jQuery.isEmptyObject( events ) ) {
			delete elemData.handle;
			data_priv.remove( elem, "events" );
		}
	},

	trigger: function( event, data, elem, onlyHandlers ) {

		var i, cur, tmp, bubbleType, ontype, handle, special,
			eventPath = [ elem || document ],
			type = hasOwn.call( event, "type" ) ? event.type : event,
			namespaces = hasOwn.call( event, "namespace" ) ? event.namespace.split(".") : [];

		cur = tmp = elem = elem || document;

		// Don't do events on text and comment nodes
		if ( elem.nodeType === 3 || elem.nodeType === 8 ) {
			return;
		}

		// focus/blur morphs to focusin/out; ensure we're not firing them right now
		if ( rfocusMorph.test( type + jQuery.event.triggered ) ) {
			return;
		}

		if ( type.indexOf(".") >= 0 ) {
			// Namespaced trigger; create a regexp to match event type in handle()
			namespaces = type.split(".");
			type = namespaces.shift();
			namespaces.sort();
		}
		ontype = type.indexOf(":") < 0 && "on" + type;

		// Caller can pass in a jQuery.Event object, Object, or just an event type string
		event = event[ jQuery.expando ] ?
			event :
			new jQuery.Event( type, typeof event === "object" && event );

		// Trigger bitmask: & 1 for native handlers; & 2 for jQuery (always true)
		event.isTrigger = onlyHandlers ? 2 : 3;
		event.namespace = namespaces.join(".");
		event.namespace_re = event.namespace ?
			new RegExp( "(^|\\.)" + namespaces.join("\\.(?:.*\\.|)") + "(\\.|$)" ) :
			null;

		// Clean up the event in case it is being reused
		event.result = undefined;
		if ( !event.target ) {
			event.target = elem;
		}

		// Clone any incoming data and prepend the event, creating the handler arg list
		data = data == null ?
			[ event ] :
			jQuery.makeArray( data, [ event ] );

		// Allow special events to draw outside the lines
		special = jQuery.event.special[ type ] || {};
		if ( !onlyHandlers && special.trigger && special.trigger.apply( elem, data ) === false ) {
			return;
		}

		// Determine event propagation path in advance, per W3C events spec (#9951)
		// Bubble up to document, then to window; watch for a global ownerDocument var (#9724)
		if ( !onlyHandlers && !special.noBubble && !jQuery.isWindow( elem ) ) {

			bubbleType = special.delegateType || type;
			if ( !rfocusMorph.test( bubbleType + type ) ) {
				cur = cur.parentNode;
			}
			for ( ; cur; cur = cur.parentNode ) {
				eventPath.push( cur );
				tmp = cur;
			}

			// Only add window if we got to document (e.g., not plain obj or detached DOM)
			if ( tmp === (elem.ownerDocument || document) ) {
				eventPath.push( tmp.defaultView || tmp.parentWindow || window );
			}
		}

		// Fire handlers on the event path
		i = 0;
		while ( (cur = eventPath[i++]) && !event.isPropagationStopped() ) {

			event.type = i > 1 ?
				bubbleType :
				special.bindType || type;

			// jQuery handler
			handle = ( data_priv.get( cur, "events" ) || {} )[ event.type ] && data_priv.get( cur, "handle" );
			if ( handle ) {
				handle.apply( cur, data );
			}

			// Native handler
			handle = ontype && cur[ ontype ];
			if ( handle && handle.apply && jQuery.acceptData( cur ) ) {
				event.result = handle.apply( cur, data );
				if ( event.result === false ) {
					event.preventDefault();
				}
			}
		}
		event.type = type;

		// If nobody prevented the default action, do it now
		if ( !onlyHandlers && !event.isDefaultPrevented() ) {

			if ( (!special._default || special._default.apply( eventPath.pop(), data ) === false) &&
				jQuery.acceptData( elem ) ) {

				// Call a native DOM method on the target with the same name name as the event.
				// Don't do default actions on window, that's where global variables be (#6170)
				if ( ontype && jQuery.isFunction( elem[ type ] ) && !jQuery.isWindow( elem ) ) {

					// Don't re-trigger an onFOO event when we call its FOO() method
					tmp = elem[ ontype ];

					if ( tmp ) {
						elem[ ontype ] = null;
					}

					// Prevent re-triggering of the same event, since we already bubbled it above
					jQuery.event.triggered = type;
					elem[ type ]();
					jQuery.event.triggered = undefined;

					if ( tmp ) {
						elem[ ontype ] = tmp;
					}
				}
			}
		}

		return event.result;
	},

	dispatch: function( event ) {

		// Make a writable jQuery.Event from the native event object
		event = jQuery.event.fix( event );

		var i, j, ret, matched, handleObj,
			handlerQueue = [],
			args = slice.call( arguments ),
			handlers = ( data_priv.get( this, "events" ) || {} )[ event.type ] || [],
			special = jQuery.event.special[ event.type ] || {};

		// Use the fix-ed jQuery.Event rather than the (read-only) native event
		args[0] = event;
		event.delegateTarget = this;

		// Call the preDispatch hook for the mapped type, and let it bail if desired
		if ( special.preDispatch && special.preDispatch.call( this, event ) === false ) {
			return;
		}

		// Determine handlers
		handlerQueue = jQuery.event.handlers.call( this, event, handlers );

		// Run delegates first; they may want to stop propagation beneath us
		i = 0;
		while ( (matched = handlerQueue[ i++ ]) && !event.isPropagationStopped() ) {
			event.currentTarget = matched.elem;

			j = 0;
			while ( (handleObj = matched.handlers[ j++ ]) && !event.isImmediatePropagationStopped() ) {

				// Triggered event must either 1) have no namespace, or 2) have namespace(s)
				// a subset or equal to those in the bound event (both can have no namespace).
				if ( !event.namespace_re || event.namespace_re.test( handleObj.namespace ) ) {

					event.handleObj = handleObj;
					event.data = handleObj.data;

					ret = ( (jQuery.event.special[ handleObj.origType ] || {}).handle || handleObj.handler )
							.apply( matched.elem, args );

					if ( ret !== undefined ) {
						if ( (event.result = ret) === false ) {
							event.preventDefault();
							event.stopPropagation();
						}
					}
				}
			}
		}

		// Call the postDispatch hook for the mapped type
		if ( special.postDispatch ) {
			special.postDispatch.call( this, event );
		}

		return event.result;
	},

	handlers: function( event, handlers ) {
		var i, matches, sel, handleObj,
			handlerQueue = [],
			delegateCount = handlers.delegateCount,
			cur = event.target;

		// Find delegate handlers
		// Black-hole SVG <use> instance trees (#13180)
		// Avoid non-left-click bubbling in Firefox (#3861)
		if ( delegateCount && cur.nodeType && (!event.button || event.type !== "click") ) {

			for ( ; cur !== this; cur = cur.parentNode || this ) {

				// Don't process clicks on disabled elements (#6911, #8165, #11382, #11764)
				if ( cur.disabled !== true || event.type !== "click" ) {
					matches = [];
					for ( i = 0; i < delegateCount; i++ ) {
						handleObj = handlers[ i ];

						// Don't conflict with Object.prototype properties (#13203)
						sel = handleObj.selector + " ";

						if ( matches[ sel ] === undefined ) {
							matches[ sel ] = handleObj.needsContext ?
								jQuery( sel, this ).index( cur ) >= 0 :
								jQuery.find( sel, this, null, [ cur ] ).length;
						}
						if ( matches[ sel ] ) {
							matches.push( handleObj );
						}
					}
					if ( matches.length ) {
						handlerQueue.push({ elem: cur, handlers: matches });
					}
				}
			}
		}

		// Add the remaining (directly-bound) handlers
		if ( delegateCount < handlers.length ) {
			handlerQueue.push({ elem: this, handlers: handlers.slice( delegateCount ) });
		}

		return handlerQueue;
	},

	// Includes some event props shared by KeyEvent and MouseEvent
	props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),

	fixHooks: {},

	keyHooks: {
		props: "char charCode key keyCode".split(" "),
		filter: function( event, original ) {

			// Add which for key events
			if ( event.which == null ) {
				event.which = original.charCode != null ? original.charCode : original.keyCode;
			}

			return event;
		}
	},

	mouseHooks: {
		props: "button buttons clientX clientY offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
		filter: function( event, original ) {
			var eventDoc, doc, body,
				button = original.button;

			// Calculate pageX/Y if missing and clientX/Y available
			if ( event.pageX == null && original.clientX != null ) {
				eventDoc = event.target.ownerDocument || document;
				doc = eventDoc.documentElement;
				body = eventDoc.body;

				event.pageX = original.clientX + ( doc && doc.scrollLeft || body && body.scrollLeft || 0 ) - ( doc && doc.clientLeft || body && body.clientLeft || 0 );
				event.pageY = original.clientY + ( doc && doc.scrollTop  || body && body.scrollTop  || 0 ) - ( doc && doc.clientTop  || body && body.clientTop  || 0 );
			}

			// Add which for click: 1 === left; 2 === middle; 3 === right
			// Note: button is not normalized, so don't use it
			if ( !event.which && button !== undefined ) {
				event.which = ( button & 1 ? 1 : ( button & 2 ? 3 : ( button & 4 ? 2 : 0 ) ) );
			}

			return event;
		}
	},

	fix: function( event ) {
		if ( event[ jQuery.expando ] ) {
			return event;
		}

		// Create a writable copy of the event object and normalize some properties
		var i, prop, copy,
			type = event.type,
			originalEvent = event,
			fixHook = this.fixHooks[ type ];

		if ( !fixHook ) {
			this.fixHooks[ type ] = fixHook =
				rmouseEvent.test( type ) ? this.mouseHooks :
				rkeyEvent.test( type ) ? this.keyHooks :
				{};
		}
		copy = fixHook.props ? this.props.concat( fixHook.props ) : this.props;

		event = new jQuery.Event( originalEvent );

		i = copy.length;
		while ( i-- ) {
			prop = copy[ i ];
			event[ prop ] = originalEvent[ prop ];
		}

		// Support: Cordova 2.5 (WebKit) (#13255)
		// All events should have a target; Cordova deviceready doesn't
		if ( !event.target ) {
			event.target = document;
		}

		// Support: Safari 6.0+, Chrome<28
		// Target should not be a text node (#504, #13143)
		if ( event.target.nodeType === 3 ) {
			event.target = event.target.parentNode;
		}

		return fixHook.filter ? fixHook.filter( event, originalEvent ) : event;
	},

	special: {
		load: {
			// Prevent triggered image.load events from bubbling to window.load
			noBubble: true
		},
		focus: {
			// Fire native event if possible so blur/focus sequence is correct
			trigger: function() {
				if ( this !== safeActiveElement() && this.focus ) {
					this.focus();
					return false;
				}
			},
			delegateType: "focusin"
		},
		blur: {
			trigger: function() {
				if ( this === safeActiveElement() && this.blur ) {
					this.blur();
					return false;
				}
			},
			delegateType: "focusout"
		},
		click: {
			// For checkbox, fire native event so checked state will be right
			trigger: function() {
				if ( this.type === "checkbox" && this.click && jQuery.nodeName( this, "input" ) ) {
					this.click();
					return false;
				}
			},

			// For cross-browser consistency, don't fire native .click() on links
			_default: function( event ) {
				return jQuery.nodeName( event.target, "a" );
			}
		},

		beforeunload: {
			postDispatch: function( event ) {

				// Support: Firefox 20+
				// Firefox doesn't alert if the returnValue field is not set.
				if ( event.result !== undefined && event.originalEvent ) {
					event.originalEvent.returnValue = event.result;
				}
			}
		}
	},

	simulate: function( type, elem, event, bubble ) {
		// Piggyback on a donor event to simulate a different one.
		// Fake originalEvent to avoid donor's stopPropagation, but if the
		// simulated event prevents default then we do the same on the donor.
		var e = jQuery.extend(
			new jQuery.Event(),
			event,
			{
				type: type,
				isSimulated: true,
				originalEvent: {}
			}
		);
		if ( bubble ) {
			jQuery.event.trigger( e, null, elem );
		} else {
			jQuery.event.dispatch.call( elem, e );
		}
		if ( e.isDefaultPrevented() ) {
			event.preventDefault();
		}
	}
};

jQuery.removeEvent = function( elem, type, handle ) {
	if ( elem.removeEventListener ) {
		elem.removeEventListener( type, handle, false );
	}
};

jQuery.Event = function( src, props ) {
	// Allow instantiation without the 'new' keyword
	if ( !(this instanceof jQuery.Event) ) {
		return new jQuery.Event( src, props );
	}

	// Event object
	if ( src && src.type ) {
		this.originalEvent = src;
		this.type = src.type;

		// Events bubbling up the document may have been marked as prevented
		// by a handler lower down the tree; reflect the correct value.
		this.isDefaultPrevented = src.defaultPrevented ||
				src.defaultPrevented === undefined &&
				// Support: Android<4.0
				src.returnValue === false ?
			returnTrue :
			returnFalse;

	// Event type
	} else {
		this.type = src;
	}

	// Put explicitly provided properties onto the event object
	if ( props ) {
		jQuery.extend( this, props );
	}

	// Create a timestamp if incoming event doesn't have one
	this.timeStamp = src && src.timeStamp || jQuery.now();

	// Mark it as fixed
	this[ jQuery.expando ] = true;
};

// jQuery.Event is based on DOM3 Events as specified by the ECMAScript Language Binding
// http://www.w3.org/TR/2003/WD-DOM-Level-3-Events-20030331/ecma-script-binding.html
jQuery.Event.prototype = {
	isDefaultPrevented: returnFalse,
	isPropagationStopped: returnFalse,
	isImmediatePropagationStopped: returnFalse,

	preventDefault: function() {
		var e = this.originalEvent;

		this.isDefaultPrevented = returnTrue;

		if ( e && e.preventDefault ) {
			e.preventDefault();
		}
	},
	stopPropagation: function() {
		var e = this.originalEvent;

		this.isPropagationStopped = returnTrue;

		if ( e && e.stopPropagation ) {
			e.stopPropagation();
		}
	},
	stopImmediatePropagation: function() {
		var e = this.originalEvent;

		this.isImmediatePropagationStopped = returnTrue;

		if ( e && e.stopImmediatePropagation ) {
			e.stopImmediatePropagation();
		}

		this.stopPropagation();
	}
};

// Create mouseenter/leave events using mouseover/out and event-time checks
// Support: Chrome 15+
jQuery.each({
	mouseenter: "mouseover",
	mouseleave: "mouseout",
	pointerenter: "pointerover",
	pointerleave: "pointerout"
}, function( orig, fix ) {
	jQuery.event.special[ orig ] = {
		delegateType: fix,
		bindType: fix,

		handle: function( event ) {
			var ret,
				target = this,
				related = event.relatedTarget,
				handleObj = event.handleObj;

			// For mousenter/leave call the handler if related is outside the target.
			// NB: No relatedTarget if the mouse left/entered the browser window
			if ( !related || (related !== target && !jQuery.contains( target, related )) ) {
				event.type = handleObj.origType;
				ret = handleObj.handler.apply( this, arguments );
				event.type = fix;
			}
			return ret;
		}
	};
});

// Support: Firefox, Chrome, Safari
// Create "bubbling" focus and blur events
if ( !support.focusinBubbles ) {
	jQuery.each({ focus: "focusin", blur: "focusout" }, function( orig, fix ) {

		// Attach a single capturing handler on the document while someone wants focusin/focusout
		var handler = function( event ) {
				jQuery.event.simulate( fix, event.target, jQuery.event.fix( event ), true );
			};

		jQuery.event.special[ fix ] = {
			setup: function() {
				var doc = this.ownerDocument || this,
					attaches = data_priv.access( doc, fix );

				if ( !attaches ) {
					doc.addEventListener( orig, handler, true );
				}
				data_priv.access( doc, fix, ( attaches || 0 ) + 1 );
			},
			teardown: function() {
				var doc = this.ownerDocument || this,
					attaches = data_priv.access( doc, fix ) - 1;

				if ( !attaches ) {
					doc.removeEventListener( orig, handler, true );
					data_priv.remove( doc, fix );

				} else {
					data_priv.access( doc, fix, attaches );
				}
			}
		};
	});
}

jQuery.fn.extend({

	on: function( types, selector, data, fn, /*INTERNAL*/ one ) {
		var origFn, type;

		// Types can be a map of types/handlers
		if ( typeof types === "object" ) {
			// ( types-Object, selector, data )
			if ( typeof selector !== "string" ) {
				// ( types-Object, data )
				data = data || selector;
				selector = undefined;
			}
			for ( type in types ) {
				this.on( type, selector, data, types[ type ], one );
			}
			return this;
		}

		if ( data == null && fn == null ) {
			// ( types, fn )
			fn = selector;
			data = selector = undefined;
		} else if ( fn == null ) {
			if ( typeof selector === "string" ) {
				// ( types, selector, fn )
				fn = data;
				data = undefined;
			} else {
				// ( types, data, fn )
				fn = data;
				data = selector;
				selector = undefined;
			}
		}
		if ( fn === false ) {
			fn = returnFalse;
		} else if ( !fn ) {
			return this;
		}

		if ( one === 1 ) {
			origFn = fn;
			fn = function( event ) {
				// Can use an empty set, since event contains the info
				jQuery().off( event );
				return origFn.apply( this, arguments );
			};
			// Use same guid so caller can remove using origFn
			fn.guid = origFn.guid || ( origFn.guid = jQuery.guid++ );
		}
		return this.each( function() {
			jQuery.event.add( this, types, fn, data, selector );
		});
	},
	one: function( types, selector, data, fn ) {
		return this.on( types, selector, data, fn, 1 );
	},
	off: function( types, selector, fn ) {
		var handleObj, type;
		if ( types && types.preventDefault && types.handleObj ) {
			// ( event )  dispatched jQuery.Event
			handleObj = types.handleObj;
			jQuery( types.delegateTarget ).off(
				handleObj.namespace ? handleObj.origType + "." + handleObj.namespace : handleObj.origType,
				handleObj.selector,
				handleObj.handler
			);
			return this;
		}
		if ( typeof types === "object" ) {
			// ( types-object [, selector] )
			for ( type in types ) {
				this.off( type, selector, types[ type ] );
			}
			return this;
		}
		if ( selector === false || typeof selector === "function" ) {
			// ( types [, fn] )
			fn = selector;
			selector = undefined;
		}
		if ( fn === false ) {
			fn = returnFalse;
		}
		return this.each(function() {
			jQuery.event.remove( this, types, fn, selector );
		});
	},

	trigger: function( type, data ) {
		return this.each(function() {
			jQuery.event.trigger( type, data, this );
		});
	},
	triggerHandler: function( type, data ) {
		var elem = this[0];
		if ( elem ) {
			return jQuery.event.trigger( type, data, elem, true );
		}
	}
});


var
	rxhtmlTag = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,
	rtagName = /<([\w:]+)/,
	rhtml = /<|&#?\w+;/,
	rnoInnerhtml = /<(?:script|style|link)/i,
	// checked="checked" or checked
	rchecked = /checked\s*(?:[^=]|=\s*.checked.)/i,
	rscriptType = /^$|\/(?:java|ecma)script/i,
	rscriptTypeMasked = /^true\/(.*)/,
	rcleanScript = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,

	// We have to close these tags to support XHTML (#13200)
	wrapMap = {

		// Support: IE9
		option: [ 1, "<select multiple='multiple'>", "</select>" ],

		thead: [ 1, "<table>", "</table>" ],
		col: [ 2, "<table><colgroup>", "</colgroup></table>" ],
		tr: [ 2, "<table><tbody>", "</tbody></table>" ],
		td: [ 3, "<table><tbody><tr>", "</tr></tbody></table>" ],

		_default: [ 0, "", "" ]
	};

// Support: IE9
wrapMap.optgroup = wrapMap.option;

wrapMap.tbody = wrapMap.tfoot = wrapMap.colgroup = wrapMap.caption = wrapMap.thead;
wrapMap.th = wrapMap.td;

// Support: 1.x compatibility
// Manipulating tables requires a tbody
function manipulationTarget( elem, content ) {
	return jQuery.nodeName( elem, "table" ) &&
		jQuery.nodeName( content.nodeType !== 11 ? content : content.firstChild, "tr" ) ?

		elem.getElementsByTagName("tbody")[0] ||
			elem.appendChild( elem.ownerDocument.createElement("tbody") ) :
		elem;
}

// Replace/restore the type attribute of script elements for safe DOM manipulation
function disableScript( elem ) {
	elem.type = (elem.getAttribute("type") !== null) + "/" + elem.type;
	return elem;
}
function restoreScript( elem ) {
	var match = rscriptTypeMasked.exec( elem.type );

	if ( match ) {
		elem.type = match[ 1 ];
	} else {
		elem.removeAttribute("type");
	}

	return elem;
}

// Mark scripts as having already been evaluated
function setGlobalEval( elems, refElements ) {
	var i = 0,
		l = elems.length;

	for ( ; i < l; i++ ) {
		data_priv.set(
			elems[ i ], "globalEval", !refElements || data_priv.get( refElements[ i ], "globalEval" )
		);
	}
}

function cloneCopyEvent( src, dest ) {
	var i, l, type, pdataOld, pdataCur, udataOld, udataCur, events;

	if ( dest.nodeType !== 1 ) {
		return;
	}

	// 1. Copy private data: events, handlers, etc.
	if ( data_priv.hasData( src ) ) {
		pdataOld = data_priv.access( src );
		pdataCur = data_priv.set( dest, pdataOld );
		events = pdataOld.events;

		if ( events ) {
			delete pdataCur.handle;
			pdataCur.events = {};

			for ( type in events ) {
				for ( i = 0, l = events[ type ].length; i < l; i++ ) {
					jQuery.event.add( dest, type, events[ type ][ i ] );
				}
			}
		}
	}

	// 2. Copy user data
	if ( data_user.hasData( src ) ) {
		udataOld = data_user.access( src );
		udataCur = jQuery.extend( {}, udataOld );

		data_user.set( dest, udataCur );
	}
}

function getAll( context, tag ) {
	var ret = context.getElementsByTagName ? context.getElementsByTagName( tag || "*" ) :
			context.querySelectorAll ? context.querySelectorAll( tag || "*" ) :
			[];

	return tag === undefined || tag && jQuery.nodeName( context, tag ) ?
		jQuery.merge( [ context ], ret ) :
		ret;
}

// Fix IE bugs, see support tests
function fixInput( src, dest ) {
	var nodeName = dest.nodeName.toLowerCase();

	// Fails to persist the checked state of a cloned checkbox or radio button.
	if ( nodeName === "input" && rcheckableType.test( src.type ) ) {
		dest.checked = src.checked;

	// Fails to return the selected option to the default selected state when cloning options
	} else if ( nodeName === "input" || nodeName === "textarea" ) {
		dest.defaultValue = src.defaultValue;
	}
}

jQuery.extend({
	clone: function( elem, dataAndEvents, deepDataAndEvents ) {
		var i, l, srcElements, destElements,
			clone = elem.cloneNode( true ),
			inPage = jQuery.contains( elem.ownerDocument, elem );

		// Fix IE cloning issues
		if ( !support.noCloneChecked && ( elem.nodeType === 1 || elem.nodeType === 11 ) &&
				!jQuery.isXMLDoc( elem ) ) {

			// We eschew Sizzle here for performance reasons: http://jsperf.com/getall-vs-sizzle/2
			destElements = getAll( clone );
			srcElements = getAll( elem );

			for ( i = 0, l = srcElements.length; i < l; i++ ) {
				fixInput( srcElements[ i ], destElements[ i ] );
			}
		}

		// Copy the events from the original to the clone
		if ( dataAndEvents ) {
			if ( deepDataAndEvents ) {
				srcElements = srcElements || getAll( elem );
				destElements = destElements || getAll( clone );

				for ( i = 0, l = srcElements.length; i < l; i++ ) {
					cloneCopyEvent( srcElements[ i ], destElements[ i ] );
				}
			} else {
				cloneCopyEvent( elem, clone );
			}
		}

		// Preserve script evaluation history
		destElements = getAll( clone, "script" );
		if ( destElements.length > 0 ) {
			setGlobalEval( destElements, !inPage && getAll( elem, "script" ) );
		}

		// Return the cloned set
		return clone;
	},

	buildFragment: function( elems, context, scripts, selection ) {
		var elem, tmp, tag, wrap, contains, j,
			fragment = context.createDocumentFragment(),
			nodes = [],
			i = 0,
			l = elems.length;

		for ( ; i < l; i++ ) {
			elem = elems[ i ];

			if ( elem || elem === 0 ) {

				// Add nodes directly
				if ( jQuery.type( elem ) === "object" ) {
					// Support: QtWebKit, PhantomJS
					// push.apply(_, arraylike) throws on ancient WebKit
					jQuery.merge( nodes, elem.nodeType ? [ elem ] : elem );

				// Convert non-html into a text node
				} else if ( !rhtml.test( elem ) ) {
					nodes.push( context.createTextNode( elem ) );

				// Convert html into DOM nodes
				} else {
					tmp = tmp || fragment.appendChild( context.createElement("div") );

					// Deserialize a standard representation
					tag = ( rtagName.exec( elem ) || [ "", "" ] )[ 1 ].toLowerCase();
					wrap = wrapMap[ tag ] || wrapMap._default;
					tmp.innerHTML = wrap[ 1 ] + elem.replace( rxhtmlTag, "<$1></$2>" ) + wrap[ 2 ];

					// Descend through wrappers to the right content
					j = wrap[ 0 ];
					while ( j-- ) {
						tmp = tmp.lastChild;
					}

					// Support: QtWebKit, PhantomJS
					// push.apply(_, arraylike) throws on ancient WebKit
					jQuery.merge( nodes, tmp.childNodes );

					// Remember the top-level container
					tmp = fragment.firstChild;

					// Ensure the created nodes are orphaned (#12392)
					tmp.textContent = "";
				}
			}
		}

		// Remove wrapper from fragment
		fragment.textContent = "";

		i = 0;
		while ( (elem = nodes[ i++ ]) ) {

			// #4087 - If origin and destination elements are the same, and this is
			// that element, do not do anything
			if ( selection && jQuery.inArray( elem, selection ) !== -1 ) {
				continue;
			}

			contains = jQuery.contains( elem.ownerDocument, elem );

			// Append to fragment
			tmp = getAll( fragment.appendChild( elem ), "script" );

			// Preserve script evaluation history
			if ( contains ) {
				setGlobalEval( tmp );
			}

			// Capture executables
			if ( scripts ) {
				j = 0;
				while ( (elem = tmp[ j++ ]) ) {
					if ( rscriptType.test( elem.type || "" ) ) {
						scripts.push( elem );
					}
				}
			}
		}

		return fragment;
	},

	cleanData: function( elems ) {
		var data, elem, type, key,
			special = jQuery.event.special,
			i = 0;

		for ( ; (elem = elems[ i ]) !== undefined; i++ ) {
			if ( jQuery.acceptData( elem ) ) {
				key = elem[ data_priv.expando ];

				if ( key && (data = data_priv.cache[ key ]) ) {
					if ( data.events ) {
						for ( type in data.events ) {
							if ( special[ type ] ) {
								jQuery.event.remove( elem, type );

							// This is a shortcut to avoid jQuery.event.remove's overhead
							} else {
								jQuery.removeEvent( elem, type, data.handle );
							}
						}
					}
					if ( data_priv.cache[ key ] ) {
						// Discard any remaining `private` data
						delete data_priv.cache[ key ];
					}
				}
			}
			// Discard any remaining `user` data
			delete data_user.cache[ elem[ data_user.expando ] ];
		}
	}
});

jQuery.fn.extend({
	text: function( value ) {
		return access( this, function( value ) {
			return value === undefined ?
				jQuery.text( this ) :
				this.empty().each(function() {
					if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
						this.textContent = value;
					}
				});
		}, null, value, arguments.length );
	},

	append: function() {
		return this.domManip( arguments, function( elem ) {
			if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
				var target = manipulationTarget( this, elem );
				target.appendChild( elem );
			}
		});
	},

	prepend: function() {
		return this.domManip( arguments, function( elem ) {
			if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
				var target = manipulationTarget( this, elem );
				target.insertBefore( elem, target.firstChild );
			}
		});
	},

	before: function() {
		return this.domManip( arguments, function( elem ) {
			if ( this.parentNode ) {
				this.parentNode.insertBefore( elem, this );
			}
		});
	},

	after: function() {
		return this.domManip( arguments, function( elem ) {
			if ( this.parentNode ) {
				this.parentNode.insertBefore( elem, this.nextSibling );
			}
		});
	},

	remove: function( selector, keepData /* Internal Use Only */ ) {
		var elem,
			elems = selector ? jQuery.filter( selector, this ) : this,
			i = 0;

		for ( ; (elem = elems[i]) != null; i++ ) {
			if ( !keepData && elem.nodeType === 1 ) {
				jQuery.cleanData( getAll( elem ) );
			}

			if ( elem.parentNode ) {
				if ( keepData && jQuery.contains( elem.ownerDocument, elem ) ) {
					setGlobalEval( getAll( elem, "script" ) );
				}
				elem.parentNode.removeChild( elem );
			}
		}

		return this;
	},

	empty: function() {
		var elem,
			i = 0;

		for ( ; (elem = this[i]) != null; i++ ) {
			if ( elem.nodeType === 1 ) {

				// Prevent memory leaks
				jQuery.cleanData( getAll( elem, false ) );

				// Remove any remaining nodes
				elem.textContent = "";
			}
		}

		return this;
	},

	clone: function( dataAndEvents, deepDataAndEvents ) {
		dataAndEvents = dataAndEvents == null ? false : dataAndEvents;
		deepDataAndEvents = deepDataAndEvents == null ? dataAndEvents : deepDataAndEvents;

		return this.map(function() {
			return jQuery.clone( this, dataAndEvents, deepDataAndEvents );
		});
	},

	html: function( value ) {
		return access( this, function( value ) {
			var elem = this[ 0 ] || {},
				i = 0,
				l = this.length;

			if ( value === undefined && elem.nodeType === 1 ) {
				return elem.innerHTML;
			}

			// See if we can take a shortcut and just use innerHTML
			if ( typeof value === "string" && !rnoInnerhtml.test( value ) &&
				!wrapMap[ ( rtagName.exec( value ) || [ "", "" ] )[ 1 ].toLowerCase() ] ) {

				value = value.replace( rxhtmlTag, "<$1></$2>" );

				try {
					for ( ; i < l; i++ ) {
						elem = this[ i ] || {};

						// Remove element nodes and prevent memory leaks
						if ( elem.nodeType === 1 ) {
							jQuery.cleanData( getAll( elem, false ) );
							elem.innerHTML = value;
						}
					}

					elem = 0;

				// If using innerHTML throws an exception, use the fallback method
				} catch( e ) {}
			}

			if ( elem ) {
				this.empty().append( value );
			}
		}, null, value, arguments.length );
	},

	replaceWith: function() {
		var arg = arguments[ 0 ];

		// Make the changes, replacing each context element with the new content
		this.domManip( arguments, function( elem ) {
			arg = this.parentNode;

			jQuery.cleanData( getAll( this ) );

			if ( arg ) {
				arg.replaceChild( elem, this );
			}
		});

		// Force removal if there was no new content (e.g., from empty arguments)
		return arg && (arg.length || arg.nodeType) ? this : this.remove();
	},

	detach: function( selector ) {
		return this.remove( selector, true );
	},

	domManip: function( args, callback ) {

		// Flatten any nested arrays
		args = concat.apply( [], args );

		var fragment, first, scripts, hasScripts, node, doc,
			i = 0,
			l = this.length,
			set = this,
			iNoClone = l - 1,
			value = args[ 0 ],
			isFunction = jQuery.isFunction( value );

		// We can't cloneNode fragments that contain checked, in WebKit
		if ( isFunction ||
				( l > 1 && typeof value === "string" &&
					!support.checkClone && rchecked.test( value ) ) ) {
			return this.each(function( index ) {
				var self = set.eq( index );
				if ( isFunction ) {
					args[ 0 ] = value.call( this, index, self.html() );
				}
				self.domManip( args, callback );
			});
		}

		if ( l ) {
			fragment = jQuery.buildFragment( args, this[ 0 ].ownerDocument, false, this );
			first = fragment.firstChild;

			if ( fragment.childNodes.length === 1 ) {
				fragment = first;
			}

			if ( first ) {
				scripts = jQuery.map( getAll( fragment, "script" ), disableScript );
				hasScripts = scripts.length;

				// Use the original fragment for the last item instead of the first because it can end up
				// being emptied incorrectly in certain situations (#8070).
				for ( ; i < l; i++ ) {
					node = fragment;

					if ( i !== iNoClone ) {
						node = jQuery.clone( node, true, true );

						// Keep references to cloned scripts for later restoration
						if ( hasScripts ) {
							// Support: QtWebKit
							// jQuery.merge because push.apply(_, arraylike) throws
							jQuery.merge( scripts, getAll( node, "script" ) );
						}
					}

					callback.call( this[ i ], node, i );
				}

				if ( hasScripts ) {
					doc = scripts[ scripts.length - 1 ].ownerDocument;

					// Reenable scripts
					jQuery.map( scripts, restoreScript );

					// Evaluate executable scripts on first document insertion
					for ( i = 0; i < hasScripts; i++ ) {
						node = scripts[ i ];
						if ( rscriptType.test( node.type || "" ) &&
							!data_priv.access( node, "globalEval" ) && jQuery.contains( doc, node ) ) {

							if ( node.src ) {
								// Optional AJAX dependency, but won't run scripts if not present
								if ( jQuery._evalUrl ) {
									jQuery._evalUrl( node.src );
								}
							} else {
								jQuery.globalEval( node.textContent.replace( rcleanScript, "" ) );
							}
						}
					}
				}
			}
		}

		return this;
	}
});

jQuery.each({
	appendTo: "append",
	prependTo: "prepend",
	insertBefore: "before",
	insertAfter: "after",
	replaceAll: "replaceWith"
}, function( name, original ) {
	jQuery.fn[ name ] = function( selector ) {
		var elems,
			ret = [],
			insert = jQuery( selector ),
			last = insert.length - 1,
			i = 0;

		for ( ; i <= last; i++ ) {
			elems = i === last ? this : this.clone( true );
			jQuery( insert[ i ] )[ original ]( elems );

			// Support: QtWebKit
			// .get() because push.apply(_, arraylike) throws
			push.apply( ret, elems.get() );
		}

		return this.pushStack( ret );
	};
});


var iframe,
	elemdisplay = {};

/**
 * Retrieve the actual display of a element
 * @param {String} name nodeName of the element
 * @param {Object} doc Document object
 */
// Called only from within defaultDisplay
function actualDisplay( name, doc ) {
	var style,
		elem = jQuery( doc.createElement( name ) ).appendTo( doc.body ),

		// getDefaultComputedStyle might be reliably used only on attached element
		display = window.getDefaultComputedStyle && ( style = window.getDefaultComputedStyle( elem[ 0 ] ) ) ?

			// Use of this method is a temporary fix (more like optimization) until something better comes along,
			// since it was removed from specification and supported only in FF
			style.display : jQuery.css( elem[ 0 ], "display" );

	// We don't have any data stored on the element,
	// so use "detach" method as fast way to get rid of the element
	elem.detach();

	return display;
}

/**
 * Try to determine the default display value of an element
 * @param {String} nodeName
 */
function defaultDisplay( nodeName ) {
	var doc = document,
		display = elemdisplay[ nodeName ];

	if ( !display ) {
		display = actualDisplay( nodeName, doc );

		// If the simple way fails, read from inside an iframe
		if ( display === "none" || !display ) {

			// Use the already-created iframe if possible
			iframe = (iframe || jQuery( "<iframe frameborder='0' width='0' height='0'/>" )).appendTo( doc.documentElement );

			// Always write a new HTML skeleton so Webkit and Firefox don't choke on reuse
			doc = iframe[ 0 ].contentDocument;

			// Support: IE
			doc.write();
			doc.close();

			display = actualDisplay( nodeName, doc );
			iframe.detach();
		}

		// Store the correct default display
		elemdisplay[ nodeName ] = display;
	}

	return display;
}
var rmargin = (/^margin/);

var rnumnonpx = new RegExp( "^(" + pnum + ")(?!px)[a-z%]+$", "i" );

var getStyles = function( elem ) {
		// Support: IE<=11+, Firefox<=30+ (#15098, #14150)
		// IE throws on elements created in popups
		// FF meanwhile throws on frame elements through "defaultView.getComputedStyle"
		if ( elem.ownerDocument.defaultView.opener ) {
			return elem.ownerDocument.defaultView.getComputedStyle( elem, null );
		}

		return window.getComputedStyle( elem, null );
	};



function curCSS( elem, name, computed ) {
	var width, minWidth, maxWidth, ret,
		style = elem.style;

	computed = computed || getStyles( elem );

	// Support: IE9
	// getPropertyValue is only needed for .css('filter') (#12537)
	if ( computed ) {
		ret = computed.getPropertyValue( name ) || computed[ name ];
	}

	if ( computed ) {

		if ( ret === "" && !jQuery.contains( elem.ownerDocument, elem ) ) {
			ret = jQuery.style( elem, name );
		}

		// Support: iOS < 6
		// A tribute to the "awesome hack by Dean Edwards"
		// iOS < 6 (at least) returns percentage for a larger set of values, but width seems to be reliably pixels
		// this is against the CSSOM draft spec: http://dev.w3.org/csswg/cssom/#resolved-values
		if ( rnumnonpx.test( ret ) && rmargin.test( name ) ) {

			// Remember the original values
			width = style.width;
			minWidth = style.minWidth;
			maxWidth = style.maxWidth;

			// Put in the new values to get a computed value out
			style.minWidth = style.maxWidth = style.width = ret;
			ret = computed.width;

			// Revert the changed values
			style.width = width;
			style.minWidth = minWidth;
			style.maxWidth = maxWidth;
		}
	}

	return ret !== undefined ?
		// Support: IE
		// IE returns zIndex value as an integer.
		ret + "" :
		ret;
}


function addGetHookIf( conditionFn, hookFn ) {
	// Define the hook, we'll check on the first run if it's really needed.
	return {
		get: function() {
			if ( conditionFn() ) {
				// Hook not needed (or it's not possible to use it due
				// to missing dependency), remove it.
				delete this.get;
				return;
			}

			// Hook needed; redefine it so that the support test is not executed again.
			return (this.get = hookFn).apply( this, arguments );
		}
	};
}


(function() {
	var pixelPositionVal, boxSizingReliableVal,
		docElem = document.documentElement,
		container = document.createElement( "div" ),
		div = document.createElement( "div" );

	if ( !div.style ) {
		return;
	}

	// Support: IE9-11+
	// Style of cloned element affects source element cloned (#8908)
	div.style.backgroundClip = "content-box";
	div.cloneNode( true ).style.backgroundClip = "";
	support.clearCloneStyle = div.style.backgroundClip === "content-box";

	container.style.cssText = "border:0;width:0;height:0;top:0;left:-9999px;margin-top:1px;" +
		"position:absolute";
	container.appendChild( div );

	// Executing both pixelPosition & boxSizingReliable tests require only one layout
	// so they're executed at the same time to save the second computation.
	function computePixelPositionAndBoxSizingReliable() {
		div.style.cssText =
			// Support: Firefox<29, Android 2.3
			// Vendor-prefix box-sizing
			"-webkit-box-sizing:border-box;-moz-box-sizing:border-box;" +
			"box-sizing:border-box;display:block;margin-top:1%;top:1%;" +
			"border:1px;padding:1px;width:4px;position:absolute";
		div.innerHTML = "";
		docElem.appendChild( container );

		var divStyle = window.getComputedStyle( div, null );
		pixelPositionVal = divStyle.top !== "1%";
		boxSizingReliableVal = divStyle.width === "4px";

		docElem.removeChild( container );
	}

	// Support: node.js jsdom
	// Don't assume that getComputedStyle is a property of the global object
	if ( window.getComputedStyle ) {
		jQuery.extend( support, {
			pixelPosition: function() {

				// This test is executed only once but we still do memoizing
				// since we can use the boxSizingReliable pre-computing.
				// No need to check if the test was already performed, though.
				computePixelPositionAndBoxSizingReliable();
				return pixelPositionVal;
			},
			boxSizingReliable: function() {
				if ( boxSizingReliableVal == null ) {
					computePixelPositionAndBoxSizingReliable();
				}
				return boxSizingReliableVal;
			},
			reliableMarginRight: function() {

				// Support: Android 2.3
				// Check if div with explicit width and no margin-right incorrectly
				// gets computed margin-right based on width of container. (#3333)
				// WebKit Bug 13343 - getComputedStyle returns wrong value for margin-right
				// This support function is only executed once so no memoizing is needed.
				var ret,
					marginDiv = div.appendChild( document.createElement( "div" ) );

				// Reset CSS: box-sizing; display; margin; border; padding
				marginDiv.style.cssText = div.style.cssText =
					// Support: Firefox<29, Android 2.3
					// Vendor-prefix box-sizing
					"-webkit-box-sizing:content-box;-moz-box-sizing:content-box;" +
					"box-sizing:content-box;display:block;margin:0;border:0;padding:0";
				marginDiv.style.marginRight = marginDiv.style.width = "0";
				div.style.width = "1px";
				docElem.appendChild( container );

				ret = !parseFloat( window.getComputedStyle( marginDiv, null ).marginRight );

				docElem.removeChild( container );
				div.removeChild( marginDiv );

				return ret;
			}
		});
	}
})();


// A method for quickly swapping in/out CSS properties to get correct calculations.
jQuery.swap = function( elem, options, callback, args ) {
	var ret, name,
		old = {};

	// Remember the old values, and insert the new ones
	for ( name in options ) {
		old[ name ] = elem.style[ name ];
		elem.style[ name ] = options[ name ];
	}

	ret = callback.apply( elem, args || [] );

	// Revert the old values
	for ( name in options ) {
		elem.style[ name ] = old[ name ];
	}

	return ret;
};


var
	// Swappable if display is none or starts with table except "table", "table-cell", or "table-caption"
	// See here for display values: https://developer.mozilla.org/en-US/docs/CSS/display
	rdisplayswap = /^(none|table(?!-c[ea]).+)/,
	rnumsplit = new RegExp( "^(" + pnum + ")(.*)$", "i" ),
	rrelNum = new RegExp( "^([+-])=(" + pnum + ")", "i" ),

	cssShow = { position: "absolute", visibility: "hidden", display: "block" },
	cssNormalTransform = {
		letterSpacing: "0",
		fontWeight: "400"
	},

	cssPrefixes = [ "Webkit", "O", "Moz", "ms" ];

// Return a css property mapped to a potentially vendor prefixed property
function vendorPropName( style, name ) {

	// Shortcut for names that are not vendor prefixed
	if ( name in style ) {
		return name;
	}

	// Check for vendor prefixed names
	var capName = name[0].toUpperCase() + name.slice(1),
		origName = name,
		i = cssPrefixes.length;

	while ( i-- ) {
		name = cssPrefixes[ i ] + capName;
		if ( name in style ) {
			return name;
		}
	}

	return origName;
}

function setPositiveNumber( elem, value, subtract ) {
	var matches = rnumsplit.exec( value );
	return matches ?
		// Guard against undefined "subtract", e.g., when used as in cssHooks
		Math.max( 0, matches[ 1 ] - ( subtract || 0 ) ) + ( matches[ 2 ] || "px" ) :
		value;
}

function augmentWidthOrHeight( elem, name, extra, isBorderBox, styles ) {
	var i = extra === ( isBorderBox ? "border" : "content" ) ?
		// If we already have the right measurement, avoid augmentation
		4 :
		// Otherwise initialize for horizontal or vertical properties
		name === "width" ? 1 : 0,

		val = 0;

	for ( ; i < 4; i += 2 ) {
		// Both box models exclude margin, so add it if we want it
		if ( extra === "margin" ) {
			val += jQuery.css( elem, extra + cssExpand[ i ], true, styles );
		}

		if ( isBorderBox ) {
			// border-box includes padding, so remove it if we want content
			if ( extra === "content" ) {
				val -= jQuery.css( elem, "padding" + cssExpand[ i ], true, styles );
			}

			// At this point, extra isn't border nor margin, so remove border
			if ( extra !== "margin" ) {
				val -= jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );
			}
		} else {
			// At this point, extra isn't content, so add padding
			val += jQuery.css( elem, "padding" + cssExpand[ i ], true, styles );

			// At this point, extra isn't content nor padding, so add border
			if ( extra !== "padding" ) {
				val += jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );
			}
		}
	}

	return val;
}

function getWidthOrHeight( elem, name, extra ) {

	// Start with offset property, which is equivalent to the border-box value
	var valueIsBorderBox = true,
		val = name === "width" ? elem.offsetWidth : elem.offsetHeight,
		styles = getStyles( elem ),
		isBorderBox = jQuery.css( elem, "boxSizing", false, styles ) === "border-box";

	// Some non-html elements return undefined for offsetWidth, so check for null/undefined
	// svg - https://bugzilla.mozilla.org/show_bug.cgi?id=649285
	// MathML - https://bugzilla.mozilla.org/show_bug.cgi?id=491668
	if ( val <= 0 || val == null ) {
		// Fall back to computed then uncomputed css if necessary
		val = curCSS( elem, name, styles );
		if ( val < 0 || val == null ) {
			val = elem.style[ name ];
		}

		// Computed unit is not pixels. Stop here and return.
		if ( rnumnonpx.test(val) ) {
			return val;
		}

		// Check for style in case a browser which returns unreliable values
		// for getComputedStyle silently falls back to the reliable elem.style
		valueIsBorderBox = isBorderBox &&
			( support.boxSizingReliable() || val === elem.style[ name ] );

		// Normalize "", auto, and prepare for extra
		val = parseFloat( val ) || 0;
	}

	// Use the active box-sizing model to add/subtract irrelevant styles
	return ( val +
		augmentWidthOrHeight(
			elem,
			name,
			extra || ( isBorderBox ? "border" : "content" ),
			valueIsBorderBox,
			styles
		)
	) + "px";
}

function showHide( elements, show ) {
	var display, elem, hidden,
		values = [],
		index = 0,
		length = elements.length;

	for ( ; index < length; index++ ) {
		elem = elements[ index ];
		if ( !elem.style ) {
			continue;
		}

		values[ index ] = data_priv.get( elem, "olddisplay" );
		display = elem.style.display;
		if ( show ) {
			// Reset the inline display of this element to learn if it is
			// being hidden by cascaded rules or not
			if ( !values[ index ] && display === "none" ) {
				elem.style.display = "";
			}

			// Set elements which have been overridden with display: none
			// in a stylesheet to whatever the default browser style is
			// for such an element
			if ( elem.style.display === "" && isHidden( elem ) ) {
				values[ index ] = data_priv.access( elem, "olddisplay", defaultDisplay(elem.nodeName) );
			}
		} else {
			hidden = isHidden( elem );

			if ( display !== "none" || !hidden ) {
				data_priv.set( elem, "olddisplay", hidden ? display : jQuery.css( elem, "display" ) );
			}
		}
	}

	// Set the display of most of the elements in a second loop
	// to avoid the constant reflow
	for ( index = 0; index < length; index++ ) {
		elem = elements[ index ];
		if ( !elem.style ) {
			continue;
		}
		if ( !show || elem.style.display === "none" || elem.style.display === "" ) {
			elem.style.display = show ? values[ index ] || "" : "none";
		}
	}

	return elements;
}

jQuery.extend({

	// Add in style property hooks for overriding the default
	// behavior of getting and setting a style property
	cssHooks: {
		opacity: {
			get: function( elem, computed ) {
				if ( computed ) {

					// We should always get a number back from opacity
					var ret = curCSS( elem, "opacity" );
					return ret === "" ? "1" : ret;
				}
			}
		}
	},

	// Don't automatically add "px" to these possibly-unitless properties
	cssNumber: {
		"columnCount": true,
		"fillOpacity": true,
		"flexGrow": true,
		"flexShrink": true,
		"fontWeight": true,
		"lineHeight": true,
		"opacity": true,
		"order": true,
		"orphans": true,
		"widows": true,
		"zIndex": true,
		"zoom": true
	},

	// Add in properties whose names you wish to fix before
	// setting or getting the value
	cssProps: {
		"float": "cssFloat"
	},

	// Get and set the style property on a DOM Node
	style: function( elem, name, value, extra ) {

		// Don't set styles on text and comment nodes
		if ( !elem || elem.nodeType === 3 || elem.nodeType === 8 || !elem.style ) {
			return;
		}

		// Make sure that we're working with the right name
		var ret, type, hooks,
			origName = jQuery.camelCase( name ),
			style = elem.style;

		name = jQuery.cssProps[ origName ] || ( jQuery.cssProps[ origName ] = vendorPropName( style, origName ) );

		// Gets hook for the prefixed version, then unprefixed version
		hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];

		// Check if we're setting a value
		if ( value !== undefined ) {
			type = typeof value;

			// Convert "+=" or "-=" to relative numbers (#7345)
			if ( type === "string" && (ret = rrelNum.exec( value )) ) {
				value = ( ret[1] + 1 ) * ret[2] + parseFloat( jQuery.css( elem, name ) );
				// Fixes bug #9237
				type = "number";
			}

			// Make sure that null and NaN values aren't set (#7116)
			if ( value == null || value !== value ) {
				return;
			}

			// If a number, add 'px' to the (except for certain CSS properties)
			if ( type === "number" && !jQuery.cssNumber[ origName ] ) {
				value += "px";
			}

			// Support: IE9-11+
			// background-* props affect original clone's values
			if ( !support.clearCloneStyle && value === "" && name.indexOf( "background" ) === 0 ) {
				style[ name ] = "inherit";
			}

			// If a hook was provided, use that value, otherwise just set the specified value
			if ( !hooks || !("set" in hooks) || (value = hooks.set( elem, value, extra )) !== undefined ) {
				style[ name ] = value;
			}

		} else {
			// If a hook was provided get the non-computed value from there
			if ( hooks && "get" in hooks && (ret = hooks.get( elem, false, extra )) !== undefined ) {
				return ret;
			}

			// Otherwise just get the value from the style object
			return style[ name ];
		}
	},

	css: function( elem, name, extra, styles ) {
		var val, num, hooks,
			origName = jQuery.camelCase( name );

		// Make sure that we're working with the right name
		name = jQuery.cssProps[ origName ] || ( jQuery.cssProps[ origName ] = vendorPropName( elem.style, origName ) );

		// Try prefixed name followed by the unprefixed name
		hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];

		// If a hook was provided get the computed value from there
		if ( hooks && "get" in hooks ) {
			val = hooks.get( elem, true, extra );
		}

		// Otherwise, if a way to get the computed value exists, use that
		if ( val === undefined ) {
			val = curCSS( elem, name, styles );
		}

		// Convert "normal" to computed value
		if ( val === "normal" && name in cssNormalTransform ) {
			val = cssNormalTransform[ name ];
		}

		// Make numeric if forced or a qualifier was provided and val looks numeric
		if ( extra === "" || extra ) {
			num = parseFloat( val );
			return extra === true || jQuery.isNumeric( num ) ? num || 0 : val;
		}
		return val;
	}
});

jQuery.each([ "height", "width" ], function( i, name ) {
	jQuery.cssHooks[ name ] = {
		get: function( elem, computed, extra ) {
			if ( computed ) {

				// Certain elements can have dimension info if we invisibly show them
				// but it must have a current display style that would benefit
				return rdisplayswap.test( jQuery.css( elem, "display" ) ) && elem.offsetWidth === 0 ?
					jQuery.swap( elem, cssShow, function() {
						return getWidthOrHeight( elem, name, extra );
					}) :
					getWidthOrHeight( elem, name, extra );
			}
		},

		set: function( elem, value, extra ) {
			var styles = extra && getStyles( elem );
			return setPositiveNumber( elem, value, extra ?
				augmentWidthOrHeight(
					elem,
					name,
					extra,
					jQuery.css( elem, "boxSizing", false, styles ) === "border-box",
					styles
				) : 0
			);
		}
	};
});

// Support: Android 2.3
jQuery.cssHooks.marginRight = addGetHookIf( support.reliableMarginRight,
	function( elem, computed ) {
		if ( computed ) {
			return jQuery.swap( elem, { "display": "inline-block" },
				curCSS, [ elem, "marginRight" ] );
		}
	}
);

// These hooks are used by animate to expand properties
jQuery.each({
	margin: "",
	padding: "",
	border: "Width"
}, function( prefix, suffix ) {
	jQuery.cssHooks[ prefix + suffix ] = {
		expand: function( value ) {
			var i = 0,
				expanded = {},

				// Assumes a single number if not a string
				parts = typeof value === "string" ? value.split(" ") : [ value ];

			for ( ; i < 4; i++ ) {
				expanded[ prefix + cssExpand[ i ] + suffix ] =
					parts[ i ] || parts[ i - 2 ] || parts[ 0 ];
			}

			return expanded;
		}
	};

	if ( !rmargin.test( prefix ) ) {
		jQuery.cssHooks[ prefix + suffix ].set = setPositiveNumber;
	}
});

jQuery.fn.extend({
	css: function( name, value ) {
		return access( this, function( elem, name, value ) {
			var styles, len,
				map = {},
				i = 0;

			if ( jQuery.isArray( name ) ) {
				styles = getStyles( elem );
				len = name.length;

				for ( ; i < len; i++ ) {
					map[ name[ i ] ] = jQuery.css( elem, name[ i ], false, styles );
				}

				return map;
			}

			return value !== undefined ?
				jQuery.style( elem, name, value ) :
				jQuery.css( elem, name );
		}, name, value, arguments.length > 1 );
	},
	show: function() {
		return showHide( this, true );
	},
	hide: function() {
		return showHide( this );
	},
	toggle: function( state ) {
		if ( typeof state === "boolean" ) {
			return state ? this.show() : this.hide();
		}

		return this.each(function() {
			if ( isHidden( this ) ) {
				jQuery( this ).show();
			} else {
				jQuery( this ).hide();
			}
		});
	}
});


function Tween( elem, options, prop, end, easing ) {
	return new Tween.prototype.init( elem, options, prop, end, easing );
}
jQuery.Tween = Tween;

Tween.prototype = {
	constructor: Tween,
	init: function( elem, options, prop, end, easing, unit ) {
		this.elem = elem;
		this.prop = prop;
		this.easing = easing || "swing";
		this.options = options;
		this.start = this.now = this.cur();
		this.end = end;
		this.unit = unit || ( jQuery.cssNumber[ prop ] ? "" : "px" );
	},
	cur: function() {
		var hooks = Tween.propHooks[ this.prop ];

		return hooks && hooks.get ?
			hooks.get( this ) :
			Tween.propHooks._default.get( this );
	},
	run: function( percent ) {
		var eased,
			hooks = Tween.propHooks[ this.prop ];

		if ( this.options.duration ) {
			this.pos = eased = jQuery.easing[ this.easing ](
				percent, this.options.duration * percent, 0, 1, this.options.duration
			);
		} else {
			this.pos = eased = percent;
		}
		this.now = ( this.end - this.start ) * eased + this.start;

		if ( this.options.step ) {
			this.options.step.call( this.elem, this.now, this );
		}

		if ( hooks && hooks.set ) {
			hooks.set( this );
		} else {
			Tween.propHooks._default.set( this );
		}
		return this;
	}
};

Tween.prototype.init.prototype = Tween.prototype;

Tween.propHooks = {
	_default: {
		get: function( tween ) {
			var result;

			if ( tween.elem[ tween.prop ] != null &&
				(!tween.elem.style || tween.elem.style[ tween.prop ] == null) ) {
				return tween.elem[ tween.prop ];
			}

			// Passing an empty string as a 3rd parameter to .css will automatically
			// attempt a parseFloat and fallback to a string if the parse fails.
			// Simple values such as "10px" are parsed to Float;
			// complex values such as "rotate(1rad)" are returned as-is.
			result = jQuery.css( tween.elem, tween.prop, "" );
			// Empty strings, null, undefined and "auto" are converted to 0.
			return !result || result === "auto" ? 0 : result;
		},
		set: function( tween ) {
			// Use step hook for back compat.
			// Use cssHook if its there.
			// Use .style if available and use plain properties where available.
			if ( jQuery.fx.step[ tween.prop ] ) {
				jQuery.fx.step[ tween.prop ]( tween );
			} else if ( tween.elem.style && ( tween.elem.style[ jQuery.cssProps[ tween.prop ] ] != null || jQuery.cssHooks[ tween.prop ] ) ) {
				jQuery.style( tween.elem, tween.prop, tween.now + tween.unit );
			} else {
				tween.elem[ tween.prop ] = tween.now;
			}
		}
	}
};

// Support: IE9
// Panic based approach to setting things on disconnected nodes
Tween.propHooks.scrollTop = Tween.propHooks.scrollLeft = {
	set: function( tween ) {
		if ( tween.elem.nodeType && tween.elem.parentNode ) {
			tween.elem[ tween.prop ] = tween.now;
		}
	}
};

jQuery.easing = {
	linear: function( p ) {
		return p;
	},
	swing: function( p ) {
		return 0.5 - Math.cos( p * Math.PI ) / 2;
	}
};

jQuery.fx = Tween.prototype.init;

// Back Compat <1.8 extension point
jQuery.fx.step = {};




var
	fxNow, timerId,
	rfxtypes = /^(?:toggle|show|hide)$/,
	rfxnum = new RegExp( "^(?:([+-])=|)(" + pnum + ")([a-z%]*)$", "i" ),
	rrun = /queueHooks$/,
	animationPrefilters = [ defaultPrefilter ],
	tweeners = {
		"*": [ function( prop, value ) {
			var tween = this.createTween( prop, value ),
				target = tween.cur(),
				parts = rfxnum.exec( value ),
				unit = parts && parts[ 3 ] || ( jQuery.cssNumber[ prop ] ? "" : "px" ),

				// Starting value computation is required for potential unit mismatches
				start = ( jQuery.cssNumber[ prop ] || unit !== "px" && +target ) &&
					rfxnum.exec( jQuery.css( tween.elem, prop ) ),
				scale = 1,
				maxIterations = 20;

			if ( start && start[ 3 ] !== unit ) {
				// Trust units reported by jQuery.css
				unit = unit || start[ 3 ];

				// Make sure we update the tween properties later on
				parts = parts || [];

				// Iteratively approximate from a nonzero starting point
				start = +target || 1;

				do {
					// If previous iteration zeroed out, double until we get *something*.
					// Use string for doubling so we don't accidentally see scale as unchanged below
					scale = scale || ".5";

					// Adjust and apply
					start = start / scale;
					jQuery.style( tween.elem, prop, start + unit );

				// Update scale, tolerating zero or NaN from tween.cur(),
				// break the loop if scale is unchanged or perfect, or if we've just had enough
				} while ( scale !== (scale = tween.cur() / target) && scale !== 1 && --maxIterations );
			}

			// Update tween properties
			if ( parts ) {
				start = tween.start = +start || +target || 0;
				tween.unit = unit;
				// If a +=/-= token was provided, we're doing a relative animation
				tween.end = parts[ 1 ] ?
					start + ( parts[ 1 ] + 1 ) * parts[ 2 ] :
					+parts[ 2 ];
			}

			return tween;
		} ]
	};

// Animations created synchronously will run synchronously
function createFxNow() {
	setTimeout(function() {
		fxNow = undefined;
	});
	return ( fxNow = jQuery.now() );
}

// Generate parameters to create a standard animation
function genFx( type, includeWidth ) {
	var which,
		i = 0,
		attrs = { height: type };

	// If we include width, step value is 1 to do all cssExpand values,
	// otherwise step value is 2 to skip over Left and Right
	includeWidth = includeWidth ? 1 : 0;
	for ( ; i < 4 ; i += 2 - includeWidth ) {
		which = cssExpand[ i ];
		attrs[ "margin" + which ] = attrs[ "padding" + which ] = type;
	}

	if ( includeWidth ) {
		attrs.opacity = attrs.width = type;
	}

	return attrs;
}

function createTween( value, prop, animation ) {
	var tween,
		collection = ( tweeners[ prop ] || [] ).concat( tweeners[ "*" ] ),
		index = 0,
		length = collection.length;
	for ( ; index < length; index++ ) {
		if ( (tween = collection[ index ].call( animation, prop, value )) ) {

			// We're done with this property
			return tween;
		}
	}
}

function defaultPrefilter( elem, props, opts ) {
	/* jshint validthis: true */
	var prop, value, toggle, tween, hooks, oldfire, display, checkDisplay,
		anim = this,
		orig = {},
		style = elem.style,
		hidden = elem.nodeType && isHidden( elem ),
		dataShow = data_priv.get( elem, "fxshow" );

	// Handle queue: false promises
	if ( !opts.queue ) {
		hooks = jQuery._queueHooks( elem, "fx" );
		if ( hooks.unqueued == null ) {
			hooks.unqueued = 0;
			oldfire = hooks.empty.fire;
			hooks.empty.fire = function() {
				if ( !hooks.unqueued ) {
					oldfire();
				}
			};
		}
		hooks.unqueued++;

		anim.always(function() {
			// Ensure the complete handler is called before this completes
			anim.always(function() {
				hooks.unqueued--;
				if ( !jQuery.queue( elem, "fx" ).length ) {
					hooks.empty.fire();
				}
			});
		});
	}

	// Height/width overflow pass
	if ( elem.nodeType === 1 && ( "height" in props || "width" in props ) ) {
		// Make sure that nothing sneaks out
		// Record all 3 overflow attributes because IE9-10 do not
		// change the overflow attribute when overflowX and
		// overflowY are set to the same value
		opts.overflow = [ style.overflow, style.overflowX, style.overflowY ];

		// Set display property to inline-block for height/width
		// animations on inline elements that are having width/height animated
		display = jQuery.css( elem, "display" );

		// Test default display if display is currently "none"
		checkDisplay = display === "none" ?
			data_priv.get( elem, "olddisplay" ) || defaultDisplay( elem.nodeName ) : display;

		if ( checkDisplay === "inline" && jQuery.css( elem, "float" ) === "none" ) {
			style.display = "inline-block";
		}
	}

	if ( opts.overflow ) {
		style.overflow = "hidden";
		anim.always(function() {
			style.overflow = opts.overflow[ 0 ];
			style.overflowX = opts.overflow[ 1 ];
			style.overflowY = opts.overflow[ 2 ];
		});
	}

	// show/hide pass
	for ( prop in props ) {
		value = props[ prop ];
		if ( rfxtypes.exec( value ) ) {
			delete props[ prop ];
			toggle = toggle || value === "toggle";
			if ( value === ( hidden ? "hide" : "show" ) ) {

				// If there is dataShow left over from a stopped hide or show and we are going to proceed with show, we should pretend to be hidden
				if ( value === "show" && dataShow && dataShow[ prop ] !== undefined ) {
					hidden = true;
				} else {
					continue;
				}
			}
			orig[ prop ] = dataShow && dataShow[ prop ] || jQuery.style( elem, prop );

		// Any non-fx value stops us from restoring the original display value
		} else {
			display = undefined;
		}
	}

	if ( !jQuery.isEmptyObject( orig ) ) {
		if ( dataShow ) {
			if ( "hidden" in dataShow ) {
				hidden = dataShow.hidden;
			}
		} else {
			dataShow = data_priv.access( elem, "fxshow", {} );
		}

		// Store state if its toggle - enables .stop().toggle() to "reverse"
		if ( toggle ) {
			dataShow.hidden = !hidden;
		}
		if ( hidden ) {
			jQuery( elem ).show();
		} else {
			anim.done(function() {
				jQuery( elem ).hide();
			});
		}
		anim.done(function() {
			var prop;

			data_priv.remove( elem, "fxshow" );
			for ( prop in orig ) {
				jQuery.style( elem, prop, orig[ prop ] );
			}
		});
		for ( prop in orig ) {
			tween = createTween( hidden ? dataShow[ prop ] : 0, prop, anim );

			if ( !( prop in dataShow ) ) {
				dataShow[ prop ] = tween.start;
				if ( hidden ) {
					tween.end = tween.start;
					tween.start = prop === "width" || prop === "height" ? 1 : 0;
				}
			}
		}

	// If this is a noop like .hide().hide(), restore an overwritten display value
	} else if ( (display === "none" ? defaultDisplay( elem.nodeName ) : display) === "inline" ) {
		style.display = display;
	}
}

function propFilter( props, specialEasing ) {
	var index, name, easing, value, hooks;

	// camelCase, specialEasing and expand cssHook pass
	for ( index in props ) {
		name = jQuery.camelCase( index );
		easing = specialEasing[ name ];
		value = props[ index ];
		if ( jQuery.isArray( value ) ) {
			easing = value[ 1 ];
			value = props[ index ] = value[ 0 ];
		}

		if ( index !== name ) {
			props[ name ] = value;
			delete props[ index ];
		}

		hooks = jQuery.cssHooks[ name ];
		if ( hooks && "expand" in hooks ) {
			value = hooks.expand( value );
			delete props[ name ];

			// Not quite $.extend, this won't overwrite existing keys.
			// Reusing 'index' because we have the correct "name"
			for ( index in value ) {
				if ( !( index in props ) ) {
					props[ index ] = value[ index ];
					specialEasing[ index ] = easing;
				}
			}
		} else {
			specialEasing[ name ] = easing;
		}
	}
}

function Animation( elem, properties, options ) {
	var result,
		stopped,
		index = 0,
		length = animationPrefilters.length,
		deferred = jQuery.Deferred().always( function() {
			// Don't match elem in the :animated selector
			delete tick.elem;
		}),
		tick = function() {
			if ( stopped ) {
				return false;
			}
			var currentTime = fxNow || createFxNow(),
				remaining = Math.max( 0, animation.startTime + animation.duration - currentTime ),
				// Support: Android 2.3
				// Archaic crash bug won't allow us to use `1 - ( 0.5 || 0 )` (#12497)
				temp = remaining / animation.duration || 0,
				percent = 1 - temp,
				index = 0,
				length = animation.tweens.length;

			for ( ; index < length ; index++ ) {
				animation.tweens[ index ].run( percent );
			}

			deferred.notifyWith( elem, [ animation, percent, remaining ]);

			if ( percent < 1 && length ) {
				return remaining;
			} else {
				deferred.resolveWith( elem, [ animation ] );
				return false;
			}
		},
		animation = deferred.promise({
			elem: elem,
			props: jQuery.extend( {}, properties ),
			opts: jQuery.extend( true, { specialEasing: {} }, options ),
			originalProperties: properties,
			originalOptions: options,
			startTime: fxNow || createFxNow(),
			duration: options.duration,
			tweens: [],
			createTween: function( prop, end ) {
				var tween = jQuery.Tween( elem, animation.opts, prop, end,
						animation.opts.specialEasing[ prop ] || animation.opts.easing );
				animation.tweens.push( tween );
				return tween;
			},
			stop: function( gotoEnd ) {
				var index = 0,
					// If we are going to the end, we want to run all the tweens
					// otherwise we skip this part
					length = gotoEnd ? animation.tweens.length : 0;
				if ( stopped ) {
					return this;
				}
				stopped = true;
				for ( ; index < length ; index++ ) {
					animation.tweens[ index ].run( 1 );
				}

				// Resolve when we played the last frame; otherwise, reject
				if ( gotoEnd ) {
					deferred.resolveWith( elem, [ animation, gotoEnd ] );
				} else {
					deferred.rejectWith( elem, [ animation, gotoEnd ] );
				}
				return this;
			}
		}),
		props = animation.props;

	propFilter( props, animation.opts.specialEasing );

	for ( ; index < length ; index++ ) {
		result = animationPrefilters[ index ].call( animation, elem, props, animation.opts );
		if ( result ) {
			return result;
		}
	}

	jQuery.map( props, createTween, animation );

	if ( jQuery.isFunction( animation.opts.start ) ) {
		animation.opts.start.call( elem, animation );
	}

	jQuery.fx.timer(
		jQuery.extend( tick, {
			elem: elem,
			anim: animation,
			queue: animation.opts.queue
		})
	);

	// attach callbacks from options
	return animation.progress( animation.opts.progress )
		.done( animation.opts.done, animation.opts.complete )
		.fail( animation.opts.fail )
		.always( animation.opts.always );
}

jQuery.Animation = jQuery.extend( Animation, {

	tweener: function( props, callback ) {
		if ( jQuery.isFunction( props ) ) {
			callback = props;
			props = [ "*" ];
		} else {
			props = props.split(" ");
		}

		var prop,
			index = 0,
			length = props.length;

		for ( ; index < length ; index++ ) {
			prop = props[ index ];
			tweeners[ prop ] = tweeners[ prop ] || [];
			tweeners[ prop ].unshift( callback );
		}
	},

	prefilter: function( callback, prepend ) {
		if ( prepend ) {
			animationPrefilters.unshift( callback );
		} else {
			animationPrefilters.push( callback );
		}
	}
});

jQuery.speed = function( speed, easing, fn ) {
	var opt = speed && typeof speed === "object" ? jQuery.extend( {}, speed ) : {
		complete: fn || !fn && easing ||
			jQuery.isFunction( speed ) && speed,
		duration: speed,
		easing: fn && easing || easing && !jQuery.isFunction( easing ) && easing
	};

	opt.duration = jQuery.fx.off ? 0 : typeof opt.duration === "number" ? opt.duration :
		opt.duration in jQuery.fx.speeds ? jQuery.fx.speeds[ opt.duration ] : jQuery.fx.speeds._default;

	// Normalize opt.queue - true/undefined/null -> "fx"
	if ( opt.queue == null || opt.queue === true ) {
		opt.queue = "fx";
	}

	// Queueing
	opt.old = opt.complete;

	opt.complete = function() {
		if ( jQuery.isFunction( opt.old ) ) {
			opt.old.call( this );
		}

		if ( opt.queue ) {
			jQuery.dequeue( this, opt.queue );
		}
	};

	return opt;
};

jQuery.fn.extend({
	fadeTo: function( speed, to, easing, callback ) {

		// Show any hidden elements after setting opacity to 0
		return this.filter( isHidden ).css( "opacity", 0 ).show()

			// Animate to the value specified
			.end().animate({ opacity: to }, speed, easing, callback );
	},
	animate: function( prop, speed, easing, callback ) {
		var empty = jQuery.isEmptyObject( prop ),
			optall = jQuery.speed( speed, easing, callback ),
			doAnimation = function() {
				// Operate on a copy of prop so per-property easing won't be lost
				var anim = Animation( this, jQuery.extend( {}, prop ), optall );

				// Empty animations, or finishing resolves immediately
				if ( empty || data_priv.get( this, "finish" ) ) {
					anim.stop( true );
				}
			};
			doAnimation.finish = doAnimation;

		return empty || optall.queue === false ?
			this.each( doAnimation ) :
			this.queue( optall.queue, doAnimation );
	},
	stop: function( type, clearQueue, gotoEnd ) {
		var stopQueue = function( hooks ) {
			var stop = hooks.stop;
			delete hooks.stop;
			stop( gotoEnd );
		};

		if ( typeof type !== "string" ) {
			gotoEnd = clearQueue;
			clearQueue = type;
			type = undefined;
		}
		if ( clearQueue && type !== false ) {
			this.queue( type || "fx", [] );
		}

		return this.each(function() {
			var dequeue = true,
				index = type != null && type + "queueHooks",
				timers = jQuery.timers,
				data = data_priv.get( this );

			if ( index ) {
				if ( data[ index ] && data[ index ].stop ) {
					stopQueue( data[ index ] );
				}
			} else {
				for ( index in data ) {
					if ( data[ index ] && data[ index ].stop && rrun.test( index ) ) {
						stopQueue( data[ index ] );
					}
				}
			}

			for ( index = timers.length; index--; ) {
				if ( timers[ index ].elem === this && (type == null || timers[ index ].queue === type) ) {
					timers[ index ].anim.stop( gotoEnd );
					dequeue = false;
					timers.splice( index, 1 );
				}
			}

			// Start the next in the queue if the last step wasn't forced.
			// Timers currently will call their complete callbacks, which
			// will dequeue but only if they were gotoEnd.
			if ( dequeue || !gotoEnd ) {
				jQuery.dequeue( this, type );
			}
		});
	},
	finish: function( type ) {
		if ( type !== false ) {
			type = type || "fx";
		}
		return this.each(function() {
			var index,
				data = data_priv.get( this ),
				queue = data[ type + "queue" ],
				hooks = data[ type + "queueHooks" ],
				timers = jQuery.timers,
				length = queue ? queue.length : 0;

			// Enable finishing flag on private data
			data.finish = true;

			// Empty the queue first
			jQuery.queue( this, type, [] );

			if ( hooks && hooks.stop ) {
				hooks.stop.call( this, true );
			}

			// Look for any active animations, and finish them
			for ( index = timers.length; index--; ) {
				if ( timers[ index ].elem === this && timers[ index ].queue === type ) {
					timers[ index ].anim.stop( true );
					timers.splice( index, 1 );
				}
			}

			// Look for any animations in the old queue and finish them
			for ( index = 0; index < length; index++ ) {
				if ( queue[ index ] && queue[ index ].finish ) {
					queue[ index ].finish.call( this );
				}
			}

			// Turn off finishing flag
			delete data.finish;
		});
	}
});

jQuery.each([ "toggle", "show", "hide" ], function( i, name ) {
	var cssFn = jQuery.fn[ name ];
	jQuery.fn[ name ] = function( speed, easing, callback ) {
		return speed == null || typeof speed === "boolean" ?
			cssFn.apply( this, arguments ) :
			this.animate( genFx( name, true ), speed, easing, callback );
	};
});

// Generate shortcuts for custom animations
jQuery.each({
	slideDown: genFx("show"),
	slideUp: genFx("hide"),
	slideToggle: genFx("toggle"),
	fadeIn: { opacity: "show" },
	fadeOut: { opacity: "hide" },
	fadeToggle: { opacity: "toggle" }
}, function( name, props ) {
	jQuery.fn[ name ] = function( speed, easing, callback ) {
		return this.animate( props, speed, easing, callback );
	};
});

jQuery.timers = [];
jQuery.fx.tick = function() {
	var timer,
		i = 0,
		timers = jQuery.timers;

	fxNow = jQuery.now();

	for ( ; i < timers.length; i++ ) {
		timer = timers[ i ];
		// Checks the timer has not already been removed
		if ( !timer() && timers[ i ] === timer ) {
			timers.splice( i--, 1 );
		}
	}

	if ( !timers.length ) {
		jQuery.fx.stop();
	}
	fxNow = undefined;
};

jQuery.fx.timer = function( timer ) {
	jQuery.timers.push( timer );
	if ( timer() ) {
		jQuery.fx.start();
	} else {
		jQuery.timers.pop();
	}
};

jQuery.fx.interval = 13;

jQuery.fx.start = function() {
	if ( !timerId ) {
		timerId = setInterval( jQuery.fx.tick, jQuery.fx.interval );
	}
};

jQuery.fx.stop = function() {
	clearInterval( timerId );
	timerId = null;
};

jQuery.fx.speeds = {
	slow: 600,
	fast: 200,
	// Default speed
	_default: 400
};


// Based off of the plugin by Clint Helfers, with permission.
// http://blindsignals.com/index.php/2009/07/jquery-delay/
jQuery.fn.delay = function( time, type ) {
	time = jQuery.fx ? jQuery.fx.speeds[ time ] || time : time;
	type = type || "fx";

	return this.queue( type, function( next, hooks ) {
		var timeout = setTimeout( next, time );
		hooks.stop = function() {
			clearTimeout( timeout );
		};
	});
};


(function() {
	var input = document.createElement( "input" ),
		select = document.createElement( "select" ),
		opt = select.appendChild( document.createElement( "option" ) );

	input.type = "checkbox";

	// Support: iOS<=5.1, Android<=4.2+
	// Default value for a checkbox should be "on"
	support.checkOn = input.value !== "";

	// Support: IE<=11+
	// Must access selectedIndex to make default options select
	support.optSelected = opt.selected;

	// Support: Android<=2.3
	// Options inside disabled selects are incorrectly marked as disabled
	select.disabled = true;
	support.optDisabled = !opt.disabled;

	// Support: IE<=11+
	// An input loses its value after becoming a radio
	input = document.createElement( "input" );
	input.value = "t";
	input.type = "radio";
	support.radioValue = input.value === "t";
})();


var nodeHook, boolHook,
	attrHandle = jQuery.expr.attrHandle;

jQuery.fn.extend({
	attr: function( name, value ) {
		return access( this, jQuery.attr, name, value, arguments.length > 1 );
	},

	removeAttr: function( name ) {
		return this.each(function() {
			jQuery.removeAttr( this, name );
		});
	}
});

jQuery.extend({
	attr: function( elem, name, value ) {
		var hooks, ret,
			nType = elem.nodeType;

		// don't get/set attributes on text, comment and attribute nodes
		if ( !elem || nType === 3 || nType === 8 || nType === 2 ) {
			return;
		}

		// Fallback to prop when attributes are not supported
		if ( typeof elem.getAttribute === strundefined ) {
			return jQuery.prop( elem, name, value );
		}

		// All attributes are lowercase
		// Grab necessary hook if one is defined
		if ( nType !== 1 || !jQuery.isXMLDoc( elem ) ) {
			name = name.toLowerCase();
			hooks = jQuery.attrHooks[ name ] ||
				( jQuery.expr.match.bool.test( name ) ? boolHook : nodeHook );
		}

		if ( value !== undefined ) {

			if ( value === null ) {
				jQuery.removeAttr( elem, name );

			} else if ( hooks && "set" in hooks && (ret = hooks.set( elem, value, name )) !== undefined ) {
				return ret;

			} else {
				elem.setAttribute( name, value + "" );
				return value;
			}

		} else if ( hooks && "get" in hooks && (ret = hooks.get( elem, name )) !== null ) {
			return ret;

		} else {
			ret = jQuery.find.attr( elem, name );

			// Non-existent attributes return null, we normalize to undefined
			return ret == null ?
				undefined :
				ret;
		}
	},

	removeAttr: function( elem, value ) {
		var name, propName,
			i = 0,
			attrNames = value && value.match( rnotwhite );

		if ( attrNames && elem.nodeType === 1 ) {
			while ( (name = attrNames[i++]) ) {
				propName = jQuery.propFix[ name ] || name;

				// Boolean attributes get special treatment (#10870)
				if ( jQuery.expr.match.bool.test( name ) ) {
					// Set corresponding property to false
					elem[ propName ] = false;
				}

				elem.removeAttribute( name );
			}
		}
	},

	attrHooks: {
		type: {
			set: function( elem, value ) {
				if ( !support.radioValue && value === "radio" &&
					jQuery.nodeName( elem, "input" ) ) {
					var val = elem.value;
					elem.setAttribute( "type", value );
					if ( val ) {
						elem.value = val;
					}
					return value;
				}
			}
		}
	}
});

// Hooks for boolean attributes
boolHook = {
	set: function( elem, value, name ) {
		if ( value === false ) {
			// Remove boolean attributes when set to false
			jQuery.removeAttr( elem, name );
		} else {
			elem.setAttribute( name, name );
		}
		return name;
	}
};
jQuery.each( jQuery.expr.match.bool.source.match( /\w+/g ), function( i, name ) {
	var getter = attrHandle[ name ] || jQuery.find.attr;

	attrHandle[ name ] = function( elem, name, isXML ) {
		var ret, handle;
		if ( !isXML ) {
			// Avoid an infinite loop by temporarily removing this function from the getter
			handle = attrHandle[ name ];
			attrHandle[ name ] = ret;
			ret = getter( elem, name, isXML ) != null ?
				name.toLowerCase() :
				null;
			attrHandle[ name ] = handle;
		}
		return ret;
	};
});




var rfocusable = /^(?:input|select|textarea|button)$/i;

jQuery.fn.extend({
	prop: function( name, value ) {
		return access( this, jQuery.prop, name, value, arguments.length > 1 );
	},

	removeProp: function( name ) {
		return this.each(function() {
			delete this[ jQuery.propFix[ name ] || name ];
		});
	}
});

jQuery.extend({
	propFix: {
		"for": "htmlFor",
		"class": "className"
	},

	prop: function( elem, name, value ) {
		var ret, hooks, notxml,
			nType = elem.nodeType;

		// Don't get/set properties on text, comment and attribute nodes
		if ( !elem || nType === 3 || nType === 8 || nType === 2 ) {
			return;
		}

		notxml = nType !== 1 || !jQuery.isXMLDoc( elem );

		if ( notxml ) {
			// Fix name and attach hooks
			name = jQuery.propFix[ name ] || name;
			hooks = jQuery.propHooks[ name ];
		}

		if ( value !== undefined ) {
			return hooks && "set" in hooks && (ret = hooks.set( elem, value, name )) !== undefined ?
				ret :
				( elem[ name ] = value );

		} else {
			return hooks && "get" in hooks && (ret = hooks.get( elem, name )) !== null ?
				ret :
				elem[ name ];
		}
	},

	propHooks: {
		tabIndex: {
			get: function( elem ) {
				return elem.hasAttribute( "tabindex" ) || rfocusable.test( elem.nodeName ) || elem.href ?
					elem.tabIndex :
					-1;
			}
		}
	}
});

if ( !support.optSelected ) {
	jQuery.propHooks.selected = {
		get: function( elem ) {
			var parent = elem.parentNode;
			if ( parent && parent.parentNode ) {
				parent.parentNode.selectedIndex;
			}
			return null;
		}
	};
}

jQuery.each([
	"tabIndex",
	"readOnly",
	"maxLength",
	"cellSpacing",
	"cellPadding",
	"rowSpan",
	"colSpan",
	"useMap",
	"frameBorder",
	"contentEditable"
], function() {
	jQuery.propFix[ this.toLowerCase() ] = this;
});




var rclass = /[\t\r\n\f]/g;

jQuery.fn.extend({
	addClass: function( value ) {
		var classes, elem, cur, clazz, j, finalValue,
			proceed = typeof value === "string" && value,
			i = 0,
			len = this.length;

		if ( jQuery.isFunction( value ) ) {
			return this.each(function( j ) {
				jQuery( this ).addClass( value.call( this, j, this.className ) );
			});
		}

		if ( proceed ) {
			// The disjunction here is for better compressibility (see removeClass)
			classes = ( value || "" ).match( rnotwhite ) || [];

			for ( ; i < len; i++ ) {
				elem = this[ i ];
				cur = elem.nodeType === 1 && ( elem.className ?
					( " " + elem.className + " " ).replace( rclass, " " ) :
					" "
				);

				if ( cur ) {
					j = 0;
					while ( (clazz = classes[j++]) ) {
						if ( cur.indexOf( " " + clazz + " " ) < 0 ) {
							cur += clazz + " ";
						}
					}

					// only assign if different to avoid unneeded rendering.
					finalValue = jQuery.trim( cur );
					if ( elem.className !== finalValue ) {
						elem.className = finalValue;
					}
				}
			}
		}

		return this;
	},

	removeClass: function( value ) {
		var classes, elem, cur, clazz, j, finalValue,
			proceed = arguments.length === 0 || typeof value === "string" && value,
			i = 0,
			len = this.length;

		if ( jQuery.isFunction( value ) ) {
			return this.each(function( j ) {
				jQuery( this ).removeClass( value.call( this, j, this.className ) );
			});
		}
		if ( proceed ) {
			classes = ( value || "" ).match( rnotwhite ) || [];

			for ( ; i < len; i++ ) {
				elem = this[ i ];
				// This expression is here for better compressibility (see addClass)
				cur = elem.nodeType === 1 && ( elem.className ?
					( " " + elem.className + " " ).replace( rclass, " " ) :
					""
				);

				if ( cur ) {
					j = 0;
					while ( (clazz = classes[j++]) ) {
						// Remove *all* instances
						while ( cur.indexOf( " " + clazz + " " ) >= 0 ) {
							cur = cur.replace( " " + clazz + " ", " " );
						}
					}

					// Only assign if different to avoid unneeded rendering.
					finalValue = value ? jQuery.trim( cur ) : "";
					if ( elem.className !== finalValue ) {
						elem.className = finalValue;
					}
				}
			}
		}

		return this;
	},

	toggleClass: function( value, stateVal ) {
		var type = typeof value;

		if ( typeof stateVal === "boolean" && type === "string" ) {
			return stateVal ? this.addClass( value ) : this.removeClass( value );
		}

		if ( jQuery.isFunction( value ) ) {
			return this.each(function( i ) {
				jQuery( this ).toggleClass( value.call(this, i, this.className, stateVal), stateVal );
			});
		}

		return this.each(function() {
			if ( type === "string" ) {
				// Toggle individual class names
				var className,
					i = 0,
					self = jQuery( this ),
					classNames = value.match( rnotwhite ) || [];

				while ( (className = classNames[ i++ ]) ) {
					// Check each className given, space separated list
					if ( self.hasClass( className ) ) {
						self.removeClass( className );
					} else {
						self.addClass( className );
					}
				}

			// Toggle whole class name
			} else if ( type === strundefined || type === "boolean" ) {
				if ( this.className ) {
					// store className if set
					data_priv.set( this, "__className__", this.className );
				}

				// If the element has a class name or if we're passed `false`,
				// then remove the whole classname (if there was one, the above saved it).
				// Otherwise bring back whatever was previously saved (if anything),
				// falling back to the empty string if nothing was stored.
				this.className = this.className || value === false ? "" : data_priv.get( this, "__className__" ) || "";
			}
		});
	},

	hasClass: function( selector ) {
		var className = " " + selector + " ",
			i = 0,
			l = this.length;
		for ( ; i < l; i++ ) {
			if ( this[i].nodeType === 1 && (" " + this[i].className + " ").replace(rclass, " ").indexOf( className ) >= 0 ) {
				return true;
			}
		}

		return false;
	}
});




var rreturn = /\r/g;

jQuery.fn.extend({
	val: function( value ) {
		var hooks, ret, isFunction,
			elem = this[0];

		if ( !arguments.length ) {
			if ( elem ) {
				hooks = jQuery.valHooks[ elem.type ] || jQuery.valHooks[ elem.nodeName.toLowerCase() ];

				if ( hooks && "get" in hooks && (ret = hooks.get( elem, "value" )) !== undefined ) {
					return ret;
				}

				ret = elem.value;

				return typeof ret === "string" ?
					// Handle most common string cases
					ret.replace(rreturn, "") :
					// Handle cases where value is null/undef or number
					ret == null ? "" : ret;
			}

			return;
		}

		isFunction = jQuery.isFunction( value );

		return this.each(function( i ) {
			var val;

			if ( this.nodeType !== 1 ) {
				return;
			}

			if ( isFunction ) {
				val = value.call( this, i, jQuery( this ).val() );
			} else {
				val = value;
			}

			// Treat null/undefined as ""; convert numbers to string
			if ( val == null ) {
				val = "";

			} else if ( typeof val === "number" ) {
				val += "";

			} else if ( jQuery.isArray( val ) ) {
				val = jQuery.map( val, function( value ) {
					return value == null ? "" : value + "";
				});
			}

			hooks = jQuery.valHooks[ this.type ] || jQuery.valHooks[ this.nodeName.toLowerCase() ];

			// If set returns undefined, fall back to normal setting
			if ( !hooks || !("set" in hooks) || hooks.set( this, val, "value" ) === undefined ) {
				this.value = val;
			}
		});
	}
});

jQuery.extend({
	valHooks: {
		option: {
			get: function( elem ) {
				var val = jQuery.find.attr( elem, "value" );
				return val != null ?
					val :
					// Support: IE10-11+
					// option.text throws exceptions (#14686, #14858)
					jQuery.trim( jQuery.text( elem ) );
			}
		},
		select: {
			get: function( elem ) {
				var value, option,
					options = elem.options,
					index = elem.selectedIndex,
					one = elem.type === "select-one" || index < 0,
					values = one ? null : [],
					max = one ? index + 1 : options.length,
					i = index < 0 ?
						max :
						one ? index : 0;

				// Loop through all the selected options
				for ( ; i < max; i++ ) {
					option = options[ i ];

					// IE6-9 doesn't update selected after form reset (#2551)
					if ( ( option.selected || i === index ) &&
							// Don't return options that are disabled or in a disabled optgroup
							( support.optDisabled ? !option.disabled : option.getAttribute( "disabled" ) === null ) &&
							( !option.parentNode.disabled || !jQuery.nodeName( option.parentNode, "optgroup" ) ) ) {

						// Get the specific value for the option
						value = jQuery( option ).val();

						// We don't need an array for one selects
						if ( one ) {
							return value;
						}

						// Multi-Selects return an array
						values.push( value );
					}
				}

				return values;
			},

			set: function( elem, value ) {
				var optionSet, option,
					options = elem.options,
					values = jQuery.makeArray( value ),
					i = options.length;

				while ( i-- ) {
					option = options[ i ];
					if ( (option.selected = jQuery.inArray( option.value, values ) >= 0) ) {
						optionSet = true;
					}
				}

				// Force browsers to behave consistently when non-matching value is set
				if ( !optionSet ) {
					elem.selectedIndex = -1;
				}
				return values;
			}
		}
	}
});

// Radios and checkboxes getter/setter
jQuery.each([ "radio", "checkbox" ], function() {
	jQuery.valHooks[ this ] = {
		set: function( elem, value ) {
			if ( jQuery.isArray( value ) ) {
				return ( elem.checked = jQuery.inArray( jQuery(elem).val(), value ) >= 0 );
			}
		}
	};
	if ( !support.checkOn ) {
		jQuery.valHooks[ this ].get = function( elem ) {
			return elem.getAttribute("value") === null ? "on" : elem.value;
		};
	}
});




// Return jQuery for attributes-only inclusion


jQuery.each( ("blur focus focusin focusout load resize scroll unload click dblclick " +
	"mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave " +
	"change select submit keydown keypress keyup error contextmenu").split(" "), function( i, name ) {

	// Handle event binding
	jQuery.fn[ name ] = function( data, fn ) {
		return arguments.length > 0 ?
			this.on( name, null, data, fn ) :
			this.trigger( name );
	};
});

jQuery.fn.extend({
	hover: function( fnOver, fnOut ) {
		return this.mouseenter( fnOver ).mouseleave( fnOut || fnOver );
	},

	bind: function( types, data, fn ) {
		return this.on( types, null, data, fn );
	},
	unbind: function( types, fn ) {
		return this.off( types, null, fn );
	},

	delegate: function( selector, types, data, fn ) {
		return this.on( types, selector, data, fn );
	},
	undelegate: function( selector, types, fn ) {
		// ( namespace ) or ( selector, types [, fn] )
		return arguments.length === 1 ? this.off( selector, "**" ) : this.off( types, selector || "**", fn );
	}
});


var nonce = jQuery.now();

var rquery = (/\?/);



// Support: Android 2.3
// Workaround failure to string-cast null input
jQuery.parseJSON = function( data ) {
	return JSON.parse( data + "" );
};


// Cross-browser xml parsing
jQuery.parseXML = function( data ) {
	var xml, tmp;
	if ( !data || typeof data !== "string" ) {
		return null;
	}

	// Support: IE9
	try {
		tmp = new DOMParser();
		xml = tmp.parseFromString( data, "text/xml" );
	} catch ( e ) {
		xml = undefined;
	}

	if ( !xml || xml.getElementsByTagName( "parsererror" ).length ) {
		jQuery.error( "Invalid XML: " + data );
	}
	return xml;
};


var
	rhash = /#.*$/,
	rts = /([?&])_=[^&]*/,
	rheaders = /^(.*?):[ \t]*([^\r\n]*)$/mg,
	// #7653, #8125, #8152: local protocol detection
	rlocalProtocol = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
	rnoContent = /^(?:GET|HEAD)$/,
	rprotocol = /^\/\//,
	rurl = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/,

	/* Prefilters
	 * 1) They are useful to introduce custom dataTypes (see ajax/jsonp.js for an example)
	 * 2) These are called:
	 *    - BEFORE asking for a transport
	 *    - AFTER param serialization (s.data is a string if s.processData is true)
	 * 3) key is the dataType
	 * 4) the catchall symbol "*" can be used
	 * 5) execution will start with transport dataType and THEN continue down to "*" if needed
	 */
	prefilters = {},

	/* Transports bindings
	 * 1) key is the dataType
	 * 2) the catchall symbol "*" can be used
	 * 3) selection will start with transport dataType and THEN go to "*" if needed
	 */
	transports = {},

	// Avoid comment-prolog char sequence (#10098); must appease lint and evade compression
	allTypes = "*/".concat( "*" ),

	// Document location
	ajaxLocation = window.location.href,

	// Segment location into parts
	ajaxLocParts = rurl.exec( ajaxLocation.toLowerCase() ) || [];

// Base "constructor" for jQuery.ajaxPrefilter and jQuery.ajaxTransport
function addToPrefiltersOrTransports( structure ) {

	// dataTypeExpression is optional and defaults to "*"
	return function( dataTypeExpression, func ) {

		if ( typeof dataTypeExpression !== "string" ) {
			func = dataTypeExpression;
			dataTypeExpression = "*";
		}

		var dataType,
			i = 0,
			dataTypes = dataTypeExpression.toLowerCase().match( rnotwhite ) || [];

		if ( jQuery.isFunction( func ) ) {
			// For each dataType in the dataTypeExpression
			while ( (dataType = dataTypes[i++]) ) {
				// Prepend if requested
				if ( dataType[0] === "+" ) {
					dataType = dataType.slice( 1 ) || "*";
					(structure[ dataType ] = structure[ dataType ] || []).unshift( func );

				// Otherwise append
				} else {
					(structure[ dataType ] = structure[ dataType ] || []).push( func );
				}
			}
		}
	};
}

// Base inspection function for prefilters and transports
function inspectPrefiltersOrTransports( structure, options, originalOptions, jqXHR ) {

	var inspected = {},
		seekingTransport = ( structure === transports );

	function inspect( dataType ) {
		var selected;
		inspected[ dataType ] = true;
		jQuery.each( structure[ dataType ] || [], function( _, prefilterOrFactory ) {
			var dataTypeOrTransport = prefilterOrFactory( options, originalOptions, jqXHR );
			if ( typeof dataTypeOrTransport === "string" && !seekingTransport && !inspected[ dataTypeOrTransport ] ) {
				options.dataTypes.unshift( dataTypeOrTransport );
				inspect( dataTypeOrTransport );
				return false;
			} else if ( seekingTransport ) {
				return !( selected = dataTypeOrTransport );
			}
		});
		return selected;
	}

	return inspect( options.dataTypes[ 0 ] ) || !inspected[ "*" ] && inspect( "*" );
}

// A special extend for ajax options
// that takes "flat" options (not to be deep extended)
// Fixes #9887
function ajaxExtend( target, src ) {
	var key, deep,
		flatOptions = jQuery.ajaxSettings.flatOptions || {};

	for ( key in src ) {
		if ( src[ key ] !== undefined ) {
			( flatOptions[ key ] ? target : ( deep || (deep = {}) ) )[ key ] = src[ key ];
		}
	}
	if ( deep ) {
		jQuery.extend( true, target, deep );
	}

	return target;
}

/* Handles responses to an ajax request:
 * - finds the right dataType (mediates between content-type and expected dataType)
 * - returns the corresponding response
 */
function ajaxHandleResponses( s, jqXHR, responses ) {

	var ct, type, finalDataType, firstDataType,
		contents = s.contents,
		dataTypes = s.dataTypes;

	// Remove auto dataType and get content-type in the process
	while ( dataTypes[ 0 ] === "*" ) {
		dataTypes.shift();
		if ( ct === undefined ) {
			ct = s.mimeType || jqXHR.getResponseHeader("Content-Type");
		}
	}

	// Check if we're dealing with a known content-type
	if ( ct ) {
		for ( type in contents ) {
			if ( contents[ type ] && contents[ type ].test( ct ) ) {
				dataTypes.unshift( type );
				break;
			}
		}
	}

	// Check to see if we have a response for the expected dataType
	if ( dataTypes[ 0 ] in responses ) {
		finalDataType = dataTypes[ 0 ];
	} else {
		// Try convertible dataTypes
		for ( type in responses ) {
			if ( !dataTypes[ 0 ] || s.converters[ type + " " + dataTypes[0] ] ) {
				finalDataType = type;
				break;
			}
			if ( !firstDataType ) {
				firstDataType = type;
			}
		}
		// Or just use first one
		finalDataType = finalDataType || firstDataType;
	}

	// If we found a dataType
	// We add the dataType to the list if needed
	// and return the corresponding response
	if ( finalDataType ) {
		if ( finalDataType !== dataTypes[ 0 ] ) {
			dataTypes.unshift( finalDataType );
		}
		return responses[ finalDataType ];
	}
}

/* Chain conversions given the request and the original response
 * Also sets the responseXXX fields on the jqXHR instance
 */
function ajaxConvert( s, response, jqXHR, isSuccess ) {
	var conv2, current, conv, tmp, prev,
		converters = {},
		// Work with a copy of dataTypes in case we need to modify it for conversion
		dataTypes = s.dataTypes.slice();

	// Create converters map with lowercased keys
	if ( dataTypes[ 1 ] ) {
		for ( conv in s.converters ) {
			converters[ conv.toLowerCase() ] = s.converters[ conv ];
		}
	}

	current = dataTypes.shift();

	// Convert to each sequential dataType
	while ( current ) {

		if ( s.responseFields[ current ] ) {
			jqXHR[ s.responseFields[ current ] ] = response;
		}

		// Apply the dataFilter if provided
		if ( !prev && isSuccess && s.dataFilter ) {
			response = s.dataFilter( response, s.dataType );
		}

		prev = current;
		current = dataTypes.shift();

		if ( current ) {

		// There's only work to do if current dataType is non-auto
			if ( current === "*" ) {

				current = prev;

			// Convert response if prev dataType is non-auto and differs from current
			} else if ( prev !== "*" && prev !== current ) {

				// Seek a direct converter
				conv = converters[ prev + " " + current ] || converters[ "* " + current ];

				// If none found, seek a pair
				if ( !conv ) {
					for ( conv2 in converters ) {

						// If conv2 outputs current
						tmp = conv2.split( " " );
						if ( tmp[ 1 ] === current ) {

							// If prev can be converted to accepted input
							conv = converters[ prev + " " + tmp[ 0 ] ] ||
								converters[ "* " + tmp[ 0 ] ];
							if ( conv ) {
								// Condense equivalence converters
								if ( conv === true ) {
									conv = converters[ conv2 ];

								// Otherwise, insert the intermediate dataType
								} else if ( converters[ conv2 ] !== true ) {
									current = tmp[ 0 ];
									dataTypes.unshift( tmp[ 1 ] );
								}
								break;
							}
						}
					}
				}

				// Apply converter (if not an equivalence)
				if ( conv !== true ) {

					// Unless errors are allowed to bubble, catch and return them
					if ( conv && s[ "throws" ] ) {
						response = conv( response );
					} else {
						try {
							response = conv( response );
						} catch ( e ) {
							return { state: "parsererror", error: conv ? e : "No conversion from " + prev + " to " + current };
						}
					}
				}
			}
		}
	}

	return { state: "success", data: response };
}

jQuery.extend({

	// Counter for holding the number of active queries
	active: 0,

	// Last-Modified header cache for next request
	lastModified: {},
	etag: {},

	ajaxSettings: {
		url: ajaxLocation,
		type: "GET",
		isLocal: rlocalProtocol.test( ajaxLocParts[ 1 ] ),
		global: true,
		processData: true,
		async: true,
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		/*
		timeout: 0,
		data: null,
		dataType: null,
		username: null,
		password: null,
		cache: null,
		throws: false,
		traditional: false,
		headers: {},
		*/

		accepts: {
			"*": allTypes,
			text: "text/plain",
			html: "text/html",
			xml: "application/xml, text/xml",
			json: "application/json, text/javascript"
		},

		contents: {
			xml: /xml/,
			html: /html/,
			json: /json/
		},

		responseFields: {
			xml: "responseXML",
			text: "responseText",
			json: "responseJSON"
		},

		// Data converters
		// Keys separate source (or catchall "*") and destination types with a single space
		converters: {

			// Convert anything to text
			"* text": String,

			// Text to html (true = no transformation)
			"text html": true,

			// Evaluate text as a json expression
			"text json": jQuery.parseJSON,

			// Parse text as xml
			"text xml": jQuery.parseXML
		},

		// For options that shouldn't be deep extended:
		// you can add your own custom options here if
		// and when you create one that shouldn't be
		// deep extended (see ajaxExtend)
		flatOptions: {
			url: true,
			context: true
		}
	},

	// Creates a full fledged settings object into target
	// with both ajaxSettings and settings fields.
	// If target is omitted, writes into ajaxSettings.
	ajaxSetup: function( target, settings ) {
		return settings ?

			// Building a settings object
			ajaxExtend( ajaxExtend( target, jQuery.ajaxSettings ), settings ) :

			// Extending ajaxSettings
			ajaxExtend( jQuery.ajaxSettings, target );
	},

	ajaxPrefilter: addToPrefiltersOrTransports( prefilters ),
	ajaxTransport: addToPrefiltersOrTransports( transports ),

	// Main method
	ajax: function( url, options ) {

		// If url is an object, simulate pre-1.5 signature
		if ( typeof url === "object" ) {
			options = url;
			url = undefined;
		}

		// Force options to be an object
		options = options || {};

		var transport,
			// URL without anti-cache param
			cacheURL,
			// Response headers
			responseHeadersString,
			responseHeaders,
			// timeout handle
			timeoutTimer,
			// Cross-domain detection vars
			parts,
			// To know if global events are to be dispatched
			fireGlobals,
			// Loop variable
			i,
			// Create the final options object
			s = jQuery.ajaxSetup( {}, options ),
			// Callbacks context
			callbackContext = s.context || s,
			// Context for global events is callbackContext if it is a DOM node or jQuery collection
			globalEventContext = s.context && ( callbackContext.nodeType || callbackContext.jquery ) ?
				jQuery( callbackContext ) :
				jQuery.event,
			// Deferreds
			deferred = jQuery.Deferred(),
			completeDeferred = jQuery.Callbacks("once memory"),
			// Status-dependent callbacks
			statusCode = s.statusCode || {},
			// Headers (they are sent all at once)
			requestHeaders = {},
			requestHeadersNames = {},
			// The jqXHR state
			state = 0,
			// Default abort message
			strAbort = "canceled",
			// Fake xhr
			jqXHR = {
				readyState: 0,

				// Builds headers hashtable if needed
				getResponseHeader: function( key ) {
					var match;
					if ( state === 2 ) {
						if ( !responseHeaders ) {
							responseHeaders = {};
							while ( (match = rheaders.exec( responseHeadersString )) ) {
								responseHeaders[ match[1].toLowerCase() ] = match[ 2 ];
							}
						}
						match = responseHeaders[ key.toLowerCase() ];
					}
					return match == null ? null : match;
				},

				// Raw string
				getAllResponseHeaders: function() {
					return state === 2 ? responseHeadersString : null;
				},

				// Caches the header
				setRequestHeader: function( name, value ) {
					var lname = name.toLowerCase();
					if ( !state ) {
						name = requestHeadersNames[ lname ] = requestHeadersNames[ lname ] || name;
						requestHeaders[ name ] = value;
					}
					return this;
				},

				// Overrides response content-type header
				overrideMimeType: function( type ) {
					if ( !state ) {
						s.mimeType = type;
					}
					return this;
				},

				// Status-dependent callbacks
				statusCode: function( map ) {
					var code;
					if ( map ) {
						if ( state < 2 ) {
							for ( code in map ) {
								// Lazy-add the new callback in a way that preserves old ones
								statusCode[ code ] = [ statusCode[ code ], map[ code ] ];
							}
						} else {
							// Execute the appropriate callbacks
							jqXHR.always( map[ jqXHR.status ] );
						}
					}
					return this;
				},

				// Cancel the request
				abort: function( statusText ) {
					var finalText = statusText || strAbort;
					if ( transport ) {
						transport.abort( finalText );
					}
					done( 0, finalText );
					return this;
				}
			};

		// Attach deferreds
		deferred.promise( jqXHR ).complete = completeDeferred.add;
		jqXHR.success = jqXHR.done;
		jqXHR.error = jqXHR.fail;

		// Remove hash character (#7531: and string promotion)
		// Add protocol if not provided (prefilters might expect it)
		// Handle falsy url in the settings object (#10093: consistency with old signature)
		// We also use the url parameter if available
		s.url = ( ( url || s.url || ajaxLocation ) + "" ).replace( rhash, "" )
			.replace( rprotocol, ajaxLocParts[ 1 ] + "//" );

		// Alias method option to type as per ticket #12004
		s.type = options.method || options.type || s.method || s.type;

		// Extract dataTypes list
		s.dataTypes = jQuery.trim( s.dataType || "*" ).toLowerCase().match( rnotwhite ) || [ "" ];

		// A cross-domain request is in order when we have a protocol:host:port mismatch
		if ( s.crossDomain == null ) {
			parts = rurl.exec( s.url.toLowerCase() );
			s.crossDomain = !!( parts &&
				( parts[ 1 ] !== ajaxLocParts[ 1 ] || parts[ 2 ] !== ajaxLocParts[ 2 ] ||
					( parts[ 3 ] || ( parts[ 1 ] === "http:" ? "80" : "443" ) ) !==
						( ajaxLocParts[ 3 ] || ( ajaxLocParts[ 1 ] === "http:" ? "80" : "443" ) ) )
			);
		}

		// Convert data if not already a string
		if ( s.data && s.processData && typeof s.data !== "string" ) {
			s.data = jQuery.param( s.data, s.traditional );
		}

		// Apply prefilters
		inspectPrefiltersOrTransports( prefilters, s, options, jqXHR );

		// If request was aborted inside a prefilter, stop there
		if ( state === 2 ) {
			return jqXHR;
		}

		// We can fire global events as of now if asked to
		// Don't fire events if jQuery.event is undefined in an AMD-usage scenario (#15118)
		fireGlobals = jQuery.event && s.global;

		// Watch for a new set of requests
		if ( fireGlobals && jQuery.active++ === 0 ) {
			jQuery.event.trigger("ajaxStart");
		}

		// Uppercase the type
		s.type = s.type.toUpperCase();

		// Determine if request has content
		s.hasContent = !rnoContent.test( s.type );

		// Save the URL in case we're toying with the If-Modified-Since
		// and/or If-None-Match header later on
		cacheURL = s.url;

		// More options handling for requests with no content
		if ( !s.hasContent ) {

			// If data is available, append data to url
			if ( s.data ) {
				cacheURL = ( s.url += ( rquery.test( cacheURL ) ? "&" : "?" ) + s.data );
				// #9682: remove data so that it's not used in an eventual retry
				delete s.data;
			}

			// Add anti-cache in url if needed
			if ( s.cache === false ) {
				s.url = rts.test( cacheURL ) ?

					// If there is already a '_' parameter, set its value
					cacheURL.replace( rts, "$1_=" + nonce++ ) :

					// Otherwise add one to the end
					cacheURL + ( rquery.test( cacheURL ) ? "&" : "?" ) + "_=" + nonce++;
			}
		}

		// Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
		if ( s.ifModified ) {
			if ( jQuery.lastModified[ cacheURL ] ) {
				jqXHR.setRequestHeader( "If-Modified-Since", jQuery.lastModified[ cacheURL ] );
			}
			if ( jQuery.etag[ cacheURL ] ) {
				jqXHR.setRequestHeader( "If-None-Match", jQuery.etag[ cacheURL ] );
			}
		}

		// Set the correct header, if data is being sent
		if ( s.data && s.hasContent && s.contentType !== false || options.contentType ) {
			jqXHR.setRequestHeader( "Content-Type", s.contentType );
		}

		// Set the Accepts header for the server, depending on the dataType
		jqXHR.setRequestHeader(
			"Accept",
			s.dataTypes[ 0 ] && s.accepts[ s.dataTypes[0] ] ?
				s.accepts[ s.dataTypes[0] ] + ( s.dataTypes[ 0 ] !== "*" ? ", " + allTypes + "; q=0.01" : "" ) :
				s.accepts[ "*" ]
		);

		// Check for headers option
		for ( i in s.headers ) {
			jqXHR.setRequestHeader( i, s.headers[ i ] );
		}

		// Allow custom headers/mimetypes and early abort
		if ( s.beforeSend && ( s.beforeSend.call( callbackContext, jqXHR, s ) === false || state === 2 ) ) {
			// Abort if not done already and return
			return jqXHR.abort();
		}

		// Aborting is no longer a cancellation
		strAbort = "abort";

		// Install callbacks on deferreds
		for ( i in { success: 1, error: 1, complete: 1 } ) {
			jqXHR[ i ]( s[ i ] );
		}

		// Get transport
		transport = inspectPrefiltersOrTransports( transports, s, options, jqXHR );

		// If no transport, we auto-abort
		if ( !transport ) {
			done( -1, "No Transport" );
		} else {
			jqXHR.readyState = 1;

			// Send global event
			if ( fireGlobals ) {
				globalEventContext.trigger( "ajaxSend", [ jqXHR, s ] );
			}
			// Timeout
			if ( s.async && s.timeout > 0 ) {
				timeoutTimer = setTimeout(function() {
					jqXHR.abort("timeout");
				}, s.timeout );
			}

			try {
				state = 1;
				transport.send( requestHeaders, done );
			} catch ( e ) {
				// Propagate exception as error if not done
				if ( state < 2 ) {
					done( -1, e );
				// Simply rethrow otherwise
				} else {
					throw e;
				}
			}
		}

		// Callback for when everything is done
		function done( status, nativeStatusText, responses, headers ) {
			var isSuccess, success, error, response, modified,
				statusText = nativeStatusText;

			// Called once
			if ( state === 2 ) {
				return;
			}

			// State is "done" now
			state = 2;

			// Clear timeout if it exists
			if ( timeoutTimer ) {
				clearTimeout( timeoutTimer );
			}

			// Dereference transport for early garbage collection
			// (no matter how long the jqXHR object will be used)
			transport = undefined;

			// Cache response headers
			responseHeadersString = headers || "";

			// Set readyState
			jqXHR.readyState = status > 0 ? 4 : 0;

			// Determine if successful
			isSuccess = status >= 200 && status < 300 || status === 304;

			// Get response data
			if ( responses ) {
				response = ajaxHandleResponses( s, jqXHR, responses );
			}

			// Convert no matter what (that way responseXXX fields are always set)
			response = ajaxConvert( s, response, jqXHR, isSuccess );

			// If successful, handle type chaining
			if ( isSuccess ) {

				// Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
				if ( s.ifModified ) {
					modified = jqXHR.getResponseHeader("Last-Modified");
					if ( modified ) {
						jQuery.lastModified[ cacheURL ] = modified;
					}
					modified = jqXHR.getResponseHeader("etag");
					if ( modified ) {
						jQuery.etag[ cacheURL ] = modified;
					}
				}

				// if no content
				if ( status === 204 || s.type === "HEAD" ) {
					statusText = "nocontent";

				// if not modified
				} else if ( status === 304 ) {
					statusText = "notmodified";

				// If we have data, let's convert it
				} else {
					statusText = response.state;
					success = response.data;
					error = response.error;
					isSuccess = !error;
				}
			} else {
				// Extract error from statusText and normalize for non-aborts
				error = statusText;
				if ( status || !statusText ) {
					statusText = "error";
					if ( status < 0 ) {
						status = 0;
					}
				}
			}

			// Set data for the fake xhr object
			jqXHR.status = status;
			jqXHR.statusText = ( nativeStatusText || statusText ) + "";

			// Success/Error
			if ( isSuccess ) {
				deferred.resolveWith( callbackContext, [ success, statusText, jqXHR ] );
			} else {
				deferred.rejectWith( callbackContext, [ jqXHR, statusText, error ] );
			}

			// Status-dependent callbacks
			jqXHR.statusCode( statusCode );
			statusCode = undefined;

			if ( fireGlobals ) {
				globalEventContext.trigger( isSuccess ? "ajaxSuccess" : "ajaxError",
					[ jqXHR, s, isSuccess ? success : error ] );
			}

			// Complete
			completeDeferred.fireWith( callbackContext, [ jqXHR, statusText ] );

			if ( fireGlobals ) {
				globalEventContext.trigger( "ajaxComplete", [ jqXHR, s ] );
				// Handle the global AJAX counter
				if ( !( --jQuery.active ) ) {
					jQuery.event.trigger("ajaxStop");
				}
			}
		}

		return jqXHR;
	},

	getJSON: function( url, data, callback ) {
		return jQuery.get( url, data, callback, "json" );
	},

	getScript: function( url, callback ) {
		return jQuery.get( url, undefined, callback, "script" );
	}
});

jQuery.each( [ "get", "post" ], function( i, method ) {
	jQuery[ method ] = function( url, data, callback, type ) {
		// Shift arguments if data argument was omitted
		if ( jQuery.isFunction( data ) ) {
			type = type || callback;
			callback = data;
			data = undefined;
		}

		return jQuery.ajax({
			url: url,
			type: method,
			dataType: type,
			data: data,
			success: callback
		});
	};
});


jQuery._evalUrl = function( url ) {
	return jQuery.ajax({
		url: url,
		type: "GET",
		dataType: "script",
		async: false,
		global: false,
		"throws": true
	});
};


jQuery.fn.extend({
	wrapAll: function( html ) {
		var wrap;

		if ( jQuery.isFunction( html ) ) {
			return this.each(function( i ) {
				jQuery( this ).wrapAll( html.call(this, i) );
			});
		}

		if ( this[ 0 ] ) {

			// The elements to wrap the target around
			wrap = jQuery( html, this[ 0 ].ownerDocument ).eq( 0 ).clone( true );

			if ( this[ 0 ].parentNode ) {
				wrap.insertBefore( this[ 0 ] );
			}

			wrap.map(function() {
				var elem = this;

				while ( elem.firstElementChild ) {
					elem = elem.firstElementChild;
				}

				return elem;
			}).append( this );
		}

		return this;
	},

	wrapInner: function( html ) {
		if ( jQuery.isFunction( html ) ) {
			return this.each(function( i ) {
				jQuery( this ).wrapInner( html.call(this, i) );
			});
		}

		return this.each(function() {
			var self = jQuery( this ),
				contents = self.contents();

			if ( contents.length ) {
				contents.wrapAll( html );

			} else {
				self.append( html );
			}
		});
	},

	wrap: function( html ) {
		var isFunction = jQuery.isFunction( html );

		return this.each(function( i ) {
			jQuery( this ).wrapAll( isFunction ? html.call(this, i) : html );
		});
	},

	unwrap: function() {
		return this.parent().each(function() {
			if ( !jQuery.nodeName( this, "body" ) ) {
				jQuery( this ).replaceWith( this.childNodes );
			}
		}).end();
	}
});


jQuery.expr.filters.hidden = function( elem ) {
	// Support: Opera <= 12.12
	// Opera reports offsetWidths and offsetHeights less than zero on some elements
	return elem.offsetWidth <= 0 && elem.offsetHeight <= 0;
};
jQuery.expr.filters.visible = function( elem ) {
	return !jQuery.expr.filters.hidden( elem );
};




var r20 = /%20/g,
	rbracket = /\[\]$/,
	rCRLF = /\r?\n/g,
	rsubmitterTypes = /^(?:submit|button|image|reset|file)$/i,
	rsubmittable = /^(?:input|select|textarea|keygen)/i;

function buildParams( prefix, obj, traditional, add ) {
	var name;

	if ( jQuery.isArray( obj ) ) {
		// Serialize array item.
		jQuery.each( obj, function( i, v ) {
			if ( traditional || rbracket.test( prefix ) ) {
				// Treat each array item as a scalar.
				add( prefix, v );

			} else {
				// Item is non-scalar (array or object), encode its numeric index.
				buildParams( prefix + "[" + ( typeof v === "object" ? i : "" ) + "]", v, traditional, add );
			}
		});

	} else if ( !traditional && jQuery.type( obj ) === "object" ) {
		// Serialize object item.
		for ( name in obj ) {
			buildParams( prefix + "[" + name + "]", obj[ name ], traditional, add );
		}

	} else {
		// Serialize scalar item.
		add( prefix, obj );
	}
}

// Serialize an array of form elements or a set of
// key/values into a query string
jQuery.param = function( a, traditional ) {
	var prefix,
		s = [],
		add = function( key, value ) {
			// If value is a function, invoke it and return its value
			value = jQuery.isFunction( value ) ? value() : ( value == null ? "" : value );
			s[ s.length ] = encodeURIComponent( key ) + "=" + encodeURIComponent( value );
		};

	// Set traditional to true for jQuery <= 1.3.2 behavior.
	if ( traditional === undefined ) {
		traditional = jQuery.ajaxSettings && jQuery.ajaxSettings.traditional;
	}

	// If an array was passed in, assume that it is an array of form elements.
	if ( jQuery.isArray( a ) || ( a.jquery && !jQuery.isPlainObject( a ) ) ) {
		// Serialize the form elements
		jQuery.each( a, function() {
			add( this.name, this.value );
		});

	} else {
		// If traditional, encode the "old" way (the way 1.3.2 or older
		// did it), otherwise encode params recursively.
		for ( prefix in a ) {
			buildParams( prefix, a[ prefix ], traditional, add );
		}
	}

	// Return the resulting serialization
	return s.join( "&" ).replace( r20, "+" );
};

jQuery.fn.extend({
	serialize: function() {
		return jQuery.param( this.serializeArray() );
	},
	serializeArray: function() {
		return this.map(function() {
			// Can add propHook for "elements" to filter or add form elements
			var elements = jQuery.prop( this, "elements" );
			return elements ? jQuery.makeArray( elements ) : this;
		})
		.filter(function() {
			var type = this.type;

			// Use .is( ":disabled" ) so that fieldset[disabled] works
			return this.name && !jQuery( this ).is( ":disabled" ) &&
				rsubmittable.test( this.nodeName ) && !rsubmitterTypes.test( type ) &&
				( this.checked || !rcheckableType.test( type ) );
		})
		.map(function( i, elem ) {
			var val = jQuery( this ).val();

			return val == null ?
				null :
				jQuery.isArray( val ) ?
					jQuery.map( val, function( val ) {
						return { name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
					}) :
					{ name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
		}).get();
	}
});


jQuery.ajaxSettings.xhr = function() {
	try {
		return new XMLHttpRequest();
	} catch( e ) {}
};

var xhrId = 0,
	xhrCallbacks = {},
	xhrSuccessStatus = {
		// file protocol always yields status code 0, assume 200
		0: 200,
		// Support: IE9
		// #1450: sometimes IE returns 1223 when it should be 204
		1223: 204
	},
	xhrSupported = jQuery.ajaxSettings.xhr();

// Support: IE9
// Open requests must be manually aborted on unload (#5280)
// See https://support.microsoft.com/kb/2856746 for more info
if ( window.attachEvent ) {
	window.attachEvent( "onunload", function() {
		for ( var key in xhrCallbacks ) {
			xhrCallbacks[ key ]();
		}
	});
}

support.cors = !!xhrSupported && ( "withCredentials" in xhrSupported );
support.ajax = xhrSupported = !!xhrSupported;

jQuery.ajaxTransport(function( options ) {
	var callback;

	// Cross domain only allowed if supported through XMLHttpRequest
	if ( support.cors || xhrSupported && !options.crossDomain ) {
		return {
			send: function( headers, complete ) {
				var i,
					xhr = options.xhr(),
					id = ++xhrId;

				xhr.open( options.type, options.url, options.async, options.username, options.password );

				// Apply custom fields if provided
				if ( options.xhrFields ) {
					for ( i in options.xhrFields ) {
						xhr[ i ] = options.xhrFields[ i ];
					}
				}

				// Override mime type if needed
				if ( options.mimeType && xhr.overrideMimeType ) {
					xhr.overrideMimeType( options.mimeType );
				}

				// X-Requested-With header
				// For cross-domain requests, seeing as conditions for a preflight are
				// akin to a jigsaw puzzle, we simply never set it to be sure.
				// (it can always be set on a per-request basis or even using ajaxSetup)
				// For same-domain requests, won't change header if already provided.
				if ( !options.crossDomain && !headers["X-Requested-With"] ) {
					headers["X-Requested-With"] = "XMLHttpRequest";
				}

				// Set headers
				for ( i in headers ) {
					xhr.setRequestHeader( i, headers[ i ] );
				}

				// Callback
				callback = function( type ) {
					return function() {
						if ( callback ) {
							delete xhrCallbacks[ id ];
							callback = xhr.onload = xhr.onerror = null;

							if ( type === "abort" ) {
								xhr.abort();
							} else if ( type === "error" ) {
								complete(
									// file: protocol always yields status 0; see #8605, #14207
									xhr.status,
									xhr.statusText
								);
							} else {
								complete(
									xhrSuccessStatus[ xhr.status ] || xhr.status,
									xhr.statusText,
									// Support: IE9
									// Accessing binary-data responseText throws an exception
									// (#11426)
									typeof xhr.responseText === "string" ? {
										text: xhr.responseText
									} : undefined,
									xhr.getAllResponseHeaders()
								);
							}
						}
					};
				};

				// Listen to events
				xhr.onload = callback();
				xhr.onerror = callback("error");

				// Create the abort callback
				callback = xhrCallbacks[ id ] = callback("abort");

				try {
					// Do send the request (this may raise an exception)
					xhr.send( options.hasContent && options.data || null );
				} catch ( e ) {
					// #14683: Only rethrow if this hasn't been notified as an error yet
					if ( callback ) {
						throw e;
					}
				}
			},

			abort: function() {
				if ( callback ) {
					callback();
				}
			}
		};
	}
});




// Install script dataType
jQuery.ajaxSetup({
	accepts: {
		script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
	},
	contents: {
		script: /(?:java|ecma)script/
	},
	converters: {
		"text script": function( text ) {
			jQuery.globalEval( text );
			return text;
		}
	}
});

// Handle cache's special case and crossDomain
jQuery.ajaxPrefilter( "script", function( s ) {
	if ( s.cache === undefined ) {
		s.cache = false;
	}
	if ( s.crossDomain ) {
		s.type = "GET";
	}
});

// Bind script tag hack transport
jQuery.ajaxTransport( "script", function( s ) {
	// This transport only deals with cross domain requests
	if ( s.crossDomain ) {
		var script, callback;
		return {
			send: function( _, complete ) {
				script = jQuery("<script>").prop({
					async: true,
					charset: s.scriptCharset,
					src: s.url
				}).on(
					"load error",
					callback = function( evt ) {
						script.remove();
						callback = null;
						if ( evt ) {
							complete( evt.type === "error" ? 404 : 200, evt.type );
						}
					}
				);
				document.head.appendChild( script[ 0 ] );
			},
			abort: function() {
				if ( callback ) {
					callback();
				}
			}
		};
	}
});




var oldCallbacks = [],
	rjsonp = /(=)\?(?=&|$)|\?\?/;

// Default jsonp settings
jQuery.ajaxSetup({
	jsonp: "callback",
	jsonpCallback: function() {
		var callback = oldCallbacks.pop() || ( jQuery.expando + "_" + ( nonce++ ) );
		this[ callback ] = true;
		return callback;
	}
});

// Detect, normalize options and install callbacks for jsonp requests
jQuery.ajaxPrefilter( "json jsonp", function( s, originalSettings, jqXHR ) {

	var callbackName, overwritten, responseContainer,
		jsonProp = s.jsonp !== false && ( rjsonp.test( s.url ) ?
			"url" :
			typeof s.data === "string" && !( s.contentType || "" ).indexOf("application/x-www-form-urlencoded") && rjsonp.test( s.data ) && "data"
		);

	// Handle iff the expected data type is "jsonp" or we have a parameter to set
	if ( jsonProp || s.dataTypes[ 0 ] === "jsonp" ) {

		// Get callback name, remembering preexisting value associated with it
		callbackName = s.jsonpCallback = jQuery.isFunction( s.jsonpCallback ) ?
			s.jsonpCallback() :
			s.jsonpCallback;

		// Insert callback into url or form data
		if ( jsonProp ) {
			s[ jsonProp ] = s[ jsonProp ].replace( rjsonp, "$1" + callbackName );
		} else if ( s.jsonp !== false ) {
			s.url += ( rquery.test( s.url ) ? "&" : "?" ) + s.jsonp + "=" + callbackName;
		}

		// Use data converter to retrieve json after script execution
		s.converters["script json"] = function() {
			if ( !responseContainer ) {
				jQuery.error( callbackName + " was not called" );
			}
			return responseContainer[ 0 ];
		};

		// force json dataType
		s.dataTypes[ 0 ] = "json";

		// Install callback
		overwritten = window[ callbackName ];
		window[ callbackName ] = function() {
			responseContainer = arguments;
		};

		// Clean-up function (fires after converters)
		jqXHR.always(function() {
			// Restore preexisting value
			window[ callbackName ] = overwritten;

			// Save back as free
			if ( s[ callbackName ] ) {
				// make sure that re-using the options doesn't screw things around
				s.jsonpCallback = originalSettings.jsonpCallback;

				// save the callback name for future use
				oldCallbacks.push( callbackName );
			}

			// Call if it was a function and we have a response
			if ( responseContainer && jQuery.isFunction( overwritten ) ) {
				overwritten( responseContainer[ 0 ] );
			}

			responseContainer = overwritten = undefined;
		});

		// Delegate to script
		return "script";
	}
});




// data: string of html
// context (optional): If specified, the fragment will be created in this context, defaults to document
// keepScripts (optional): If true, will include scripts passed in the html string
jQuery.parseHTML = function( data, context, keepScripts ) {
	if ( !data || typeof data !== "string" ) {
		return null;
	}
	if ( typeof context === "boolean" ) {
		keepScripts = context;
		context = false;
	}
	context = context || document;

	var parsed = rsingleTag.exec( data ),
		scripts = !keepScripts && [];

	// Single tag
	if ( parsed ) {
		return [ context.createElement( parsed[1] ) ];
	}

	parsed = jQuery.buildFragment( [ data ], context, scripts );

	if ( scripts && scripts.length ) {
		jQuery( scripts ).remove();
	}

	return jQuery.merge( [], parsed.childNodes );
};


// Keep a copy of the old load method
var _load = jQuery.fn.load;

/**
 * Load a url into a page
 */
jQuery.fn.load = function( url, params, callback ) {
	if ( typeof url !== "string" && _load ) {
		return _load.apply( this, arguments );
	}

	var selector, type, response,
		self = this,
		off = url.indexOf(" ");

	if ( off >= 0 ) {
		selector = jQuery.trim( url.slice( off ) );
		url = url.slice( 0, off );
	}

	// If it's a function
	if ( jQuery.isFunction( params ) ) {

		// We assume that it's the callback
		callback = params;
		params = undefined;

	// Otherwise, build a param string
	} else if ( params && typeof params === "object" ) {
		type = "POST";
	}

	// If we have elements to modify, make the request
	if ( self.length > 0 ) {
		jQuery.ajax({
			url: url,

			// if "type" variable is undefined, then "GET" method will be used
			type: type,
			dataType: "html",
			data: params
		}).done(function( responseText ) {

			// Save response for use in complete callback
			response = arguments;

			self.html( selector ?

				// If a selector was specified, locate the right elements in a dummy div
				// Exclude scripts to avoid IE 'Permission Denied' errors
				jQuery("<div>").append( jQuery.parseHTML( responseText ) ).find( selector ) :

				// Otherwise use the full result
				responseText );

		}).complete( callback && function( jqXHR, status ) {
			self.each( callback, response || [ jqXHR.responseText, status, jqXHR ] );
		});
	}

	return this;
};




// Attach a bunch of functions for handling common AJAX events
jQuery.each( [ "ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend" ], function( i, type ) {
	jQuery.fn[ type ] = function( fn ) {
		return this.on( type, fn );
	};
});




jQuery.expr.filters.animated = function( elem ) {
	return jQuery.grep(jQuery.timers, function( fn ) {
		return elem === fn.elem;
	}).length;
};




var docElem = window.document.documentElement;

/**
 * Gets a window from an element
 */
function getWindow( elem ) {
	return jQuery.isWindow( elem ) ? elem : elem.nodeType === 9 && elem.defaultView;
}

jQuery.offset = {
	setOffset: function( elem, options, i ) {
		var curPosition, curLeft, curCSSTop, curTop, curOffset, curCSSLeft, calculatePosition,
			position = jQuery.css( elem, "position" ),
			curElem = jQuery( elem ),
			props = {};

		// Set position first, in-case top/left are set even on static elem
		if ( position === "static" ) {
			elem.style.position = "relative";
		}

		curOffset = curElem.offset();
		curCSSTop = jQuery.css( elem, "top" );
		curCSSLeft = jQuery.css( elem, "left" );
		calculatePosition = ( position === "absolute" || position === "fixed" ) &&
			( curCSSTop + curCSSLeft ).indexOf("auto") > -1;

		// Need to be able to calculate position if either
		// top or left is auto and position is either absolute or fixed
		if ( calculatePosition ) {
			curPosition = curElem.position();
			curTop = curPosition.top;
			curLeft = curPosition.left;

		} else {
			curTop = parseFloat( curCSSTop ) || 0;
			curLeft = parseFloat( curCSSLeft ) || 0;
		}

		if ( jQuery.isFunction( options ) ) {
			options = options.call( elem, i, curOffset );
		}

		if ( options.top != null ) {
			props.top = ( options.top - curOffset.top ) + curTop;
		}
		if ( options.left != null ) {
			props.left = ( options.left - curOffset.left ) + curLeft;
		}

		if ( "using" in options ) {
			options.using.call( elem, props );

		} else {
			curElem.css( props );
		}
	}
};

jQuery.fn.extend({
	offset: function( options ) {
		if ( arguments.length ) {
			return options === undefined ?
				this :
				this.each(function( i ) {
					jQuery.offset.setOffset( this, options, i );
				});
		}

		var docElem, win,
			elem = this[ 0 ],
			box = { top: 0, left: 0 },
			doc = elem && elem.ownerDocument;

		if ( !doc ) {
			return;
		}

		docElem = doc.documentElement;

		// Make sure it's not a disconnected DOM node
		if ( !jQuery.contains( docElem, elem ) ) {
			return box;
		}

		// Support: BlackBerry 5, iOS 3 (original iPhone)
		// If we don't have gBCR, just use 0,0 rather than error
		if ( typeof elem.getBoundingClientRect !== strundefined ) {
			box = elem.getBoundingClientRect();
		}
		win = getWindow( doc );
		return {
			top: box.top + win.pageYOffset - docElem.clientTop,
			left: box.left + win.pageXOffset - docElem.clientLeft
		};
	},

	position: function() {
		if ( !this[ 0 ] ) {
			return;
		}

		var offsetParent, offset,
			elem = this[ 0 ],
			parentOffset = { top: 0, left: 0 };

		// Fixed elements are offset from window (parentOffset = {top:0, left: 0}, because it is its only offset parent
		if ( jQuery.css( elem, "position" ) === "fixed" ) {
			// Assume getBoundingClientRect is there when computed position is fixed
			offset = elem.getBoundingClientRect();

		} else {
			// Get *real* offsetParent
			offsetParent = this.offsetParent();

			// Get correct offsets
			offset = this.offset();
			if ( !jQuery.nodeName( offsetParent[ 0 ], "html" ) ) {
				parentOffset = offsetParent.offset();
			}

			// Add offsetParent borders
			parentOffset.top += jQuery.css( offsetParent[ 0 ], "borderTopWidth", true );
			parentOffset.left += jQuery.css( offsetParent[ 0 ], "borderLeftWidth", true );
		}

		// Subtract parent offsets and element margins
		return {
			top: offset.top - parentOffset.top - jQuery.css( elem, "marginTop", true ),
			left: offset.left - parentOffset.left - jQuery.css( elem, "marginLeft", true )
		};
	},

	offsetParent: function() {
		return this.map(function() {
			var offsetParent = this.offsetParent || docElem;

			while ( offsetParent && ( !jQuery.nodeName( offsetParent, "html" ) && jQuery.css( offsetParent, "position" ) === "static" ) ) {
				offsetParent = offsetParent.offsetParent;
			}

			return offsetParent || docElem;
		});
	}
});

// Create scrollLeft and scrollTop methods
jQuery.each( { scrollLeft: "pageXOffset", scrollTop: "pageYOffset" }, function( method, prop ) {
	var top = "pageYOffset" === prop;

	jQuery.fn[ method ] = function( val ) {
		return access( this, function( elem, method, val ) {
			var win = getWindow( elem );

			if ( val === undefined ) {
				return win ? win[ prop ] : elem[ method ];
			}

			if ( win ) {
				win.scrollTo(
					!top ? val : window.pageXOffset,
					top ? val : window.pageYOffset
				);

			} else {
				elem[ method ] = val;
			}
		}, method, val, arguments.length, null );
	};
});

// Support: Safari<7+, Chrome<37+
// Add the top/left cssHooks using jQuery.fn.position
// Webkit bug: https://bugs.webkit.org/show_bug.cgi?id=29084
// Blink bug: https://code.google.com/p/chromium/issues/detail?id=229280
// getComputedStyle returns percent when specified for top/left/bottom/right;
// rather than make the css module depend on the offset module, just check for it here
jQuery.each( [ "top", "left" ], function( i, prop ) {
	jQuery.cssHooks[ prop ] = addGetHookIf( support.pixelPosition,
		function( elem, computed ) {
			if ( computed ) {
				computed = curCSS( elem, prop );
				// If curCSS returns percentage, fallback to offset
				return rnumnonpx.test( computed ) ?
					jQuery( elem ).position()[ prop ] + "px" :
					computed;
			}
		}
	);
});


// Create innerHeight, innerWidth, height, width, outerHeight and outerWidth methods
jQuery.each( { Height: "height", Width: "width" }, function( name, type ) {
	jQuery.each( { padding: "inner" + name, content: type, "": "outer" + name }, function( defaultExtra, funcName ) {
		// Margin is only for outerHeight, outerWidth
		jQuery.fn[ funcName ] = function( margin, value ) {
			var chainable = arguments.length && ( defaultExtra || typeof margin !== "boolean" ),
				extra = defaultExtra || ( margin === true || value === true ? "margin" : "border" );

			return access( this, function( elem, type, value ) {
				var doc;

				if ( jQuery.isWindow( elem ) ) {
					// As of 5/8/2012 this will yield incorrect results for Mobile Safari, but there
					// isn't a whole lot we can do. See pull request at this URL for discussion:
					// https://github.com/jquery/jquery/pull/764
					return elem.document.documentElement[ "client" + name ];
				}

				// Get document width or height
				if ( elem.nodeType === 9 ) {
					doc = elem.documentElement;

					// Either scroll[Width/Height] or offset[Width/Height] or client[Width/Height],
					// whichever is greatest
					return Math.max(
						elem.body[ "scroll" + name ], doc[ "scroll" + name ],
						elem.body[ "offset" + name ], doc[ "offset" + name ],
						doc[ "client" + name ]
					);
				}

				return value === undefined ?
					// Get width or height on the element, requesting but not forcing parseFloat
					jQuery.css( elem, type, extra ) :

					// Set width or height on the element
					jQuery.style( elem, type, value, extra );
			}, type, chainable ? margin : undefined, chainable, null );
		};
	});
});


// The number of elements contained in the matched element set
jQuery.fn.size = function() {
	return this.length;
};

jQuery.fn.andSelf = jQuery.fn.addBack;




// Register as a named AMD module, since jQuery can be concatenated with other
// files that may use define, but not via a proper concatenation script that
// understands anonymous AMD modules. A named AMD is safest and most robust
// way to register. Lowercase jquery is used because AMD module names are
// derived from file names, and jQuery is normally delivered in a lowercase
// file name. Do this after creating the global so that if an AMD module wants
// to call noConflict to hide this version of jQuery, it will work.

// Note that for maximum portability, libraries that are not jQuery should
// declare themselves as anonymous modules, and avoid setting a global if an
// AMD loader is present. jQuery is a special case. For more information, see
// https://github.com/jrburke/requirejs/wiki/Updating-existing-libraries#wiki-anon

if ( typeof define === "function" && define.amd ) {
	define( "jquery", [], function() {
		return jQuery;
	});
}




var
	// Map over jQuery in case of overwrite
	_jQuery = window.jQuery,

	// Map over the $ in case of overwrite
	_$ = window.$;

jQuery.noConflict = function( deep ) {
	if ( window.$ === jQuery ) {
		window.$ = _$;
	}

	if ( deep && window.jQuery === jQuery ) {
		window.jQuery = _jQuery;
	}

	return jQuery;
};

// Expose jQuery and $ identifiers, even in AMD
// (#7102#comment:10, https://github.com/jquery/jquery/pull/557)
// and CommonJS for browser emulators (#13566)
if ( typeof noGlobal === strundefined ) {
	window.jQuery = window.$ = jQuery;
}




return jQuery;

}));
;/*!
 * Bootstrap v3.3.4 (http://getbootstrap.com)
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 */

if (typeof jQuery === 'undefined') {
  throw new Error('Bootstrap\'s JavaScript requires jQuery')
}

+function ($) {
  'use strict';
  var version = $.fn.jquery.split(' ')[0].split('.')
  if ((version[0] < 2 && version[1] < 9) || (version[0] == 1 && version[1] == 9 && version[2] < 1)) {
    throw new Error('Bootstrap\'s JavaScript requires jQuery version 1.9.1 or higher')
  }
}(jQuery);

/* ========================================================================
 * Bootstrap: transition.js v3.3.4
 * http://getbootstrap.com/javascript/#transitions
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // CSS TRANSITION SUPPORT (Shoutout: http://www.modernizr.com/)
  // ============================================================

  function transitionEnd() {
    var el = document.createElement('bootstrap')

    var transEndEventNames = {
      WebkitTransition : 'webkitTransitionEnd',
      MozTransition    : 'transitionend',
      OTransition      : 'oTransitionEnd otransitionend',
      transition       : 'transitionend'
    }

    for (var name in transEndEventNames) {
      if (el.style[name] !== undefined) {
        return { end: transEndEventNames[name] }
      }
    }

    return false // explicit for ie8 (  ._.)
  }

  // http://blog.alexmaccaw.com/css-transitions
  $.fn.emulateTransitionEnd = function (duration) {
    var called = false
    var $el = this
    $(this).one('bsTransitionEnd', function () { called = true })
    var callback = function () { if (!called) $($el).trigger($.support.transition.end) }
    setTimeout(callback, duration)
    return this
  }

  $(function () {
    $.support.transition = transitionEnd()

    if (!$.support.transition) return

    $.event.special.bsTransitionEnd = {
      bindType: $.support.transition.end,
      delegateType: $.support.transition.end,
      handle: function (e) {
        if ($(e.target).is(this)) return e.handleObj.handler.apply(this, arguments)
      }
    }
  })

}(jQuery);

/* ========================================================================
 * Bootstrap: alert.js v3.3.4
 * http://getbootstrap.com/javascript/#alerts
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // ALERT CLASS DEFINITION
  // ======================

  var dismiss = '[data-dismiss="alert"]'
  var Alert   = function (el) {
    $(el).on('click', dismiss, this.close)
  }

  Alert.VERSION = '3.3.4'

  Alert.TRANSITION_DURATION = 150

  Alert.prototype.close = function (e) {
    var $this    = $(this)
    var selector = $this.attr('data-target')

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') // strip for ie7
    }

    var $parent = $(selector)

    if (e) e.preventDefault()

    if (!$parent.length) {
      $parent = $this.closest('.alert')
    }

    $parent.trigger(e = $.Event('close.bs.alert'))

    if (e.isDefaultPrevented()) return

    $parent.removeClass('in')

    function removeElement() {
      // detach from parent, fire event then clean up data
      $parent.detach().trigger('closed.bs.alert').remove()
    }

    $.support.transition && $parent.hasClass('fade') ?
      $parent
        .one('bsTransitionEnd', removeElement)
        .emulateTransitionEnd(Alert.TRANSITION_DURATION) :
      removeElement()
  }


  // ALERT PLUGIN DEFINITION
  // =======================

  function Plugin(option) {
    return this.each(function () {
      var $this = $(this)
      var data  = $this.data('bs.alert')

      if (!data) $this.data('bs.alert', (data = new Alert(this)))
      if (typeof option == 'string') data[option].call($this)
    })
  }

  var old = $.fn.alert

  $.fn.alert             = Plugin
  $.fn.alert.Constructor = Alert


  // ALERT NO CONFLICT
  // =================

  $.fn.alert.noConflict = function () {
    $.fn.alert = old
    return this
  }


  // ALERT DATA-API
  // ==============

  $(document).on('click.bs.alert.data-api', dismiss, Alert.prototype.close)

}(jQuery);

/* ========================================================================
 * Bootstrap: button.js v3.3.4
 * http://getbootstrap.com/javascript/#buttons
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // BUTTON PUBLIC CLASS DEFINITION
  // ==============================

  var Button = function (element, options) {
    this.$element  = $(element)
    this.options   = $.extend({}, Button.DEFAULTS, options)
    this.isLoading = false
  }

  Button.VERSION  = '3.3.4'

  Button.DEFAULTS = {
    loadingText: 'loading...'
  }

  Button.prototype.setState = function (state) {
    var d    = 'disabled'
    var $el  = this.$element
    var val  = $el.is('input') ? 'val' : 'html'
    var data = $el.data()

    state = state + 'Text'

    if (data.resetText == null) $el.data('resetText', $el[val]())

    // push to event loop to allow forms to submit
    setTimeout($.proxy(function () {
      $el[val](data[state] == null ? this.options[state] : data[state])

      if (state == 'loadingText') {
        this.isLoading = true
        $el.addClass(d).attr(d, d)
      } else if (this.isLoading) {
        this.isLoading = false
        $el.removeClass(d).removeAttr(d)
      }
    }, this), 0)
  }

  Button.prototype.toggle = function () {
    var changed = true
    var $parent = this.$element.closest('[data-toggle="buttons"]')

    if ($parent.length) {
      var $input = this.$element.find('input')
      if ($input.prop('type') == 'radio') {
        if ($input.prop('checked') && this.$element.hasClass('active')) changed = false
        else $parent.find('.active').removeClass('active')
      }
      if (changed) $input.prop('checked', !this.$element.hasClass('active')).trigger('change')
    } else {
      this.$element.attr('aria-pressed', !this.$element.hasClass('active'))
    }

    if (changed) this.$element.toggleClass('active')
  }


  // BUTTON PLUGIN DEFINITION
  // ========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.button')
      var options = typeof option == 'object' && option

      if (!data) $this.data('bs.button', (data = new Button(this, options)))

      if (option == 'toggle') data.toggle()
      else if (option) data.setState(option)
    })
  }

  var old = $.fn.button

  $.fn.button             = Plugin
  $.fn.button.Constructor = Button


  // BUTTON NO CONFLICT
  // ==================

  $.fn.button.noConflict = function () {
    $.fn.button = old
    return this
  }


  // BUTTON DATA-API
  // ===============

  $(document)
    .on('click.bs.button.data-api', '[data-toggle^="button"]', function (e) {
      var $btn = $(e.target)
      if (!$btn.hasClass('btn')) $btn = $btn.closest('.btn')
      Plugin.call($btn, 'toggle')
      e.preventDefault()
    })
    .on('focus.bs.button.data-api blur.bs.button.data-api', '[data-toggle^="button"]', function (e) {
      $(e.target).closest('.btn').toggleClass('focus', /^focus(in)?$/.test(e.type))
    })

}(jQuery);

/* ========================================================================
 * Bootstrap: carousel.js v3.3.4
 * http://getbootstrap.com/javascript/#carousel
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // CAROUSEL CLASS DEFINITION
  // =========================

  var Carousel = function (element, options) {
    this.$element    = $(element)
    this.$indicators = this.$element.find('.carousel-indicators')
    this.options     = options
    this.paused      = null
    this.sliding     = null
    this.interval    = null
    this.$active     = null
    this.$items      = null

    this.options.keyboard && this.$element.on('keydown.bs.carousel', $.proxy(this.keydown, this))

    this.options.pause == 'hover' && !('ontouchstart' in document.documentElement) && this.$element
      .on('mouseenter.bs.carousel', $.proxy(this.pause, this))
      .on('mouseleave.bs.carousel', $.proxy(this.cycle, this))
  }

  Carousel.VERSION  = '3.3.4'

  Carousel.TRANSITION_DURATION = 600

  Carousel.DEFAULTS = {
    interval: 5000,
    pause: 'hover',
    wrap: true,
    keyboard: true
  }

  Carousel.prototype.keydown = function (e) {
    if (/input|textarea/i.test(e.target.tagName)) return
    switch (e.which) {
      case 37: this.prev(); break
      case 39: this.next(); break
      default: return
    }

    e.preventDefault()
  }

  Carousel.prototype.cycle = function (e) {
    e || (this.paused = false)

    this.interval && clearInterval(this.interval)

    this.options.interval
      && !this.paused
      && (this.interval = setInterval($.proxy(this.next, this), this.options.interval))

    return this
  }

  Carousel.prototype.getItemIndex = function (item) {
    this.$items = item.parent().children('.item')
    return this.$items.index(item || this.$active)
  }

  Carousel.prototype.getItemForDirection = function (direction, active) {
    var activeIndex = this.getItemIndex(active)
    var willWrap = (direction == 'prev' && activeIndex === 0)
                || (direction == 'next' && activeIndex == (this.$items.length - 1))
    if (willWrap && !this.options.wrap) return active
    var delta = direction == 'prev' ? -1 : 1
    var itemIndex = (activeIndex + delta) % this.$items.length
    return this.$items.eq(itemIndex)
  }

  Carousel.prototype.to = function (pos) {
    var that        = this
    var activeIndex = this.getItemIndex(this.$active = this.$element.find('.item.active'))

    if (pos > (this.$items.length - 1) || pos < 0) return

    if (this.sliding)       return this.$element.one('slid.bs.carousel', function () { that.to(pos) }) // yes, "slid"
    if (activeIndex == pos) return this.pause().cycle()

    return this.slide(pos > activeIndex ? 'next' : 'prev', this.$items.eq(pos))
  }

  Carousel.prototype.pause = function (e) {
    e || (this.paused = true)

    if (this.$element.find('.next, .prev').length && $.support.transition) {
      this.$element.trigger($.support.transition.end)
      this.cycle(true)
    }

    this.interval = clearInterval(this.interval)

    return this
  }

  Carousel.prototype.next = function () {
    if (this.sliding) return
    return this.slide('next')
  }

  Carousel.prototype.prev = function () {
    if (this.sliding) return
    return this.slide('prev')
  }

  Carousel.prototype.slide = function (type, next) {
    var $active   = this.$element.find('.item.active')
    var $next     = next || this.getItemForDirection(type, $active)
    var isCycling = this.interval
    var direction = type == 'next' ? 'left' : 'right'
    var that      = this

    if ($next.hasClass('active')) return (this.sliding = false)

    var relatedTarget = $next[0]
    var slideEvent = $.Event('slide.bs.carousel', {
      relatedTarget: relatedTarget,
      direction: direction
    })
    this.$element.trigger(slideEvent)
    if (slideEvent.isDefaultPrevented()) return

    this.sliding = true

    isCycling && this.pause()

    if (this.$indicators.length) {
      this.$indicators.find('.active').removeClass('active')
      var $nextIndicator = $(this.$indicators.children()[this.getItemIndex($next)])
      $nextIndicator && $nextIndicator.addClass('active')
    }

    var slidEvent = $.Event('slid.bs.carousel', { relatedTarget: relatedTarget, direction: direction }) // yes, "slid"
    if ($.support.transition && this.$element.hasClass('slide')) {
      $next.addClass(type)
      $next[0].offsetWidth // force reflow
      $active.addClass(direction)
      $next.addClass(direction)
      $active
        .one('bsTransitionEnd', function () {
          $next.removeClass([type, direction].join(' ')).addClass('active')
          $active.removeClass(['active', direction].join(' '))
          that.sliding = false
          setTimeout(function () {
            that.$element.trigger(slidEvent)
          }, 0)
        })
        .emulateTransitionEnd(Carousel.TRANSITION_DURATION)
    } else {
      $active.removeClass('active')
      $next.addClass('active')
      this.sliding = false
      this.$element.trigger(slidEvent)
    }

    isCycling && this.cycle()

    return this
  }


  // CAROUSEL PLUGIN DEFINITION
  // ==========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.carousel')
      var options = $.extend({}, Carousel.DEFAULTS, $this.data(), typeof option == 'object' && option)
      var action  = typeof option == 'string' ? option : options.slide

      if (!data) $this.data('bs.carousel', (data = new Carousel(this, options)))
      if (typeof option == 'number') data.to(option)
      else if (action) data[action]()
      else if (options.interval) data.pause().cycle()
    })
  }

  var old = $.fn.carousel

  $.fn.carousel             = Plugin
  $.fn.carousel.Constructor = Carousel


  // CAROUSEL NO CONFLICT
  // ====================

  $.fn.carousel.noConflict = function () {
    $.fn.carousel = old
    return this
  }


  // CAROUSEL DATA-API
  // =================

  var clickHandler = function (e) {
    var href
    var $this   = $(this)
    var $target = $($this.attr('data-target') || (href = $this.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '')) // strip for ie7
    if (!$target.hasClass('carousel')) return
    var options = $.extend({}, $target.data(), $this.data())
    var slideIndex = $this.attr('data-slide-to')
    if (slideIndex) options.interval = false

    Plugin.call($target, options)

    if (slideIndex) {
      $target.data('bs.carousel').to(slideIndex)
    }

    e.preventDefault()
  }

  $(document)
    .on('click.bs.carousel.data-api', '[data-slide]', clickHandler)
    .on('click.bs.carousel.data-api', '[data-slide-to]', clickHandler)

  $(window).on('load', function () {
    $('[data-ride="carousel"]').each(function () {
      var $carousel = $(this)
      Plugin.call($carousel, $carousel.data())
    })
  })

}(jQuery);

/* ========================================================================
 * Bootstrap: collapse.js v3.3.4
 * http://getbootstrap.com/javascript/#collapse
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // COLLAPSE PUBLIC CLASS DEFINITION
  // ================================

  var Collapse = function (element, options) {
    this.$element      = $(element)
    this.options       = $.extend({}, Collapse.DEFAULTS, options)
    this.$trigger      = $('[data-toggle="collapse"][href="#' + element.id + '"],' +
                           '[data-toggle="collapse"][data-target="#' + element.id + '"]')
    this.transitioning = null

    if (this.options.parent) {
      this.$parent = this.getParent()
    } else {
      this.addAriaAndCollapsedClass(this.$element, this.$trigger)
    }

    if (this.options.toggle) this.toggle()
  }

  Collapse.VERSION  = '3.3.4'

  Collapse.TRANSITION_DURATION = 350

  Collapse.DEFAULTS = {
    toggle: true
  }

  Collapse.prototype.dimension = function () {
    var hasWidth = this.$element.hasClass('width')
    return hasWidth ? 'width' : 'height'
  }

  Collapse.prototype.show = function () {
    if (this.transitioning || this.$element.hasClass('in')) return

    var activesData
    var actives = this.$parent && this.$parent.children('.panel').children('.in, .collapsing')

    if (actives && actives.length) {
      activesData = actives.data('bs.collapse')
      if (activesData && activesData.transitioning) return
    }

    var startEvent = $.Event('show.bs.collapse')
    this.$element.trigger(startEvent)
    if (startEvent.isDefaultPrevented()) return

    if (actives && actives.length) {
      Plugin.call(actives, 'hide')
      activesData || actives.data('bs.collapse', null)
    }

    var dimension = this.dimension()

    this.$element
      .removeClass('collapse')
      .addClass('collapsing')[dimension](0)
      .attr('aria-expanded', true)

    this.$trigger
      .removeClass('collapsed')
      .attr('aria-expanded', true)

    this.transitioning = 1

    var complete = function () {
      this.$element
        .removeClass('collapsing')
        .addClass('collapse in')[dimension]('')
      this.transitioning = 0
      this.$element
        .trigger('shown.bs.collapse')
    }

    if (!$.support.transition) return complete.call(this)

    var scrollSize = $.camelCase(['scroll', dimension].join('-'))

    this.$element
      .one('bsTransitionEnd', $.proxy(complete, this))
      .emulateTransitionEnd(Collapse.TRANSITION_DURATION)[dimension](this.$element[0][scrollSize])
  }

  Collapse.prototype.hide = function () {
    if (this.transitioning || !this.$element.hasClass('in')) return

    var startEvent = $.Event('hide.bs.collapse')
    this.$element.trigger(startEvent)
    if (startEvent.isDefaultPrevented()) return

    var dimension = this.dimension()

    this.$element[dimension](this.$element[dimension]())[0].offsetHeight

    this.$element
      .addClass('collapsing')
      .removeClass('collapse in')
      .attr('aria-expanded', false)

    this.$trigger
      .addClass('collapsed')
      .attr('aria-expanded', false)

    this.transitioning = 1

    var complete = function () {
      this.transitioning = 0
      this.$element
        .removeClass('collapsing')
        .addClass('collapse')
        .trigger('hidden.bs.collapse')
    }

    if (!$.support.transition) return complete.call(this)

    this.$element
      [dimension](0)
      .one('bsTransitionEnd', $.proxy(complete, this))
      .emulateTransitionEnd(Collapse.TRANSITION_DURATION)
  }

  Collapse.prototype.toggle = function () {
    this[this.$element.hasClass('in') ? 'hide' : 'show']()
  }

  Collapse.prototype.getParent = function () {
    return $(this.options.parent)
      .find('[data-toggle="collapse"][data-parent="' + this.options.parent + '"]')
      .each($.proxy(function (i, element) {
        var $element = $(element)
        this.addAriaAndCollapsedClass(getTargetFromTrigger($element), $element)
      }, this))
      .end()
  }

  Collapse.prototype.addAriaAndCollapsedClass = function ($element, $trigger) {
    var isOpen = $element.hasClass('in')

    $element.attr('aria-expanded', isOpen)
    $trigger
      .toggleClass('collapsed', !isOpen)
      .attr('aria-expanded', isOpen)
  }

  function getTargetFromTrigger($trigger) {
    var href
    var target = $trigger.attr('data-target')
      || (href = $trigger.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '') // strip for ie7

    return $(target)
  }


  // COLLAPSE PLUGIN DEFINITION
  // ==========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.collapse')
      var options = $.extend({}, Collapse.DEFAULTS, $this.data(), typeof option == 'object' && option)

      if (!data && options.toggle && /show|hide/.test(option)) options.toggle = false
      if (!data) $this.data('bs.collapse', (data = new Collapse(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.collapse

  $.fn.collapse             = Plugin
  $.fn.collapse.Constructor = Collapse


  // COLLAPSE NO CONFLICT
  // ====================

  $.fn.collapse.noConflict = function () {
    $.fn.collapse = old
    return this
  }


  // COLLAPSE DATA-API
  // =================

  $(document).on('click.bs.collapse.data-api', '[data-toggle="collapse"]', function (e) {
    var $this   = $(this)

    if (!$this.attr('data-target')) e.preventDefault()

    var $target = getTargetFromTrigger($this)
    var data    = $target.data('bs.collapse')
    var option  = data ? 'toggle' : $this.data()

    Plugin.call($target, option)
  })

}(jQuery);

/* ========================================================================
 * Bootstrap: dropdown.js v3.3.4
 * http://getbootstrap.com/javascript/#dropdowns
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // DROPDOWN CLASS DEFINITION
  // =========================

  var backdrop = '.dropdown-backdrop'
  var toggle   = '[data-toggle="dropdown"]'
  var Dropdown = function (element) {
    $(element).on('click.bs.dropdown', this.toggle)
  }

  Dropdown.VERSION = '3.3.4'

  Dropdown.prototype.toggle = function (e) {
    var $this = $(this)

    if ($this.is('.disabled, :disabled')) return

    var $parent  = getParent($this)
    var isActive = $parent.hasClass('open')

    clearMenus()

    if (!isActive) {
      if ('ontouchstart' in document.documentElement && !$parent.closest('.navbar-nav').length) {
        // if mobile we use a backdrop because click events don't delegate
        $('<div class="dropdown-backdrop"/>').insertAfter($(this)).on('click', clearMenus)
      }

      var relatedTarget = { relatedTarget: this }
      $parent.trigger(e = $.Event('show.bs.dropdown', relatedTarget))

      if (e.isDefaultPrevented()) return

      $this
        .trigger('focus')
        .attr('aria-expanded', 'true')

      $parent
        .toggleClass('open')
        .trigger('shown.bs.dropdown', relatedTarget)
    }

    return false
  }

  Dropdown.prototype.keydown = function (e) {
    if (!/(38|40|27|32)/.test(e.which) || /input|textarea/i.test(e.target.tagName)) return

    var $this = $(this)

    e.preventDefault()
    e.stopPropagation()

    if ($this.is('.disabled, :disabled')) return

    var $parent  = getParent($this)
    var isActive = $parent.hasClass('open')

    if ((!isActive && e.which != 27) || (isActive && e.which == 27)) {
      if (e.which == 27) $parent.find(toggle).trigger('focus')
      return $this.trigger('click')
    }

    var desc = ' li:not(.disabled):visible a'
    var $items = $parent.find('[role="menu"]' + desc + ', [role="listbox"]' + desc)

    if (!$items.length) return

    var index = $items.index(e.target)

    if (e.which == 38 && index > 0)                 index--                        // up
    if (e.which == 40 && index < $items.length - 1) index++                        // down
    if (!~index)                                      index = 0

    $items.eq(index).trigger('focus')
  }

  function clearMenus(e) {
    if (e && e.which === 3) return
    $(backdrop).remove()
    $(toggle).each(function () {
      var $this         = $(this)
      var $parent       = getParent($this)
      var relatedTarget = { relatedTarget: this }

      if (!$parent.hasClass('open')) return

      $parent.trigger(e = $.Event('hide.bs.dropdown', relatedTarget))

      if (e.isDefaultPrevented()) return

      $this.attr('aria-expanded', 'false')
      $parent.removeClass('open').trigger('hidden.bs.dropdown', relatedTarget)
    })
  }

  function getParent($this) {
    var selector = $this.attr('data-target')

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && /#[A-Za-z]/.test(selector) && selector.replace(/.*(?=#[^\s]*$)/, '') // strip for ie7
    }

    var $parent = selector && $(selector)

    return $parent && $parent.length ? $parent : $this.parent()
  }


  // DROPDOWN PLUGIN DEFINITION
  // ==========================

  function Plugin(option) {
    return this.each(function () {
      var $this = $(this)
      var data  = $this.data('bs.dropdown')

      if (!data) $this.data('bs.dropdown', (data = new Dropdown(this)))
      if (typeof option == 'string') data[option].call($this)
    })
  }

  var old = $.fn.dropdown

  $.fn.dropdown             = Plugin
  $.fn.dropdown.Constructor = Dropdown


  // DROPDOWN NO CONFLICT
  // ====================

  $.fn.dropdown.noConflict = function () {
    $.fn.dropdown = old
    return this
  }


  // APPLY TO STANDARD DROPDOWN ELEMENTS
  // ===================================

  $(document)
    .on('click.bs.dropdown.data-api', clearMenus)
    .on('click.bs.dropdown.data-api', '.dropdown form', function (e) { e.stopPropagation() })
    .on('click.bs.dropdown.data-api', toggle, Dropdown.prototype.toggle)
    .on('keydown.bs.dropdown.data-api', toggle, Dropdown.prototype.keydown)
    .on('keydown.bs.dropdown.data-api', '[role="menu"]', Dropdown.prototype.keydown)
    .on('keydown.bs.dropdown.data-api', '[role="listbox"]', Dropdown.prototype.keydown)

}(jQuery);

/* ========================================================================
 * Bootstrap: modal.js v3.3.4
 * http://getbootstrap.com/javascript/#modals
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // MODAL CLASS DEFINITION
  // ======================

  var Modal = function (element, options) {
    this.options             = options
    this.$body               = $(document.body)
    this.$element            = $(element)
    this.$dialog             = this.$element.find('.modal-dialog')
    this.$backdrop           = null
    this.isShown             = null
    this.originalBodyPad     = null
    this.scrollbarWidth      = 0
    this.ignoreBackdropClick = false

    if (this.options.remote) {
      this.$element
        .find('.modal-content')
        .load(this.options.remote, $.proxy(function () {
          this.$element.trigger('loaded.bs.modal')
        }, this))
    }
  }

  Modal.VERSION  = '3.3.4'

  Modal.TRANSITION_DURATION = 300
  Modal.BACKDROP_TRANSITION_DURATION = 150

  Modal.DEFAULTS = {
    backdrop: true,
    keyboard: true,
    show: true
  }

  Modal.prototype.toggle = function (_relatedTarget) {
    return this.isShown ? this.hide() : this.show(_relatedTarget)
  }

  Modal.prototype.show = function (_relatedTarget) {
    var that = this
    var e    = $.Event('show.bs.modal', { relatedTarget: _relatedTarget })

    this.$element.trigger(e)

    if (this.isShown || e.isDefaultPrevented()) return

    this.isShown = true

    this.checkScrollbar()
    this.setScrollbar()
    this.$body.addClass('modal-open')

    this.escape()
    this.resize()

    this.$element.on('click.dismiss.bs.modal', '[data-dismiss="modal"]', $.proxy(this.hide, this))

    this.$dialog.on('mousedown.dismiss.bs.modal', function () {
      that.$element.one('mouseup.dismiss.bs.modal', function (e) {
        if ($(e.target).is(that.$element)) that.ignoreBackdropClick = true
      })
    })

    this.backdrop(function () {
      var transition = $.support.transition && that.$element.hasClass('fade')

      if (!that.$element.parent().length) {
        that.$element.appendTo(that.$body) // don't move modals dom position
      }

      that.$element
        .show()
        .scrollTop(0)

      that.adjustDialog()

      if (transition) {
        that.$element[0].offsetWidth // force reflow
      }

      that.$element
        .addClass('in')
        .attr('aria-hidden', false)

      that.enforceFocus()

      var e = $.Event('shown.bs.modal', { relatedTarget: _relatedTarget })

      transition ?
        that.$dialog // wait for modal to slide in
          .one('bsTransitionEnd', function () {
            that.$element.trigger('focus').trigger(e)
          })
          .emulateTransitionEnd(Modal.TRANSITION_DURATION) :
        that.$element.trigger('focus').trigger(e)
    })
  }

  Modal.prototype.hide = function (e) {
    if (e) e.preventDefault()

    e = $.Event('hide.bs.modal')

    this.$element.trigger(e)

    if (!this.isShown || e.isDefaultPrevented()) return

    this.isShown = false

    this.escape()
    this.resize()

    $(document).off('focusin.bs.modal')

    this.$element
      .removeClass('in')
      .attr('aria-hidden', true)
      .off('click.dismiss.bs.modal')
      .off('mouseup.dismiss.bs.modal')

    this.$dialog.off('mousedown.dismiss.bs.modal')

    $.support.transition && this.$element.hasClass('fade') ?
      this.$element
        .one('bsTransitionEnd', $.proxy(this.hideModal, this))
        .emulateTransitionEnd(Modal.TRANSITION_DURATION) :
      this.hideModal()
  }

  Modal.prototype.enforceFocus = function () {
    $(document)
      .off('focusin.bs.modal') // guard against infinite focus loop
      .on('focusin.bs.modal', $.proxy(function (e) {
        if (this.$element[0] !== e.target && !this.$element.has(e.target).length) {
          this.$element.trigger('focus')
        }
      }, this))
  }

  Modal.prototype.escape = function () {
    if (this.isShown && this.options.keyboard) {
      this.$element.on('keydown.dismiss.bs.modal', $.proxy(function (e) {
        e.which == 27 && this.hide()
      }, this))
    } else if (!this.isShown) {
      this.$element.off('keydown.dismiss.bs.modal')
    }
  }

  Modal.prototype.resize = function () {
    if (this.isShown) {
      $(window).on('resize.bs.modal', $.proxy(this.handleUpdate, this))
    } else {
      $(window).off('resize.bs.modal')
    }
  }

  Modal.prototype.hideModal = function () {
    var that = this
    this.$element.hide()
    this.backdrop(function () {
      that.$body.removeClass('modal-open')
      that.resetAdjustments()
      that.resetScrollbar()
      that.$element.trigger('hidden.bs.modal')
    })
  }

  Modal.prototype.removeBackdrop = function () {
    this.$backdrop && this.$backdrop.remove()
    this.$backdrop = null
  }

  Modal.prototype.backdrop = function (callback) {
    var that = this
    var animate = this.$element.hasClass('fade') ? 'fade' : ''

    if (this.isShown && this.options.backdrop) {
      var doAnimate = $.support.transition && animate

      this.$backdrop = $('<div class="modal-backdrop ' + animate + '" />')
        .appendTo(this.$body)

      this.$element.on('click.dismiss.bs.modal', $.proxy(function (e) {
        if (this.ignoreBackdropClick) {
          this.ignoreBackdropClick = false
          return
        }
        if (e.target !== e.currentTarget) return
        this.options.backdrop == 'static'
          ? this.$element[0].focus()
          : this.hide()
      }, this))

      if (doAnimate) this.$backdrop[0].offsetWidth // force reflow

      this.$backdrop.addClass('in')

      if (!callback) return

      doAnimate ?
        this.$backdrop
          .one('bsTransitionEnd', callback)
          .emulateTransitionEnd(Modal.BACKDROP_TRANSITION_DURATION) :
        callback()

    } else if (!this.isShown && this.$backdrop) {
      this.$backdrop.removeClass('in')

      var callbackRemove = function () {
        that.removeBackdrop()
        callback && callback()
      }
      $.support.transition && this.$element.hasClass('fade') ?
        this.$backdrop
          .one('bsTransitionEnd', callbackRemove)
          .emulateTransitionEnd(Modal.BACKDROP_TRANSITION_DURATION) :
        callbackRemove()

    } else if (callback) {
      callback()
    }
  }

  // these following methods are used to handle overflowing modals

  Modal.prototype.handleUpdate = function () {
    this.adjustDialog()
  }

  Modal.prototype.adjustDialog = function () {
    var modalIsOverflowing = this.$element[0].scrollHeight > document.documentElement.clientHeight

    this.$element.css({
      paddingLeft:  !this.bodyIsOverflowing && modalIsOverflowing ? this.scrollbarWidth : '',
      paddingRight: this.bodyIsOverflowing && !modalIsOverflowing ? this.scrollbarWidth : ''
    })
  }

  Modal.prototype.resetAdjustments = function () {
    this.$element.css({
      paddingLeft: '',
      paddingRight: ''
    })
  }

  Modal.prototype.checkScrollbar = function () {
    var fullWindowWidth = window.innerWidth
    if (!fullWindowWidth) { // workaround for missing window.innerWidth in IE8
      var documentElementRect = document.documentElement.getBoundingClientRect()
      fullWindowWidth = documentElementRect.right - Math.abs(documentElementRect.left)
    }
    this.bodyIsOverflowing = document.body.clientWidth < fullWindowWidth
    this.scrollbarWidth = this.measureScrollbar()
  }

  Modal.prototype.setScrollbar = function () {
    var bodyPad = parseInt((this.$body.css('padding-right') || 0), 10)
    this.originalBodyPad = document.body.style.paddingRight || ''
    if (this.bodyIsOverflowing) this.$body.css('padding-right', bodyPad + this.scrollbarWidth)
  }

  Modal.prototype.resetScrollbar = function () {
    this.$body.css('padding-right', this.originalBodyPad)
  }

  Modal.prototype.measureScrollbar = function () { // thx walsh
    var scrollDiv = document.createElement('div')
    scrollDiv.className = 'modal-scrollbar-measure'
    this.$body.append(scrollDiv)
    var scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth
    this.$body[0].removeChild(scrollDiv)
    return scrollbarWidth
  }


  // MODAL PLUGIN DEFINITION
  // =======================

  function Plugin(option, _relatedTarget) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.modal')
      var options = $.extend({}, Modal.DEFAULTS, $this.data(), typeof option == 'object' && option)

      if (!data) $this.data('bs.modal', (data = new Modal(this, options)))
      if (typeof option == 'string') data[option](_relatedTarget)
      else if (options.show) data.show(_relatedTarget)
    })
  }

  var old = $.fn.modal

  $.fn.modal             = Plugin
  $.fn.modal.Constructor = Modal


  // MODAL NO CONFLICT
  // =================

  $.fn.modal.noConflict = function () {
    $.fn.modal = old
    return this
  }


  // MODAL DATA-API
  // ==============

  $(document).on('click.bs.modal.data-api', '[data-toggle="modal"]', function (e) {
    var $this   = $(this)
    var href    = $this.attr('href')
    var $target = $($this.attr('data-target') || (href && href.replace(/.*(?=#[^\s]+$)/, ''))) // strip for ie7
    var option  = $target.data('bs.modal') ? 'toggle' : $.extend({ remote: !/#/.test(href) && href }, $target.data(), $this.data())

    if ($this.is('a')) e.preventDefault()

    $target.one('show.bs.modal', function (showEvent) {
      if (showEvent.isDefaultPrevented()) return // only register focus restorer if modal will actually get shown
      $target.one('hidden.bs.modal', function () {
        $this.is(':visible') && $this.trigger('focus')
      })
    })
    Plugin.call($target, option, this)
  })

}(jQuery);

/* ========================================================================
 * Bootstrap: tooltip.js v3.3.4
 * http://getbootstrap.com/javascript/#tooltip
 * Inspired by the original jQuery.tipsy by Jason Frame
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // TOOLTIP PUBLIC CLASS DEFINITION
  // ===============================

  var Tooltip = function (element, options) {
    this.type       = null
    this.options    = null
    this.enabled    = null
    this.timeout    = null
    this.hoverState = null
    this.$element   = null

    this.init('tooltip', element, options)
  }

  Tooltip.VERSION  = '3.3.4'

  Tooltip.TRANSITION_DURATION = 150

  Tooltip.DEFAULTS = {
    animation: true,
    placement: 'top',
    selector: false,
    template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
    trigger: 'hover focus',
    title: '',
    delay: 0,
    html: false,
    container: false,
    viewport: {
      selector: 'body',
      padding: 0
    }
  }

  Tooltip.prototype.init = function (type, element, options) {
    this.enabled   = true
    this.type      = type
    this.$element  = $(element)
    this.options   = this.getOptions(options)
    this.$viewport = this.options.viewport && $(this.options.viewport.selector || this.options.viewport)

    if (this.$element[0] instanceof document.constructor && !this.options.selector) {
      throw new Error('`selector` option must be specified when initializing ' + this.type + ' on the window.document object!')
    }

    var triggers = this.options.trigger.split(' ')

    for (var i = triggers.length; i--;) {
      var trigger = triggers[i]

      if (trigger == 'click') {
        this.$element.on('click.' + this.type, this.options.selector, $.proxy(this.toggle, this))
      } else if (trigger != 'manual') {
        var eventIn  = trigger == 'hover' ? 'mouseenter' : 'focusin'
        var eventOut = trigger == 'hover' ? 'mouseleave' : 'focusout'

        this.$element.on(eventIn  + '.' + this.type, this.options.selector, $.proxy(this.enter, this))
        this.$element.on(eventOut + '.' + this.type, this.options.selector, $.proxy(this.leave, this))
      }
    }

    this.options.selector ?
      (this._options = $.extend({}, this.options, { trigger: 'manual', selector: '' })) :
      this.fixTitle()
  }

  Tooltip.prototype.getDefaults = function () {
    return Tooltip.DEFAULTS
  }

  Tooltip.prototype.getOptions = function (options) {
    options = $.extend({}, this.getDefaults(), this.$element.data(), options)

    if (options.delay && typeof options.delay == 'number') {
      options.delay = {
        show: options.delay,
        hide: options.delay
      }
    }

    return options
  }

  Tooltip.prototype.getDelegateOptions = function () {
    var options  = {}
    var defaults = this.getDefaults()

    this._options && $.each(this._options, function (key, value) {
      if (defaults[key] != value) options[key] = value
    })

    return options
  }

  Tooltip.prototype.enter = function (obj) {
    var self = obj instanceof this.constructor ?
      obj : $(obj.currentTarget).data('bs.' + this.type)

    if (self && self.$tip && self.$tip.is(':visible')) {
      self.hoverState = 'in'
      return
    }

    if (!self) {
      self = new this.constructor(obj.currentTarget, this.getDelegateOptions())
      $(obj.currentTarget).data('bs.' + this.type, self)
    }

    clearTimeout(self.timeout)

    self.hoverState = 'in'

    if (!self.options.delay || !self.options.delay.show) return self.show()

    self.timeout = setTimeout(function () {
      if (self.hoverState == 'in') self.show()
    }, self.options.delay.show)
  }

  Tooltip.prototype.leave = function (obj) {
    var self = obj instanceof this.constructor ?
      obj : $(obj.currentTarget).data('bs.' + this.type)

    if (!self) {
      self = new this.constructor(obj.currentTarget, this.getDelegateOptions())
      $(obj.currentTarget).data('bs.' + this.type, self)
    }

    clearTimeout(self.timeout)

    self.hoverState = 'out'

    if (!self.options.delay || !self.options.delay.hide) return self.hide()

    self.timeout = setTimeout(function () {
      if (self.hoverState == 'out') self.hide()
    }, self.options.delay.hide)
  }

  Tooltip.prototype.show = function () {
    var e = $.Event('show.bs.' + this.type)

    if (this.hasContent() && this.enabled) {
      this.$element.trigger(e)

      var inDom = $.contains(this.$element[0].ownerDocument.documentElement, this.$element[0])
      if (e.isDefaultPrevented() || !inDom) return
      var that = this

      var $tip = this.tip()

      var tipId = this.getUID(this.type)

      this.setContent()
      $tip.attr('id', tipId)
      this.$element.attr('aria-describedby', tipId)

      if (this.options.animation) $tip.addClass('fade')

      var placement = typeof this.options.placement == 'function' ?
        this.options.placement.call(this, $tip[0], this.$element[0]) :
        this.options.placement

      var autoToken = /\s?auto?\s?/i
      var autoPlace = autoToken.test(placement)
      if (autoPlace) placement = placement.replace(autoToken, '') || 'top'

      $tip
        .detach()
        .css({ top: 0, left: 0, display: 'block' })
        .addClass(placement)
        .data('bs.' + this.type, this)

      this.options.container ? $tip.appendTo(this.options.container) : $tip.insertAfter(this.$element)

      var pos          = this.getPosition()
      var actualWidth  = $tip[0].offsetWidth
      var actualHeight = $tip[0].offsetHeight

      if (autoPlace) {
        var orgPlacement = placement
        var $container   = this.options.container ? $(this.options.container) : this.$element.parent()
        var containerDim = this.getPosition($container)

        placement = placement == 'bottom' && pos.bottom + actualHeight > containerDim.bottom ? 'top'    :
                    placement == 'top'    && pos.top    - actualHeight < containerDim.top    ? 'bottom' :
                    placement == 'right'  && pos.right  + actualWidth  > containerDim.width  ? 'left'   :
                    placement == 'left'   && pos.left   - actualWidth  < containerDim.left   ? 'right'  :
                    placement

        $tip
          .removeClass(orgPlacement)
          .addClass(placement)
      }

      var calculatedOffset = this.getCalculatedOffset(placement, pos, actualWidth, actualHeight)

      this.applyPlacement(calculatedOffset, placement)

      var complete = function () {
        var prevHoverState = that.hoverState
        that.$element.trigger('shown.bs.' + that.type)
        that.hoverState = null

        if (prevHoverState == 'out') that.leave(that)
      }

      $.support.transition && this.$tip.hasClass('fade') ?
        $tip
          .one('bsTransitionEnd', complete)
          .emulateTransitionEnd(Tooltip.TRANSITION_DURATION) :
        complete()
    }
  }

  Tooltip.prototype.applyPlacement = function (offset, placement) {
    var $tip   = this.tip()
    var width  = $tip[0].offsetWidth
    var height = $tip[0].offsetHeight

    // manually read margins because getBoundingClientRect includes difference
    var marginTop = parseInt($tip.css('margin-top'), 10)
    var marginLeft = parseInt($tip.css('margin-left'), 10)

    // we must check for NaN for ie 8/9
    if (isNaN(marginTop))  marginTop  = 0
    if (isNaN(marginLeft)) marginLeft = 0

    offset.top  = offset.top  + marginTop
    offset.left = offset.left + marginLeft

    // $.fn.offset doesn't round pixel values
    // so we use setOffset directly with our own function B-0
    $.offset.setOffset($tip[0], $.extend({
      using: function (props) {
        $tip.css({
          top: Math.round(props.top),
          left: Math.round(props.left)
        })
      }
    }, offset), 0)

    $tip.addClass('in')

    // check to see if placing tip in new offset caused the tip to resize itself
    var actualWidth  = $tip[0].offsetWidth
    var actualHeight = $tip[0].offsetHeight

    if (placement == 'top' && actualHeight != height) {
      offset.top = offset.top + height - actualHeight
    }

    var delta = this.getViewportAdjustedDelta(placement, offset, actualWidth, actualHeight)

    if (delta.left) offset.left += delta.left
    else offset.top += delta.top

    var isVertical          = /top|bottom/.test(placement)
    var arrowDelta          = isVertical ? delta.left * 2 - width + actualWidth : delta.top * 2 - height + actualHeight
    var arrowOffsetPosition = isVertical ? 'offsetWidth' : 'offsetHeight'

    $tip.offset(offset)
    this.replaceArrow(arrowDelta, $tip[0][arrowOffsetPosition], isVertical)
  }

  Tooltip.prototype.replaceArrow = function (delta, dimension, isVertical) {
    this.arrow()
      .css(isVertical ? 'left' : 'top', 50 * (1 - delta / dimension) + '%')
      .css(isVertical ? 'top' : 'left', '')
  }

  Tooltip.prototype.setContent = function () {
    var $tip  = this.tip()
    var title = this.getTitle()

    $tip.find('.tooltip-inner')[this.options.html ? 'html' : 'text'](title)
    $tip.removeClass('fade in top bottom left right')
  }

  Tooltip.prototype.hide = function (callback) {
    var that = this
    var $tip = $(this.$tip)
    var e    = $.Event('hide.bs.' + this.type)

    function complete() {
      if (that.hoverState != 'in') $tip.detach()
      that.$element
        .removeAttr('aria-describedby')
        .trigger('hidden.bs.' + that.type)
      callback && callback()
    }

    this.$element.trigger(e)

    if (e.isDefaultPrevented()) return

    $tip.removeClass('in')

    $.support.transition && $tip.hasClass('fade') ?
      $tip
        .one('bsTransitionEnd', complete)
        .emulateTransitionEnd(Tooltip.TRANSITION_DURATION) :
      complete()

    this.hoverState = null

    return this
  }

  Tooltip.prototype.fixTitle = function () {
    var $e = this.$element
    if ($e.attr('title') || typeof ($e.attr('data-original-title')) != 'string') {
      $e.attr('data-original-title', $e.attr('title') || '').attr('title', '')
    }
  }

  Tooltip.prototype.hasContent = function () {
    return this.getTitle()
  }

  Tooltip.prototype.getPosition = function ($element) {
    $element   = $element || this.$element

    var el     = $element[0]
    var isBody = el.tagName == 'BODY'

    var elRect    = el.getBoundingClientRect()
    if (elRect.width == null) {
      // width and height are missing in IE8, so compute them manually; see https://github.com/twbs/bootstrap/issues/14093
      elRect = $.extend({}, elRect, { width: elRect.right - elRect.left, height: elRect.bottom - elRect.top })
    }
    var elOffset  = isBody ? { top: 0, left: 0 } : $element.offset()
    var scroll    = { scroll: isBody ? document.documentElement.scrollTop || document.body.scrollTop : $element.scrollTop() }
    var outerDims = isBody ? { width: $(window).width(), height: $(window).height() } : null

    return $.extend({}, elRect, scroll, outerDims, elOffset)
  }

  Tooltip.prototype.getCalculatedOffset = function (placement, pos, actualWidth, actualHeight) {
    return placement == 'bottom' ? { top: pos.top + pos.height,   left: pos.left + pos.width / 2 - actualWidth / 2 } :
           placement == 'top'    ? { top: pos.top - actualHeight, left: pos.left + pos.width / 2 - actualWidth / 2 } :
           placement == 'left'   ? { top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left - actualWidth } :
        /* placement == 'right' */ { top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left + pos.width }

  }

  Tooltip.prototype.getViewportAdjustedDelta = function (placement, pos, actualWidth, actualHeight) {
    var delta = { top: 0, left: 0 }
    if (!this.$viewport) return delta

    var viewportPadding = this.options.viewport && this.options.viewport.padding || 0
    var viewportDimensions = this.getPosition(this.$viewport)

    if (/right|left/.test(placement)) {
      var topEdgeOffset    = pos.top - viewportPadding - viewportDimensions.scroll
      var bottomEdgeOffset = pos.top + viewportPadding - viewportDimensions.scroll + actualHeight
      if (topEdgeOffset < viewportDimensions.top) { // top overflow
        delta.top = viewportDimensions.top - topEdgeOffset
      } else if (bottomEdgeOffset > viewportDimensions.top + viewportDimensions.height) { // bottom overflow
        delta.top = viewportDimensions.top + viewportDimensions.height - bottomEdgeOffset
      }
    } else {
      var leftEdgeOffset  = pos.left - viewportPadding
      var rightEdgeOffset = pos.left + viewportPadding + actualWidth
      if (leftEdgeOffset < viewportDimensions.left) { // left overflow
        delta.left = viewportDimensions.left - leftEdgeOffset
      } else if (rightEdgeOffset > viewportDimensions.width) { // right overflow
        delta.left = viewportDimensions.left + viewportDimensions.width - rightEdgeOffset
      }
    }

    return delta
  }

  Tooltip.prototype.getTitle = function () {
    var title
    var $e = this.$element
    var o  = this.options

    title = $e.attr('data-original-title')
      || (typeof o.title == 'function' ? o.title.call($e[0]) :  o.title)

    return title
  }

  Tooltip.prototype.getUID = function (prefix) {
    do prefix += ~~(Math.random() * 1000000)
    while (document.getElementById(prefix))
    return prefix
  }

  Tooltip.prototype.tip = function () {
    return (this.$tip = this.$tip || $(this.options.template))
  }

  Tooltip.prototype.arrow = function () {
    return (this.$arrow = this.$arrow || this.tip().find('.tooltip-arrow'))
  }

  Tooltip.prototype.enable = function () {
    this.enabled = true
  }

  Tooltip.prototype.disable = function () {
    this.enabled = false
  }

  Tooltip.prototype.toggleEnabled = function () {
    this.enabled = !this.enabled
  }

  Tooltip.prototype.toggle = function (e) {
    var self = this
    if (e) {
      self = $(e.currentTarget).data('bs.' + this.type)
      if (!self) {
        self = new this.constructor(e.currentTarget, this.getDelegateOptions())
        $(e.currentTarget).data('bs.' + this.type, self)
      }
    }

    self.tip().hasClass('in') ? self.leave(self) : self.enter(self)
  }

  Tooltip.prototype.destroy = function () {
    var that = this
    clearTimeout(this.timeout)
    this.hide(function () {
      that.$element.off('.' + that.type).removeData('bs.' + that.type)
    })
  }


  // TOOLTIP PLUGIN DEFINITION
  // =========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.tooltip')
      var options = typeof option == 'object' && option

      if (!data && /destroy|hide/.test(option)) return
      if (!data) $this.data('bs.tooltip', (data = new Tooltip(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.tooltip

  $.fn.tooltip             = Plugin
  $.fn.tooltip.Constructor = Tooltip


  // TOOLTIP NO CONFLICT
  // ===================

  $.fn.tooltip.noConflict = function () {
    $.fn.tooltip = old
    return this
  }

}(jQuery);

/* ========================================================================
 * Bootstrap: popover.js v3.3.4
 * http://getbootstrap.com/javascript/#popovers
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // POPOVER PUBLIC CLASS DEFINITION
  // ===============================

  var Popover = function (element, options) {
    this.init('popover', element, options)
  }

  if (!$.fn.tooltip) throw new Error('Popover requires tooltip.js')

  Popover.VERSION  = '3.3.4'

  Popover.DEFAULTS = $.extend({}, $.fn.tooltip.Constructor.DEFAULTS, {
    placement: 'right',
    trigger: 'click',
    content: '',
    template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
  })


  // NOTE: POPOVER EXTENDS tooltip.js
  // ================================

  Popover.prototype = $.extend({}, $.fn.tooltip.Constructor.prototype)

  Popover.prototype.constructor = Popover

  Popover.prototype.getDefaults = function () {
    return Popover.DEFAULTS
  }

  Popover.prototype.setContent = function () {
    var $tip    = this.tip()
    var title   = this.getTitle()
    var content = this.getContent()

    $tip.find('.popover-title')[this.options.html ? 'html' : 'text'](title)
    $tip.find('.popover-content').children().detach().end()[ // we use append for html objects to maintain js events
      this.options.html ? (typeof content == 'string' ? 'html' : 'append') : 'text'
    ](content)

    $tip.removeClass('fade top bottom left right in')

    // IE8 doesn't accept hiding via the `:empty` pseudo selector, we have to do
    // this manually by checking the contents.
    if (!$tip.find('.popover-title').html()) $tip.find('.popover-title').hide()
  }

  Popover.prototype.hasContent = function () {
    return this.getTitle() || this.getContent()
  }

  Popover.prototype.getContent = function () {
    var $e = this.$element
    var o  = this.options

    return $e.attr('data-content')
      || (typeof o.content == 'function' ?
            o.content.call($e[0]) :
            o.content)
  }

  Popover.prototype.arrow = function () {
    return (this.$arrow = this.$arrow || this.tip().find('.arrow'))
  }


  // POPOVER PLUGIN DEFINITION
  // =========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.popover')
      var options = typeof option == 'object' && option

      if (!data && /destroy|hide/.test(option)) return
      if (!data) $this.data('bs.popover', (data = new Popover(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.popover

  $.fn.popover             = Plugin
  $.fn.popover.Constructor = Popover


  // POPOVER NO CONFLICT
  // ===================

  $.fn.popover.noConflict = function () {
    $.fn.popover = old
    return this
  }

}(jQuery);

/* ========================================================================
 * Bootstrap: scrollspy.js v3.3.4
 * http://getbootstrap.com/javascript/#scrollspy
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // SCROLLSPY CLASS DEFINITION
  // ==========================

  function ScrollSpy(element, options) {
    this.$body          = $(document.body)
    this.$scrollElement = $(element).is(document.body) ? $(window) : $(element)
    this.options        = $.extend({}, ScrollSpy.DEFAULTS, options)
    this.selector       = (this.options.target || '') + ' .nav li > a'
    this.offsets        = []
    this.targets        = []
    this.activeTarget   = null
    this.scrollHeight   = 0

    this.$scrollElement.on('scroll.bs.scrollspy', $.proxy(this.process, this))
    this.refresh()
    this.process()
  }

  ScrollSpy.VERSION  = '3.3.4'

  ScrollSpy.DEFAULTS = {
    offset: 10
  }

  ScrollSpy.prototype.getScrollHeight = function () {
    return this.$scrollElement[0].scrollHeight || Math.max(this.$body[0].scrollHeight, document.documentElement.scrollHeight)
  }

  ScrollSpy.prototype.refresh = function () {
    var that          = this
    var offsetMethod  = 'offset'
    var offsetBase    = 0

    this.offsets      = []
    this.targets      = []
    this.scrollHeight = this.getScrollHeight()

    if (!$.isWindow(this.$scrollElement[0])) {
      offsetMethod = 'position'
      offsetBase   = this.$scrollElement.scrollTop()
    }

    this.$body
      .find(this.selector)
      .map(function () {
        var $el   = $(this)
        var href  = $el.data('target') || $el.attr('href')
        var $href = /^#./.test(href) && $(href)

        return ($href
          && $href.length
          && $href.is(':visible')
          && [[$href[offsetMethod]().top + offsetBase, href]]) || null
      })
      .sort(function (a, b) { return a[0] - b[0] })
      .each(function () {
        that.offsets.push(this[0])
        that.targets.push(this[1])
      })
  }

  ScrollSpy.prototype.process = function () {
    var scrollTop    = this.$scrollElement.scrollTop() + this.options.offset
    var scrollHeight = this.getScrollHeight()
    var maxScroll    = this.options.offset + scrollHeight - this.$scrollElement.height()
    var offsets      = this.offsets
    var targets      = this.targets
    var activeTarget = this.activeTarget
    var i

    if (this.scrollHeight != scrollHeight) {
      this.refresh()
    }

    if (scrollTop >= maxScroll) {
      return activeTarget != (i = targets[targets.length - 1]) && this.activate(i)
    }

    if (activeTarget && scrollTop < offsets[0]) {
      this.activeTarget = null
      return this.clear()
    }

    for (i = offsets.length; i--;) {
      activeTarget != targets[i]
        && scrollTop >= offsets[i]
        && (offsets[i + 1] === undefined || scrollTop < offsets[i + 1])
        && this.activate(targets[i])
    }
  }

  ScrollSpy.prototype.activate = function (target) {
    this.activeTarget = target

    this.clear()

    var selector = this.selector +
      '[data-target="' + target + '"],' +
      this.selector + '[href="' + target + '"]'

    var active = $(selector)
      .parents('li')
      .addClass('active')

    if (active.parent('.dropdown-menu').length) {
      active = active
        .closest('li.dropdown')
        .addClass('active')
    }

    active.trigger('activate.bs.scrollspy')
  }

  ScrollSpy.prototype.clear = function () {
    $(this.selector)
      .parentsUntil(this.options.target, '.active')
      .removeClass('active')
  }


  // SCROLLSPY PLUGIN DEFINITION
  // ===========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.scrollspy')
      var options = typeof option == 'object' && option

      if (!data) $this.data('bs.scrollspy', (data = new ScrollSpy(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.scrollspy

  $.fn.scrollspy             = Plugin
  $.fn.scrollspy.Constructor = ScrollSpy


  // SCROLLSPY NO CONFLICT
  // =====================

  $.fn.scrollspy.noConflict = function () {
    $.fn.scrollspy = old
    return this
  }


  // SCROLLSPY DATA-API
  // ==================

  $(window).on('load.bs.scrollspy.data-api', function () {
    $('[data-spy="scroll"]').each(function () {
      var $spy = $(this)
      Plugin.call($spy, $spy.data())
    })
  })

}(jQuery);

/* ========================================================================
 * Bootstrap: tab.js v3.3.4
 * http://getbootstrap.com/javascript/#tabs
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // TAB CLASS DEFINITION
  // ====================

  var Tab = function (element) {
    this.element = $(element)
  }

  Tab.VERSION = '3.3.4'

  Tab.TRANSITION_DURATION = 150

  Tab.prototype.show = function () {
    var $this    = this.element
    var $ul      = $this.closest('ul:not(.dropdown-menu)')
    var selector = $this.data('target')

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') // strip for ie7
    }

    if ($this.parent('li').hasClass('active')) return

    var $previous = $ul.find('.active:last a')
    var hideEvent = $.Event('hide.bs.tab', {
      relatedTarget: $this[0]
    })
    var showEvent = $.Event('show.bs.tab', {
      relatedTarget: $previous[0]
    })

    $previous.trigger(hideEvent)
    $this.trigger(showEvent)

    if (showEvent.isDefaultPrevented() || hideEvent.isDefaultPrevented()) return

    var $target = $(selector)

    this.activate($this.closest('li'), $ul)
    this.activate($target, $target.parent(), function () {
      $previous.trigger({
        type: 'hidden.bs.tab',
        relatedTarget: $this[0]
      })
      $this.trigger({
        type: 'shown.bs.tab',
        relatedTarget: $previous[0]
      })
    })
  }

  Tab.prototype.activate = function (element, container, callback) {
    var $active    = container.find('> .active')
    var transition = callback
      && $.support.transition
      && (($active.length && $active.hasClass('fade')) || !!container.find('> .fade').length)

    function next() {
      $active
        .removeClass('active')
        .find('> .dropdown-menu > .active')
          .removeClass('active')
        .end()
        .find('[data-toggle="tab"]')
          .attr('aria-expanded', false)

      element
        .addClass('active')
        .find('[data-toggle="tab"]')
          .attr('aria-expanded', true)

      if (transition) {
        element[0].offsetWidth // reflow for transition
        element.addClass('in')
      } else {
        element.removeClass('fade')
      }

      if (element.parent('.dropdown-menu').length) {
        element
          .closest('li.dropdown')
            .addClass('active')
          .end()
          .find('[data-toggle="tab"]')
            .attr('aria-expanded', true)
      }

      callback && callback()
    }

    $active.length && transition ?
      $active
        .one('bsTransitionEnd', next)
        .emulateTransitionEnd(Tab.TRANSITION_DURATION) :
      next()

    $active.removeClass('in')
  }


  // TAB PLUGIN DEFINITION
  // =====================

  function Plugin(option) {
    return this.each(function () {
      var $this = $(this)
      var data  = $this.data('bs.tab')

      if (!data) $this.data('bs.tab', (data = new Tab(this)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.tab

  $.fn.tab             = Plugin
  $.fn.tab.Constructor = Tab


  // TAB NO CONFLICT
  // ===============

  $.fn.tab.noConflict = function () {
    $.fn.tab = old
    return this
  }


  // TAB DATA-API
  // ============

  var clickHandler = function (e) {
    e.preventDefault()
    Plugin.call($(this), 'show')
  }

  $(document)
    .on('click.bs.tab.data-api', '[data-toggle="tab"]', clickHandler)
    .on('click.bs.tab.data-api', '[data-toggle="pill"]', clickHandler)

}(jQuery);

/* ========================================================================
 * Bootstrap: affix.js v3.3.4
 * http://getbootstrap.com/javascript/#affix
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // AFFIX CLASS DEFINITION
  // ======================

  var Affix = function (element, options) {
    this.options = $.extend({}, Affix.DEFAULTS, options)

    this.$target = $(this.options.target)
      .on('scroll.bs.affix.data-api', $.proxy(this.checkPosition, this))
      .on('click.bs.affix.data-api',  $.proxy(this.checkPositionWithEventLoop, this))

    this.$element     = $(element)
    this.affixed      = null
    this.unpin        = null
    this.pinnedOffset = null

    this.checkPosition()
  }

  Affix.VERSION  = '3.3.4'

  Affix.RESET    = 'affix affix-top affix-bottom'

  Affix.DEFAULTS = {
    offset: 0,
    target: window
  }

  Affix.prototype.getState = function (scrollHeight, height, offsetTop, offsetBottom) {
    var scrollTop    = this.$target.scrollTop()
    var position     = this.$element.offset()
    var targetHeight = this.$target.height()

    if (offsetTop != null && this.affixed == 'top') return scrollTop < offsetTop ? 'top' : false

    if (this.affixed == 'bottom') {
      if (offsetTop != null) return (scrollTop + this.unpin <= position.top) ? false : 'bottom'
      return (scrollTop + targetHeight <= scrollHeight - offsetBottom) ? false : 'bottom'
    }

    var initializing   = this.affixed == null
    var colliderTop    = initializing ? scrollTop : position.top
    var colliderHeight = initializing ? targetHeight : height

    if (offsetTop != null && scrollTop <= offsetTop) return 'top'
    if (offsetBottom != null && (colliderTop + colliderHeight >= scrollHeight - offsetBottom)) return 'bottom'

    return false
  }

  Affix.prototype.getPinnedOffset = function () {
    if (this.pinnedOffset) return this.pinnedOffset
    this.$element.removeClass(Affix.RESET).addClass('affix')
    var scrollTop = this.$target.scrollTop()
    var position  = this.$element.offset()
    return (this.pinnedOffset = position.top - scrollTop)
  }

  Affix.prototype.checkPositionWithEventLoop = function () {
    setTimeout($.proxy(this.checkPosition, this), 1)
  }

  Affix.prototype.checkPosition = function () {
    if (!this.$element.is(':visible')) return

    var height       = this.$element.height()
    var offset       = this.options.offset
    var offsetTop    = offset.top
    var offsetBottom = offset.bottom
    var scrollHeight = $(document.body).height()

    if (typeof offset != 'object')         offsetBottom = offsetTop = offset
    if (typeof offsetTop == 'function')    offsetTop    = offset.top(this.$element)
    if (typeof offsetBottom == 'function') offsetBottom = offset.bottom(this.$element)

    var affix = this.getState(scrollHeight, height, offsetTop, offsetBottom)

    if (this.affixed != affix) {
      if (this.unpin != null) this.$element.css('top', '')

      var affixType = 'affix' + (affix ? '-' + affix : '')
      var e         = $.Event(affixType + '.bs.affix')

      this.$element.trigger(e)

      if (e.isDefaultPrevented()) return

      this.affixed = affix
      this.unpin = affix == 'bottom' ? this.getPinnedOffset() : null

      this.$element
        .removeClass(Affix.RESET)
        .addClass(affixType)
        .trigger(affixType.replace('affix', 'affixed') + '.bs.affix')
    }

    if (affix == 'bottom') {
      this.$element.offset({
        top: scrollHeight - height - offsetBottom
      })
    }
  }


  // AFFIX PLUGIN DEFINITION
  // =======================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.affix')
      var options = typeof option == 'object' && option

      if (!data) $this.data('bs.affix', (data = new Affix(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.affix

  $.fn.affix             = Plugin
  $.fn.affix.Constructor = Affix


  // AFFIX NO CONFLICT
  // =================

  $.fn.affix.noConflict = function () {
    $.fn.affix = old
    return this
  }


  // AFFIX DATA-API
  // ==============

  $(window).on('load', function () {
    $('[data-spy="affix"]').each(function () {
      var $spy = $(this)
      var data = $spy.data()

      data.offset = data.offset || {}

      if (data.offsetBottom != null) data.offset.bottom = data.offsetBottom
      if (data.offsetTop    != null) data.offset.top    = data.offsetTop

      Plugin.call($spy, data)
    })
  })

}(jQuery);
;/*!
Chosen, a Select Box Enhancer for jQuery and Prototype
by Patrick Filler for Harvest, http://getharvest.com

Version 1.4.2
Full source at https://github.com/harvesthq/chosen
Copyright (c) 2011-2015 Harvest http://getharvest.com

MIT License, https://github.com/harvesthq/chosen/blob/master/LICENSE.md
This file is generated by `grunt build`, do not edit it by hand.
*/

(function() {
  var $, AbstractChosen, Chosen, SelectParser, _ref,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  SelectParser = (function() {
    function SelectParser() {
      this.options_index = 0;
      this.parsed = [];
    }

    SelectParser.prototype.add_node = function(child) {
      if (child.nodeName.toUpperCase() === "OPTGROUP") {
        return this.add_group(child);
      } else {
        return this.add_option(child);
      }
    };

    SelectParser.prototype.add_group = function(group) {
      var group_position, option, _i, _len, _ref, _results;
      group_position = this.parsed.length;
      this.parsed.push({
        array_index: group_position,
        group: true,
        label: this.escapeExpression(group.label),
        title: group.title ? group.title : void 0,
        children: 0,
        disabled: group.disabled,
        classes: group.className
      });
      _ref = group.childNodes;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        option = _ref[_i];
        _results.push(this.add_option(option, group_position, group.disabled));
      }
      return _results;
    };

    SelectParser.prototype.add_option = function(option, group_position, group_disabled) {
      if (option.nodeName.toUpperCase() === "OPTION") {
        if (option.text !== "") {
          if (group_position != null) {
            this.parsed[group_position].children += 1;
          }
          this.parsed.push({
            array_index: this.parsed.length,
            options_index: this.options_index,
            value: option.value,
            text: option.text,
            html: option.innerHTML,
            title: option.title ? option.title : void 0,
            selected: option.selected,
            disabled: group_disabled === true ? group_disabled : option.disabled,
            group_array_index: group_position,
            group_label: group_position != null ? this.parsed[group_position].label : null,
            classes: option.className,
            style: option.style.cssText
          });
        } else {
          this.parsed.push({
            array_index: this.parsed.length,
            options_index: this.options_index,
            empty: true
          });
        }
        return this.options_index += 1;
      }
    };

    SelectParser.prototype.escapeExpression = function(text) {
      var map, unsafe_chars;
      if ((text == null) || text === false) {
        return "";
      }
      if (!/[\&\<\>\"\'\`]/.test(text)) {
        return text;
      }
      map = {
        "<": "&lt;",
        ">": "&gt;",
        '"': "&quot;",
        "'": "&#x27;",
        "`": "&#x60;"
      };
      unsafe_chars = /&(?!\w+;)|[\<\>\"\'\`]/g;
      return text.replace(unsafe_chars, function(chr) {
        return map[chr] || "&amp;";
      });
    };

    return SelectParser;

  })();

  SelectParser.select_to_array = function(select) {
    var child, parser, _i, _len, _ref;
    parser = new SelectParser();
    _ref = select.childNodes;
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      child = _ref[_i];
      parser.add_node(child);
    }
    return parser.parsed;
  };

  AbstractChosen = (function() {
    function AbstractChosen(form_field, options) {
      this.form_field = form_field;
      this.options = options != null ? options : {};
      if (!AbstractChosen.browser_is_supported()) {
        return;
      }
      this.is_multiple = this.form_field.multiple;
      this.set_default_text();
      this.set_default_values();
      this.setup();
      this.set_up_html();
      this.register_observers();
      this.on_ready();
    }

    AbstractChosen.prototype.set_default_values = function() {
      var _this = this;
      this.click_test_action = function(evt) {
        return _this.test_active_click(evt);
      };
      this.activate_action = function(evt) {
        return _this.activate_field(evt);
      };
      this.active_field = false;
      this.mouse_on_container = false;
      this.results_showing = false;
      this.result_highlighted = null;
      this.allow_single_deselect = (this.options.allow_single_deselect != null) && (this.form_field.options[0] != null) && this.form_field.options[0].text === "" ? this.options.allow_single_deselect : false;
      this.disable_search_threshold = this.options.disable_search_threshold || 0;
      this.disable_search = this.options.disable_search || false;
      this.enable_split_word_search = this.options.enable_split_word_search != null ? this.options.enable_split_word_search : true;
      this.group_search = this.options.group_search != null ? this.options.group_search : true;
      this.search_contains = this.options.search_contains || false;
      this.single_backstroke_delete = this.options.single_backstroke_delete != null ? this.options.single_backstroke_delete : true;
      this.max_selected_options = this.options.max_selected_options || Infinity;
      this.inherit_select_classes = this.options.inherit_select_classes || false;
      this.display_selected_options = this.options.display_selected_options != null ? this.options.display_selected_options : true;
      this.display_disabled_options = this.options.display_disabled_options != null ? this.options.display_disabled_options : true;
      return this.include_group_label_in_selected = this.options.include_group_label_in_selected || false;
    };

    AbstractChosen.prototype.set_default_text = function() {
      if (this.form_field.getAttribute("data-placeholder")) {
        this.default_text = this.form_field.getAttribute("data-placeholder");
      } else if (this.is_multiple) {
        this.default_text = this.options.placeholder_text_multiple || this.options.placeholder_text || AbstractChosen.default_multiple_text;
      } else {
        this.default_text = this.options.placeholder_text_single || this.options.placeholder_text || AbstractChosen.default_single_text;
      }
      return this.results_none_found = this.form_field.getAttribute("data-no_results_text") || this.options.no_results_text || AbstractChosen.default_no_result_text;
    };

    AbstractChosen.prototype.choice_label = function(item) {
      if (this.include_group_label_in_selected && (item.group_label != null)) {
        return "<b class='group-name'>" + item.group_label + "</b>" + item.html;
      } else {
        return item.html;
      }
    };

    AbstractChosen.prototype.mouse_enter = function() {
      return this.mouse_on_container = true;
    };

    AbstractChosen.prototype.mouse_leave = function() {
      return this.mouse_on_container = false;
    };

    AbstractChosen.prototype.input_focus = function(evt) {
      var _this = this;
      if (this.is_multiple) {
        if (!this.active_field) {
          return setTimeout((function() {
            return _this.container_mousedown();
          }), 50);
        }
      } else {
        if (!this.active_field) {
          return this.activate_field();
        }
      }
    };

    AbstractChosen.prototype.input_blur = function(evt) {
      var _this = this;
      if (!this.mouse_on_container) {
        this.active_field = false;
        return setTimeout((function() {
          return _this.blur_test();
        }), 100);
      }
    };

    AbstractChosen.prototype.results_option_build = function(options) {
      var content, data, _i, _len, _ref;
      content = '';
      _ref = this.results_data;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        data = _ref[_i];
        if (data.group) {
          content += this.result_add_group(data);
        } else {
          content += this.result_add_option(data);
        }
        if (options != null ? options.first : void 0) {
          if (data.selected && this.is_multiple) {
            this.choice_build(data);
          } else if (data.selected && !this.is_multiple) {
            this.single_set_selected_text(this.choice_label(data));
          }
        }
      }
      return content;
    };

    AbstractChosen.prototype.result_add_option = function(option) {
      var classes, option_el;
      if (!option.search_match) {
        return '';
      }
      if (!this.include_option_in_results(option)) {
        return '';
      }
      classes = [];
      if (!option.disabled && !(option.selected && this.is_multiple)) {
        classes.push("active-result");
      }
      if (option.disabled && !(option.selected && this.is_multiple)) {
        classes.push("disabled-result");
      }
      if (option.selected) {
        classes.push("result-selected");
      }
      if (option.group_array_index != null) {
        classes.push("group-option");
      }
      if (option.classes !== "") {
        classes.push(option.classes);
      }
      option_el = document.createElement("li");
      option_el.className = classes.join(" ");
      option_el.style.cssText = option.style;
      option_el.setAttribute("data-option-array-index", option.array_index);
      option_el.innerHTML = option.search_text;
      if (option.title) {
        option_el.title = option.title;
      }
      return this.outerHTML(option_el);
    };

    AbstractChosen.prototype.result_add_group = function(group) {
      var classes, group_el;
      if (!(group.search_match || group.group_match)) {
        return '';
      }
      if (!(group.active_options > 0)) {
        return '';
      }
      classes = [];
      classes.push("group-result");
      if (group.classes) {
        classes.push(group.classes);
      }
      group_el = document.createElement("li");
      group_el.className = classes.join(" ");
      group_el.innerHTML = group.search_text;
      if (group.title) {
        group_el.title = group.title;
      }
      return this.outerHTML(group_el);
    };

    AbstractChosen.prototype.results_update_field = function() {
      this.set_default_text();
      if (!this.is_multiple) {
        this.results_reset_cleanup();
      }
      this.result_clear_highlight();
      this.results_build();
      if (this.results_showing) {
        return this.winnow_results();
      }
    };

    AbstractChosen.prototype.reset_single_select_options = function() {
      var result, _i, _len, _ref, _results;
      _ref = this.results_data;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        result = _ref[_i];
        if (result.selected) {
          _results.push(result.selected = false);
        } else {
          _results.push(void 0);
        }
      }
      return _results;
    };

    AbstractChosen.prototype.results_toggle = function() {
      if (this.results_showing) {
        return this.results_hide();
      } else {
        return this.results_show();
      }
    };

    AbstractChosen.prototype.results_search = function(evt) {
      if (this.results_showing) {
        return this.winnow_results();
      } else {
        return this.results_show();
      }
    };

    AbstractChosen.prototype.winnow_results = function() {
      var escapedSearchText, option, regex, results, results_group, searchText, startpos, text, zregex, _i, _len, _ref;
      this.no_results_clear();
      results = 0;
      searchText = this.get_search_text();
      escapedSearchText = searchText.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
      zregex = new RegExp(escapedSearchText, 'i');
      regex = this.get_search_regex(escapedSearchText);
      _ref = this.results_data;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        option = _ref[_i];
        option.search_match = false;
        results_group = null;
        if (this.include_option_in_results(option)) {
          if (option.group) {
            option.group_match = false;
            option.active_options = 0;
          }
          if ((option.group_array_index != null) && this.results_data[option.group_array_index]) {
            results_group = this.results_data[option.group_array_index];
            if (results_group.active_options === 0 && results_group.search_match) {
              results += 1;
            }
            results_group.active_options += 1;
          }
          option.search_text = option.group ? option.label : option.html;
          if (!(option.group && !this.group_search)) {
            option.search_match = this.search_string_match(option.search_text, regex);
            if (option.search_match && !option.group) {
              results += 1;
            }
            if (option.search_match) {
              if (searchText.length) {
                startpos = option.search_text.search(zregex);
                text = option.search_text.substr(0, startpos + searchText.length) + '</em>' + option.search_text.substr(startpos + searchText.length);
                option.search_text = text.substr(0, startpos) + '<em>' + text.substr(startpos);
              }
              if (results_group != null) {
                results_group.group_match = true;
              }
            } else if ((option.group_array_index != null) && this.results_data[option.group_array_index].search_match) {
              option.search_match = true;
            }
          }
        }
      }
      this.result_clear_highlight();
      if (results < 1 && searchText.length) {
        this.update_results_content("");
        return this.no_results(searchText);
      } else {
        this.update_results_content(this.results_option_build());
        return this.winnow_results_set_highlight();
      }
    };

    AbstractChosen.prototype.get_search_regex = function(escaped_search_string) {
      var regex_anchor;
      regex_anchor = this.search_contains ? "" : "^";
      return new RegExp(regex_anchor + escaped_search_string, 'i');
    };

    AbstractChosen.prototype.search_string_match = function(search_string, regex) {
      var part, parts, _i, _len;
      if (regex.test(search_string)) {
        return true;
      } else if (this.enable_split_word_search && (search_string.indexOf(" ") >= 0 || search_string.indexOf("[") === 0)) {
        parts = search_string.replace(/\[|\]/g, "").split(" ");
        if (parts.length) {
          for (_i = 0, _len = parts.length; _i < _len; _i++) {
            part = parts[_i];
            if (regex.test(part)) {
              return true;
            }
          }
        }
      }
    };

    AbstractChosen.prototype.choices_count = function() {
      var option, _i, _len, _ref;
      if (this.selected_option_count != null) {
        return this.selected_option_count;
      }
      this.selected_option_count = 0;
      _ref = this.form_field.options;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        option = _ref[_i];
        if (option.selected) {
          this.selected_option_count += 1;
        }
      }
      return this.selected_option_count;
    };

    AbstractChosen.prototype.choices_click = function(evt) {
      evt.preventDefault();
      if (!(this.results_showing || this.is_disabled)) {
        return this.results_show();
      }
    };

    AbstractChosen.prototype.keyup_checker = function(evt) {
      var stroke, _ref;
      stroke = (_ref = evt.which) != null ? _ref : evt.keyCode;
      this.search_field_scale();
      switch (stroke) {
        case 8:
          if (this.is_multiple && this.backstroke_length < 1 && this.choices_count() > 0) {
            return this.keydown_backstroke();
          } else if (!this.pending_backstroke) {
            this.result_clear_highlight();
            return this.results_search();
          }
          break;
        case 13:
          evt.preventDefault();
          if (this.results_showing) {
            return this.result_select(evt);
          }
          break;
        case 27:
          if (this.results_showing) {
            this.results_hide();
          }
          return true;
        case 9:
        case 38:
        case 40:
        case 16:
        case 91:
        case 17:
          break;
        default:
          return this.results_search();
      }
    };

    AbstractChosen.prototype.clipboard_event_checker = function(evt) {
      var _this = this;
      return setTimeout((function() {
        return _this.results_search();
      }), 50);
    };

    AbstractChosen.prototype.container_width = function() {
      if (this.options.width != null) {
        return this.options.width;
      } else {
        return "" + this.form_field.offsetWidth + "px";
      }
    };

    AbstractChosen.prototype.include_option_in_results = function(option) {
      if (this.is_multiple && (!this.display_selected_options && option.selected)) {
        return false;
      }
      if (!this.display_disabled_options && option.disabled) {
        return false;
      }
      if (option.empty) {
        return false;
      }
      return true;
    };

    AbstractChosen.prototype.search_results_touchstart = function(evt) {
      this.touch_started = true;
      return this.search_results_mouseover(evt);
    };

    AbstractChosen.prototype.search_results_touchmove = function(evt) {
      this.touch_started = false;
      return this.search_results_mouseout(evt);
    };

    AbstractChosen.prototype.search_results_touchend = function(evt) {
      if (this.touch_started) {
        return this.search_results_mouseup(evt);
      }
    };

    AbstractChosen.prototype.outerHTML = function(element) {
      var tmp;
      if (element.outerHTML) {
        return element.outerHTML;
      }
      tmp = document.createElement("div");
      tmp.appendChild(element);
      return tmp.innerHTML;
    };

    AbstractChosen.browser_is_supported = function() {
      if (window.navigator.appName === "Microsoft Internet Explorer") {
        return document.documentMode >= 8;
      }
      if (/iP(od|hone)/i.test(window.navigator.userAgent)) {
        return false;
      }
      if (/Android/i.test(window.navigator.userAgent)) {
        if (/Mobile/i.test(window.navigator.userAgent)) {
          return false;
        }
      }
      return true;
    };

    AbstractChosen.default_multiple_text = "Select Some Options";

    AbstractChosen.default_single_text = "Select an Option";

    AbstractChosen.default_no_result_text = "No results match";

    return AbstractChosen;

  })();

  $ = jQuery;

  $.fn.extend({
    chosen: function(options) {
      if (!AbstractChosen.browser_is_supported()) {
        return this;
      }
      return this.each(function(input_field) {
        var $this, chosen;
        $this = $(this);
        chosen = $this.data('chosen');
        if (options === 'destroy' && chosen instanceof Chosen) {
          chosen.destroy();
        } else if (!(chosen instanceof Chosen)) {
          $this.data('chosen', new Chosen(this, options));
        }
      });
    }
  });

  Chosen = (function(_super) {
    __extends(Chosen, _super);

    function Chosen() {
      _ref = Chosen.__super__.constructor.apply(this, arguments);
      return _ref;
    }

    Chosen.prototype.setup = function() {
      this.form_field_jq = $(this.form_field);
      this.current_selectedIndex = this.form_field.selectedIndex;
      return this.is_rtl = this.form_field_jq.hasClass("chosen-rtl");
    };

    Chosen.prototype.set_up_html = function() {
      var container_classes, container_props;
      container_classes = ["chosen-container"];
      container_classes.push("chosen-container-" + (this.is_multiple ? "multi" : "single"));
      if (this.inherit_select_classes && this.form_field.className) {
        container_classes.push(this.form_field.className);
      }
      if (this.is_rtl) {
        container_classes.push("chosen-rtl");
      }
      container_props = {
        'class': container_classes.join(' '),
        'style': "width: " + (this.container_width()) + ";",
        'title': this.form_field.title
      };
      if (this.form_field.id.length) {
        container_props.id = this.form_field.id.replace(/[^\w]/g, '_') + "_chosen";
      }
      this.container = $("<div />", container_props);
      if (this.is_multiple) {
        this.container.html('<ul class="chosen-choices"><li class="search-field"><input type="text" value="' + this.default_text + '" class="default" autocomplete="off" style="width:25px;" /></li></ul><div class="chosen-drop"><ul class="chosen-results"></ul></div>');
      } else {
        this.container.html('<a class="chosen-single chosen-default" tabindex="-1"><span>' + this.default_text + '</span><div><b></b></div></a><div class="chosen-drop"><div class="chosen-search"><input type="text" autocomplete="off" /></div><ul class="chosen-results"></ul></div>');
      }
      this.form_field_jq.hide().after(this.container);
      this.dropdown = this.container.find('div.chosen-drop').first();
      this.search_field = this.container.find('input').first();
      this.search_results = this.container.find('ul.chosen-results').first();
      this.search_field_scale();
      this.search_no_results = this.container.find('li.no-results').first();
      if (this.is_multiple) {
        this.search_choices = this.container.find('ul.chosen-choices').first();
        this.search_container = this.container.find('li.search-field').first();
      } else {
        this.search_container = this.container.find('div.chosen-search').first();
        this.selected_item = this.container.find('.chosen-single').first();
      }
      this.results_build();
      this.set_tab_index();
      return this.set_label_behavior();
    };

    Chosen.prototype.on_ready = function() {
      return this.form_field_jq.trigger("chosen:ready", {
        chosen: this
      });
    };

    Chosen.prototype.register_observers = function() {
      var _this = this;
      this.container.bind('touchstart.chosen', function(evt) {
        _this.container_mousedown(evt);
        return evt.preventDefault();
      });
      this.container.bind('touchend.chosen', function(evt) {
        _this.container_mouseup(evt);
        return evt.preventDefault();
      });
      this.container.bind('mousedown.chosen', function(evt) {
        _this.container_mousedown(evt);
      });
      this.container.bind('mouseup.chosen', function(evt) {
        _this.container_mouseup(evt);
      });
      this.container.bind('mouseenter.chosen', function(evt) {
        _this.mouse_enter(evt);
      });
      this.container.bind('mouseleave.chosen', function(evt) {
        _this.mouse_leave(evt);
      });
      this.search_results.bind('mouseup.chosen', function(evt) {
        _this.search_results_mouseup(evt);
      });
      this.search_results.bind('mouseover.chosen', function(evt) {
        _this.search_results_mouseover(evt);
      });
      this.search_results.bind('mouseout.chosen', function(evt) {
        _this.search_results_mouseout(evt);
      });
      this.search_results.bind('mousewheel.chosen DOMMouseScroll.chosen', function(evt) {
        _this.search_results_mousewheel(evt);
      });
      this.search_results.bind('touchstart.chosen', function(evt) {
        _this.search_results_touchstart(evt);
      });
      this.search_results.bind('touchmove.chosen', function(evt) {
        _this.search_results_touchmove(evt);
      });
      this.search_results.bind('touchend.chosen', function(evt) {
        _this.search_results_touchend(evt);
      });
      this.form_field_jq.bind("chosen:updated.chosen", function(evt) {
        _this.results_update_field(evt);
      });
      this.form_field_jq.bind("chosen:activate.chosen", function(evt) {
        _this.activate_field(evt);
      });
      this.form_field_jq.bind("chosen:open.chosen", function(evt) {
        _this.container_mousedown(evt);
      });
      this.form_field_jq.bind("chosen:close.chosen", function(evt) {
        _this.input_blur(evt);
      });
      this.search_field.bind('blur.chosen', function(evt) {
        _this.input_blur(evt);
      });
      this.search_field.bind('keyup.chosen', function(evt) {
        _this.keyup_checker(evt);
      });
      this.search_field.bind('keydown.chosen', function(evt) {
        _this.keydown_checker(evt);
      });
      this.search_field.bind('focus.chosen', function(evt) {
        _this.input_focus(evt);
      });
      this.search_field.bind('cut.chosen', function(evt) {
        _this.clipboard_event_checker(evt);
      });
      this.search_field.bind('paste.chosen', function(evt) {
        _this.clipboard_event_checker(evt);
      });
      if (this.is_multiple) {
        return this.search_choices.bind('click.chosen', function(evt) {
          _this.choices_click(evt);
        });
      } else {
        return this.container.bind('click.chosen', function(evt) {
          evt.preventDefault();
        });
      }
    };

    Chosen.prototype.destroy = function() {
      $(this.container[0].ownerDocument).unbind("click.chosen", this.click_test_action);
      if (this.search_field[0].tabIndex) {
        this.form_field_jq[0].tabIndex = this.search_field[0].tabIndex;
      }
      this.container.remove();
      this.form_field_jq.removeData('chosen');
      return this.form_field_jq.show();
    };

    Chosen.prototype.search_field_disabled = function() {
      this.is_disabled = this.form_field_jq[0].disabled;
      if (this.is_disabled) {
        this.container.addClass('chosen-disabled');
        this.search_field[0].disabled = true;
        if (!this.is_multiple) {
          this.selected_item.unbind("focus.chosen", this.activate_action);
        }
        return this.close_field();
      } else {
        this.container.removeClass('chosen-disabled');
        this.search_field[0].disabled = false;
        if (!this.is_multiple) {
          return this.selected_item.bind("focus.chosen", this.activate_action);
        }
      }
    };

    Chosen.prototype.container_mousedown = function(evt) {
      if (!this.is_disabled) {
        if (evt && evt.type === "mousedown" && !this.results_showing) {
          evt.preventDefault();
        }
        if (!((evt != null) && ($(evt.target)).hasClass("search-choice-close"))) {
          if (!this.active_field) {
            if (this.is_multiple) {
              this.search_field.val("");
            }
            $(this.container[0].ownerDocument).bind('click.chosen', this.click_test_action);
            this.results_show();
          } else if (!this.is_multiple && evt && (($(evt.target)[0] === this.selected_item[0]) || $(evt.target).parents("a.chosen-single").length)) {
            evt.preventDefault();
            this.results_toggle();
          }
          return this.activate_field();
        }
      }
    };

    Chosen.prototype.container_mouseup = function(evt) {
      if (evt.target.nodeName === "ABBR" && !this.is_disabled) {
        return this.results_reset(evt);
      }
    };

    Chosen.prototype.search_results_mousewheel = function(evt) {
      var delta;
      if (evt.originalEvent) {
        delta = evt.originalEvent.deltaY || -evt.originalEvent.wheelDelta || evt.originalEvent.detail;
      }
      if (delta != null) {
        evt.preventDefault();
        if (evt.type === 'DOMMouseScroll') {
          delta = delta * 40;
        }
        return this.search_results.scrollTop(delta + this.search_results.scrollTop());
      }
    };

    Chosen.prototype.blur_test = function(evt) {
      if (!this.active_field && this.container.hasClass("chosen-container-active")) {
        return this.close_field();
      }
    };

    Chosen.prototype.close_field = function() {
      $(this.container[0].ownerDocument).unbind("click.chosen", this.click_test_action);
      this.active_field = false;
      this.results_hide();
      this.container.removeClass("chosen-container-active");
      this.clear_backstroke();
      this.show_search_field_default();
      return this.search_field_scale();
    };

    Chosen.prototype.activate_field = function() {
      this.container.addClass("chosen-container-active");
      this.active_field = true;
      this.search_field.val(this.search_field.val());
      return this.search_field.focus();
    };

    Chosen.prototype.test_active_click = function(evt) {
      var active_container;
      active_container = $(evt.target).closest('.chosen-container');
      if (active_container.length && this.container[0] === active_container[0]) {
        return this.active_field = true;
      } else {
        return this.close_field();
      }
    };

    Chosen.prototype.results_build = function() {
      this.parsing = true;
      this.selected_option_count = null;
      this.results_data = SelectParser.select_to_array(this.form_field);
      if (this.is_multiple) {
        this.search_choices.find("li.search-choice").remove();
      } else if (!this.is_multiple) {
        this.single_set_selected_text();
        if (this.disable_search || this.form_field.options.length <= this.disable_search_threshold) {
          this.search_field[0].readOnly = true;
          this.container.addClass("chosen-container-single-nosearch");
        } else {
          this.search_field[0].readOnly = false;
          this.container.removeClass("chosen-container-single-nosearch");
        }
      }
      this.update_results_content(this.results_option_build({
        first: true
      }));
      this.search_field_disabled();
      this.show_search_field_default();
      this.search_field_scale();
      return this.parsing = false;
    };

    Chosen.prototype.result_do_highlight = function(el) {
      var high_bottom, high_top, maxHeight, visible_bottom, visible_top;
      if (el.length) {
        this.result_clear_highlight();
        this.result_highlight = el;
        this.result_highlight.addClass("highlighted");
        maxHeight = parseInt(this.search_results.css("maxHeight"), 10);
        visible_top = this.search_results.scrollTop();
        visible_bottom = maxHeight + visible_top;
        high_top = this.result_highlight.position().top + this.search_results.scrollTop();
        high_bottom = high_top + this.result_highlight.outerHeight();
        if (high_bottom >= visible_bottom) {
          return this.search_results.scrollTop((high_bottom - maxHeight) > 0 ? high_bottom - maxHeight : 0);
        } else if (high_top < visible_top) {
          return this.search_results.scrollTop(high_top);
        }
      }
    };

    Chosen.prototype.result_clear_highlight = function() {
      if (this.result_highlight) {
        this.result_highlight.removeClass("highlighted");
      }
      return this.result_highlight = null;
    };

    Chosen.prototype.results_show = function() {
      if (this.is_multiple && this.max_selected_options <= this.choices_count()) {
        this.form_field_jq.trigger("chosen:maxselected", {
          chosen: this
        });
        return false;
      }
      this.container.addClass("chosen-with-drop");
      this.results_showing = true;
      this.search_field.focus();
      this.search_field.val(this.search_field.val());
      this.winnow_results();
      return this.form_field_jq.trigger("chosen:showing_dropdown", {
        chosen: this
      });
    };

    Chosen.prototype.update_results_content = function(content) {
      return this.search_results.html(content);
    };

    Chosen.prototype.results_hide = function() {
      if (this.results_showing) {
        this.result_clear_highlight();
        this.container.removeClass("chosen-with-drop");
        this.form_field_jq.trigger("chosen:hiding_dropdown", {
          chosen: this
        });
      }
      return this.results_showing = false;
    };

    Chosen.prototype.set_tab_index = function(el) {
      var ti;
      if (this.form_field.tabIndex) {
        ti = this.form_field.tabIndex;
        this.form_field.tabIndex = -1;
        return this.search_field[0].tabIndex = ti;
      }
    };

    Chosen.prototype.set_label_behavior = function() {
      var _this = this;
      this.form_field_label = this.form_field_jq.parents("label");
      if (!this.form_field_label.length && this.form_field.id.length) {
        this.form_field_label = $("label[for='" + this.form_field.id + "']");
      }
      if (this.form_field_label.length > 0) {
        return this.form_field_label.bind('click.chosen', function(evt) {
          if (_this.is_multiple) {
            return _this.container_mousedown(evt);
          } else {
            return _this.activate_field();
          }
        });
      }
    };

    Chosen.prototype.show_search_field_default = function() {
      if (this.is_multiple && this.choices_count() < 1 && !this.active_field) {
        this.search_field.val(this.default_text);
        return this.search_field.addClass("default");
      } else {
        this.search_field.val("");
        return this.search_field.removeClass("default");
      }
    };

    Chosen.prototype.search_results_mouseup = function(evt) {
      var target;
      target = $(evt.target).hasClass("active-result") ? $(evt.target) : $(evt.target).parents(".active-result").first();
      if (target.length) {
        this.result_highlight = target;
        this.result_select(evt);
        return this.search_field.focus();
      }
    };

    Chosen.prototype.search_results_mouseover = function(evt) {
      var target;
      target = $(evt.target).hasClass("active-result") ? $(evt.target) : $(evt.target).parents(".active-result").first();
      if (target) {
        return this.result_do_highlight(target);
      }
    };

    Chosen.prototype.search_results_mouseout = function(evt) {
      if ($(evt.target).hasClass("active-result" || $(evt.target).parents('.active-result').first())) {
        return this.result_clear_highlight();
      }
    };

    Chosen.prototype.choice_build = function(item) {
      var choice, close_link,
        _this = this;
      choice = $('<li />', {
        "class": "search-choice"
      }).html("<span>" + (this.choice_label(item)) + "</span>");
      if (item.disabled) {
        choice.addClass('search-choice-disabled');
      } else {
        close_link = $('<a />', {
          "class": 'search-choice-close',
          'data-option-array-index': item.array_index
        });
        close_link.bind('click.chosen', function(evt) {
          return _this.choice_destroy_link_click(evt);
        });
        choice.append(close_link);
      }
      return this.search_container.before(choice);
    };

    Chosen.prototype.choice_destroy_link_click = function(evt) {
      evt.preventDefault();
      evt.stopPropagation();
      if (!this.is_disabled) {
        return this.choice_destroy($(evt.target));
      }
    };

    Chosen.prototype.choice_destroy = function(link) {
      if (this.result_deselect(link[0].getAttribute("data-option-array-index"))) {
        this.show_search_field_default();
        if (this.is_multiple && this.choices_count() > 0 && this.search_field.val().length < 1) {
          this.results_hide();
        }
        link.parents('li').first().remove();
        return this.search_field_scale();
      }
    };

    Chosen.prototype.results_reset = function() {
      this.reset_single_select_options();
      this.form_field.options[0].selected = true;
      this.single_set_selected_text();
      this.show_search_field_default();
      this.results_reset_cleanup();
      this.form_field_jq.trigger("change");
      if (this.active_field) {
        return this.results_hide();
      }
    };

    Chosen.prototype.results_reset_cleanup = function() {
      this.current_selectedIndex = this.form_field.selectedIndex;
      return this.selected_item.find("abbr").remove();
    };

    Chosen.prototype.result_select = function(evt) {
      var high, item;
      if (this.result_highlight) {
        high = this.result_highlight;
        this.result_clear_highlight();
        if (this.is_multiple && this.max_selected_options <= this.choices_count()) {
          this.form_field_jq.trigger("chosen:maxselected", {
            chosen: this
          });
          return false;
        }
        if (this.is_multiple) {
          high.removeClass("active-result");
        } else {
          this.reset_single_select_options();
        }
        high.addClass("result-selected");
        item = this.results_data[high[0].getAttribute("data-option-array-index")];
        item.selected = true;
        this.form_field.options[item.options_index].selected = true;
        this.selected_option_count = null;
        if (this.is_multiple) {
          this.choice_build(item);
        } else {
          this.single_set_selected_text(this.choice_label(item));
        }
        if (!((evt.metaKey || evt.ctrlKey) && this.is_multiple)) {
          this.results_hide();
        }
        this.search_field.val("");
        if (this.is_multiple || this.form_field.selectedIndex !== this.current_selectedIndex) {
          this.form_field_jq.trigger("change", {
            'selected': this.form_field.options[item.options_index].value
          });
        }
        this.current_selectedIndex = this.form_field.selectedIndex;
        evt.preventDefault();
        return this.search_field_scale();
      }
    };

    Chosen.prototype.single_set_selected_text = function(text) {
      if (text == null) {
        text = this.default_text;
      }
      if (text === this.default_text) {
        this.selected_item.addClass("chosen-default");
      } else {
        this.single_deselect_control_build();
        this.selected_item.removeClass("chosen-default");
      }
      return this.selected_item.find("span").html(text);
    };

    Chosen.prototype.result_deselect = function(pos) {
      var result_data;
      result_data = this.results_data[pos];
      if (!this.form_field.options[result_data.options_index].disabled) {
        result_data.selected = false;
        this.form_field.options[result_data.options_index].selected = false;
        this.selected_option_count = null;
        this.result_clear_highlight();
        if (this.results_showing) {
          this.winnow_results();
        }
        this.form_field_jq.trigger("change", {
          deselected: this.form_field.options[result_data.options_index].value
        });
        this.search_field_scale();
        return true;
      } else {
        return false;
      }
    };

    Chosen.prototype.single_deselect_control_build = function() {
      if (!this.allow_single_deselect) {
        return;
      }
      if (!this.selected_item.find("abbr").length) {
        this.selected_item.find("span").first().after("<abbr class=\"search-choice-close\"></abbr>");
      }
      return this.selected_item.addClass("chosen-single-with-deselect");
    };

    Chosen.prototype.get_search_text = function() {
      return $('<div/>').text($.trim(this.search_field.val())).html();
    };

    Chosen.prototype.winnow_results_set_highlight = function() {
      var do_high, selected_results;
      selected_results = !this.is_multiple ? this.search_results.find(".result-selected.active-result") : [];
      do_high = selected_results.length ? selected_results.first() : this.search_results.find(".active-result").first();
      if (do_high != null) {
        return this.result_do_highlight(do_high);
      }
    };

    Chosen.prototype.no_results = function(terms) {
      var no_results_html;
      no_results_html = $('<li class="no-results">' + this.results_none_found + ' "<span></span>"</li>');
      no_results_html.find("span").first().html(terms);
      this.search_results.append(no_results_html);
      return this.form_field_jq.trigger("chosen:no_results", {
        chosen: this
      });
    };

    Chosen.prototype.no_results_clear = function() {
      return this.search_results.find(".no-results").remove();
    };

    Chosen.prototype.keydown_arrow = function() {
      var next_sib;
      if (this.results_showing && this.result_highlight) {
        next_sib = this.result_highlight.nextAll("li.active-result").first();
        if (next_sib) {
          return this.result_do_highlight(next_sib);
        }
      } else {
        return this.results_show();
      }
    };

    Chosen.prototype.keyup_arrow = function() {
      var prev_sibs;
      if (!this.results_showing && !this.is_multiple) {
        return this.results_show();
      } else if (this.result_highlight) {
        prev_sibs = this.result_highlight.prevAll("li.active-result");
        if (prev_sibs.length) {
          return this.result_do_highlight(prev_sibs.first());
        } else {
          if (this.choices_count() > 0) {
            this.results_hide();
          }
          return this.result_clear_highlight();
        }
      }
    };

    Chosen.prototype.keydown_backstroke = function() {
      var next_available_destroy;
      if (this.pending_backstroke) {
        this.choice_destroy(this.pending_backstroke.find("a").first());
        return this.clear_backstroke();
      } else {
        next_available_destroy = this.search_container.siblings("li.search-choice").last();
        if (next_available_destroy.length && !next_available_destroy.hasClass("search-choice-disabled")) {
          this.pending_backstroke = next_available_destroy;
          if (this.single_backstroke_delete) {
            return this.keydown_backstroke();
          } else {
            return this.pending_backstroke.addClass("search-choice-focus");
          }
        }
      }
    };

    Chosen.prototype.clear_backstroke = function() {
      if (this.pending_backstroke) {
        this.pending_backstroke.removeClass("search-choice-focus");
      }
      return this.pending_backstroke = null;
    };

    Chosen.prototype.keydown_checker = function(evt) {
      var stroke, _ref1;
      stroke = (_ref1 = evt.which) != null ? _ref1 : evt.keyCode;
      this.search_field_scale();
      if (stroke !== 8 && this.pending_backstroke) {
        this.clear_backstroke();
      }
      switch (stroke) {
        case 8:
          this.backstroke_length = this.search_field.val().length;
          break;
        case 9:
          if (this.results_showing && !this.is_multiple) {
            this.result_select(evt);
          }
          this.mouse_on_container = false;
          break;
        case 13:
          if (this.results_showing) {
            evt.preventDefault();
          }
          break;
        case 32:
          if (this.disable_search) {
            evt.preventDefault();
          }
          break;
        case 38:
          evt.preventDefault();
          this.keyup_arrow();
          break;
        case 40:
          evt.preventDefault();
          this.keydown_arrow();
          break;
      }
    };

    Chosen.prototype.search_field_scale = function() {
      var div, f_width, h, style, style_block, styles, w, _i, _len;
      if (this.is_multiple) {
        h = 0;
        w = 0;
        style_block = "position:absolute; left: -1000px; top: -1000px; display:none;";
        styles = ['font-size', 'font-style', 'font-weight', 'font-family', 'line-height', 'text-transform', 'letter-spacing'];
        for (_i = 0, _len = styles.length; _i < _len; _i++) {
          style = styles[_i];
          style_block += style + ":" + this.search_field.css(style) + ";";
        }
        div = $('<div />', {
          'style': style_block
        });
        div.text(this.search_field.val());
        $('body').append(div);
        w = div.width() + 25;
        div.remove();
        f_width = this.container.outerWidth();
        if (w > f_width - 10) {
          w = f_width - 10;
        }
        return this.search_field.css({
          'width': w + 'px'
        });
      }
    };

    return Chosen;

  })(AbstractChosen);

}).call(this);
;/*! "Kalypto - Replace Radio/Checkbox Inputs" MIT license, LocalPCGuy, http://localpcguy.github.com/Kalypto */
/********************************
* Kalypto - Replace checkboxes and radio buttons
* Created & copyright (c)  by Mike Behnke
* v.0.2.2
* http://www.local-pc-guy.com
* Twitter: @LocalPCGuy
*
* Released under MIT License
*
* usage:
*        $("input[name=rDemo]").kalypto();
*        $("#checkboxDemo").kalypto();
* events: (bound on the input)
*        k_elbuilt: when an element is built
*        k_checked: when an element is checked
*        k_unchecked: when an element is checked
********************************/
; (function ($, undefined) {

    $.kalypto = function (element, options) {

        var plugin = this,
            $element = $(element),
            defaults = {
                toggleClass: "toggle",
                checkedClass: "checked",
                hideInputs: true,
                copyInputClasses: true,
                dataLabel: $element.data("label") || "",
                checkedEvent: "k_checked",
                uncheckedEvent: "k_unchecked",
                elBuiltEvent: "k_elbuilt",
                customClasses: ""
            },
            $customEl,
            buildCustomElement = function () {
                if ($element.next().hasClass(plugin.settings.toggleClass)) { return; }
                $element.after(function () {
                    var classes = plugin.settings.toggleClass;
                    if (plugin.settings.copyInputClasses) {
                        var elClass = $element.attr("class");
                        if(elClass){
                            classes += " " + elClass;
                        }
                    }
                    if (plugin.settings.customClasses.length) {
                        classes += " " + plugin.settings.customClasses;
                    }
                    if ($element.is(":checked")) {
                        return "<a href='#' class='" + classes + " " + plugin.settings.checkedClass + "'>" + plugin.settings.dataLabel + "</a>";
                    } else {
                        return "<a href='#' class='" + classes + "'>" + plugin.settings.dataLabel + "</a>";
                    }
                });
                if (plugin.settings.hideInputs) {
                    $element.hide();
                }
                $customEl = $element.next();
                $element.trigger(plugin.settings.elBuiltEvent);
            },
            lastClickedEl,
            handleChange = function (e) {
                var $elementCollection = $element.attr("type") === "radio" ? $('input[name="' + $element.attr("name") + '"]') : $element,
                    doTriggerAndChangeClasses = function() {
                        if ($element.attr("type") === "radio") {
                            $elementCollection.each(function (k, el) {
                                var $el = $(el);
                                $el.next().removeClass(plugin.settings.checkedClass);
                                if (!$el.is(":checked") && plugin.lastClickedEl !== $el.next().get(0)) {
                                    // Should this only trigger on the radio button that was previously checked
                                    // Currently fires on all radio buttons BUT the one justed checked
                                    $el.trigger(plugin.settings.uncheckedEvent);
                                }
                            });
                        }                   
                        if ($element.is(":checked")) { $element.trigger(plugin.settings.checkedEvent); }
                        else { $element.trigger(plugin.settings.uncheckedEvent); }
                        $element.next().toggleClass(plugin.settings.checkedClass);
                    };
                
                if (this.tagName !== "INPUT") {
                    e.preventDefault();
                    plugin.lastClickedEl = this;
                    $element.trigger('click');
                } else {
                    setTimeout(doTriggerAndChangeClasses, 0);
                }
            },
            initEvents = function () {
                $element.next().bind("click", handleChange);
                $element.bind("change", handleChange);
            };

        plugin.settings = {};

        plugin.init = function () {
            plugin.settings = $.extend({}, defaults, options);
            buildCustomElement();
            initEvents();
        };

        plugin.init();
    };
    $.fn.kalypto = function (options) {
        return this.each(function () {
            if (undefined === $(this).data('kalypto')) {
                var plugin = new $.kalypto(this, options);
                $(this).data('kalypto', plugin);
            }
        });
    };

})(jQuery);;/*
	Redactor v9.2.5
	Updated: Jun 5, 2014

	http://imperavi.com/redactor/

	Copyright (c) 2009-2014, Imperavi LLC.
	License: http://imperavi.com/redactor/license/

	Usage: $('#content').redactor();
*/
eval(function(p,a,c,k,e,d){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--){d[e(c)]=k[c]||e(c)}k=[function(e){return d[e]}];e=function(){return'\\w+'};c=1};while(c--){if(k[c]){p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c])}}return p}('(B($){q 7d=0;"pp pt";q cF=B(O){c[0]=O.p8;c[1]=O.iK;c.O=O;F c};cF.5j.kb=B(){F c[0]===c[1]};q 8r=/5l?:\\/\\/(?:[0-9A-Z-]+\\.)?(?:pS\\.be\\/|d5\\.7w\\S*[^\\w\\-\\s])([\\w\\-]{11})(?=[^\\w\\-]|$)(?![?=&+%\\w.-]*(?:[\'"][^<>]*>|<\\/a>))[?=&+%\\w.-]*/ig;q 8J=/5l?:\\/\\/(ao\\.)?ci.7w\\/(\\d+)($|\\/)/;$.fn.U=B(3N){q 1p=[];q ft=gy.5j.j3.5K(fU,1);if(1E 3N==="7R"){c.1u(B(){q 7a=$.1a(c,"U");if(1E 7a!=="1I"&&$.7J(7a[3N])){q bc=7a[3N].cJ(7a,ft);if(bc!==1I&&bc!==7a){1p.3b(bc)}}I{F $.43(\'pC pA 5v "\'+3N+\'" 3v 3E\')}})}I{c.1u(B(){if(!$.1a(c,"U")){$.1a(c,"U",3E(c,3N))}})}if(1p.1m===0){F c}I{if(1p.1m===1){F 1p[0]}I{F 1p}}};B 3E(el,3N){F 28 3E.5j.74(el,3N)}$.3E=3E;$.3E.ot="9.2.5";$.3E.C={3Y:E,1R:E,4m:E,1f:E,1y:"en",69:"ou",4T:E,8o:E,hS:E,fq:N,gx:N,fZ:N,kF:E,9P:N,lb:N,cS:N,e9:E,hy:E,5i:N,2d:E,7q:E,4Z:N,7L:E,ff:E,6B:{"3w+m, 4B+m":"c.25(\'oA\', E)","3w+b, 4B+b":"c.25(\'3p\', E)","3w+i, 4B+i":"c.25(\'3m\', E)","3w+h, 4B+h":"c.25(\'hj\', E)","3w+l, 4B+l":"c.25(\'hi\', E)","3w+k, 4B+k":"c.a1()","3w+8h+7":"c.25(\'8M\', E)","3w+8h+8":"c.25(\'8e\', E)"},gb:E,7b:E,bk:60,cV:E,85:"al://",fy:E,7D:50,eY:E,8q:"9e",7j:E,gc:N,k1:N,3X:E,aK:"26",js:N,81:E,e5:"26",9u:N,c7:E,cR:["T/l8","T/kf","T/l4"],5B:E,3J:E,4H:N,5r:N,bo:N,aX:E,gw:N,30:E,go:["6w","3p","3m","5u","6u","64","6S","6g"],1z:N,6x:E,8u:X,8U:0,9k:E,8z:E,gd:E,gh:N,44:["o","6w","3p","3m","5u","6u","64","6S","6g","T","3z","26","1o","1s","aQ","|","8c"],eg:[],9L:["5u","3m","3p","4J","6u","64","d4","cE","cq","cu","1o"],ez:{b:"3p",3T:"3p",i:"3m",em:"3m",5b:"5u",5E:"5u",2q:"6u",ol:"64",u:"4J",3f:"1o",1g:"1o",1o:"1o"},gg:["p","2g","2r","h1","h2","h3","h4","h5","h6"],1P:E,62:N,9w:N,6J:N,6m:E,7E:E,hc:E,8R:E,5S:E,8t:["o","9D","1s","2w","4B","3l","1n","ox"],6o:"3T",6n:"em",9V:20,3H:[],7P:[],67:E,52:"<p>&#b2;</p>",2e:"&#b2;",cs:/^(P|H[1-6]|3e|aV|b3|bg|ae|am|9J)$/i,5D:["P","fo","fL","fv","fr","fk","fO","fQ","fR","fg","8y","6j","3D","hs","cg","aV","b3","bg","ae","am","9J"],e2:["fl","2w","9D","hr","i?2Z","1s","4B","od","1n","3l","1o","8L","3U","d0"],dH:["li","dt","dt","h[1-6]","48","3l"],hK:["2g","12","dl","fm","2t","p1","fs","ol","p","2r","3o","1g","fc","3f","2q"],9N:["P","fo","fL","fv","fr","fk","fO","fQ","fR","fg","8y","3e","3D","hs","cg","6f","aV","b3","bg","ae","am","9J","6j"],ga:{en:{o:"kW",3z:"6q he",T:"6q h8",1o:"bz",1s:"hg",er:"6q 1s",hX:"hH 1s",6e:"rn",6w:"ru",gn:"q5 Y",fY:"q2",2k:"hB",fX:"8B 1",g0:"8B 2",g1:"8B 3",g6:"8B 4",g7:"8B 5",3p:"q9",3m:"qd",qa:"qt hn",qN:"qL hn",6u:"qz ht",64:"qy ht",6S:"qx",6g:"qv",6M:"qA",4v:"6q",7X:"qG",ii:"8x",dv:"6q bz",e8:"8s cQ qF",eR:"8s cQ qC",eU:"8s cI hw",ej:"8s cI hl",ca:"8x cI",bv:"8x cQ",bI:"8x bz",aw:"ln",av:"lq",eb:"8s hv",eu:"8x hv",1c:"lw",ie:"lu",3n:"lz",1t:"hw",4K:"hl",77:"hC",j0:"h8 lS hg",Y:"lV",eH:"lr",o6:"j4",ke:"he o0 hB",26:"6q nR",89:"jE",nF:"nP",dK:"mu",jK:"mm dK",jM:"md 26 m9",ch:"hY Y hV i2 1t",cv:"hC Y",ea:"hY Y hV i2 4K",dJ:"mW Y",8c:"6q mS mT",5u:"n4",n0:"mQ",do:"mF 1s in 28 55",4J:"mD",aQ:"mE",dX:"mJ (mO)",84:"hH"}}};3E.fn=$.3E.5j={2M:{9t:8,dF:46,bs:40,aY:13,bK:27,gs:9,mM:17,mL:91,mN:37,gA:91},74:B(el,3N){c.7Z=E;c.$2h=c.$1v=$(el);c.7d=7d++;q C=$.4F(N,{},$.3E.C);c.C=$.4F({},C,c.$2h.1a(),3N);c.2v=N;c.mP=[];c.as=c.$1v.1f("22");c.mK=c.$1v.1f("2p");if(c.C.4m){c.C.1R=N}if(c.C.1P){c.C.62=E}if(c.C.62){c.C.1P=E}if(c.C.9k){c.C.6x=N}c.X=X;c.3Z=3Z;c.5h=E;c.fN=28 2G("^<(/?"+c.C.e2.63("|/?")+"|"+c.C.dH.63("|")+")[ >]");c.fE=28 2G("^<(br|/?"+c.C.e2.63("|/?")+"|/"+c.C.dH.63("|/")+")[ >]");c.cX=28 2G("^</?("+c.C.hK.63("|")+")[ >]");c.a6=28 2G("^("+c.C.9N.63("|")+")$","i");if(c.C.1P===E){if(c.C.5S!==E){q dA=["3T","em","5b"];q hO=["b","i","5E"];if($.4W("p",c.C.5S)==="-1"){c.C.5S.3b("p")}3v(i in dA){if($.4W(dA[i],c.C.5S)!="-1"){c.C.5S.3b(hO[i])}}}if(c.C.8t!==E){q 49=$.4W("p",c.C.8t);if(49!=="-1"){c.C.8t.9C(49,49)}}}if(c.1D("3q")||c.1D("bj")){c.C.44=c.iQ(c.C.44,"8c")}c.C.1F=c.C.ga[c.C.1y];$.4F(c.C.6B,c.C.gb);c.gJ();c.fp()},hM:B(1y){F{o:{1c:1y.o,1H:"e6"},6w:{1c:1y.6w,1H:"2N",1Q:{p:{1c:1y.gn,1H:"5F"},2g:{1c:1y.fY,1H:"cY",2R:"mR"},2r:{1c:1y.2k,1H:"5F",2R:"n1"},h1:{1c:1y.fX,1H:"5F",2R:"n3"},h2:{1c:1y.g0,1H:"5F",2R:"mY"},h3:{1c:1y.g1,1H:"5F",2R:"mU"},h4:{1c:1y.g6,1H:"5F",2R:"mX"},h5:{1c:1y.g7,1H:"5F",2R:"mB"}}},3p:{1c:1y.3p,2o:"3p"},3m:{1c:1y.3m,2o:"3m"},5u:{1c:1y.5u,2o:"hh"},4J:{1c:1y.4J,2o:"4J"},6u:{1c:"&mh; "+1y.6u,2o:"8e"},64:{1c:"1. "+1y.64,2o:"8M"},6S:{1c:"< "+1y.6S,1H:"bF"},6g:{1c:"> "+1y.6g,1H:"bJ"},T:{1c:1y.T,1H:"jP"},3z:{1c:1y.3z,1H:"iz"},26:{1c:1y.26,1H:"jG"},1o:{1c:1y.1o,1H:"2N",1Q:{dv:{1c:1y.dv,1H:"im"},mg:{2m:"93"},e8:{1c:1y.e8,1H:"iA"},eR:{1c:1y.eR,1H:"iC"},eU:{1c:1y.eU,1H:"iD"},ej:{1c:1y.ej,1H:"iE"},ma:{2m:"93"},eb:{1c:1y.eb,1H:"ir"},eu:{1c:1y.eu,1H:"ed"},mc:{2m:"93"},ca:{1c:1y.ca,1H:"ic"},bv:{1c:1y.bv,1H:"ib"},bI:{1c:1y.bI,1H:"i9"}}},1s:{1c:1y.1s,1H:"2N",1Q:{1s:{1c:1y.er,1H:"a1"},6e:{1c:1y.6e,2o:"6e"}}},aQ:{1c:1y.aQ,1H:"2N",1Q:{d4:{1c:1y.ch,1H:"ck"},cE:{1c:1y.cv,1H:"cz"},cq:{1c:1y.ea,1H:"cf"},cu:{1c:1y.dJ,1H:"cy"}}},d4:{1c:1y.ch,1H:"ck"},cE:{1c:1y.cv,1H:"cz"},cq:{1c:1y.ea,1H:"cf"},mA:{1c:1y.dJ,1H:"cy"},8c:{2o:"h7",1c:1y.8c}}},1d:B(1G,5U,1a){q 1d=c.C[1G+"mo"];if($.7J(1d)){if(5U===E){F 1d.5K(c,1a)}I{F 1d.5K(c,5U,1a)}}I{F 1a}},mq:B(){gQ(c.bk);$(3Z).3C(".U");c.$1v.3C("U-5H");c.$2h.3C(".U").mr("U");q o=c.2T();if(c.C.67){c.$2C.2L(c.$1v);c.$2C.1w();c.$1v.1p(o).2N()}I{q $1A=c.$K;if(c.C.1R){$1A=c.$2h}c.$2C.2L($1A);c.$2C.1w();$1A.3d("4o").3d("hI").2D("3L").o(o).2N()}if(c.C.8z){$(c.C.8z).o("")}if(c.C.30){$("#gf"+c.7d).1w()}},mt:B(){F $.4F({},c)},ms:B(){F c.$K},n5:B(){F c.$2C},n6:B(){F(c.C.1R)?c.$2Z:E},nN:B(){F(c.$1z)?c.$1z:E},2T:B(){F c.$1v.1p()},ha:B(){c.$K.2D("3L").2D("5Z");q o=c.3W(c.$2Z.1X().4c());c.$K.1i({3L:N,5Z:c.C.69});F o},7h:B(o,aZ,cD){o=o.4b();o=o.G(/\\$/g,"&#36;");if(c.C.4m){c.gL(o)}I{c.gu(o,aZ)}if(o==""){cD=E}if(cD!==E){c.gq()}},gu:B(o,aZ){if(aZ!==E){o=c.dP(o);o=c.7Q(o);o=c.eA(o);o=c.8T(o,N);if(c.C.1P===E){o=c.c1(o)}I{o=o.G(/<p(.*?)>([\\w\\W]*?)<\\/p>/gi,"$2<br>")}}o=o.G(/&9Q;#36;/g,"$");o=c.by(o);c.$K.o(o);c.8K();c.aL();c.1j()},gL:B(o){q 3A=c.bR();c.$2Z[0].3r="nM:nO";o=c.eA(o);o=c.8T(o);o=c.7C(o);3A.aW();3A.gW(o);3A.gV();if(c.C.4m){c.$K=c.$2Z.1X().1b("2w").1i({3L:N,5Z:c.C.69})}c.8K();c.aL();c.1j()},e3:B(o){c.7t=o.1V(/^<\\!gO[^>]*>/i);if(c.7t&&c.7t.1m==1){o=o.G(/^<\\!gO[^>]*>/i,"")}o=c.dP(o,N);o=c.c1(o);o=c.by(o);c.$K.o(o);c.8K();c.aL();c.1j()},fI:B(){if(c.7t&&c.7t.1m==1){q 1v=c.7t[0]+"\\n"+c.$1v.1p();c.$1v.1p(1v)}},aL:B(){q aE=c.$K.1b("V");q 7f="47";$.1u(aE,B(){q 9b=c.l6;q 5M=28 2G("<"+c.Q,"gi");q 5Q=9b.G(5M,"<"+7f);5M=28 2G("</"+c.Q,"gi");5Q=5Q.G(5M,"</"+7f);$(c).2i(5Q)})},bd:B(o){o=o.G(/<V(.*?)>/,"<47$1>");F o.G(/<\\/V>/,"</47>")},8K:B(){c.$K.1b(".nQ").1i("3L",E)},1j:B(e){q o="";c.fz();if(c.C.4m){o=c.ha()}I{o=c.$K.o()}o=c.bq(o);o=c.dQ(o);q 1v=c.7C(c.$1v.1p(),E);q K=c.7C(o,E);if(1v==K){F E}o=o.G(/<\\/li><(2q|ol)>([\\w\\W]*?)<\\/(2q|ol)>/gi,"<$1>$2</$1></li>");if($.2a(o)==="<br>"){o=""}if(c.C.hy){q hu=["br","hr","1B","1s","2S","4B"];$.1u(hu,B(i,s){o=o.G(28 2G("<"+s+"(.*?[^/$]?)>","gi"),"<"+s+"$1 />")})}o=c.1d("nE",E,o);c.$1v.1p(o);c.fI();c.1d("nH",E,o);if(c.2v===E){if(1E e!="1I"){kz(e.5Y){5L 37:54;5L 38:54;5L 39:54;5L 40:54;nJ:c.1d("5W",E,o)}}I{c.1d("5W",E,o)}}},bq:B(o){if(!c.C.4m){o=c.7Q(o)}o=$.2a(o);o=c.gp(o);o=o.G(/&#b2;/gi,"");o=o.G(/&#nI;/gi,"");o=o.G(/<\\/a>&3j;/gi,"</a> ");o=o.G(/\\7K/g,"");if(o=="<p></p>"||o=="<p> </p>"||o=="<p>&3j;</p>"){o=""}if(c.C.fy){o=o.G(/<a(.*?)4n="fD"(.*?)>/gi,"<a$1$2>");o=o.G(/<a(.*?)>/gi,\'<a$1 4n="fD">\')}o=o.G("<!--?4M","<?4M");o=o.G("?-->","?>");o=o.G(/<(.*?)1x="hR"(.*?) 3L="E"(.*?)>/gi,\'<$nS="hR"$2$3>\');o=o.G(/ 1a-8b=""/gi,"");o=o.G(/<br\\s?\\/?>\\n?<\\/(P|H[1-6]|3e|aV|b3|bg|ae|am|9J)>/gi,"</$1>");o=o.G(/<V(.*?)id="U-T-2C"(.*?)>([\\w\\W]*?)<1B(.*?)><\\/V>/gi,"$3<1B$4>");o=o.G(/<V(.*?)id="U-T-cK"(.*?)>(.*?)<\\/V>/gi,"");o=o.G(/<V(.*?)id="U-T-d9"(.*?)>(.*?)<\\/V>/gi,"");o=o.G(/<(2q|ol)>\\s*\\t*\\n*<\\/(2q|ol)>/gi,"");if(c.C.cS){o=o.G(/<2u(.*?)>([\\w\\W]*?)<\\/2u>/gi,"$2")}o=o.G(/<V(.*?)>([\\w\\W]*?)<\\/V>/gi,"$2");o=o.G(/<47>([\\w\\W]*?)<\\/47>/gi,"$1");o=o.G(/<47>/gi,"<V>");o=o.G(/<47 /gi,"<V ");o=o.G(/<\\/47>/gi,"</V>");if(c.C.9P){o=o.G(/<V>([\\w\\W]*?)<\\/V>/gi,"$1")}o=o.G(/<V(.*?)1x="5c"(.*?)>([\\w\\W]*?)<\\/V>/gi,"");o=o.G(/<1B(.*?)3L="E"(.*?)>/gi,"<1B$1$2>");o=o.G(/&/gi,"&");o=o.G(/\\nZ/gi,"&nU;");o=o.G(/\\nT/gi,"&nV;");o=o.G(/\\nW/gi,"&nY;");o=o.G(/\\nX/gi,"&nD;");o=o.G(/\\nC/gi,"&ng;");o=c.hq(o);F o},fp:B(){c.3G="";c.$2C=$(\'<12 1x="nh" />\');if(c.$1v[0].Q==="ni"){c.C.67=N}if(c.C.fq===E&&c.5f()){c.ho()}I{c.hm();if(c.C.1R){c.C.4Z=E;c.5m()}I{if(c.C.67){c.hz()}I{c.hA()}}if(!c.C.1R){c.et();c.eB()}}},ho:B(){if(!c.C.67){c.$K=c.$1v;c.$K.2U();c.$1v=c.aR(c.$K);c.$1v.1p(c.3G)}c.$2C.aT(c.$1v).1h(c.$1v)},hm:B(){if(c.C.67){c.3G=$.2a(c.$1v.1p())}I{c.3G=$.2a(c.$1v.o())}},hz:B(){c.$K=$("<12 />");c.$2C.aT(c.$1v).1h(c.$K).1h(c.$1v);c.hk(c.$K);c.bN()},hA:B(){c.$K=c.$1v;c.$1v=c.aR(c.$K);c.$2C.aT(c.$K).1h(c.$K).1h(c.$1v);c.bN()},aR:B($1v){F $("<5H />").1i("2m",$1v.1i("id")).1f("22",c.as)},hk:B(el){$.1u(c.$1v.2T(0).2R.4d(/\\s+/),B(i,s){el.2z("n8"+s)})},bN:B(){c.$K.2z("4o").1i({3L:N,5Z:c.C.69});c.$1v.1i("5Z",c.C.69).2U();c.7h(c.3G,N,E)},et:B(){q $1v=c.$K;if(c.C.1R){$1v=c.$2Z}if(c.C.7q){$1v.1i("7q",c.C.7q)}if(c.C.7L){$1v.1f("hd-22",c.C.7L+"px")}I{if(c.1D("31")&&c.C.1P){c.$K.1f("hd-22","n7")}}if(c.1D("31")&&c.C.1P){c.$K.1f("9a-jt","9e")}if(c.C.ff){c.C.4Z=E;c.as=c.C.ff}if(c.C.hS){c.$K.2z("hI")}if(c.C.8o){c.$K.2z("U-K-8o")}if(!c.C.4Z){$1v.1f("22",c.as)}},eB:B(){c.2v=E;if(c.C.1z){c.C.1z=c.hM(c.C.1F);c.gm()}c.jJ();c.gR();c.dL();if(c.C.7b){c.7b()}2Y($.M(c.7F,c),4);if(c.1D("31")){c2{c.X.25("nc",E,E);c.X.25("nb",E,E)}c6(e){}}if(c.C.2d){2Y($.M(c.2d,c),3O)}if(!c.C.5i){2Y($.M(B(){c.C.5i=N;c.e6(E)},c),5R)}c.1d("74")},dL:B(){c.8i=0;if(c.C.gc&&(c.C.3X!==E||c.C.5B!==E)){c.$K.on("6D.U",$.M(c.gK,c))}c.$K.on("23.U",$.M(B(){c.5P=E},c));c.$K.on("2S.U",$.M(c.1j,c));c.$K.on("9z.U",$.M(c.gT,c));c.$K.on("5y.U",$.M(c.gG,c));c.$K.on("5a.U",$.M(c.gI,c));if($.7J(c.C.g9)){c.$1v.on("5y.U-5H",$.M(c.C.g9,c))}if($.7J(c.C.g8)){c.$K.on("2d.U",$.M(c.C.g8,c))}q az;$(X).92(B(e){az=$(e.1O)});c.$K.on("eX.U",$.M(B(e){if(!$(az).3x("cd")&&$(az).8f(".cd").1U()==0){c.5P=E;if($.7J(c.C.nm)){c.1d("eX",e)}}},c))},gK:B(e){e=e.i5||e;if(3Z.es===1I||!e.9c){F N}q 1m=e.9c.7e.1m;if(1m==0){F N}e.2A();q 26=e.9c.7e[0];if(c.C.cR!==E&&c.C.cR.3V(26.1G)==-1){F N}c.21();c.80();if(c.C.5B===E){c.bm(c.C.3X,26,N,e,c.C.aK)}I{c.cU(26)}},gT:B(e){q d3=E;if(c.1D("4y")&&6r.7g.3V("nz")===-1){q 2O=c.1D("9r").4d(".");if(2O[0]<nA){d3=N}}if(d3){F N}if(c.1D("bj")){F N}if(c.C.9u&&c.gC(e)){F N}if(c.C.gx){c.7Z=N;c.29();if(!c.5P){if(c.C.4Z===N&&c.ei!==N){c.$K.22(c.$K.22());c.9v=c.X.2w.3c}I{c.9v=c.$K.3c()}}q 4k=c.dh();2Y($.M(B(){q gB=c.dh();c.$K.1h(4k);c.1Y();q o=c.lc(gB);c.k9(o);if(c.C.4Z===N&&c.ei!==N){c.$K.1f("22","4g")}},c),1)}},gC:B(e){q 5U=e.i5||e;c.gE=E;if(1E(5U.ee)==="1I"){F E}if(5U.ee.gD){q 26=5U.ee.gD[0].nv();if(26!==2H){c.21();c.gE=N;q bP=28 no();bP.jL=$.M(c.kO,c);bP.np(26);F N}}F E},gG:B(e){if(c.7Z){F E}q 1k=e.5Y;q 3w=e.aO||e.87;q L=c.2B();q 1r=c.3S();q 1l=c.2Q();q 2r=E;c.1d("5y",e);if(c.1D("31")&&"c9"in 3Z.2b()){if((3w)&&(e.2M===37||e.2M===39)){q 1J=c.2b();q bE=(e.87?"bh":"nt");if(e.2M===37){1J.c9("4F","1t",bE);if(!e.5d){1J.hD()}}if(e.2M===39){1J.c9("4F","4K",bE);if(!e.5d){1J.lo()}}e.2A()}}c.6y(E);if((L&&$(L).2T(0).Q==="6f")||(1r&&$(1r).2T(0).Q==="6f")){2r=N;if(1k===c.2M.bs){c.7r(1l)}}if(1k===c.2M.bs){if(L&&$(L)[0].Q==="3D"){c.7r(L)}if(1r&&$(1r)[0].Q==="3D"){c.7r(1r)}if(L&&$(L)[0].Q==="P"&&$(L).L()[0].Q=="3D"){c.7r(L,$(L).L()[0])}if(1r&&$(1r)[0].Q==="P"&&L&&$(L)[0].Q=="3D"){c.7r(1r,L)}}c.6B(e,1k);if(3w&&1k===90&&!e.5d&&!e.gF){e.2A();if(c.C.3H.1m){c.kU()}I{c.X.25("ll",E,E)}F}I{if(3w&&1k===90&&e.5d&&!e.gF){e.2A();if(c.C.7P.1m!=0){c.kT()}I{c.X.25("lX",E,E)}F}}if(1k==32){c.21()}if(3w&&1k===65){c.21();c.5P=N}I{if(1k!=c.2M.gA&&!3w){c.5P=E}}if(1k==c.2M.aY&&!e.5d&&!e.aO&&!e.87){q O=c.3s();if(O&&O.4X===E){1q=c.2b();if(1q.4S){O.94()}}if(c.1D("3q")&&(L.4q==1&&(L.Q=="6j"||L.Q=="lZ"))){e.2A();c.21();c.3B(X.4u("br"));c.1d("7H",e);F E}if(1l&&(1l.Q=="3D"||$(1l).L()[0].Q=="3D")){if(c.eW()){if(c.8i==1){q 2h;q 2V;if(1l.Q=="3D"){2V="br";2h=1l}I{2V="p";2h=$(1l).L()[0]}e.2A();c.dp(2h);c.8i=0;if(2V=="p"){$(1l).L().1b("p").2V().1w()}I{q 2J=$.2a($(1l).o());$(1l).o(2J.G(/<br\\s?\\/?>$/i,""))}F}I{c.8i++}}I{c.8i++}}if(2r===N){F c.gr(e,1r)}I{if(!c.C.1P){if(1l&&1l.Q=="3e"){q 5N=c.2Q();if(5N!==E||5N.Q==="3e"){q 9R=$.2a($(1l).Y());q gz=$.2a($(5N).Y());if(9R==""&&gz==""&&$(5N).4e("li").1U()==0&&$(5N).8f("li").1U()==0){c.21();q $3i=$(5N).2f("ol, 2q");$(5N).1w();q J=$("<p>"+c.C.2e+"</p>");$3i.2L(J);c.4j(J);c.1j();c.1d("7H",e);F E}}}if(1l&&c.C.cs.4h(1l.Q)){c.21();2Y($.M(B(){q 58=c.2Q();if(58.Q==="8y"&&!$(58).3x("4o")){q J=$("<p>"+c.C.2e+"</p>");$(58).2i(J);c.4j(J)}},c),1)}I{if(1l===E){c.21();q J=$("<p>"+c.C.2e+"</p>");c.3B(J[0]);c.4j(J);c.1d("7H",e);F E}}}if(c.C.1P){if(1l&&c.C.cs.4h(1l.Q)){c.21();2Y($.M(B(){q 58=c.2Q();if((58.Q==="8y"||58.Q==="P")&&!$(58).3x("4o")){c.kc(58)}},c),1)}I{F c.d1(e)}}if(1l.Q=="3D"||1l.Q=="cg"){F c.d1(e)}}c.1d("7H",e)}I{if(1k===c.2M.aY&&(e.aO||e.5d)){c.21();e.2A();c.ai()}}if((1k===c.2M.gs||e.87&&1k===g4)&&c.C.6B){F c.gv(e,2r,1k)}if(1k===c.2M.9t){c.gH(e,1r,L)}},gr:B(e,1r){e.2A();c.21();q o=$(1r).L().Y();c.3B(X.8N("\\n"));if(o.51(/\\s$/)==-1){c.3B(X.8N("\\n"))}c.1j();c.1d("7H",e);F E},gv:B(e,2r,1k){if(!c.C.gw){F N}if(c.bb(c.2T())&&c.C.aX===E){F N}e.2A();if(2r===N&&!e.5d){c.21();c.3B(X.8N("\\t"));c.1j();F E}I{if(c.C.aX!==E){c.21();c.3B(X.8N(gy(c.C.aX+1).63("\\lM")));c.1j();F E}I{if(!e.5d){c.bJ()}I{c.bF()}}}F E},gH:B(e,1r,L){if(L&&1r&&L.4V.Q=="6j"&&L.Q=="h9"&&1r.Q=="3e"&&$(L).4c("li").1U()==1){q Y=$(1r).Y().G(/[\\7K-\\fP\\fC]/g,"");if(Y==""){q J=L.4V;$(L).1w();c.4j(J);c.1j();F E}}if(1E 1r.Q!=="1I"&&/^(H[1-6])$/i.4h(1r.Q)){q J;if(c.C.1P===E){J=$("<p>"+c.C.2e+"</p>")}I{J=$("<br>"+c.C.2e)}$(1r).2i(J);c.4j(J);c.1j()}if(1E 1r.aq!=="1I"&&1r.aq!==2H){if(1r.1w&&1r.4q===3&&1r.aq.1V(/[^\\7K]/g)==2H){$(1r).4L().1w();c.1j()}}},d1:B(e){c.21();e.2A();c.ai();c.1d("7H",e);F},gI:B(e){if(c.7Z){F E}q 1k=e.5Y;q L=c.2B();q 1r=c.3S();if(!c.C.1P&&1r.4q==3&&(L==E||L.Q=="bT")){q J=$("<p>").1h($(1r).68());$(1r).2i(J);q 4e=$(J).4e();if(1E(4e[0])!=="1I"&&4e[0].Q=="cr"){4e.1w()}c.7v(J)}if((c.C.6J||c.C.6m||c.C.7E)&&1k===c.2M.aY){c.gS()}if(1k===c.2M.dF||1k===c.2M.9t){F c.fF(e)}c.1d("5a",e);c.1j(e)},gS:B(){c.dZ(c.C.85,c.C.6J,c.C.6m,c.C.7E,c.C.7D);2Y($.M(B(){if(c.C.6m){c.4H()}if(c.C.5r){c.5r()}},c),5)},gR:B(){if(!c.C.cV){F}$.1u(c.C.cV,$.M(B(i,s){if(cL[s]){$.4F(c,cL[s]);if($.7J(cL[s].74)){c.74()}}},c))},5m:B(){c.gU();if(c.C.67){c.bG(c.$1v)}I{c.$bC=c.$1v.2U();c.$1v=c.aR(c.$bC);c.bG(c.$bC)}},bG:B(el){c.$1v.1i("5Z",c.C.69).2U();c.$2C.aT(el).1h(c.$2Z).1h(c.$1v)},gU:B(){c.$2Z=$(\'<1R 1n="2p: 3O%;" cN="0" />\').8V("jl",$.M(B(){if(c.C.4m){c.bR();if(c.3G===""){c.3G=c.C.2e}c.$2Z.1X()[0].gW(c.3G);c.$2Z.1X()[0].gV();q gP=gl($.M(B(){if(c.$2Z.1X().1b("2w").o()){gQ(gP);c.bS()}},c),0)}I{c.bS()}},c))},cB:B(){F c.$2Z[0].bt.X},bR:B(){q 3A=c.cB();if(3A.aM){3A.lR(3A.aM)}F 3A},bW:B(1f){1f=1f||c.C.1f;if(c.kd(1f)){c.$2Z.1X().1b("9D").1h(\'<1s 4n="m2" 1S="\'+1f+\'" />\')}if($.m1(1f)){$.1u(1f,$.M(B(i,1M){c.bW(1M)},c))}},bS:B(){c.$K=c.$2Z.1X().1b("2w").1i({3L:N,5Z:c.C.69});if(c.$K[0]){c.X=c.$K[0].m3;c.3Z=c.X.m6||3Z}c.bW();if(c.C.4m){c.e3(c.$1v.1p())}I{c.7h(c.3G,N,E)}c.et();c.eB()},gJ:B(){if(c.C.4T!==E){c.eV=c.C.4T;c.C.4T=N}I{if(1E c.$2h.1i("4T")=="1I"||c.$2h.1i("4T")==""){c.C.4T=E}I{c.eV=c.$2h.1i("4T");c.C.4T=N}}},hp:B(o){if(c.C.4T===E){F E}if(c.bb(o)){c.C.2d=E;c.fe();c.eh();F c.f0()}I{c.eh()}F E},fe:B(){c.$K.on("2d.5c",$.M(c.gN,c))},eh:B(){c.$K.on("eX.5c",$.M(c.gM,c))},f0:B(){q ph=$(\'<V 1x="5c">\').1a("U","9i").1i("3L",E).Y(c.eV);if(c.C.1P===E){F $("<p>").1h(ph)}I{F ph}},gM:B(){q o=c.2T();if(c.bb(o)){c.fe();c.$K.o(c.f0())}},gN:B(){c.$K.1b("V.5c").1w();q o="";if(c.C.1P===E){o=c.C.52}c.$K.3C("2d.5c");c.$K.o(o);if(c.C.1P===E){c.4j(c.$K.4c()[0])}I{c.2d()}c.1j()},gq:B(){c.$K.1b("V.5c").1w();c.$K.3C("2d.5c")},gp:B(o){F o.G(/<V 1x="5c"(.*?)>(.*?)<\\/V>/i,"")},6B:B(e,1k){if(!c.C.6B){if((e.aO||e.87)&&(1k===66||1k===73)){e.2A()}F E}$.1u(c.C.6B,$.M(B(4C,g3){q 5e=4C.4d(",");3v(q i in 5e){if(1E 5e[i]==="7R"){c.g2(e,$.2a(5e[i]),$.M(B(){lv(g3)},c))}}},c))},g2:B(e,5e,fV){q g5={8:"lx",9:"55",10:"F",13:"F",16:"8h",17:"3w",18:"8d",19:"lk",20:"lm",27:"lp",32:"6k",33:"oO",34:"qD",35:"3t",36:"qE",37:"1t",38:"qB",39:"4K",40:"qw",45:"4v",46:"5b",59:";",61:"=",96:"0",97:"1",98:"2",99:"3",3O:"4",qH:"5",qI:"6",qR:"7",qQ:"8",qS:"9",qT:"*",qU:"+",qP:"-",qO:".",qK:"/",qJ:"f1",qM:"f2",qu:"f3",qb:"f4",qc:"f5",qe:"f6",q8:"f7",q3:"f8",q4:"f9",q7:"q6",qf:"qg",qp:"qo",qq:"qr",qs:"ge",qn:"-",qm:";",qW:"=",qh:",",qj:"-",qk:".",ql:"/",o7:"`",g4:"[",qX:"\\\\",rs:"]",rt:"\'"};q dn={"`":"~","1":"!","2":"@","3":"#","4":"$","5":"%","6":"^","7":"&","8":"*","9":"(","0":")","-":"rw","=":"+",";":": ","\'":\'"\',",":"<",".":">","/":"?","\\\\":"|"};5e=5e.3R().4d(" ");q au=g5[e.2M],7T=4U.rr(e.5Y).3R(),7n="",7i={};$.1u(["8d","3w","4B","8h"],B(2K,at){if(e[at+"rm"]&&au!==at){7n+=at+"+"}});if(au){7i[7n+au]=N}if(7T){7i[7n+7T]=N;7i[7n+dn[7T]]=N;if(7n==="8h+"){7i[dn[7T]]=N}}3v(q i=0,l=5e.1m;i<l;i++){if(7i[5e[i]]){e.2A();F fV.cJ(c,fU)}}},2d:B(){if(!c.1D("bj")){c.3Z.2Y($.M(c.aN,c,N),1)}I{c.$K.2d()}},4l:B(){if(c.1D("3q")){q 1T=c.X.aM.3c}c.$K.2d();if(c.1D("3q")){c.X.aM.3c=1T}},df:B(){if(!c.1D("31")){c.aN()}I{if(c.C.1P===E){q 2V=c.$K.4c().2V();c.$K.2d();c.7v(2V)}I{c.aN()}}},aN:B(5p,2h){c.$K.2d();if(1E 2h=="1I"){2h=c.$K[0]}q O=c.3s();O.9H(2h);O.5p(5p||E);q 1q=c.2b();1q.4N();1q.53(O)},e6:B(aG){if(c.C.5i){c.fW(aG)}I{c.fT()}},fT:B(){q o=c.$1v.2U().1p();if(1E c.6v!=="1I"){q 6v=c.6v.G(/\\n/g,"");q 7x=o.G(/\\n/g,"");7x=c.7C(7x,E);c.6v=c.7C(6v,E)!==7x}if(c.6v){if(c.C.4m&&o===""){c.e3(o)}I{c.7h(o);if(c.C.4m){c.dL()}}c.1d("5W",E,o)}if(c.C.1R){c.$2Z.2N()}I{c.$K.2N()}if(c.C.4m){c.$K.1i("3L",N)}c.$1v.3C("5y.U-5H-gk");c.$K.2d();c.1Y();c.7F();c.fS();c.ds("o");c.C.5i=N},fW:B(aG){if(aG!==E){c.29()}q 22=2H;if(c.C.1R){22=c.$2Z.22();if(c.C.4m){c.$K.2D("3L")}c.$2Z.2U()}I{22=c.$K.hG();c.$K.2U()}q o=c.$1v.1p();if(o!==""&&c.C.fZ){c.$1v.1p(c.fw(o))}c.6v=o;c.$1v.22(22).2N().2d();c.$1v.on("5y.U-5H-gk",c.gj);c.i0();c.6H("o");c.C.5i=E},gj:B(e){if(e.2M===9){q $el=$(c);q 2v=$el.2T(0).4j;$el.1p($el.1p().a3(0,2v)+"\\t"+$el.1p().a3($el.2T(0).7v));$el.2T(0).4j=$el.2T(0).7v=2v+1;F E}},7b:B(){q dB=E;c.bk=gl($.M(B(){q o=c.2T();if(dB!==o){q 2m=c.$1v.1i("2m");$.iI({1M:c.C.7b,1G:"8k",1a:"2m="+2m+"&"+2m+"="+rF(rE(o)),4f:$.M(B(1a){q 1L=$.8g(1a);if(1E 1L.43=="1I"){c.1d("7b",E,1L)}I{c.1d("rC",E,1L)}dB=o},c)})}},c),c.C.bk*rB)},gm:B(){if(c.5f()&&c.C.eg.1m>0){$.1u(c.C.eg,$.M(B(i,s){q 2K=c.C.44.3V(s);c.C.44.9C(2K,1)},c))}if(c.C.30){c.C.44=c.C.go}I{if(!c.C.gh){q 2K=c.C.44.3V("o");c.C.44.9C(2K,1)}}if(c.C.1z){$.1u(c.C.1z.6w.1Q,$.M(B(i,s){if($.4W(i,c.C.gg)=="-1"){dW c.C.1z.6w.1Q[i]}},c))}if(c.C.44.1m===0){F E}c.hN();c.$1z=$("<2q>").2z("cd").1i("id","rl"+c.7d);if(c.C.8o){c.$1z.2z("U-1z-8o")}if(c.C.gd&&c.5f()){c.$1z.2z("U-1z-aS")}if(c.C.30){c.$30=$(\'<12 1x="hQ">\').1i("id","gf"+c.7d).2U();c.$30.1h(c.$1z);$("2w").1h(c.$30)}I{if(c.C.8z){c.$1z.2z("U-1z-r2");$(c.C.8z).o(c.$1z)}I{c.$2C.6a(c.$1z)}}$.1u(c.C.44,$.M(B(i,2x){if(c.C.1z[2x]){q 2j=c.C.1z[2x];if(c.C.81===E&&2x==="26"){F N}c.$1z.1h($("<li>").1h(c.6O(2x,2j)))}},c));c.$1z.1b("a").1i("7q","-1");if(c.C.6x){c.ey();$(c.C.8u).on("ge.U",$.M(c.ey,c))}if(c.C.9L){c.$K.on("9o.U 5a.U",$.M(c.8I,c))}},ey:B(){q 3c=$(c.C.8u).3c();q 7M=0;q 1t=0;q 3t=0;if(c.C.8u===X){7M=c.$2C.2X().1T}I{7M=1}3t=7M+c.$2C.22()+40;if(3c>7M){q 2p="3O%";if(c.C.9k){1t=c.$2C.2X().1t;2p=c.$2C.hJ();c.$1z.2z("hL")}c.6x=N;if(c.C.8u===X){c.$1z.1f({3k:"b4",2p:2p,9g:gX,1T:c.C.8U+"px",1t:1t})}I{c.$1z.1f({3k:"7S",2p:2p,9g:gX,1T:(c.C.8U+3c)+"px",1t:0})}if(3c<3t){c.$1z.1f("gY","l5")}I{c.$1z.1f("gY","8w")}}I{c.6x=E;c.$1z.1f({3k:"jy",2p:"4g",1T:0,1t:1t});if(c.C.9k){c.$1z.3d("hL")}}},hN:B(){if(!c.C.30){F}c.$K.on("9o.U 5a.U",c,$.M(B(e){q Y=c.9S();if(e.1G==="9o"&&Y!=""){c.dy(e)}if(e.1G==="5a"&&e.5d&&Y!=""){q $du=$(c.8G(c.2b().r8)),2X=$du.2X();2X.22=$du.22();c.dy(2X,N)}},c))},dy:B(e,hP){if(!c.C.30){F}c.29();q 1t,1T;$(".hQ").2U();if(hP){1t=e.1t;1T=e.1T+e.22+14;if(c.C.1R){1T+=c.$2C.3k().1T-$(c.X).3c();1t+=c.$2C.3k().1t}}I{q 2p=c.$30.hJ();1t=e.kt;if($(c.X).2p()<(1t+2p)){1t-=2p}1T=e.ku+14;if(c.C.1R){1T+=c.$2C.3k().1T;1t+=c.$2C.3k().1t}I{1T+=$(c.X).3c()}}c.$30.1f({1t:1t+"px",1T:1T+"px"}).2N();c.hE()},hE:B(){if(!c.C.30){F}q 76=$.M(B(3A){$(3A).on("92.U",$.M(B(e){if($(e.1O).2f(c.$1z).1m===0){c.$30.72(3O);c.co();$(3A).3C(e)}},c)).on("5y.U",$.M(B(e){if(e.5Y===c.2M.bK){c.2b().hD()}c.$30.72(3O);$(3A).3C(e)},c))},c);76(X);if(c.C.1R){76(c.X)}},a5:B(){if(!c.C.30){F}q 76=$.M(B(3A){$(3A).on("jh.U",$.M(B(e){if($(e.1O).2f(c.$1z).1m===0){c.$30.72(3O);$(3A).3C(e)}},c))},c);76(X);if(c.C.1R){76(c.X)}},i6:B($1Q,hF){$.1u(hF,$.M(B(2x,2j){if(!2j.2R){2j.2R=""}q $78;if(2j.2m==="93"){$78=$(\'<a 1x="ra">\')}I{$78=$(\'<a 1S="#" 1x="\'+2j.2R+" rb"+2x+\'">\'+2j.1c+"</a>");$78.on("23",$.M(B(e){if(c.C.30){c.1Y()}if(e.2A){e.2A()}if(c.1D("3q")){e.i3=E}if(2j.1d){2j.1d.5K(c,2x,$78,2j,e)}if(2j.2o){c.25(2j.2o,2x)}if(2j.1H){c[2j.1H](2x)}c.8I();if(c.C.30){c.$30.72(3O)}},c))}$1Q.1h($78)},c))},dc:B(e,1k){if(!c.C.5i){e.2A();F E}q $1C=c.4E(1k);q $1Q=$1C.1a("1Q").83(X.2w);if($1C.3x("79")){c.a9()}I{c.a9();c.1d("dc",{1Q:$1Q,1k:1k,1C:$1C});c.6H(1k);$1C.2z("79");q 8S=$1C.2X();q dG=$1Q.2p();if((8S.1t+dG)>$(X).2p()){8S.1t-=dG}q 1t=8S.1t+"px";q dU=$1C.hG();q 3k="7S";q 1T=(dU+c.C.8U)+"px";if(c.C.6x&&c.6x){3k="b4"}I{1T=8S.1T+dU+"px"}$1Q.1f({3k:3k,1t:1t,1T:1T}).2N();c.1d("qZ",{1Q:$1Q,1k:1k,1C:$1C})}q 8W=$.M(B(e){c.dV(e,$1Q)},c);$(X).8V("23",8W);c.$K.8V("23",8W);c.$K.8V("r5",8W);e.r4();c.4l()},a9:B(){c.$1z.1b("a.79").3d("8a").3d("79");$(".i4").2U();c.1d("dV")},dV:B(e,$1Q){if(!$(e.1O).3x("79")){$1Q.3d("79");c.a9()}},6O:B(2x,2j,i1){q $1C=$(\'<a 1S="9h:;" 1c="\'+2j.1c+\'" 7q="-1" 1x="re-ah re-\'+2x+\'"></a>\');if(1E i1!="1I"){$1C.2z("U-2c-T")}$1C.on("23",$.M(B(e){if(e.2A){e.2A()}if(c.1D("3q")){e.i3=E}if($1C.3x("eL")){F E}if(c.7u()===E&&!2j.2o){c.4l()}if(2j.2o){c.4l();c.25(2j.2o,2x);c.a5()}I{if(2j.1H&&2j.1H!=="2N"){c[2j.1H](2x);c.a5()}I{if(2j.1d){2j.1d.5K(c,2x,$1C,2j,e);c.a5()}I{if(2j.1Q){c.dc(e,2x)}}}}c.8I(E,2x)},c));if(2j.1Q){q $1Q=$(\'<12 1x="i4 oL\'+2x+\'" 1n="3a: 3n;">\');$1C.1a("1Q",$1Q);c.i6($1Q,2j.1Q)}F $1C},4E:B(1k){if(!c.C.1z){F E}F $(c.$1z.1b("a.re-"+1k))},oM:B(dC,Q){c.C.9L.3b(dC);c.C.ez[Q]=dC},hW:B(1k){q 2c=c.4E(1k);if(2c.3x("8a")){c.ds(1k)}I{c.6H(1k)}},6H:B(1k){q 2c=c.4E(1k);2c.2z("8a")},ds:B(1k){q 2c=c.4E(1k);2c.3d("8a")},hT:B(2x){c.$1z.1b("a.re-ah").b1(".re-"+2x).3d("8a")},fS:B(){c.$1z.1b("a.re-ah").b1("a.re-o").3d("eL")},i0:B(){c.$1z.1b("a.re-ah").b1("a.re-o").2z("eL")},q1:B(1k,ag){c.4E(1k).2z("re-"+ag)},oJ:B(1k,ag){c.4E(1k).3d("re-"+ag)},oI:B(1k,2m){q 1C=c.4E(1k);1C.3d("U-2c-T");1C.2z("fa-U-2c");1C.o(\'<i 1x="fa \'+2m+\'"></i>\')},oE:B(1k,1c,1d,1Q){if(!c.C.1z){F}q 2c=c.6O(1k,{1c:1c,1d:1d,1Q:1Q},N);c.$1z.1h($("<li>").1h(2c));F 2c},oD:B(1k,1c,1d,1Q){if(!c.C.1z){F}q 2c=c.6O(1k,{1c:1c,1d:1d,1Q:1Q},N);c.$1z.6a($("<li>").1h(2c))},oF:B(hZ,1k,1c,1d,1Q){if(!c.C.1z){F}q 2c=c.6O(1k,{1c:1c,1d:1d,1Q:1Q},N);q $2c=c.4E(hZ);if($2c.1U()!==0){$2c.L().2L($("<li>").1h(2c))}I{c.$1z.1h($("<li>").1h(2c))}F 2c},oH:B(hU,1k,1c,1d,1Q){if(!c.C.1z){F}q 2c=c.6O(1k,{1c:1c,1d:1d,1Q:1Q},N);q $2c=c.4E(hU);if($2c.1U()!==0){$2c.L().3Q($("<li>").1h(2c))}I{c.$1z.1h($("<li>").1h(2c))}F 2c},oQ:B(1k){q $2c=c.4E(1k);$2c.1w()},8I:B(e,2x){q L=c.2B();c.hT(2x);if(e===E&&2x!=="o"){if($.4W(2x,c.C.9L)!=-1){c.hW(2x)}F}if(L&&L.Q==="A"){c.$1z.1b("a.hf").Y(c.C.1F.hX)}I{c.$1z.1b("a.hf").Y(c.C.1F.er)}$.1u(c.C.ez,$.M(B(1k,2s){if($(L).2f(1k,c.$K.2T()[0]).1m!=0){c.6H(2s)}},c));q $L=$(L).2f(c.C.5D.4b().3R(),c.$K[0]);if($L.1m){q 56=$L.1f("Y-56");if(56==""){56="1t"}c.6H("56"+56)}},9j:B(o){q 1q=c.2b();if(1q.41&&1q.4S){q O=c.3s();O.94();q el=c.X.4u("12");el.3P=o;q 4k=c.X.dz(),J,5G;3y((J=el.7V)){5G=4k.6L(J)}q p0=4k.7V;O.3B(4k);if(5G){O=O.a0();O.an(5G);O.5p(N)}1q.4N();1q.53(O)}},2o:B(1N,2F,1j){if(1N==="cm"&&c.1D("3q")){2F="<"+2F+">"}if(1N==="57"&&c.1D("3q")){if(!c.8H()){c.4l();c.X.1J.5n().cp(2F)}I{c.9j(2F)}}I{c.X.25(1N,E,2F)}if(1j!==E){c.1j()}c.1d("25",1N,2F)},25:B(1N,2F,1j){if(!c.C.5i){c.$1v.2d();F E}if(1N==="3p"||1N==="3m"||1N==="4J"||1N==="hh"){c.21()}if(1N==="hj"||1N==="hi"){q L=c.2B();if(L.Q==="p2"||L.Q==="oX"){c.aH(L)}}if(1N==="57"){c.ad(2F,1j);c.1d("25",1N,2F);F}if(c.6K("6f")&&!c.C.hc){F E}if(1N==="8e"||1N==="8M"){F c.gZ(1N,2F)}if(1N==="6e"){F c.h0(1N,2F)}c.2o(1N,2F,1j);if(1N==="h7"){c.$K.1b("hr").2D("id")}},h0:B(1N,2F){c.21();q 1s=c.6K("A");if(1s){$(1s).2i($(1s).Y());c.1j();c.1d("25",1N,2F);F}},gZ:B(1N,2F){c.21();q L=c.2B();q $3i=$(L).2f("ol, 2q");if(!c.4i($3i)&&$3i.1U()!=0){$3i=E}q 1w=E;if($3i&&$3i.1m){1w=N;q 5o=$3i[0].Q;if((1N==="8e"&&5o==="oR")||(1N==="8M"&&5o==="h9")){1w=E}}c.29();if(1w){q 1W=c.6W();q 4D=c.3K(1W);if(1E 1W[0]!="1I"&&1W.1m>1&&1W[0].4q==3){4D.j5(c.2Q())}q 1a="",5T="";$.1u(4D,$.M(B(i,s){if(s.Q=="3e"){q $s=$(s);q 7o=$s.68();7o.1b("2q","ol").1w();if(c.C.1P===E){1a+=c.3W($("<p>").1h(7o.1X()))}I{q hb=7o.o().G(/<br\\s?\\/?>$/i,"");1a+=hb+"<br>"}if(i==0){$s.2z("U-5T").6c();5T=c.3W($s)}I{$s.1w()}}},c));o=c.$K.o().G(5T,"</"+5o+">"+1a+"<"+5o+">");c.$K.o(o);c.$K.1b(5o+":6c").1w()}I{q hx=$(c.2B()).2f("1g");if(c.1D("3q")&&!c.8H()&&c.C.1P){q 3F=c.ce("12");q 9Y=$(3F).o();q 6X=$("<2q>");if(1N=="8M"){6X=$("<ol>")}q 8n=$("<li>");if($.2a(9Y)==""){8n.1h(9Y+\'<V id="1J-1K-1">\'+c.C.2e+"</V>");6X.1h(8n);c.$K.1b("#1J-1K-1").2i(6X)}I{8n.1h(9Y);6X.1h(8n);$(3F).2i(6X)}}I{c.X.25(1N)}q L=c.2B();q $3i=$(L).2f("ol, 2q");if(c.C.1P===E){q 9R=$.2a($3i.Y());if(9R==""){$3i.4c("li").1b("br").1w();$3i.4c("li").1h(\'<V id="1J-1K-1">\'+c.C.2e+"</V>")}}if(hx.1U()!=0){$3i.fG("<1g>")}if($3i.1m){q $6T=$3i.L();if(c.4i($6T)&&$6T[0].Q!="3e"&&c.6V($6T[0])){$6T.2i($6T.1X())}}if(c.1D("31")){c.$K.2d()}}c.1Y();c.$K.1b("#1J-1K-1").2D("id");c.1j();c.1d("25",1N,2F);F},bJ:B(){c.bH("6g")},bF:B(){c.bH("6S")},bH:B(1N){c.21();if(1N==="6g"){q 1l=c.2Q();c.29();if(1l&&1l.Q=="3e"){q L=c.2B();q $3i=$(L).2f("ol, 2q");q 5o=$3i[0].Q;q 4D=c.3K();$.1u(4D,B(i,s){if(s.Q=="3e"){q $4L=$(s).4L();if($4L.1U()!=0&&$4L[0].Q=="3e"){q $cT=$4L.4c("2q, ol");if($cT.1U()==0){$4L.1h($("<"+5o+">").1h(s))}I{$cT.1h(s)}}}})}I{if(1l===E&&c.C.1P===N){c.2o("6l","2g");q 7B=c.2Q();q 1l=$(\'<12 1a-8b="">\').o($(7B).o());$(7B).2i(1l);q 1t=c.aU($(1l).1f("2I-1t"))+c.C.9V;$(1l).1f("2I-1t",1t+"px")}I{q 7G=c.3K();$.1u(7G,$.M(B(i,1A){q $el=E;if(1A.Q==="6j"){F}if($.4W(1A.Q,c.C.5D)!==-1){$el=$(1A)}I{$el=$(1A).2f(c.C.5D.4b().3R(),c.$K[0])}q 1t=c.aU($el.1f("2I-1t"))+c.C.9V;$el.1f("2I-1t",1t+"px")},c))}}c.1Y()}I{c.29();q 1l=c.2Q();if(1l&&1l.Q=="3e"){q 4D=c.3K();q 2K=0;c.cG(1l,2K,4D)}I{q 7G=c.3K();$.1u(7G,$.M(B(i,1A){q $el=E;if($.4W(1A.Q,c.C.5D)!==-1){$el=$(1A)}I{$el=$(1A).2f(c.C.5D.4b().3R(),c.$K[0])}q 1t=c.aU($el.1f("2I-1t"))-c.C.9V;if(1t<=0){if(c.C.1P===N&&1E($el.1a("8b"))!=="1I"){$el.2i($el.o()+"<br>")}I{$el.1f("2I-1t","");c.4Q($el,"1n")}}I{$el.1f("2I-1t",1t+"px")}},c))}c.1Y()}c.1j()},cG:B(li,2K,4D){if(li&&li.Q=="3e"){q $L=$(li).L().L();if($L.1U()!=0&&$L[0].Q=="3e"){$L.2L(li)}I{if(1E 4D[2K]!="1I"){li=4D[2K];2K++;c.cG(li,2K,4D)}I{c.25("8e")}}}},ck:B(){c.88("","o9")},cf:B(){c.88("4K","o8")},cz:B(){c.88("77","oa")},cy:B(){c.88("cu","ob")},88:B(1G,1N){c.21();if(c.db()){c.X.25(1N,E,E);F N}c.29();q 1l=c.2Q();if(!1l&&c.C.1P){c.2o("cm","12");q 7B=c.2Q();q 1l=$(\'<12 1a-8b="">\').o($(7B).o());$(7B).2i(1l);$(1l).1f("Y-56",1G);c.4Q(1l,"1n");if(1G==""&&1E($(1l).1a("8b"))!=="1I"){$(1l).2i($(1l).o())}}I{q 7G=c.3K();$.1u(7G,$.M(B(i,1A){q $el=E;if($.4W(1A.Q,c.C.5D)!==-1){$el=$(1A)}I{$el=$(1A).2f(c.C.5D.4b().3R(),c.$K[0])}if($el){$el.1f("Y-56",1G);c.4Q($el,"1n")}},c))}c.1Y();c.1j()},by:B(o){q ph=c.hp(o);if(ph!==E){F ph}if(c.C.1P===E){if(o===""){o=c.C.52}I{if(o.51(/^<hr\\s?\\/?>$/gi)!==-1){o="<hr>"+c.C.52}}}F o},c1:B(o){if(c.C.9w&&!c.C.ow){o=o.G(/<12(.*?)>([\\w\\W]*?)<\\/12>/gi,"<p$1>$2</p>")}if(c.C.62){o=c.ap(o)}F o},eA:B(o){if(c.C.e9){o=o.G(/\\{\\{(.*?)\\}\\}/gi,"<!-- 9f i7 $1 -->");o=o.G(/\\{(.*?)\\}/gi,"<!-- 9f $1 -->")}o=o.G(/<3l(.*?)>([\\w\\W]*?)<\\/3l>/gi,\'<1c 1G="Y/9h" 1n="3a: 3n;" 1x="U-3l-1e"$1>$2</1c>\');o=o.G(/<1n(.*?)>([\\w\\W]*?)<\\/1n>/gi,\'<2l$1 1n="3a: 3n;" 4n="U-1n-1e">$2</2l>\');o=o.G(/<2t(.*?)>([\\w\\W]*?)<\\/2t>/gi,\'<2l$1 4n="U-2t-1e">$2</2l>\');if(c.C.8R){o=o.G(/<\\?4M([\\w\\W]*?)\\?>/gi,\'<2l 1n="3a: 3n;" 4n="U-4M-1e">$1</2l>\')}I{o=o.G(/<\\?4M([\\w\\W]*?)\\?>/gi,"")}F o},hq:B(o){if(c.C.e9){o=o.G(/<!-- 9f i7 (.*?) -->/gi,"{{$1}}");o=o.G(/<!-- 9f (.*?) -->/gi,"{$1}")}o=o.G(/<1c 1G="Y\\/9h" 1n="3a: 3n;" 1x="U-3l-1e"(.*?)>([\\w\\W]*?)<\\/1c>/gi,\'<3l$1 1G="Y/9h">$2<\\/3l>\');o=o.G(/<2l(.*?) 1n="3a: 3n;" 4n="U-1n-1e">([\\w\\W]*?)<\\/2l>/gi,"<1n$1>$2</1n>");o=o.G(/<2l(.*?)4n="U-2t-1e"(.*?)>([\\w\\W]*?)<\\/2l>/gi,"<2t$1$2>$3</2t>");if(c.C.8R){o=o.G(/<2l 1n="3a: 3n;" 4n="U-4M-1e">([\\w\\W]*?)<\\/2l>/gi,"<?4M\\r\\n$1\\r\\n?>")}F o},7C:B(o,3H){if(3H!==E){q 3H=[];q 2y=o.1V(/<(2r|1n|3l|1c)(.*?)>([\\w\\W]*?)<\\/(2r|1n|3l|1c)>/gi);if(2y===2H){2y=[]}if(c.C.8R){q 6P=o.1V(/<\\?4M([\\w\\W]*?)\\?>/gi);if(6P){2y=$.bU(2y,6P)}}if(2y){$.1u(2y,B(i,s){o=o.G(s,"fJ"+i);3H.3b(s)})}}o=o.G(/\\n/g," ");o=o.G(/[\\t]*/g,"");o=o.G(/\\n\\s*\\n/g,"\\n");o=o.G(/^[\\s\\n]*/g," ");o=o.G(/[\\s\\n]*$/g," ");o=o.G(/>\\s{2,}</g,"> <");o=c.fj(o,3H);o=o.G(/\\n\\n/g,"\\n");F o},fj:B(o,3H){if(3H===E){F o}$.1u(3H,B(i,s){o=o.G("fJ"+i,s)});F o},dQ:B(o){o=o.G(/[\\7K-\\fP\\fC]/g,"");q bQ=["<b>\\\\s*</b>","<b>&3j;</b>","<em>\\\\s*</em>"];q 70=["<2r></2r>","<2g>\\\\s*</2g>","<dd></dd>","<dt></dt>","<2q></2q>","<ol></ol>","<li></li>","<1o></1o>","<3f></3f>","<V>\\\\s*<V>","<V>&3j;<V>","<p>\\\\s*</p>","<p></p>","<p>&3j;</p>","<p>\\\\s*<br>\\\\s*</p>","<12>\\\\s*</12>","<12>\\\\s*<br>\\\\s*</12>"];if(c.C.9P){70=70.oq(bQ)}I{70=bQ}q 4P=70.1m;3v(q i=0;i<4P;++i){o=o.G(28 2G(70[i],"gi"),"")}F o},ap:B(o){o=$.2a(o);if(c.C.1P===N){F o}if(o===""||o==="<p></p>"){F c.C.52}o=o+"\\n";if(c.C.9P===E){F o}q eP=[];q 2y=o.1V(/<(1o|12|2r|3M)(.*?)>([\\w\\W]*?)<\\/(1o|12|2r|3M)>/gi);if(!2y){2y=[]}q bM=o.1V(/<!--([\\w\\W]*?)-->/gi);if(bM){2y=$.bU(2y,bM)}if(c.C.8R){q 6P=o.1V(/<2l(.*?)4n="U-4M-1e">([\\w\\W]*?)<\\/2l>/gi);if(6P){2y=$.bU(2y,6P)}}if(2y){$.1u(2y,B(i,s){eP[i]=s;o=o.G(s,"{G"+i+"}\\n")})}o=o.G(/<br \\/>\\s*<br \\/>/gi,"\\n\\n");o=o.G(/<br><br>/gi,"\\n\\n");B R(4C,fB,r){F o.G(28 2G(4C,fB),r)}q 3I="(pI|o|2w|9D|1c|4B|1n|3l|1s|1R|1o|3U|d0|pK|pL|pG|8L|3f|1g|fc|12|dl|dd|dt|2q|ol|li|2r|3o|48|2t|fs|fl|2g|ec|pB|1n|p|h[1-6]|hr|fm|pE|2l|kX|kL|pM|aP|4a|pN|pW|pV|pX|pY|pZ)";o=R("(<"+3I+"[^>]*>)","gi","\\n$1");o=R("(</"+3I+">)","gi","$1\\n\\n");o=R("\\r\\n","g","\\n");o=R("\\r","g","\\n");o=R("/\\n\\n+/","g","\\n\\n");q 4A=o.4d(28 2G("\\ns*\\n","g"),-1);o="";3v(q i in 4A){if(4A.pU(i)){if(4A[i].51("{G")==-1){4A[i]=4A[i].G(/<p>\\n\\t?<\\/p>/gi,"");4A[i]=4A[i].G(/<p><\\/p>/gi,"");if(4A[i]!=""){o+="<p>"+4A[i].G(/^\\n+|\\n+$/g,"")+"</p>"}}I{o+=4A[i]}}}o=R("<p><p>","gi","<p>");o=R("</p></p>","gi","</p>");o=R("<p>s?</p>","gi","");o=R("<p>([^<]+)</(12|ec|2t)>","gi","<p>$1</p></$2>");o=R("<p>(</?"+3I+"[^>]*>)</p>","gi","$1");o=R("<p>(<li.+?)</p>","gi","$1");o=R("<p>s?(</?"+3I+"[^>]*>)","gi","$1");o=R("(</?"+3I+"[^>]*>)s?</p>","gi","$1");o=R("(</?"+3I+"[^>]*>)s?<br />","gi","$1");o=R("<br />(s*</?(?:p|li|12|dl|dd|dt|fc|2r|1g|2q|ol)[^>]*>)","gi","$1");o=R("\\n</p>","gi","</p>");o=R("<li><p>","gi","<li>");o=R("</p></li>","gi","</li>");o=R("</li><p>","gi","</li>");o=R("<p>\\t?\\n?<p>","gi","<p>");o=R("</dt><p>","gi","</dt>");o=R("</dd><p>","gi","</dd>");o=R("<br></p></2g>","gi","</2g>");o=R("<p>\\t*</p>","gi","");$.1u(eP,B(i,s){o=o.G("{G"+i+"}",s)});F $.2a(o)},8T:B(o,7h){q 6o="3T";if(c.C.6o==="b"){6o="b"}q 6n="em";if(c.C.6n==="i"){6n="i"}o=o.G(/<V 1n="2u-1n: 3m;">([\\w\\W]*?)<\\/V>/gi,"<"+6n+">$1</"+6n+">");o=o.G(/<V 1n="2u-71: 3p;">([\\w\\W]*?)<\\/V>/gi,"<"+6o+">$1</"+6o+">");if(c.C.6o==="3T"){o=o.G(/<b>([\\w\\W]*?)<\\/b>/gi,"<3T>$1</3T>")}I{o=o.G(/<3T>([\\w\\W]*?)<\\/3T>/gi,"<b>$1</b>")}if(c.C.6n==="em"){o=o.G(/<i>([\\w\\W]*?)<\\/i>/gi,"<em>$1</em>")}I{o=o.G(/<em>([\\w\\W]*?)<\\/em>/gi,"<i>$1</i>")}o=o.G(/<V 1n="Y-c4: 4J;">([\\w\\W]*?)<\\/V>/gi,"<u>$1</u>");if(7h!==N){o=o.G(/<5E>([\\w\\W]*?)<\\/5E>/gi,"<5b>$1</5b>")}I{o=o.G(/<5b>([\\w\\W]*?)<\\/5b>/gi,"<5E>$1</5E>")}F o},7Q:B(o){if(o==""||1E o=="1I"){F o}q 8X=E;if(c.C.5S!==E){8X=N}q 2O=8X===N?c.C.5S:c.C.8t;q fx=/<\\/?([a-z][a-eM-9]*)\\b[^>]*>/gi;o=o.G(fx,B($0,$1){if(8X===N){F $.4W($1.3R(),2O)>"-1"?$0:""}I{F $.4W($1.3R(),2O)>"-1"?"":$0}});o=c.8T(o);F o},dP:B(o,fM){q 2r=o.1V(/<(2r|2k)(.*?)>([\\w\\W]*?)<\\/(2r|2k)>/gi);if(2r!==2H){$.1u(2r,$.M(B(i,s){q 2O=s.1V(/<(2r|2k)(.*?)>([\\w\\W]*?)<\\/(2r|2k)>/i);2O[3]=2O[3].G(/&3j;/g," ");if(fM!==E){2O[3]=c.e4(2O[3])}2O[3]=2O[3].G(/\\$/g,"&#36;");o=o.G(s,"<"+2O[1]+2O[2]+">"+2O[3]+"</"+2O[1]+">")},c))}F o},e4:B(4C){4C=4U(4C).G(/&9Q;/g,"&").G(/&lt;/g,"<").G(/&gt;/g,">").G(/&fA;/g,\'"\');F 4C.G(/&/g,"&9Q;").G(/</g,"&lt;").G(/>/g,"&gt;").G(/"/g,"&fA;")},fz:B(){q $1A=c.$K.1b("li, 1B, a, b, 3T, pz, py, i, em, u, pe, 5E, 5b, V, pd");$1A.fu(\'[1n*="7N-6z: fK;"][1n*="bh-22"]\').1f("7N-6z","").1f("bh-22","");$1A.fu(\'[1n*="7N-6z: fK;"]\').1f("7N-6z","");$1A.1f("bh-22","");$.1u($1A,$.M(B(i,s){c.4Q(s,"1n")},c));q $c8=c.$K.1b("b, 3T, i, em, u, 5E, 5b");$c8.1f("2u-1U","");$.1u($c8,$.M(B(i,s){c.4Q(s,"1n")},c));c.$K.1b(\'12[1n="Y-56: -4y-4g;"]\').1X().le();c.$K.1b("2q, ol, li").2D("1n")},fw:B(2k){q i=0,aF=2k.1m,3g=0,2v=2H,3t=2H,1e="",1Z="",4I="";c.8m=0;3v(;i<aF;i++){3g=i;if(-1==2k.4G(i).3V("<")){1Z+=2k.4G(i);F c.dk(1Z)}3y(3g<aF&&2k.5J(3g)!="<"){3g++}if(i!=3g){4I=2k.4G(i,3g-i);if(!4I.1V(/^\\s{2,}$/g)){if("\\n"==1Z.5J(1Z.1m-1)){1Z+=c.6Y()}I{if("\\n"==4I.5J(0)){1Z+="\\n"+c.6Y();4I=4I.G(/^\\s+/,"")}}1Z+=4I}if(4I.1V(/\\n/)){1Z+="\\n"+c.6Y()}}2v=3g;3y(3g<aF&&">"!=2k.5J(3g)){3g++}1e=2k.4G(2v,3g-2v);i=3g;q t;if("!--"==1e.4G(1,3)){if(!1e.1V(/--$/)){3y("-->"!=2k.4G(3g,3)){3g++}3g+=2;1e=2k.4G(2v,3g-2v);i=3g}if("\\n"!=1Z.5J(1Z.1m-1)){1Z+="\\n"}1Z+=c.6Y();1Z+=1e+">\\n"}I{if("!"==1e[1]){1Z=c.b9(1e+">",1Z)}I{if("?"==1e[1]){1Z+=1e+">\\n"}I{if(t=1e.1V(/^<(3l|1n|2r)/i)){t[1]=t[1].3R();1e=c.fb(1e);1Z=c.b9(1e,1Z);3t=4U(2k.4G(i+1)).3R().3V("</"+t[1]);if(3t){4I=2k.4G(i+1,3t);i+=3t;1Z+=4I}}I{1e=c.fb(1e);1Z=c.b9(1e,1Z)}}}}}F c.dk(1Z)},6Y:B(){q s="";3v(q j=0;j<c.8m;j++){s+="\\t"}F s},dk:B(2k){2k=2k.G(/\\n\\s*\\n/g,"\\n");2k=2k.G(/^[\\s\\n]*/,"");2k=2k.G(/[\\s\\n]*$/,"");2k=2k.G(/<3l(.*?)>\\n<\\/3l>/gi,"<3l$1><\\/3l>");c.8m=0;F 2k},fb:B(1e){q 8E="";1e=1e.G(/\\n/g," ");1e=1e.G(/\\s{2,}/g," ");1e=1e.G(/^\\s+|\\s+$/g," ");q bZ="";if(1e.1V(/\\/$/)){bZ="/";1e=1e.G(/\\/+$/,"")}q m;3y(m=/\\s*([^= ]+)(?:=(([\'"\']).*?\\3|[^ ]+))?/.2o(1e)){if(m[2]){8E+=m[1].3R()+"="+m[2]}I{if(m[1]){8E+=m[1].3R()}}8E+=" ";1e=1e.4G(m[0].1m)}F 8E.G(/\\s*$/,"")+bZ+">"},b9:B(1e,1Z){q nl=1e.1V(c.cX);if(1e.1V(c.fN)||nl){1Z=1Z.G(/\\s*$/,"");1Z+="\\n"}if(nl&&"/"==1e.5J(1)){c.8m--}if("\\n"==1Z.5J(1Z.1m-1)){1Z+=c.6Y()}if(nl&&"/"!=1e.5J(1)){c.8m++}1Z+=1e;if(1e.1V(c.fE)||1e.1V(c.cX)){1Z=1Z.G(/ *$/,"");1Z+="\\n"}F 1Z},fF:B(e){q o=$.2a(c.$K.o());if(c.C.1P){if(o==""){e.2A();c.$K.o("");c.2d()}}I{o=o.G(/<br\\s?\\/?>/i,"");q 7x=o.G(/<p>\\s?<\\/p>/gi,"");if(o===""||7x===""){e.2A();q J=$(c.C.52).2T(0);c.$K.o(J);c.2d()}}c.1j()},5F:B(1e){if(c.1D("31")&&c.7u()){c.$K.2d()}c.21();q 1W=c.3K();c.29();$.1u(1W,$.M(B(i,J){if(J.Q!=="3e"){q L=$(J).L();if(1e==="p"){if((J.Q==="P"&&L.1U()!=0&&L[0].Q==="3D")||J.Q==="3D"){c.cY();F}I{if(c.C.1P){if(J&&J.Q.51(/H[1-6]/)==0){$(J).2i(J.3P+"<br>")}I{F}}I{c.6l(1e,J)}}}I{c.6l(1e,J)}}},c));c.1Y();c.1j()},6l:B(1e,1l){if(1l===E){1l=c.2Q()}if(1l===E&&c.C.1P===N){c.25("cm",1e);F N}q 1X="";if(1e!=="2r"){1X=$(1l).1X()}I{1X=$(1l).o();if($.2a(1X)===""){1X=\'<V id="1J-1K-1"></V>\'}}if(1l.Q==="6f"){1e="p"}if(c.C.1P===N&&1e==="p"){$(1l).2i($("<12>").1h(1X).o()+"<br>")}I{q L=c.2B();q J=$("<"+1e+">").1h(1X);$(1l).2i(J);if(L&&L.Q=="6j"){$(J).fG("<1g>")}}},iN:B(fi,fH,7X){if(7X!==E){c.29()}q 8D=$("<"+fH+"/>");$(fi).2i(B(){F 8D.1h($(c).1X())});if(7X!==E){c.1Y()}F 8D},cY:B(){if(c.1D("31")&&c.7u()){c.$K.2d()}c.21();if(c.C.1P===E){c.29();q 3I=c.3K();q 2g=E;q fh=3I.1m;if(3I){q 1a="";q 5T="";q G=E;q dE=N;$.1u(3I,B(i,s){if(s.Q!=="P"){dE=E}});$.1u(3I,$.M(B(i,s){if(s.Q==="3D"){c.6l("p",s,E)}I{if(s.Q==="P"){2g=$(s).L();if(2g[0].Q=="3D"){q 75=$(2g).4c("p").1U();if(75==1){$(2g).2i(s)}I{if(75==fh){G="2g";1a+=c.3W(s)}I{G="o";1a+=c.3W(s);if(i==0){$(s).2z("U-5T").6c();5T=c.3W(s)}I{$(s).1w()}}}}I{if(dE===E||3I.1m==1){c.6l("2g",s,E)}I{G="k7";1a+=c.3W(s)}}}I{if(s.Q!=="3e"){c.6l("2g",s,E)}}}},c));if(G){if(G=="k7"){$(3I[0]).2i("<2g>"+1a+"</2g>");$(3I).1w()}I{if(G=="2g"){$(2g).2i(1a)}I{if(G=="o"){q o=c.$K.o().G(5T,"</2g>"+1a+"<2g>");c.$K.o(o);c.$K.1b("2g").1u(B(){if($.2a($(c).o())==""){$(c).1w()}})}}}}}c.1Y()}I{q 1l=c.2Q();if(1l.Q==="3D"){c.29();q o=$.2a($(1l).o());q 1J=$.2a(c.j1());o=o.G(/<V(.*?)id="1J-1K(.*?)<\\/V>/gi,"");if(o==1J){$(1l).2i($(1l).o()+"<br>")}I{c.kM("2J");q 2J=c.$K.1b("2J");2J.6c();q kQ=c.$K.o().G("<2J></2J>",\'</2g><V id="1J-1K-1">\'+c.C.2e+"</V>"+1J+"<2g>");c.$K.o(kQ);2J.1w();c.$K.1b("2g").1u(B(){if($.2a($(c).o())==""){$(c).1w()}})}c.1Y();c.$K.1b("V#1J-1K-1").1i("id",E)}I{q 3F=c.ce("2g");q o=$(3F).o();q kN=["2q","ol","1o","3f","8L","3U","d0","dl"];$.1u(kN,B(i,s){o=o.G(28 2G("<"+s+"(.*?)>","gi"),"");o=o.G(28 2G("</"+s+">","gi"),"")});q 6G=c.C.9N;$.1u(6G,B(i,s){o=o.G(28 2G("<"+s+"(.*?)>","gi"),"");o=o.G(28 2G("</"+s+">","gi"),"<br>")});$(3F).o(o);c.cl(3F);q 4e=$(3F).4e();if(4e.1U()!=0&&4e[0].Q==="cr"){4e.1w()}}}c.1j()},po:B(1i,2s){q 1W=c.3K();$(1W).2D(1i);c.1j()},pn:B(1i,2s){q 1W=c.3K();$(1W).1i(1i,2s);c.1j()},pl:B(5I){q 1W=c.3K();$(1W).1f(5I,"");c.4Q(1W,"1n");c.1j()},pm:B(5I,2s){q 1W=c.3K();$(1W).1f(5I,2s);c.1j()},pq:B(2R){q 1W=c.3K();$(1W).3d(2R);c.4Q(1W,"1x");c.1j()},pr:B(2R){q 1W=c.3K();$(1W).2z(2R);c.1j()},pw:B(2R){c.29();c.dD(B(J){$(J).3d(2R);c.4Q(J,"1x")});c.1Y();c.1j()},pv:B(2R){q 1r=c.3S();if(!$(1r).3x(2R)){c.b8("2z",2R)}},pu:B(5I){c.29();c.dD(B(J){$(J).1f(5I,"");c.4Q(J,"1n")});c.1Y();c.1j()},ps:B(5I,2s){c.b8("1f",5I,2s)},pk:B(1i){c.29();q O=c.3s(),J=c.8G(),1W=c.6W();if(O.4X||O.6h===O.8Q&&J){1W=$(J)}$(1W).2D(1i);c.k6();c.1Y();c.1j()},pj:B(1i,2s){c.b8("1i",1i,2s)},b8:B(1G,1i,2s){c.21();c.29();q O=c.3s();q el=c.8G();if((O.4X||O.6h===O.8Q)&&el&&!c.6V(el)){$(el)[1G](1i,2s)}I{q 1N,eI=2s;kz(1i){5L"2u-1U":1N="7W";eI=4;54;5L"2u-p9":1N="pa";54;5L"6z":1N="p7";54;5L"7N-6z":1N="p5";54}c.X.25(1N,E,eI);q aJ=c.$K.1b("2u");$.1u(aJ,$.M(B(i,s){c.kD(1G,s,1i,2s)},c))}c.1Y();c.1j()},kD:B(1G,s,1i,2s){q L=$(s).L(),el;q aB=c.9S();q ay=$(L).Y();q ax=aB==ay;if(ax&&L&&L[0].Q==="di"&&L[0].p6.1m!=0){el=L;$(s).2i($(s).o())}I{el=$("<47>").1h($(s).1X());$(s).2i(el)}$(el)[1G](1i,2s);F el},dD:B(1d){q O=c.3s(),J=c.8G(),1W=c.6W(),4X;if(O.4X||O.6h===O.8Q&&J){1W=$(J);4X=N}$.1u(1W,$.M(B(i,J){if(!4X&&J.Q!=="di"){q aB=c.9S();q ay=$(J).L().Y();q ax=aB==ay;if(ax&&J.4V.Q==="di"&&!$(J.4V).3x("4o")){J=J.4V}I{F}}1d.5K(c,J)},c))},k6:B(){q $aE=c.$K.1b("47");$.1u($aE,$.M(B(i,V){q $V=$(V);if($V.1i("1x")===1I&&$V.1i("1n")===1I){$V.1X().le()}},c))},kM:B(1e){c.29();c.X.25("7W",E,4);q aJ=c.$K.1b("2u");q 2V;$.1u(aJ,B(i,s){q el=$("<"+1e+"/>").1h($(s).1X());$(s).2i(el);2V=el});c.1Y();c.1j()},pb:B(1e){c.29();q dN=1e.pc();q 1W=c.6W();q L=$(c.2B()).L();$.1u(1W,B(i,s){if(s.Q===dN){c.aH(s)}});if(L&&L[0].Q===dN){c.aH(L)}c.1Y();c.1j()},aH:B(el){$(el).2i($(el).1X())},ad:B(o,1j){q 1r=c.3S();q L=1r.4V;c.4l();c.21();q $o=$("<12>").1h($.d7(o));o=$o.o();o=c.dQ(o);$o=$("<12>").1h($.d7(o));q dj=c.2Q();if($o.1X().1m==1){q ba=$o.1X()[0].Q;if(ba!="P"&&ba==dj.Q||ba=="6f"){$o=$("<12>").1h(o)}}if(c.C.1P){o=o.G(/<p(.*?)>([\\w\\W]*?)<\\/p>/gi,"$2<br>")}if(!c.C.1P&&$o.1X().1m==1&&$o.1X()[0].4q==3&&(c.bx().1m>2||(!1r||1r.Q=="bT"&&!L||L.Q=="kW"))){o="<p>"+o+"</p>"}o=c.bd(o);if($o.1X().1m>1&&dj||$o.1X().is("p, :aP, 2q, ol, li, 12, 1o, 1g, 2g, 2r, ec, 2l, aP, 4a, kL, kX")){if(c.1D("3q")){if(!c.8H()){c.X.1J.5n().cp(o)}I{c.9j(o)}}I{c.X.25("57",E,o)}}I{c.9K(o,E)}if(c.5P){c.3Z.2Y($.M(B(){if(!c.C.1P){c.7v(c.$K.1X().2V())}I{c.df()}},c),1)}c.7F();c.8K();if(1j!==E){c.1j()}},9K:B(o,1j){o=c.bd(o);q 1q=c.2b();if(1q.41&&1q.4S){q O=1q.41(0);O.94();q el=c.X.4u("12");el.3P=o;q 4k=c.X.dz(),J,5G;3y((J=el.7V)){5G=4k.6L(J)}O.3B(4k);if(5G){O=O.a0();O.an(5G);O.5p(N);1q.4N();1q.53(O)}}if(1j!==E){c.1j()}},pi:B(o){o=c.bd(o);q J=$(o);q 6k=X.4u("V");6k.3P="\\7K";q O=c.3s();O.3B(6k);O.3B(J[0]);O.5p(E);q 1q=c.2b();1q.4N();1q.53(O);c.1j()},pg:B(o){q $o=$($.d7(o));if($o.1m){o=$o.Y()}c.4l();if(c.1D("3q")){if(!c.8H()){c.X.1J.5n().cp(o)}I{c.9j(o)}}I{c.X.25("57",E,o)}c.1j()},3B:B(J){J=J[0]||J;if(J.Q=="kx"){q 7f="47";q 9b=J.l6;q 5M=28 2G("<"+J.Q,"i");q 5Q=9b.G(5M,"<"+7f);5M=28 2G("</"+J.Q,"i");5Q=5Q.G(5M,"</"+7f);J=$(5Q)[0]}q 1q=c.2b();if(1q.41&&1q.4S){O=1q.41(0);O.94();O.3B(J);O.pf(J);O.an(J);1q.4N();1q.53(O)}F J},kG:B(e,J){q O;q x=e.kt,y=e.ku;if(c.X.kr){q 49=c.X.kr(x,y);O=c.3s();O.8l(49.pR,49.2X);O.5p(N);O.3B(J)}I{if(c.X.kp){O=c.X.kp(x,y);O.3B(J)}I{if(1E X.2w.kv!="1I"){O=c.X.2w.kv();O.kC(x,y);q dm=O.pQ();dm.kC(x,y);O.pO("pP",dm);O.3o()}}}},7r:B(2h,L){if(1E(L)!="1I"){2h=L}if(c.eW()){if(c.C.1P){q 1X=$("<12>").1h($.2a(c.$K.o())).1X();q 2V=1X.2V()[0];if(2V.Q=="kx"&&2V.3P==""){2V=1X.4L()[0]}if(c.3W(2V)!=c.3W(2h)){F E}}I{if(c.$K.1X().2V()[0]!==2h){F E}}c.dp(2h)}},dp:B(2h){c.21();if(c.C.1P===E){q J=$(c.C.52);$(2h).2L(J);c.4j(J)}I{q J=$(\'<V id="1J-1K-1">\'+c.C.2e+"</V>",c.X)[0];$(2h).2L(J);$(J).2L(c.C.2e);c.1Y();c.$K.1b("V#1J-1K-1").2D("id")}},ai:B(ky){c.29();q br="<br>";if(ky==N){br="<br><br>"}if(c.1D("31")){q V=$("<V>").o(c.C.2e);c.$K.1b("#1J-1K-1").3Q(br).3Q(V).3Q(c.C.2e);c.iL(V[0]);V.1w();c.8P()}I{q L=c.2B();if(L&&L.Q==="A"){q 2X=c.eE(L);q Y=$.2a($(L).Y()).G(/\\n\\r\\n/g,"");q 4P=Y.1m;if(2X==4P){c.8P();q J=$(\'<V id="1J-1K-1">\'+c.C.2e+"</V>",c.X)[0];$(L).2L(J);$(J).3Q(br+(c.1D("4y")?c.C.2e:""));c.1Y();F N}}c.$K.1b("#1J-1K-1").3Q(br+(c.1D("4y")?c.C.2e:""));c.1Y()}},pT:B(){c.ai(N)},kc:B(2h){q J=$("<br>"+c.C.2e);$(2h).2i(J);c.4j(J)},k9:B(o){o=c.1d("pD",E,o);if(c.1D("3q")){q 2J=$.2a(o);if(2J.51(/^<a(.*?)>(.*?)<\\/a>$/i)==0){o=o.G(/^<a(.*?)>(.*?)<\\/a>$/i,"$2")}}if(c.C.kF){q 2J=c.X.4u("12");o=o.G(/<br>|<\\/H[1-6]>|<\\/p>|<\\/12>/gi,"\\n");2J.3P=o;o=2J.b7||2J.cb;o=$.2a(o);o=o.G("\\n","<br>");o=c.ap(o);c.9d(o);F E}q 9n=E;if(c.6K("6j")){9n=N;q 6G=c.C.9N;6G.3b("3f");6G.3b("1o");$.1u(6G,B(i,s){o=o.G(28 2G("<"+s+"(.*?)>","gi"),"");o=o.G(28 2G("</"+s+">","gi"),"<br>")})}if(c.6K("6f")){o=c.kK(o);c.9d(o);F N}o=o.G(/<1B(.*?)v:pF=(.*?)>/gi,"");o=o.G(/<p(.*?)1x="pJ"([\\w\\W]*?)<\\/p>/gi,"<2q><li$2</li>");o=o.G(/<p(.*?)1x="pH"([\\w\\W]*?)<\\/p>/gi,"<li$2</li>");o=o.G(/<p(.*?)1x="p4"([\\w\\W]*?)<\\/p>/gi,"<li$2</li></2q>");o=o.G(/<p(.*?)1x="p3"([\\w\\W]*?)<\\/p>/gi,"<2q><li$2</li></2q>");o=o.G(/·/g,"");o=o.G(/<!--[\\s\\S]*?-->|<\\?(?:4M)?[\\s\\S]*?\\?>/gi,"");if(c.C.lb===N){o=o.G(/(&3j;){2,}/gi,"&3j;");o=o.G(/&3j;/gi," ")}o=o.G(/<b\\os="l9-1v-1K(.*?)">([\\w\\W]*?)<\\/b>/gi,"$2");o=o.G(/<b(.*?)id="or-l9-op(.*?)">([\\w\\W]*?)<\\/b>/gi,"$3");o=o.G(/<V[^>]*(2u-1n: 3m; 2u-71: 3p|2u-71: 3p; 2u-1n: 3m)[^>]*>/gi,\'<V 1n="2u-71: 3p;"><V 1n="2u-1n: 3m;">\');o=o.G(/<V[^>]*2u-1n: 3m[^>]*>/gi,\'<V 1n="2u-1n: 3m;">\');o=o.G(/<V[^>]*2u-71: 3p[^>]*>/gi,\'<V 1n="2u-71: 3p;">\');o=o.G(/<V[^>]*Y-c4: 4J[^>]*>/gi,\'<V 1n="Y-c4: 4J;">\');o=o.G(/<1g>\\ov*<\\/1g>/gi,"[1g]");o=o.G(/<1g>&3j;<\\/1g>/gi,"[1g]");o=o.G(/<1g><br><\\/1g>/gi,"[1g]");o=o.G(/<1g(.*?)9U="(.*?)"(.*?)>([\\w\\W]*?)<\\/1g>/gi,\'[1g 9U="$2"]$4[/1g]\');o=o.G(/<1g(.*?)ak="(.*?)"(.*?)>([\\w\\W]*?)<\\/1g>/gi,\'[1g ak="$2"]$4[/1g]\');o=o.G(/<a(.*?)1S="(.*?)"(.*?)>([\\w\\W]*?)<\\/a>/gi,\'[a 1S="$2"]$4[/a]\');o=o.G(/<1R(.*?)>([\\w\\W]*?)<\\/1R>/gi,"[1R$1]$2[/1R]");o=o.G(/<3z(.*?)>([\\w\\W]*?)<\\/3z>/gi,"[3z$1]$2[/3z]");o=o.G(/<5O(.*?)>([\\w\\W]*?)<\\/5O>/gi,"[5O$1]$2[/5O]");o=o.G(/<4O(.*?)>([\\w\\W]*?)<\\/4O>/gi,"[4O$1]$2[/4O]");o=o.G(/<3M(.*?)>([\\w\\W]*?)<\\/3M>/gi,"[3M$1]$2[/3M]");o=o.G(/<2F(.*?)>/gi,"[2F$1]");o=o.G(/<1B(.*?)>/gi,"[1B$1]");o=o.G(/ 1x="(.*?)"/gi,"");o=o.G(/<(\\w+)([\\w\\W]*?)>/gi,"<$1>");if(c.C.1P){o=o.G(/<3T><\\/3T>/gi,"");o=o.G(/<u><\\/u>/gi,"");if(c.C.cS){o=o.G(/<2u(.*?)>([\\w\\W]*?)<\\/2u>/gi,"$2")}o=o.G(/<[^\\/>][^>]*>(\\s*|\\t*|\\n*|&3j;|<br>)<\\/[^>]+>/gi,"<br>")}I{o=o.G(/<[^\\/>][^>]*>(\\s*|\\t*|\\n*|&3j;|<br>)<\\/[^>]+>/gi,"")}o=o.G(/<12>\\s*?\\t*?\\n*?(<2q>|<ol>|<p>)/gi,"$1");o=o.G(/\\[1g 9U="(.*?)"\\]([\\w\\W]*?)\\[\\/1g\\]/gi,\'<1g 9U="$1">$2</1g>\');o=o.G(/\\[1g ak="(.*?)"\\]([\\w\\W]*?)\\[\\/1g\\]/gi,\'<1g ak="$1">$2</1g>\');o=o.G(/\\[1g\\]/gi,"<1g>&3j;</1g>");o=o.G(/\\[a 1S="(.*?)"\\]([\\w\\W]*?)\\[\\/a\\]/gi,\'<a 1S="$1">$2</a>\');o=o.G(/\\[1R(.*?)\\]([\\w\\W]*?)\\[\\/1R\\]/gi,"<1R$1>$2</1R>");o=o.G(/\\[3z(.*?)\\]([\\w\\W]*?)\\[\\/3z\\]/gi,"<3z$1>$2</3z>");o=o.G(/\\[5O(.*?)\\]([\\w\\W]*?)\\[\\/5O\\]/gi,"<5O$1>$2</5O>");o=o.G(/\\[4O(.*?)\\]([\\w\\W]*?)\\[\\/4O\\]/gi,"<4O$1>$2</4O>");o=o.G(/\\[3M(.*?)\\]([\\w\\W]*?)\\[\\/3M\\]/gi,"<3M$1>$2</3M>");o=o.G(/\\[2F(.*?)\\]/gi,"<2F$1>");o=o.G(/\\[1B(.*?)\\]/gi,"<1B$1>");if(c.C.9w){o=o.G(/<12(.*?)>([\\w\\W]*?)<\\/12>/gi,"<p>$2</p>");o=o.G(/<\\/12><p>/gi,"<p>");o=o.G(/<\\/p><\\/12>/gi,"</p>");o=o.G(/<p><\\/p>/gi,"<br />")}I{o=o.G(/<12><\\/12>/gi,"<br />")}o=c.7Q(o);if(c.6K("3e")){o=o.G(/<p>([\\w\\W]*?)<\\/p>/gi,"$1<br>")}I{if(9n===E){o=c.ap(o)}}o=o.G(/<V(.*?)>([\\w\\W]*?)<\\/V>/gi,"$2");o=o.G(/<1B>/gi,"");o=o.G(/<[^\\/>][^>][^1B|2F|1v|1g][^<]*>(\\s*|\\t*|\\n*| |<br>)<\\/[^>]+>/gi,"");o=o.G(/\\n{3,}/gi,"\\n");o=o.G(/<p><p>/gi,"<p>");o=o.G(/<\\/p><\\/p>/gi,"</p>");o=o.G(/<li>(\\s*|\\t*|\\n*)<p>/gi,"<li>");o=o.G(/<\\/p>(\\s*|\\t*|\\n*)<\\/li>/gi,"</li>");if(c.C.1P===N){o=o.G(/<p(.*?)>([\\w\\W]*?)<\\/p>/gi,"$2<br>")}o=o.G(/<[^\\/>][^>][^1B|2F|1v|1g][^<]*>(\\s*|\\t*|\\n*| |<br>)<\\/[^>]+>/gi,"");o=o.G(/<1B 3r="4y-oz-1M\\:\\/\\/(.*?)"(.*?)>/gi,"");o=o.G(/<1g(.*?)>(\\s*|\\t*|\\n*)<p>([\\w\\W]*?)<\\/p>(\\s*|\\t*|\\n*)<\\/1g>/gi,"<1g$1>$3</1g>");if(c.C.9w){o=o.G(/<12(.*?)>([\\w\\W]*?)<\\/12>/gi,"$2");o=o.G(/<12(.*?)>([\\w\\W]*?)<\\/12>/gi,"$2")}c.eF=E;if(c.1D("31")){if(c.C.9u){q 2y=o.1V(/<1B 3r="1a:T(.*?)"(.*?)>/gi);if(2y!==2H){c.eF=2y;3v(k in 2y){q 1B=2y[k].G("<1B",\'<1B 1a-31-9z-T="\'+k+\'" \');o=o.G(2y[k],1B)}}}3y(/<br>$/gi.4h(o)){o=o.G(/<br>$/gi,"")}}o=o.G(/<p>•([\\w\\W]*?)<\\/p>/gi,"<li>$1</li>");if(c.1D("3q")){3y(/<2u>([\\w\\W]*?)<\\/2u>/gi.4h(o)){o=o.G(/<2u>([\\w\\W]*?)<\\/2u>/gi,"$1")}}if(9n===E){o=o.G(/<1g(.*?)>([\\w\\W]*?)<p(.*?)>([\\w\\W]*?)<\\/1g>/gi,"<1g$1>$2$4</1g>");o=o.G(/<1g(.*?)>([\\w\\W]*?)<\\/p>([\\w\\W]*?)<\\/1g>/gi,"<1g$1>$2$3</1g>");o=o.G(/<1g(.*?)>([\\w\\W]*?)<p(.*?)>([\\w\\W]*?)<\\/1g>/gi,"<1g$1>$2$4</1g>");o=o.G(/<1g(.*?)>([\\w\\W]*?)<\\/p>([\\w\\W]*?)<\\/1g>/gi,"<1g$1>$2$3</1g>")}o=o.G(/\\n/g," ");o=o.G(/<p>\\n?<li>/gi,"<li>");c.9d(o)},kK:B(s){s=s.G(/<br>|<\\/H[1-6]>|<\\/p>|<\\/12>/gi,"\\n");q 2J=c.X.4u("12");2J.3P=s;F c.e4(2J.b7||2J.cb)},9d:B(o){o=c.1d("oy",E,o);if(c.5P){c.$K.o(o);c.co();c.df();c.1j()}I{c.ad(o)}c.5P=E;2Y($.M(B(){c.7Z=E;if(c.1D("31")){c.$K.1b("p:6c").1w()}if(c.eF!==E){c.kH()}},c),3O);if(c.C.4Z&&c.ei!==N){$(c.X.2w).3c(c.9v)}I{c.$K.3c(c.9v)}},bY:B(4x){if(c.C.3J!==E&&1E c.C.3J==="3M"){$.1u(c.C.3J,$.M(B(k,v){if(v!=2H&&v.4b().3V("#")===0){v=$(v).1p()}4x[k]=v},c))}F 4x},kH:B(){q kI=c.$K.1b("1B[1a-31-9z-T]");$.1u(kI,$.M(B(i,s){q $s=$(s);q 2O=s.3r.4d(",");q 4x={c0:2O[0].4d(";")[0].4d(":")[1],1a:2O[1]};4x=c.bY(4x);$.8k(c.C.c7,4x,$.M(B(1a){q 1L=(1E 1a==="7R"?$.8g(1a):1a);$s.1i("3r",1L.6F);$s.2D("1a-31-9z-T");c.1j();c.1d("3X",$s,1L)},c))},c))},kO:B(e){q a8=e.1O.a8;q 2O=a8.4d(",");q 4x={c0:2O[0].4d(";")[0].4d(":")[1],1a:2O[1]};if(c.C.9u){4x=c.bY(4x);$.8k(c.C.c7,4x,$.M(B(1a){q 1L=(1E 1a==="7R"?$.8g(1a):1a);q o=\'<1B 3r="\'+1L.6F+\'" id="kV-T-1K" />\';c.25("57",o,E);q T=$(c.$K.1b("1B#kV-T-1K"));if(T.1m){T.2D("id")}I{T=E}c.1j();if(T){c.1d("3X",T,1L)}},c))}I{c.ad(\'<1B 3r="\'+a8+\'" />\')}},21:B(29){if(29!==E){c.29()}c.C.3H.3b(c.$K.o());if(29!==E){c.8P("3H")}},kU:B(){if(c.C.3H.1m===0){c.4l();F}c.29();c.C.7P.3b(c.$K.o());c.1Y(E,N);c.$K.o(c.C.3H.i8());c.1Y();2Y($.M(c.7F,c),3O)},kT:B(){if(c.C.7P.1m===0){c.4l();F E}c.29();c.C.3H.3b(c.$K.o());c.1Y(E,N);c.$K.o(c.C.7P.i8());c.1Y(N);2Y($.M(c.7F,c),4)},7F:B(){c.4H();if(c.C.5r){c.5r()}},5r:B(){c.$K.1b("a").on("23",$.M(c.kA,c));c.$K.on("23.U",$.M(B(e){c.7I(e)},c));$(X).on("23.U",$.M(B(e){c.7I(e)},c))},4H:B(){if(c.C.4H===E){F E}c.$K.1b("1B").1u($.M(B(i,1A){if(c.1D("3q")){$(1A).1i("om","on")}q L=$(1A).L();if(!L.3x("ev")&&!L.3x("cx")){c.jC(1A)}},c));c.$K.1b(".cx, .ev").on("23",$.M(c.oc,c))},kA:B(e){q $1s=$(e.1O);q L=$(e.1O).L();if(L.3x("ev")||L.3x("cx")){F}if($1s.1U()==0||$1s[0].Q!=="A"){F}q 49=$1s.2X();if(c.C.1R){q cw=c.$2Z.2X();49.1T=cw.1T+(49.1T-$(c.X).3c());49.1t+=cw.1t}q 4z=$(\'<V 1x="U-1s-4z"></V>\');q 1S=$1s.1i("1S");if(1S===1I){1S=""}if(1S.1m>24){1S=1S.a3(0,24)+"..."}q iP=$(\'<a 1S="\'+$1s.1i("1S")+\'" 1O="6C">\'+1S+"</a>").on("23",$.M(B(e){c.7I(E)},c));q iR=$(\'<a 1S="#">\'+c.C.1F.84+"</a>").on("23",$.M(B(e){e.2A();c.a1();c.7I(E)},c));q k4=$(\'<a 1S="#">\'+c.C.1F.6e+"</a>").on("23",$.M(B(e){e.2A();c.25("6e");c.7I(E)},c));4z.1h(iP);4z.1h(" | ");4z.1h(iR);4z.1h(" | ");4z.1h(k4);4z.1f({1T:(49.1T+20)+"px",1t:49.1t+"px"});$(".U-1s-4z").1w();$("2w").1h(4z)},7I:B(e){if(e!==E&&e.1O.Q=="A"){F E}$(".U-1s-4z").1w()},2b:B(){if(!c.C.3Y){F c.X.2b()}I{if(!c.C.1R){F 3Y.2b()}I{F 3Y.2b(c.$2Z[0])}}},3s:B(){if(!c.C.3Y){if(c.X.2b){q 1q=c.2b();if(1q.41&&1q.4S){F 1q.41(0)}}F c.X.5n()}I{if(!c.C.1R){F 3Y.5n()}I{F 3Y.5n(c.cB())}}},cl:B(J){c.iM(J)},4j:B(J){c.8O(J[0]||J,0,2H,0)},7v:B(J){c.8O(J[0]||J,1,2H,1)},8O:B(4R,cj,86,aj){if(86==2H){86=4R}if(aj==2H){aj=cj}q 1q=c.2b();if(!1q){F}if(4R.Q=="P"&&4R.3P==""){4R.3P=c.C.2e}if(4R.Q=="cr"&&c.C.1P===E){q 6p=$(c.C.52)[0];$(4R).2i(6p);4R=6p;86=4R}q O=c.3s();O.8l(4R,cj);O.8A(86,aj);c2{1q.4N()}c6(e){}1q.53(O)},ce:B(1e){1e=1e.3R();q 1l=c.2Q();if(1l){q 3F=c.iN(1l,1e);c.1j();F 3F}q 1q=c.2b();q O=1q.41(0);q 3F=X.4u(1e);3F.6L(O.oe());O.3B(3F);c.cl(3F);F 3F},of:B(){q O=c.3s();O.9H(c.$K[0]);q 1q=c.2b();1q.4N();1q.53(O)},co:B(){c.2b().4N()},eE:B(2h){q cn=0;q O=c.3s();q 9I=O.a0();9I.9H(2h);9I.8A(O.8Q,O.iK);cn=$.2a(9I.4b()).1m;F cn},ka:B(){F 28 cF(c.2b().41(0))},iM:B(el,2v,3t){if(1E 3t==="1I"){3t=2v}el=el[0]||el;q O=c.3s();O.9H(el);q 4r=c.cH(el);q 9F=E;q 7y=0,7z;if(4r.1m==1&&2v){O.8l(4r[0],2v);O.8A(4r[0],3t)}I{3v(q i=0,8j;8j=4r[i++];){7z=7y+8j.1m;if(!9F&&2v>=7y&&(2v<7z||(2v==7z&&i<4r.1m))){O.8l(8j,2v-7y);9F=N}if(9F&&3t<=7z){O.8A(8j,3t-7y);54}7y=7z}}q 1q=c.2b();1q.4N();1q.53(O)},iL:B(J){c.$K.2d();J=J[0]||J;q O=c.X.5n();q 2v=1;q 3t=-1;O.8l(J,2v);O.8A(J,3t+2);q 1J=c.3Z.2b();q cM=c.X.5n();q 8Z=c.X.8N("\\7K");$(J).2L(8Z);cM.an(8Z);1J.4N();1J.53(cM);$(8Z).1w()},cH:B(J){q 4r=[];if(J.4q==3){4r.3b(J)}I{q 4c=J.7Y;3v(q i=0,4P=4c.1m;i<4P;++i){4r.3b.cJ(4r,c.cH(4c[i]))}}F 4r},3S:B(){q el=E;q 1q=c.2b();if(1q&&1q.4S>0){el=1q.41(0).6h}F c.4i(el)},2B:B(1A){1A=1A||c.3S();if(1A){F c.4i($(1A).L()[0])}I{F E}},2Q:B(J){if(1E J==="1I"){J=c.3S()}3y(J){if(c.6V(J)){if($(J).3x("4o")){F E}F J}J=J.4V}F E},3K:B(1W){q 8p=[];if(1E 1W=="1I"){q O=c.3s();if(O&&O.4X===N){F[c.2Q()]}q 1W=c.6W(O)}$.1u(1W,$.M(B(i,J){if(c.C.1R===E&&$(J).8f("12.4o").1U()==0){F E}if(c.6V(J)){8p.3b(J)}},c));if(8p.1m===0){8p=[c.2Q()]}F 8p},ok:B(J){if(J.4q!=1){F E}F!c.a6.4h(J.iV)},6V:B(J){F J.4q==1&&c.a6.4h(J.iV)},c3:B(1e){F c.a6.4h(1e)},6W:B(O,1e){if(1E O=="1I"||O==E){q O=c.3s()}if(O&&O.4X===N){if(1E 1e==="1I"&&c.c3(1e)){q 1l=c.2Q();if(1l.Q==1e){F[1l]}I{F[]}}I{F[c.3S()]}}q 1W=[],4s=[];q 1q=c.X.2b();if(!1q.oj){1W=c.bx(1q.41(0))}$.1u(1W,$.M(B(i,J){if(c.C.1R===E&&$(J).8f("12.4o").1U()==0){F E}if(1E 1e==="1I"){if($.2a(J.b7)!=""){4s.3b(J)}}I{if(J.Q==1e){4s.3b(J)}}},c));if(4s.1m==0){if(1E 1e==="1I"&&c.c3(1e)){q 1l=c.2Q();if(1l.Q==1e){F 4s.3b(1l)}I{F[]}}I{4s.3b(c.3S())}}q 2V=4s[4s.1m-1];if(c.6V(2V)){4s=4s.j3(0,-1)}F 4s},8G:B(J){if(!J){J=c.3S()}3y(J){if(J.4q==1){if($(J).3x("4o")){F E}F J}J=J.4V}F E},bx:B(O){O=O||c.3s();q J=O.6h;q bD=O.8Q;if(J==bD){F[J]}q 9T=[];3y(J&&J!=bD){9T.3b(J=c.j6(J))}J=O.6h;3y(J&&J!=O.oi){9T.j5(J);J=J.4V}F 9T},j6:B(J){if(J.og()){F J.7V}I{3y(J&&!J.j2){J=J.4V}if(!J){F 2H}F J.j2}},9S:B(){F c.2b().4b()},j1:B(){q o="";q 1q=c.2b();if(1q.4S){q bu=c.X.4u("12");q 4P=1q.4S;3v(q i=0;i<4P;++i){bu.6L(1q.41(i).oh())}o=bu.3P}F c.bq(o)},29:B(){if(!c.7u()){c.4l()}if(!c.C.3Y){c.iY(c.3s())}I{c.5h=3Y.oB()}},iY:B(O,1w){if(!O){F}q 5k=$(\'<V id="1J-1K-1" 1x="U-1J-1K">\'+c.C.2e+"</V>",c.X)[0];q 8C=$(\'<V id="1J-1K-2" 1x="U-1J-1K">\'+c.C.2e+"</V>",c.X)[0];if(O.4X===N){c.9W(O,5k,N)}I{c.9W(O,5k,N);c.9W(O,8C,E)}c.5h=c.$K.o();c.1Y(E,E)},9W:B(O,J,1G){q 9Z=O.a0();c2{9Z.5p(1G);9Z.3B(J);9Z.oC()}c6(e){q o=c.C.52;if(c.C.1P){o="<br>"}c.$K.6a(o);c.2d()}},1Y:B(G,1w){if(!c.C.3Y){if(G===N&&c.5h){c.$K.o(c.5h)}q 5k=c.$K.1b("V#1J-1K-1");q 8C=c.$K.1b("V#1J-1K-2");if(c.1D("31")){c.$K.2d()}I{if(!c.7u()){c.4l()}}if(5k.1m!=0&&8C.1m!=0){c.8O(5k[0],0,8C[0],0)}I{if(5k.1m!=0){c.8O(5k[0],0,2H,0)}}if(1w!==E){c.8P();c.5h=E}}I{3Y.oV(c.5h)}},8P:B(1G){if(!c.C.3Y){$.1u(c.$K.1b("V.U-1J-1K"),B(){q o=$.2a($(c).o().G(/[^\\oU-\\oT]/g,""));if(o==""){$(c).1w()}I{$(c).2D("1x").2D("id")}})}I{3Y.oS(c.5h)}},im:B(){c.29();c.5X(c.C.1F.1o,c.C.iH,oW,$.M(B(){$("#k3").23($.M(c.ip,c));2Y(B(){$("#da").2d()},5R)},c))},ip:B(){c.21(E);q aw=$("#da").1p(),av=$("#iS").1p(),$bV=$("<12></12>"),ew=4p.ja(4p.jg()*jm),$1o=$(\'<1o id="1o\'+ew+\'"><8L></8L></1o>\'),i,$9G,z,$9O;3v(i=0;i<aw;i++){$9G=$("<3f></3f>");3v(z=0;z<av;z++){$9O=$("<1g>"+c.C.2e+"</1g>");if(i===0&&z===0){$9O.1h(\'<V id="1J-1K-1">\'+c.C.2e+"</V>")}$($9G).1h($9O)}$1o.1h($9G)}$bV.1h($1o);q o=$bV.o();if(c.C.1P===E&&c.1D("31")){o+="<p>"+c.C.2e+"</p>"}c.3h();c.1Y();q 1r=c.2Q()||c.3S();if(1r&&1r.Q!="bT"){if(1r.Q=="3e"){q 1r=$(1r).2f("2q, ol")}$(1r).2L(o)}I{c.9K(o,E)}c.1Y();q 1o=c.$K.1b("#1o"+ew);c.8I();1o.1b("V#1J-1K-1, 47#1J-1K-1").1w();1o.2D("id");c.1j()},i9:B(){q $1o=$(c.2B()).2f("1o");if(!c.4i($1o)){F E}if($1o.1U()==0){F E}c.21();$1o.1w();c.1j()},ib:B(){q L=c.2B();q $1o=$(L).2f("1o");if(!c.4i($1o)){F E}if($1o.1U()==0){F E}c.21();q $4t=$(L).2f("3f");q $ex=$4t.4L().1m?$4t.4L():$4t.4e();if($ex.1m){q $eD=$ex.4c("1g").iB();if($eD.1m){$eD.6a(\'<V id="1J-1K-1">\'+c.C.2e+"</V>")}}$4t.1w();c.1Y();$1o.1b("V#1J-1K-1").1w();c.1j()},ic:B(){q L=c.2B();q $1o=$(L).2f("1o");if(!c.4i($1o)){F E}if($1o.1U()==0){F E}c.21();q $6i=$(L).2f("1g");if(!($6i.is("1g"))){$6i=$6i.2f("1g")}q 2K=$6i.2T(0).oY;$1o.1b("3f").1u($.M(B(i,1A){q iq=2K-1<0?2K+1:2K-1;if(i===0){$(1A).1b("1g").eq(iq).6a(\'<V id="1J-1K-1">\'+c.C.2e+"</V>")}$(1A).1b("1g").eq(2K).1w()},c));c.1Y();$1o.1b("V#1J-1K-1").1w();c.1j()},ir:B(){q $1o=$(c.2B()).2f("1o");if(!c.4i($1o)){F E}if($1o.1U()==0){F E}c.21();if($1o.1b("3U").1U()!==0){c.ed()}I{q 3f=$1o.1b("3f").iB().68();3f.1b("1g").o(c.C.2e);$3U=$("<3U></3U>");$3U.1h(3f);$1o.6a($3U);c.1j()}},ed:B(){q $1o=$(c.2B()).2f("1o");if(!c.4i($1o)){F E}q $3U=$1o.1b("3U");if($3U.1U()==0){F E}c.21();$3U.1w();c.1j()},iA:B(){c.ef("3Q")},iC:B(){c.ef("2L")},iD:B(){c.eo("3Q")},iE:B(){c.eo("2L")},ef:B(1G){q $1o=$(c.2B()).2f("1o");if(!c.4i($1o)){F E}if($1o.1U()==0){F E}c.21();q $4t=$(c.2B()).2f("3f");q 9M=$4t.68();9M.1b("1g").o(c.C.2e);if(1G==="2L"){$4t.2L(9M)}I{$4t.3Q(9M)}c.1j()},eo:B(1G){q L=c.2B();q $1o=$(L).2f("1o");if(!c.4i($1o)){F E}if($1o.1U()==0){F E}c.21();q 2K=0;q 1r=c.3S();q $4t=$(1r).2f("3f");q $6i=$(1r).2f("1g");$4t.1b("1g").1u($.M(B(i,1A){if($(1A)[0]===$6i[0]){2K=i}},c));$1o.1b("3f").1u($.M(B(i,1A){q $1r=$(1A).1b("1g").eq(2K);q 1g=$1r.68();1g.o(c.C.2e);1G==="2L"?$1r.2L(1g):$1r.3Q(1g)},c));c.1j()},iz:B(){c.29();c.5X(c.C.1F.3z,c.C.kR,oZ,$.M(B(){$("#kE").23($.M(c.iu,c));2Y(B(){$("#dr").2d()},5R)},c))},iu:B(){q 1a=$("#dr").1p();1a=c.7Q(1a);q 5m=\'<1R 2p="d8" 22="jN" 3r="\',7A=\'" cN="0" kP></1R>\';if(1a.1V(8r)){1a=1a.G(8r,5m+"//ao.d5.7w/4O/$1"+7A)}I{if(1a.1V(8J)){1a=1a.G(8J,5m+"//kn.ci.7w/3z/$2"+7A)}}c.1Y();q 1r=c.2Q()||c.3S();if(1r){$(1r).2L(1a)}I{c.9K(1a,E)}c.1j();c.3h()},a1:B(){c.29();q 1d=$.M(B(){if(c.C.eY!==E){c.a2={};q 4Y=c;$.jQ(c.C.eY,B(1a){q $3o=$("#U-iX-j7");$3o.o("");$.1u(1a,B(1k,1p){4Y.a2[1k]=1p;$3o.1h($("<48>").1p(1k).o(1p.2m))});$3o.on("5W",B(){q 1k=$(c).1p();q 2m="",1M="";if(1k!=0){2m=4Y.a2[1k].2m;1M=4Y.a2[1k].1M}$("#7O").1p(1M);$("#aD").1p(2m)});$3o.2N()})}c.6N=E;q 1q=c.2b();q 1M="",Y="",1O="";q 1A=c.2B();q 6p=$(1A).L().2T(0);if(6p&&6p.Q==="A"){1A=6p}if(1A&&1A.Q==="A"){1M=1A.1S;Y=$(1A).Y();1O=1A.1O;c.6N=1A}I{Y=1q.4b()}$("#aD").1p(Y);q it=iw.ix.1S.G(/\\/$/i,"");1M=1M.G(it,"");1M=1M.G(/^\\/#/,"#");1M=1M.G("eH:","");if(c.C.85===E){q re=28 2G("^(al|9X|5l)://"+iw.ix.oP,"i");1M=1M.G(re,"")}$("#7O").1p(1M);if(1O==="6C"){$("#7s").aa("ar",N)}c.eG=E;$("#iJ").on("23",$.M(c.jO,c));2Y(B(){$("#7O").2d()},5R)},c);c.5X(c.C.1F.1s,c.C.iW,oG,1d)},jO:B(){if(c.eG){F}c.eG=N;q 1O="",eO="";q 1s=$("#7O").1p();q Y=$("#aD").1p();if(1s.51("@")!=-1&&/(al|9X|5l):\\/\\//i.4h(1s)===E){1s="eH:"+1s}I{if(1s.51("#")!=0){if($("#7s").aa("ar")){1O=\' 1O="6C"\';eO="6C"}q eQ="((oN--)?[a-eM-9]+(-[a-eM-9]+)*.)+[a-z]{2,}";q re=28 2G("^(al|9X|5l)://"+eQ,"i");q jD=28 2G("^"+eQ,"i");if(1s.51(re)==-1&&1s.51(jD)==0&&c.C.85){1s=c.C.85+1s}}}Y=Y.G(/<|>/g,"");q eN="&3j;";if(c.1D("31")){eN="&3j;"}c.jF(\'<a 1S="\'+1s+\'"\'+1O+">"+Y+"</a>"+eN,$.2a(Y),1s,eO)},jF:B(a,Y,1s,1O){c.1Y();if(Y!==""){if(c.6N){c.21();$(c.6N).Y(Y).1i("1S",1s);if(1O!==""){$(c.6N).1i("1O",1O)}I{$(c.6N).2D("1O")}}I{q $a=$(a).2z("U-dw-1s");c.2o("57",c.3W($a),E);q 1s=c.$K.1b("a.U-dw-1s");1s.2D("1n").3d("U-dw-1s").1u(B(){if(c.2R==""){$(c).2D("1x")}})}c.1j()}2Y($.M(B(){if(c.C.5r){c.5r()}},c),5);c.3h()},jG:B(){c.29();q 1d=$.M(B(){q 1q=c.2b();q Y="";if(c.db()){Y=1q.Y}I{Y=1q.4b()}$("#e0").1p(Y);if(!c.5f()&&!c.cW()){c.ek("#5t",{1M:c.C.81,3J:c.C.3J,4f:$.M(c.dx,c),43:$.M(B(6I,1L){c.1d("jH",1L)},c),6E:c.C.e5})}c.ep("5t",{4g:N,1M:c.C.81,4f:$.M(c.dx,c),43:$.M(B(6I,1L){c.1d("jH",1L)},c)})},c);c.5X(c.C.1F.26,c.C.j9,d8,1d)},dx:B(1L){c.1Y();if(1L!==E){q Y=$("#e0").1p();if(Y===""){Y=1L.dX}q 1s=\'<a 1S="\'+1L.6F+\'" id="6F-1K">\'+Y+"</a>";if(c.1D("4y")&&!!c.3Z.eS){1s=1s+"&3j;"}c.25("57",1s,E);q 7U=$(c.$K.1b("a#6F-1K"));if(7U.1U()!=0){7U.2D("id")}I{7U=E}c.1j();c.1d("81",7U,1L)}c.3h()},jP:B(){c.29();q 1d=$.M(B(){if(c.C.7j){$.jQ(c.C.7j,$.M(B(1a){q 7k={},75=0;$.1u(1a,$.M(B(1k,1p){if(1E 1p.9B!=="1I"){75++;7k[1p.9B]=75}},c));q a7=E;$.1u(1a,$.M(B(1k,1p){q dg="";if(1E 1p.1c!=="1I"){dg=1p.1c}q af=0;if(!$.jY(7k)&&1E 1p.9B!=="1I"){af=7k[1p.9B];if(a7===E){a7=".7l"+af}}q 1B=$(\'<1B 3r="\'+1p.oK+\'" 1x="7l 7l\'+af+\'" 4n="\'+1p.T+\'" 1c="\'+dg+\'" />\');$("#e1").1h(1B);$(1B).23($.M(c.jB,c))},c));if(!$.jY(7k)){$(".7l").2U();$(a7).2N();q k0=B(e){$(".7l").2U();$(".7l"+$(e.1O).1p()).2N()};q 3o=$(\'<3o id="q0">\');$.1u(7k,B(k,v){3o.1h($(\'<48 2s="\'+v+\'">\'+k+"</48>"))});$("#e1").3Q(3o);3o.5W(k0)}},c))}I{$("#U-55-6s-2").1w()}if(c.C.3X||c.C.5B){if(!c.5f()&&!c.cW()&&c.C.5B===E){if($("#5t").1m){c.ek("#5t",{1M:c.C.3X,3J:c.C.3J,4f:$.M(c.bL,c),43:$.M(B(6I,1L){c.1d("eZ",1L)},c),6E:c.C.aK})}}if(c.C.5B===E){c.ep("5t",{4g:N,1M:c.C.3X,4f:$.M(c.bL,c),43:$.M(B(6I,1L){c.1d("eZ",1L)},c)})}I{$("#5t").on("5W.U",$.M(c.l3,c))}}I{$(".5C").2U();if(!c.C.7j){$("#5q").1w();$("#iG").2N()}I{$("#U-55-6s-1").1w();$("#U-55-6s-2").2z("7m");$("#iF").2N()}}if(!c.C.k1&&(c.C.3X||c.C.7j)){$("#U-55-6s-3").2U()}$("#iZ").23($.M(c.jV,c));if(!c.C.3X&&!c.C.7j){2Y(B(){$("#6t").2d()},5R)}},c);c.5X(c.C.1F.T,c.C.io,qi,1d)},jr:B(T){q $el=T;q L=$el.L().L();q 1d=$.M(B(){$("#dR").1p($el.1i("8d"));$("#rq").1i("1S",$el.1i("3r"));if($el.1f("3a")=="1l"&&$el.1f("5z")=="3n"){$("#aI").1p("77")}I{$("#aI").1p($el.1f("5z"))}if($(L).2T(0).Q==="A"){$("#6t").1p($(L).1i("1S"));if($(L).1i("1O")=="6C"){$("#7s").aa("ar",N)}}$("#ih").23($.M(B(){c.de($el)},c));$("#ij").23($.M(B(){c.jT($el)},c))},c);c.5X(c.C.1F.84,c.C.iy,ro,1d)},de:B(el){q a4=$(el).L().L();q L=$(el).L();q ac=E;if(a4.1m&&a4[0].Q==="A"){ac=N;$(a4).1w()}I{if(L.1m&&L[0].Q==="A"){ac=N;$(L).1w()}I{$(el).1w()}}if(L.1m&&L[0].Q==="P"){c.4l();if(ac===E){c.4j(L)}}c.1d("r3",el);c.3h();c.1j()},jT:B(el){c.6y(E);q $el=$(el);q L=$el.L();$el.1i("8d",$("#dR").1p());q ab=$("#aI").1p();q 2I="";if(ab==="1t"){2I="0 "+c.C.8q+" "+c.C.8q+" 0";$el.1f({"5z":"1t",2I:2I})}I{if(ab==="4K"){2I="0 0 "+c.C.8q+" "+c.C.8q+"";$el.1f({"5z":"4K",2I:2I})}I{if(ab==="77"){$el.1f({"5z":"",3a:"1l",2I:"4g"})}I{$el.1f({"5z":"",3a:"",2I:""})}}}q 1s=$.2a($("#6t").1p());if(1s!==""){q 1O=E;if($("#7s").aa("ar")){1O=N}if(L.2T(0).Q!=="A"){q a=$(\'<a 1S="\'+1s+\'">\'+c.3W(el)+"</a>");if(1O){a.1i("1O","6C")}$el.2i(a)}I{L.1i("1S",1s);if(1O){L.1i("1O","6C")}I{L.2D("1O")}}}I{if(L.2T(0).Q==="A"){L.2i(c.3W(el))}}c.3h();c.4H();c.1j()},6y:B(e){if(e!==E&&$(e.1O).L().1U()!=0&&$(e.1O).L()[0].id==="U-T-2C"){F E}q 2W=c.$K.1b("#U-T-2C");if(2W.1U()==0){F E}c.$K.1b("#U-T-d9, #U-T-cK").1w();2W.1b("1B").1f({5A:2W[0].1n.5A,9m:2W[0].1n.9m,5s:2W[0].1n.5s,9l:2W[0].1n.9l});2W.1f("2I","");2W.1b("1B").1f("ju","");2W.2i(B(){F $(c).1X()});$(X).3C("23.U-T-8v-2U");c.$K.3C("23.U-T-8v-2U");c.$K.3C("5y.U-T-dW");c.1j()},jC:B(T){q $T=$(T);$T.on("92",$.M(B(){c.6y(E)},c));$T.on("r7",$.M(B(){c.$K.on("6D.U-T-ji-6D",$.M(B(){2Y($.M(B(){c.4H();c.$K.3C("6D.U-T-ji-6D");c.1j()},c),1)},c))},c));$T.on("23",$.M(B(e){if(c.$K.1b("#U-T-2C").1U()!=0){F E}q rj=E,9q,9p,dS=$T.2p()/$T.22(),je=20,rk=10;q 5x=c.jw($T);q 9s=E;if(5x!==E){5x.on("92",B(e){9s=N;e.2A();dS=$T.2p()/$T.22();9q=4p.6d(e.dO-$T.eq(0).2X().1t);9p=4p.6d(e.dI-$T.eq(0).2X().1T)});$(c.X.2w).on("jh",$.M(B(e){if(9s){q rc=4p.6d(e.dO-$T.eq(0).2X().1t)-9q;q jd=4p.6d(e.dI-$T.eq(0).2X().1T)-9p;q jb=$T.22();q jf=9y(jb,10)+jd;q 8Y=4p.6d(jf*dS);if(8Y>je){$T.2p(8Y);if(8Y<3O){c.6A.1f({5A:"-bf",5s:"-r9",7W:"rd",9a:"rf jp"})}I{c.6A.1f({5A:"-95",5s:"-jq",7W:"95",9a:"bf 9e"})}}9q=4p.6d(e.dO-$T.eq(0).2X().1t);9p=4p.6d(e.dI-$T.eq(0).2X().1T);c.1j()}},c)).on("9o",B(){9s=E})}c.$K.on("5y.U-T-dW",$.M(B(e){q 1k=e.5Y;if(c.2M.9t==1k||c.2M.dF==1k){c.21(E);c.6y(E);c.de($T)}},c));$(X).on("23.U-T-8v-2U",$.M(c.6y,c));c.$K.on("23.U-T-8v-2U",$.M(c.6y,c))},c))},jw:B($T){q 2W=$(\'<V id="U-T-2C" 1a-U="9i">\');2W.1f({3k:"jy",3a:"47-1l",cZ:0,ri:"jA rh rg(0, 0, 0, .6)","5z":$T.1f("5z")});2W.1i("3L",E);if($T[0].1n.2I!="4g"){2W.1f({5A:$T[0].1n.5A,9m:$T[0].1n.9m,5s:$T[0].1n.5s,9l:$T[0].1n.9l});$T.1f("2I","")}I{2W.1f({3a:"1l",2I:"4g"})}$T.1f("ju",0.5).2L(2W);c.6A=$(\'<V id="U-T-d9" 1a-U="9i">\'+c.C.1F.84+"</V>");c.6A.1f({3k:"7S",9g:5,1T:"50%",1t:"50%",5A:"-95",5s:"-jq",cZ:1,jx:"#jn",6z:"#jz",7W:"95",9a:"bf 9e",bO:"r0"});c.6A.1i("3L",E);c.6A.on("23",$.M(B(){c.jr($T)},c));2W.1h(c.6A);if(c.C.js){q 5x=$(\'<V id="U-T-cK" 1a-U="9i"></V>\');5x.1f({3k:"7S",9g:2,cZ:1,bO:"nw-8v",jt:"-qY",4K:"-jp",r1:"jA r6 #jz",jx:"#jn",2p:"jj",22:"jj"});5x.1i("3L",E);2W.1h(5x);2W.1h($T);F 5x}I{2W.1h($T);F E}},jB:B(e){q 1B=\'<1B id="T-1K" 3r="\'+$(e.1O).1i("4n")+\'" 8d="\'+$(e.1O).1i("1c")+\'" />\';q L=c.2B();if(c.C.62&&$(L).2f("li").1U()==0){1B="<p>"+1B+"</p>"}c.bi(1B,N)},jV:B(){q 1p=$("#6t").1p();if(1p!==""){q 1a=\'<1B id="T-1K" 3r="\'+1p+\'" />\';if(c.C.1P===E){1a="<p>"+1a+"</p>"}c.bi(1a,N)}I{c.3h()}},bL:B(1a){c.bi(1a)},bi:B(1L,1s){c.1Y();if(1L!==E){q o="";if(1s!==N){o=\'<1B id="T-1K" 3r="\'+1L.6F+\'" />\';q L=c.2B();if(c.C.62&&$(L).2f("li").1U()==0){o="<p>"+o+"</p>"}}I{o=1L}c.25("57",o,E);q T=$(c.$K.1b("1B#T-1K"));if(T.1m){T.2D("id")}I{T=E}c.1j();1s!==N&&c.1d("3X",T,1L)}c.3h();c.4H()},k2:B(){if($("#U-82").1U()!=0){F}c.$jS=$(\'<12 id="U-82"><V></V></12>\');$(X.2w).1h(c.$jS)},80:B(){c.k2();$("#U-82").rz()},9x:B(){$("#U-82").72(rD)},jJ:B(){$.4F(c.C,{j9:4U()+\'<2l id="U-5V-26-4v"><2t id="rA" 5v="8k" 6Z="" aA="aC/2t-1a"><2E>\'+c.C.1F.dX+\'</2E><2S 1G="Y" id="e0" 1x="7p" /><12 1n="2I-1T: bf;"><2S 1G="26" id="5t" 2m="\'+c.C.e5+\'" /></12></2t></2l>\',iy:4U()+\'<2l id="U-5V-T-84"><2E>\'+c.C.1F.1c+\'</2E><2S 1G="Y" id="dR" 1x="7p" /><2E>\'+c.C.1F.1s+\'</2E><2S 1G="Y" id="6t" 1x="7p" /><2E><2S 1G="iU" id="7s"> \'+c.C.1F.do+"</2E><2E>"+c.C.1F.ie+\'</2E><3o id="aI"><48 2s="3n">\'+c.C.1F.3n+\'</48><48 2s="1t">\'+c.C.1F.1t+\'</48><48 2s="77">\'+c.C.1F.77+\'</48><48 2s="4K">\'+c.C.1F.4K+\'</48></3o></2l><4a><1C id="ih" 1x="4w rx">\'+c.C.1F.ii+\'</1C><1C 1x="4w 6R">\'+c.C.1F.6M+\'</1C><1C id="ij" 1x="4w 6Q">\'+c.C.1F.7X+"</1C></4a>",io:4U()+\'<2l id="U-5V-T-4v"><12 id="5q"><a 1S="#" id="U-55-6s-1" 1x="7m">\'+c.C.1F.89+\'</a><a 1S="#" id="U-55-6s-2">\'+c.C.1F.dK+\'</a><a 1S="#" id="U-55-6s-3">\'+c.C.1F.1s+\'</a></12><2t id="ry" 5v="8k" 6Z="" aA="aC/2t-1a"><12 id="rp" 1x="5C"><2S 1G="26" id="5t" 2m="\'+c.C.aK+\'" /></12><12 id="iF" 1x="5C" 1n="3a: 3n;"><12 id="e1"></12></12></2t><12 id="iG" 1x="5C" 1n="3a: 3n;"><2E>\'+c.C.1F.j0+\'</2E><2S 1G="Y" 2m="6t" id="6t" 1x="7p"  /><br><br></12></2l><4a><1C 1x="4w 6R">\'+c.C.1F.6M+\'</1C><1C 1x="4w 6Q" id="iZ">\'+c.C.1F.4v+"</1C></4a>",iW:4U()+\'<2l id="U-5V-1s-4v"><3o id="U-iX-j7" 1n="2p: 99.5%; 3a: 3n;"></3o><2E>j4</2E><2S 1G="Y" 1x="7p" id="7O" /><2E>\'+c.C.1F.Y+\'</2E><2S 1G="Y" 1x="7p" id="aD" /><2E><2S 1G="iU" id="7s"> \'+c.C.1F.do+\'</2E></2l><4a><1C 1x="4w 6R">\'+c.C.1F.6M+\'</1C><1C id="iJ" 1x="4w 6Q">\'+c.C.1F.4v+"</1C></4a>",iH:4U()+\'<2l id="U-5V-1o-4v"><2E>\'+c.C.1F.aw+\'</2E><2S 1G="Y" 1U="5" 2s="2" id="da" /><2E>\'+c.C.1F.av+\'</2E><2S 1G="Y" 1U="5" 2s="3" id="iS" /></2l><4a><1C 1x="4w 6R">\'+c.C.1F.6M+\'</1C><1C id="k3" 1x="4w 6Q">\'+c.C.1F.4v+"</1C></4a>",kR:4U()+\'<2l id="U-5V-3z-4v"><2t id="qV"><2E>\'+c.C.1F.ke+\'</2E><5H id="dr" 1n="2p: 99%; 22: ls;"></5H></2t></2l><4a><1C 1x="4w 6R">\'+c.C.1F.6M+\'</1C><1C id="kE" 1x="4w 6Q">\'+c.C.1F.4v+"</1C></4a>"})},5X:B(1c,3G,2p,1d){c.km();c.$b5=2p;c.$2P=$("#bn");if(!c.$2P.1m){c.$2P=$(\'<12 id="bn" 1n="3a: 3n;" />\');c.$2P.1h($(\'<12 id="cc">&lg;</12>\'));c.$2P.1h($(\'<aP id="b6" />\'));c.$2P.1h($(\'<12 id="b0" />\'));c.$2P.83(X.2w)}$("#cc").on("23",$.M(c.3h,c));$(X).5a($.M(c.c5,c));c.$K.5a($.M(c.c5,c));c.ks(3G);c.ko(1c);c.k8();c.kl();c.kB();c.kw();c.6U=c.X.2w.3c;if(c.C.4Z===E){c.6U=c.$K.3c()}if(c.5f()===E){c.l0()}I{c.kq()}if(1E 1d==="B"){1d()}2Y($.M(B(){c.1d("lh",c.$2P)},c),11);$(X).3C("m7.5V");c.$2P.1b("2S[1G=Y]").on("lY",$.M(B(e){if(e.5Y===13){c.$2P.1b(".6Q").23();e.2A()}},c));F c.$2P},l0:B(){c.$2P.1f({3k:"b4",1T:"-eJ",1t:"50%",2p:c.$b5+"px",5s:"-"+(c.$b5/2)+"px"}).2N();c.bw=$(X.2w).1f("aS");$(X.2w).1f("aS","8w");2Y($.M(B(){q 22=c.$2P.ki();c.$2P.1f({1T:"50%",22:"4g",7L:"4g",5A:"-"+(22+10)/2+"px"})},c),15)},kq:B(){c.$2P.1f({3k:"b4",2p:"3O%",22:"3O%",1T:"0",1t:"0",2I:"0",7L:"lU"}).2N()},ks:B(3G){c.6b=E;if(3G.3V("#")==0){c.6b=$(3G);$("#b0").6c().1h(c.6b.o());c.6b.o("")}I{$("#b0").6c().1h(3G)}},ko:B(1c){c.$2P.1b("#b6").o(1c)},kw:B(){q 44=c.$2P.1b("4a 1C").b1(".m5");q cP=44.1U();if(cP>0){$(44).1f("2p",(c.$b5/cP)+"px")}},kB:B(){c.$2P.1b(".6R").on("23",$.M(c.3h,c))},km:B(){if(c.C.bo){c.$8F=$("#bl");if(!c.$8F.1m){c.$8F=$(\'<12 id="bl" 1n="3a: 3n;"></12>\');$("2w").6a(c.$8F)}c.$8F.2N().on("23",$.M(c.3h,c))}},k8:B(){if(1E $.fn.k5!=="1I"){c.$2P.k5({lQ:"#b6"});c.$2P.1b("#b6").1f("bO","lG")}},kl:B(){q $5q=$("#5q");if(!$5q.1m){F E}q 4Y=c;$5q.1b("a").1u(B(i,s){i++;$(s).on("23",B(e){e.2A();$5q.1b("a").3d("7m");$(c).2z("7m");$(".5C").2U();$("#5C"+i).2N();$("#lH").1p(i);if(4Y.5f()===E){q 22=4Y.$2P.ki();4Y.$2P.1f("2I-1T","-"+(22+10)/2+"px")}})})},c5:B(e){if(e.2M===c.2M.bK){c.3h();F E}},3h:B(){$("#cc").3C("23",c.3h);$("#bn").72("lF",$.M(B(){q bp=$("#b0");if(c.6b!==E){c.6b.o(bp.o());c.6b=E}bp.o("");if(c.C.bo){$("#bl").2U().3C("23",c.3h)}$(X).kg("5a",c.l7);c.$K.kg("5a",c.l7);c.1Y();if(c.C.4Z&&c.6U){$(c.X.2w).3c(c.6U)}I{if(c.C.4Z===E&&c.6U){c.$K.3c(c.6U)}}c.1d("lE")},c));if(c.5f()===E){$(X.2w).1f("aS",c.bw?c.bw:"l5")}F E},lB:B(cO){$(".5C").2U();$("#5q").1b("a").3d("7m").eq(cO-1).2z("7m");$("#5C"+cO).2N()},l3:B(e){q 7e=e.1O.7e;3v(q i=0,f;f=7e[i];i++){c.cU(f)}},cU:B(26){c.kZ(26,$.M(B(l2){c.ik(26,l2)},c))},kZ:B(26,1d){q 2n=28 iO();q d6="?";if(c.C.5B.51(/\\?/)!="-1"){d6="&"}2n.aW("lC",c.C.5B+d6+"2m="+26.2m+"&1G="+26.1G,N);if(2n.l1){2n.l1("Y/lD; lI=x-lJ-lP")}q 4Y=c;2n.lN=B(e){if(c.kS==4&&c.cC==5R){4Y.80();1d(lK(c.lL))}I{if(c.kS==4&&c.cC!=5R){}}};2n.jo()},ia:B(5v,1M){q 2n=28 iO();if("lO"in 2n){2n.aW(5v,1M,N)}I{if(1E il!="1I"){2n=28 il();2n.aW(5v,1M)}I{2n=2H}}F 2n},ik:B(26,1M){q 2n=c.ia("m4",1M);if(!2n){}I{2n.jL=$.M(B(){if(2n.cC==5R){c.9x();q ct=1M.4d("?");if(!ct[0]){F E}c.1Y();q o="";o=\'<1B id="T-1K" 3r="\'+ct[0]+\'" />\';if(c.C.62){o="<p>"+o+"</p>"}c.25("57",o,E);q T=$(c.$K.1b("1B#T-1K"));if(T.1m){T.2D("id")}I{T=E}c.1j();c.1d("3X",T,E);c.3h();c.4H()}I{}},c);2n.lA=B(){};2n.89.m0=B(e){};2n.jv("lT-lj",26.1G);2n.jv("x-nr-nq","nn-nu");2n.jo(26)}},ep:B(el,3N){c.3u={1M:E,4f:E,43:E,2v:E,dY:E,4g:E,2S:E};$.4F(c.3u,3N);q $el=$("#"+el);if($el.1m&&$el[0].Q==="nB"){c.3u.2S=$el;c.el=$($el[0].2t)}I{c.el=$el}c.jI=c.el.1i("6Z");if(c.3u.4g){$(c.3u.2S).5W($.M(B(e){c.el.dT(B(e){F E});c.dM(e)},c))}I{if(c.3u.dY){$("#"+c.3u.dY).23($.M(c.dM,c))}}},dM:B(e){c.80();c.jk(c.2h,c.jc())},jc:B(){c.id="f"+4p.ja(4p.jg()*jm);q d=c.X.4u("12");q 1R=\'<1R 1n="3a:3n" id="\'+c.id+\'" 2m="\'+c.id+\'"></1R>\';d.3P=1R;$(d).83("2w");if(c.3u.2v){c.3u.2v()}$("#"+c.id).jl($.M(c.jW,c));F c.id},jk:B(f,2m){if(c.3u.2S){q bA="nx"+c.id,jR="ny"+c.id;c.2t=$(\'<2t  6Z="\'+c.3u.1M+\'" 5v="eC" 1O="\'+2m+\'" 2m="\'+bA+\'" id="\'+bA+\'" aA="aC/2t-1a" />\');if(c.C.3J!==E&&1E c.C.3J==="3M"){$.1u(c.C.3J,$.M(B(k,v){if(v!=2H&&v.4b().3V("#")===0){v=$(v).1p()}q 8w=$("<2S/>",{1G:"8w",2m:k,2s:v});$(c.2t).1h(8w)},c))}q bX=c.3u.2S;q 8D=$(bX).68();$(bX).1i("id",jR).3Q(8D).83(c.2t);$(c.2t).1f("3k","7S").1f("1T","-eJ").1f("1t","-eJ").83("2w");c.2t.dT()}I{f.1i("1O",2m).1i("5v","eC").1i("aA","aC/2t-1a").1i("6Z",c.3u.1M);c.2h.dT()}},jW:B(){q i=$("#"+c.id)[0],d;if(i.jX){d=i.jX}I{if(i.bt){d=i.bt.X}I{d=3Z.nk[c.id].X}}if(c.3u.4f){c.9x();if(1E d!=="1I"){q jZ=d.2w.3P;q 7c=jZ.1V(/\\{(.|\\n)*\\}/)[0];7c=7c.G(/^\\[/,"");7c=7c.G(/\\]$/,"");q 1L=$.8g(7c);if(1E 1L.43=="1I"){c.3u.4f(1L)}I{c.3u.43(c,1L);c.3h()}}I{c.3h();na("jE n9!")}}c.el.1i("6Z",c.jI);c.el.1i("1O","")},ek:B(el,3N){c.5w=$.4F({1M:E,4f:E,43:E,m8:E,3J:E,Y:c.C.1F.jM,j8:c.C.1F.jK,6E:E},3N);if(3Z.es===1I){F E}c.dq=$(\'<12 1x="nd"></12>\');c.5g=$(\'<12 1x="ne">\'+c.5w.Y+"</12>");c.iv=$(\'<12 1x="nj">\'+c.5w.j8+"</12>");c.dq.1h(c.5g);$(el).3Q(c.dq);$(el).3Q(c.iv);c.5g.on("nf",$.M(B(){F c.kY()},c));c.5g.on("o5",$.M(B(){F c.ld()},c));c.5g.2T(0).o4=$.M(B(e){e.2A();c.5g.3d("cA").2z("6D");c.80();c.bm(c.5w.1M,e.9c.7e[0],E,e,c.5w.6E)},c)},bm:B(1M,26,bB,e,6E){if(!bB){q 2n=$.o3.2n();if(2n.89){2n.89.o1("82",$.M(c.lf,c),E)}$.o2({2n:B(){F 2n}})}c.1d("6D",e);q fd=28 es();if(6E!==E){fd.1h(6E,26)}I{fd.1h("26",26)}if(c.C.3J!==E&&1E c.C.3J==="3M"){$.1u(c.C.3J,$.M(B(k,v){if(v!=2H&&v.4b().3V("#")===0){v=$(v).1p()}fd.1h(k,v)},c))}$.iI({1M:1M,nG:"o",1a:fd,nK:E,c0:E,nL:E,1G:"eC",4f:$.M(B(1a){1a=1a.G(/^\\[/,"");1a=1a.G(/\\]$/,"");q 1L=(1E 1a==="7R"?$.8g(1a):1a);c.9x();if(bB){q $1B=$("<1B>");$1B.1i("3r",1L.6F).1i("id","kJ-T-1K");c.kG(e,$1B[0]);q T=$(c.$K.1b("1B#kJ-T-1K"));if(T.1m){T.2D("id")}I{T=E}c.1j();c.4H();if(T){c.1d("3X",T,1L)}if(1E 1L.43!=="1I"){c.1d("eZ",1L)}}I{if(1E 1L.43=="1I"){c.5w.4f(1L)}I{c.5w.43(c,1L);c.5w.4f(E)}}},c)})},kY:B(){c.5g.2z("cA");F E},ld:B(){c.5g.3d("cA");F E},lf:B(e,Y){q kh=e.la?9y(e.la/e.mp*3O,10):e;c.5g.Y("mv "+kh+"% "+(Y||""))},5f:B(){F/(mz|my|mw|mx)/.4h(6r.7g)},cW:B(){F/mn/.4h(6r.7g)},aU:B(4C){if(1E(4C)==="1I"){F 0}F 9y(4C.G("px",""),10)},3W:B(el){F $("<12>").1h($(el).eq(0).68()).o()},me:B(o){q 2J=X.4u("8y");2J.3P=o;F 2J.b7||2J.cb||""},kd:B(6I){F mb.5j.4b.5K(6I)=="[3M 4U]"},bb:B(o){o=o.G(/&#b2;|<br>|<br\\/>|&3j;/gi,"");o=o.G(/\\s/g,"");o=o.G(/^<p>[^\\W\\w\\D\\d]*?<\\/p>$/i,"");F o==""},ml:B(){q rv=E;if(6r.mk=="mj mi mC"){q 42=6r.7g;q re=28 2G("mV ([0-9]{1,}[.0-9]{0,})");if(re.2o(42)!=2H){rv=mZ(2G.$1)}}F rv},8H:B(){F!!6r.7g.1V(/n2\\/7\\./)},1D:B(1D){q 42=6r.7g.3R();q 1V=/(kj)[\\/]([\\w.]+)/.2o(42)||/(eS)[ \\/]([\\w.]+)/.2o(42)||/(4y)[ \\/]([\\w.]+).*(mH)[ \\/]([\\w.]+)/.2o(42)||/(4y)[ \\/]([\\w.]+)/.2o(42)||/(bj)(?:.*9r|)[ \\/]([\\w.]+)/.2o(42)||/(3q) ([\\w.]+)/.2o(42)||42.3V("mI")>=0&&/(rv)(?::| )([\\w.]+)/.2o(42)||42.3V("mG")<0&&/(31)(?:.*? rv:([\\w.]+)|)/.2o(42)||[];if(1D=="9r"){F 1V[2]}if(1D=="4y"){F(1V[1]=="eS"||1V[1]=="4y")}if(1V[1]=="rv"){F 1D=="3q"}if(1V[1]=="kj"){F 1D=="4y"}F 1D==1V[1]},db:B(){if(c.1D("3q")&&9y(c.1D("9r"),10)<9){F N}F E},lc:B(kk){q 7o=kk.mf(N);q 12=c.X.4u("12");12.6L(7o);F 12.3P},dh:B(){q J=c.$K[0];q 4k=c.X.dz();q eT;3y((eT=J.7V)){4k.6L(eT)}F 4k},4i:B(el){if(!el){F E}if(c.C.1R){F el}if($(el).8f("12.4o").1m==0||$(el).3x("4o")){F E}I{F el}},6K:B(Q){q L=c.2B(),1r=c.3S();F L&&L.Q===Q?L:1r&&1r.Q===Q?1r:E},eW:B(){q 1r=c.2Q();q 2X=c.eE(1r);q Y=$.2a($(1r).Y()).G(/\\n\\r\\n/g,"");q 4P=Y.1m;if(2X==4P){F N}I{F E}},7u:B(){q el,1q=c.2b();if(1q&&1q.4S&&1q.4S>0){el=1q.41(0).6h}if(!el){F E}if(c.C.1R){if(c.ka().kb()){F!c.$K.is(el)}I{F N}}F $(el).2f("12.4o").1m!=0},4Q:B(el,1i){if($(el).1i(1i)==""){$(el).2D(1i)}},iQ:B(9E,2s){q 2K=2H;3y((2K=9E.3V(2s))!==-1){9E.9C(2K,1)}F 9E}};3E.5j.74.5j=3E.5j;$.3E.fn.dZ=B(eK,6J,6m,7E,7D){q 1M=/(((5l?|lW?):\\/\\/)|ao[.][^\\s])(.+?\\..+?)([.),]?)(\\s|\\.\\s+|\\)|$)/gi,iT=/(5l?|9X):\\/\\//i,d2=/(5l?:\\/\\/.*\\.(?:l8|ly|kf|l4))/gi;q 7Y=(c.$K?c.$K.2T(0):c).7Y,i=7Y.1m;3y(i--){q n=7Y[i];if(n.4q===3){q o=n.aq;if(7E&&o){q 5m=\'<1R 2p="d8" 22="jN" 3r="\',7A=\'" cN="0" kP></1R>\';if(o.1V(8r)){o=o.G(8r,5m+"//ao.d5.7w/4O/$1"+7A);$(n).2L(o).1w()}I{if(o.1V(8J)){o=o.G(8J,5m+"//kn.ci.7w/3z/$2"+7A);$(n).2L(o).1w()}}}if(6m&&o&&o.1V(d2)){o=o.G(d2,\'<1B 3r="$1">\');$(n).2L(o).1w()}if(6J&&o&&o.1V(1M)){q 2y=o.1V(1M);3v(q i in 2y){q 1S=2y[i];q Y=1S;q 6k="";if(1S.1V(/\\s$/)!==2H){6k=" "}q e7=eK;if(1S.1V(iT)!==2H){e7=""}if(Y.1m>7D){Y=Y.a3(0,7D)+"..."}Y=Y.G(/&/g,"&9Q;").G(/</g,"&lt;").G(/>/g,"&gt;");q jU=Y.G("$","$$$");o=o.G(1S,\'<a 1S="\'+e7+$.2a(1S)+\'">\'+$.2a(jU)+"</a>"+6k)}$(n).2L(o).1w()}}I{if(n.4q===1&&!/^(a|1C|5H)$/i.4h(n.Q)){$.3E.fn.dZ.5K(n,eK,6J,6m,7E,7D)}}}}})(oo);',62,1716,'||||||||||||this||||||||||||html||var|||||||||||function|opts||false|return|replace||else|node|editor|parent|proxy|true|range||tagName|||image|redactor|span||document|text||||div||||||||data|find|title|callback|tag|css|td|append|attr|sync|key|block|length|style|table|val|sel|current|link|left|each|source|remove|class|lang|toolbar|elem|img|button|browser|typeof|curLang|type|func|undefined|selection|marker|json|url|cmd|target|linebreaks|dropdown|iframe|href|top|size|match|nodes|contents|selectionRestore|out||bufferSet|height|click||execCommand|file||new|selectionSave|trim|getSelection|btn|focus|invisibleSpace|closest|blockquote|element|replaceWith|btnObject|code|section|name|xhr|exec|width|ul|pre|value|form|font|start|body|btnName|matches|addClass|preventDefault|getParent|box|removeAttr|label|param|RegExp|null|margin|tmp|index|after|keyCode|show|arr|redactorModal|getBlock|className|input|get|hide|last|imageBox|offset|setTimeout|frame|air|mozilla|||||||||display|push|scrollTop|removeClass|LI|tr|point|modalClose|list|nbsp|position|script|italic|none|select|bold|msie|src|getRange|end|uploadOptions|for|ctrl|hasClass|while|video|doc|insertNode|off|BLOCKQUOTE|Redactor|wrapper|content|buffer|blocks|uploadFields|getBlocks|contenteditable|object|options|100|innerHTML|before|toLowerCase|getCurrent|strong|thead|indexOf|outerHtml|imageUpload|rangy|window||getRangeAt|ua|error|buttons|||inline|option|pos|footer|toString|children|split|next|success|auto|test|isParentRedactor|selectionStart|frag|focusWithSaveScroll|fullpage|rel|redactor_editor|Math|nodeType|textNodes|finalnodes|current_tr|createElement|insert|redactor_modal_btn|postData|webkit|tooltip|htmls|meta|str|elems|buttonGet|extend|substr|observeImages|cont|underline|right|prev|php|removeAllRanges|embed|len|removeEmptyAttr|orgn|rangeCount|placeholder|String|parentNode|inArray|collapsed|that|autoresize||search|emptyHtml|addRange|break|tab|align|inserthtml|blockElem||keyup|del|redactor_placeholder|shiftKey|keys|isMobile|dropareabox|savedSel|visual|prototype|node1|https|iframeStart|createRange|listTag|collapse|redactor_tabs|observeLinks|marginLeft|redactor_file|deleted|method|draguploadOptions|imageResizer|keydown|float|marginTop|s3|redactor_tab|alignmentTags|strike|formatBlocks|lastNode|textarea|rule|charAt|call|case|regex|listCurrent|audio|selectall|newTag|200|allowedTags|replaced|event|modal|change|modalInit|which|dir|||paragraphy|join|orderedlist|||textareamode|clone|direction|prepend|modalcontent|empty|round|unlink|PRE|indent|startContainer|current_td|TD|space|formatBlock|convertImageLinks|italicTag|boldTag|par|Insert|navigator|control|redactor_file_link|unorderedlist|modified|formatting|toolbarFixed|imageResizeHide|color|imageEditter|shortcuts|_blank|drop|uploadParam|filelink|blocksElems|buttonActive|obj|convertLinks|currentOrParentIs|appendChild|cancel|insert_link_node|buttonBuild|phpMatches|redactor_modal_action_btn|redactor_btn_modal_close|outdent|listParent|saveModalScroll|nodeTestBlocks|getNodes|tmpList|cleanGetTabs|action|etags|weight|fadeOut||init|count|hideHandler|center|item|dropact|instance|autosave|jsonString|uuid|files|replacementTag|userAgent|set|possible|imageGetJson|folders|redactorfolder|redactor_tabs_act|modif|cloned|redactor_input|tabindex|insertAfterLastElement|redactor_link_blank|fullpageDoctype|isFocused|selectionEnd|com|thtml|charCount|endCharCount|iframeEnd|newblock|cleanRemoveSpaces|linkSize|convertVideoLinks|observeStart|elements|enter|linkObserverTooltipClose|isFunction|u200B|minHeight|boxTop|background|redactor_link_url|rebuffer|cleanStripTags|string|absolute|character|linkmarker|firstChild|fontSize|save|childNodes|rtePaste|showProgressBar|fileUpload|progress|appendTo|edit|linkProtocol|focn|metaKey|alignmentSet|upload|redactor_act|tagblock|horizontalrule|alt|insertunorderedlist|parents|parseJSON|shift|dblEnter|textNode|post|setStart|cleanlevel|tmpLi|typewriter|newnodes|imageFloatMargin|reUrlYoutube|Add|deniedTags|toolbarFixedTarget|resize|hidden|Delete|DIV|toolbarExternal|setEnd|Header|node2|newElement|tagout|redactorModalOverlay|getElement|isIe11|buttonActiveObserver|reUrlVimeo|setNonEditable|tbody|insertorderedlist|createTextNode|selectionSet|selectionRemoveMarkers|endContainer|phpTags|keyPosition|cleanConvertInlineTags|toolbarFixedTopOffset|one|hdlHideDropDown|allowed|new_w|emptyElement|||mousedown|separator|deleteContents|11px|||||padding|outer|dataTransfer|pasteInsert|10px|template|zIndex|javascript|verified|execPasteFrag|toolbarFixedBox|marginRight|marginBottom|tablePaste|mouseup|start_y|start_x|version|isResizing|BACKSPACE|clipboardUpload|saveScroll|convertDivs|hideProgressBar|parseInt|paste||folder|splice|head|array|foundStart|row|selectNodeContents|preCaretRange|ARTICLE|insertHtmlAdvanced|activeButtons|new_tr|blockLevelElements|column|removeEmptyTags|amp|listText|getSelectionText|rangeNodes|colspan|indentValue|selectionSetMarker|ftp|wrapperHtml|boundaryRange|cloneRange|linkShow|predefinedLinksStorage|substring|parentLink|airBindMousemoveHide|rTestBlock|folderclass|result|dropdownHideAll|prop|floating|parentEl|insertHtml|FOOTER|folderkey|classname|icon|insertLineBreak|foco|rowspan|http|ASIDE|setStartAfter|www|cleanParagraphy|nodeValue|checked|sourceHeight|specialKey|special|columns|rows|selected|parentHtml|clickedElement|enctype|selectionHtml|multipart|redactor_link_url_text|spans|codeLength|direct|inlineRemoveFormatReplace|redactor_form_image_align|fonts|imageUploadParam|setSpansVerified|documentElement|focusSet|ctrlKey|header|alignment|buildCodearea|overflow|insertAfter|normalize|ADDRESS|open|tabSpaces|ENTER|strip|redactor_modal_inner|not|x200b|SECTION|fixed|redactorModalWidth|redactor_modal_header|textContent|inlineMethods|placeTag|htmlTagName|isEmpty|methodVal|setSpansVerifiedHtml||7px|HEADER|line|imageInsert|opera|autosaveInterval|redactor_modal_overlay|dragUploadAjax|redactor_modal|modalOverlay|redactorModalInner|syncClean||DOWN|contentWindow|container|delete_row|modalSaveBodyOveflow|getRangeSelectedNodes|cleanEmpty|Table|formId|directupload|sourceOld|endNode|lineOrWord|indentingOutdent|iframeAppend|indentingStart|delete_table|indentingIndent|ESC|imageCallback|commentsMatches|buildEnable|cursor|reader|etagsInline|iframePage|iframeLoad|BODY|merge|table_box|iframeAddCss|oldElement|pasteClipboardAppendFields|suffix|contentType|cleanConverters|try|tagTestBlock|decoration|modalCloseHandler|catch|clipboardUploadUrl|elem2|modify|delete_column|innerText|redactor_modal_close|redactor_toolbar|selectionWrap|alignmentRight|FIGCAPTION|align_left|vimeo|orgo|alignmentLeft|selectionElement|formatblock|caretOffset|selectionRemove|pasteHTML|alignright|BR|rBlockTest|s3image|justify|align_center|posFrame|fotorama|alignmentJustify|alignmentCenter|hover|iframeDoc|status|placeholderRemove|aligncenter|Range|insideOutdent|getTextNodesIn|Column|apply|resizer|RedactorPlugins|cursorRange|frameborder|num|buttonsSize|Row|dnbImageTypes|cleanFontTag|childList|s3uploadFile|plugins|isIPad|cleannewLevel|formatQuote|lineHeight|tfoot|buildEventKeydownInsertLineBreak|urlImage|oldsafari|alignleft|youtube|mark|parseHTML|500|editter|redactor_table_rows|oldIE|dropdownShow||imageRemove|focusEnd|thumbtitle|extractContent|INLINE|currBlock|cleanFinish||endRange|hotkeysShiftNums|link_new_tab|insertingAfterLastElement|droparea|redactor_insert_video_area|buttonInactive||focusElem|insert_table|added|fileCallback|airShow|createDocumentFragment|arrSearch|savedHtml|buttonName|inlineEachNodes|paragraphsOnly|DELETE|dropdownWidth|contOwnLine|pageY|align_justify|choose|buildBindKeyboard|uploadSubmit|utag|pageX|cleanSavePreCode|cleanRemoveEmptyTags|redactor_file_alt|ratio|submit|btnHeight|dropdownHide|delete|filename|trigger|formatLinkify|redactor_filename|redactor_image_box|ownLine|setFullpageOnInit|cleanEncodeEntities|fileUploadParam|toggle|addProtocol|insert_row_above|templateVars|align_right|add_head|address|tableDeleteHead|clipboardData|tableAddRow|buttonsHideOnMobile|placeholderOnBlur|fullscreen|insert_column_right|draguploadInit||||tableAddColumn|uploadInit||link_insert|FormData|buildOptions|delete_head|royalSlider|tableId|focus_tr|toolbarObserveScroll|activeButtonsStates|cleanConvertProtected|buildAfter|POST|focus_td|getCaretOffset|pasteClipboardMozilla|linkInsertPressed|mailto|arg|2000px|protocol|redactor_button_disabled|z0|extra|targetBlank|safes|pattern|insert_row_below|chrome|child|insert_column_left|placeholderText|isEndOfElement|blur|predefinedLinks|imageUploadError|placeholderGet|||||||||||cleanTag|th||placeholderOnFocus|maxHeight|DT|blocksLen|fromElement|cleanReplacer|H5|area|fieldset||H1|buildStart|mobile|H4|map|args|filter|H3|cleanHtml|tags|linkNofollow|cleanUnverified|quot|mod|uFEFF|nofollow|cleanlineAfter|formatEmpty|wrapAll|toTagName|setFullpageDoctype|buffer_|transparent|H2|encode|cleanlineBefore|H6|u200D|DD|DL|buttonActiveVisual|toggleVisual|arguments|origHandler|toggleCode|header1|quote|tidyHtml|header2|header3|shortcutsHandler|command|219|hotkeysSpecialKeys|header4|header5|focusCallback|textareaKeydownCallback|langs|shortcutsAdd|dragUpload|toolbarOverflow|scroll|redactor_air_|formattingTags|buttonSource||textareaIndenting|indenting|setInterval|toolbarBuild|paragraph|airButtons|placeholderRemoveFromCode|placeholderRemoveFromEditor|buildEventKeydownPre|TAB||setEditor|buildEventKeydownTab|tabFocus|cleanup|Array|listCurrentText|LEFT_WIN|pastedFrag|buildEventClipboardUpload|items|clipboardFilePaste|altKey|buildEventKeydown|buildEventKeydownBackspace|buildEventKeyup|placeholderInit|buildEventDrop|setCodeIframe|placeholderBlur|placeholderFocus|doctype|timer|clearInterval|buildPlugins|buildEventKeyupConverters|buildEventPaste|iframeCreate|close|write|10005|visibility|execLists|execUnlink|||||||inserthorizontalrule|Image|UL|getCodeIframe|clonedHtml|formattingPre|min|Video|redactor_dropdown_link|Link|strikethrough|subscript|superscript|buildAddClasses|Right|buildContent|Color|buildMobile|placeholderStart|cleanReConvertProtected||OUTPUT|List|xhtmlTags|Head|Left|firstParent|xhtml|buildFromTextarea|buildFromElement|Code|Center|collapseToStart|airBindHide|dropdownObject|innerHeight|Edit|redactor_editor_wym|innerWidth|newLevel|toolbar_fixed_box|toolbarInit|airEnable|arrAdd|keyboard|redactor_air|noeditable|wym|buttonInactiveAll|beforekey|to|buttonActiveToggle|link_edit|Align|afterkey|buttonInactiveVisual|buttonImage|the|returnValue|redactor_dropdown|originalEvent|dropdownBuild|double|pop|tableDeleteTable|s3createCORSRequest|tableDeleteRow|tableDeleteColumn||image_position|||redactor_image_delete_btn|_delete|redactorSaveBtn|s3uploadToS3|XDomainRequest|tableShow||modal_image|tableInsert|focusIndex|tableAddHead||thref|videoInsert|dropalternative|self|location|modal_image_edit|videoShow|tableAddRowAbove|first|tableAddRowBelow|tableAddColumnLeft|tableAddColumnRight|redactor_tab2|redactor_tab3|modal_table|ajax|redactor_insert_link_btn|endOffset|setCaretAfter|setCaret|formatChangeTag|XMLHttpRequest|aLink|removeFromArrayByValue|aEdit|redactor_table_columns|rProtocol|checkbox|nodeName|modal_link|predefined|selectionCreateMarker|redactor_upload_btn|image_web_link|getSelectionHtml|nextSibling|slice|URL|unshift|nextNode|links|atext|modal_file|floor|div_h|uploadFrame|mouse_y|min_w|new_h|random|mousemove|inside|8px|uploadForm|load|99999|000|send|5px|18px|imageEdit|imageResizable|bottom|opacity|setRequestHeader|imageResizeControls|backgroundColor|relative|fff|1px|imageThumbClick|imageResize|re2|Upload|linkInsert|fileShow|fileUploadError|element_action|modalTemplatesInit|or_choose|onload|drop_file_here|281|linkProcess|imageShow|getJSON|fileId|progressBar|imageSave|escapedBackReferences|imageCallbackLink|uploadLoaded|contentDocument|isEmptyObject|rawString|onchangeFunc|imageTabLink|buildProgressBar|redactor_insert_table_btn|aUnlink|draggable|inlineUnwrapSpan|paragraphs|modalSetDraggable|pasteClean|getCaretOffsetRange|equals|replaceLineBreak|isString|video_html_code|jpeg|unbind|percent|outerHeight|opr|fragment|modalLoadTabs|modalSetOverlay|player|modalSetTitle|caretRangeFromPoint|modalShowOnMobile|caretPositionFromPoint|modalSetContent|clientX|clientY|createTextRange|modalSetButtonsWidth|SPAN|twice|switch|linkObserver|modalOnCloseButton|moveToPoint|inlineSetMethods|redactor_insert_video_btn|pastePlainText|insertNodeToCaretPositionFromPoint|pasteClipboardUploadMozilla|imgs|drag|pastePre|aside|inlineFormat|blocksElemsRemove|pasteClipboardUpload|allowfullscreen|newhtml|modal_video|readyState|bufferRedo|bufferUndo|clipboard|HTML|article|draguploadOndrag|s3executeOnSignedUrl|modalShowOnDesktop|overrideMimeType|signedURL|s3handleFileSelect|gif|visible|outerHTML|hdlModalClose|png|internal|loaded|cleanSpaces|getFragmentHtml|draguploadOndragleave|unwrap|uploadProgress|times|modalOpened||Type|pause|undo|capslock|Rows|collapseToEnd|esc|Columns|Email|160px||Position|eval|Title|backspace|jpg|None|onerror|modalSetTab|GET|plain|modalClosed|fast|move|redactor_tab_selected|charset|user|decodeURIComponent|responseText|u00a0|onreadystatechange|withCredentials|defined|handle|removeChild|Web|Content|300px|Text|ftps|redo|keypress|TH|onprogress|isArray|stylesheet|ownerDocument|PUT|redactor_modal_btn_hidden|defaultView|focusin|preview|here|separator_drop2|Object|separator_drop3|Drop|stripHtml|cloneNode|separator_drop1|bull|Internet|Microsoft|appName|getInternetExplorerVersion|Or|iPad|Callback|total|destroy|removeData|getEditor|getObject|Choose|Loading|BlackBerry|Android|iPod|iPhone|alignjustify|redactor_format_h5|Explorer|Underline|Alignment|Open|compatible|safari|trident|Name|sourceWidth|META|CTRL|LEFT|optional|dropdowns|Anchor|redactor_format_blockquote|Horizontal|Rule|redactor_format_h3|MSIE|Justify|redactor_format_h4|redactor_format_h2|parseFloat|anchor|redactor_format_pre|Trident|redactor_format_h1|Deleted|getBox|getIframe|45px|redactor_|failed|alert|enableInlineTableEditing|enableObjectResizing|redactor_droparea|redactor_dropareabox|dragover|dash|redactor_box|TEXTAREA|redactor_dropalternative|frames||blurCallback|public|FileReader|readAsDataURL|acl|amz||word|read|getAsFile||redactorUploadForm|redactorUploadFile|Chrome|536|INPUT|u2010|mdash|syncBefore|download|dataType|syncAfter|8203|default|cache|processData|about|getToolbar|blank|Download|noneditable|File|1class|u00a9|trade|copy|u2026|u2014|hellip|u2122|Embed|addEventListener|ajaxSetup|ajaxSettings|ondrop|dragleave|web|192|JustifyRight|JustifyLeft|JustifyCenter|JustifyFull|editGallery|noscript|extractContents|selectionAll|hasChildNodes|cloneContents|commonAncestorContainer|isCollapsed|isInlineNode||unselectable||jQuery|guid|concat|docs|sid|VERSION|ltr|u200b|gallery|applet|pasteAfter|fake|removeFormat|saveSelection|detach|buttonAddFirst|buttonAdd|buttonAddAfter|460|buttonAddBefore|buttonAwesome|buttonRemoveIcon|thumb|redactor_dropdown_box_|buttonTagToActiveState|xn|pageup|host|buttonRemove|OL|removeMarkers|u1C7F|u0000|restoreSelection|300|SUB|cellIndex|600|firstNode|frameset|SUP|MsoListParagraph|MsoListParagraphCxSpLast|backColor|attributes|foreColor|startOffset|family|fontName|inlineRemoveFormat|toUpperCase|cite|small|setEndAfter|insertText||insertBeforeCursor|inlineSetAttr|inlineRemoveAttr|blockRemoveStyle|blockSetStyle|blockSetAttr|blockRemoveAttr|use|blockRemoveClass|blockSetClass|inlineSetStyle|strict|inlineRemoveStyle|inlineSetClass|inlineRemoveClass||sup|sub|such|math|No|pasteBefore|legend|shapes|colgroup|MsoListParagraphCxSpMiddle|comment|MsoListParagraphCxSpFirst|caption|col|hgroup|nav|setEndPoint|EndToEnd|duplicate|offsetNode|youtu|insertDoubleLineBreak|hasOwnProperty|figcaption|figure|details|menu|summary|redactor_image_box_select|buttonChangeIcon|Quote|119|120|Normal|f10|121|118|Bold|fontcolor|115|116|Italic|117|122|f11|188|610|189|190|191|186|173|f12|123|144|numlock|145|Font|114|Indent|down|Outdent|Ordered|Unordered|Cancel|up|Below|pagedown|home|Above|Save|101|102|112|111|Back|113|backcolor|110|109|104|103|105|106|107|redactorInsertVideoForm|187|220|4px|dropdownShown|pointer|border|external|imageDelete|stopPropagation|touchstart|solid|dragstart|focusNode|13px|redactor_separator_drop|redactor_dropdown_|mouse_x|9px||3px|rgba|dashed|outline|clicked|min_h|redactor_toolbar_|Key|Unlink|380|redactor_tab1|redactor_image_edit_src|fromCharCode|221|222|Formatting||_|redactor_modal_delete_btn|redactorInsertImageForm|fadeIn|redactorUploadFileForm|1000|autosaveError|1500|encodeURIComponent|escape'.split('|'),0,{}));$(document).ready(function(){
    removeLinks();
});

// Once loaded remove any modal links to ensure that
// the modal code doesn't try and load the URL.
removeLinks = function(){
	$('.remove-link').each(function(){
		$(this).attr('href','#');
	});
};