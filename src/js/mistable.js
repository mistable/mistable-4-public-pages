﻿$(document).ready(function(){
    removeLinks();
});

// Once loaded remove any modal links to ensure that
// the modal code doesn't try and load the URL.
removeLinks = function(){
	$('.remove-link').each(function(){
		$(this).attr('href','#');
	});
};