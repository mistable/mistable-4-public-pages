#MiStable Public

##Directory structure

 - **dist** - This is the production ready code for viewing
   - **css**
   - **fonts**
   - **images**
   - **js**
   - index.html - Final style guide HTML
 - **src**
   - **icons** - SVG icons
   - **js** - not really used
   - **kit** - Kit files, included much the same as php includes
     - **_components** - MiStable components
     - **_includes** - Bootstrap components and other file includes
   - **less** - LESS files used to generate dist/css
     - **bootstrap** - Custom MiStable Bootstrap
     - **plugins** - Styles from plugins being used
   - **sprites** - Grunt uses this folder to generate sprites
 - package.json - Used by NPM to install modules
 - Gruntfile.js - Grunt configuration file


# Using Grunt

 1. Install Node.js
 2. Install grunt using ```npm install -g grunt-cli```
 3. Open a command prompt and ```cd``` to the checked out directory
 4. Type ```npm install``` to install required components
 5. Type ```grunt``` to build
 5. Type ```grunt watch``` to live build every time there are changes