module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        codekit: {
            globbed_example_config : {
                src : 'src/kit/**/*.kit',
                dest : 'dist/'
            }
        },

        jshint: {
            files: ['Gruntfile.js', 'src/**/*.js', 'test/**/*.js'],
            options: {
                globals: {
                    jQuery: true,
                    console: true,
                    module: true,
                    document: true,
                },
                ignores: [
                    'src/js/vendor/*.js',
                    'src/js/libraries/*.js',
                    'src/js/plugins/*.js'
                ]
            }
        },

        concat: {
            options: {
                separator: ';'
            },
            dist: {
                src: ['src/js/vendor/*.js', 'src/js/libraries/*.js', 'src/js/plugins/*.js', 'src/js/*.js'],
                dest: 'dist/js/<%= pkg.name %>.js'
            }
        },

        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
            },
            dist: {
                files: {
                    'dist/js/<%= pkg.name %>.min.js': ['<%= concat.dist.dest %>']
                }
            }
        },

        sprite: {
            all: {
                src: ['src/sprites/*.png'],

                retinaSrcFilter: ['src/sprites/*-2x.png'],

                dest: 'src/images/spritesheet.png',
                imgPath: '../images/spritesheet.png',

                retinaDest: 'src/images/spritesheet-2x.png',
                retinaImgPath: '../images/spritesheet-2x.png',
                destCss: 'src/less/mistable-sprites.less',

                cssVarMap: function (sprite) {
                    sprite.name = 'sprite_' + sprite.name;
                }
            }
        },

        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: 'src/images/',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: 'dist/images/'
                }]
            }
        },

        webfont: {
            options: {
                engine: "node",
                font: 'icons'
            },
            icons: {
                src: 'src/icons/*.svg',
                dest: 'dist/fonts/',
                destCss: 'src/less/',
                options: {
                    stylesheet: 'less',
                    hashes: false,
                    relativeFontPath: '../fonts/',
                    templateOptions: {
                        baseClass: 'icon',
                        classPrefix: 'icon_',
                        mixinPrefix: 'icon-',
                    }
                }
            }
        },

        less: {
            development: {
                options: {
                    paths: ["src/less/**/*.less"]
                },
                files: {
                    "dist/css/mistable.css": "src/less/mistable.less"
                }
            }
        },

        copy: {
            main: {
                files: [
                    {
                        expand: true,
                        cwd: 'src/fonts/',
                        src: ['**'],
                        dest: 'dist/fonts/'
                    },
                ],
            },
        },

        watch: {
            kit: {
                files: ['<%= codekit.globbed_example_config.src %>'],
                tasks: ['codekit'],
                options: {
                    livereload: {
                        port: 35729
                    },
                }
            },

            less: {
                files: ['<%= less.development.options.paths %>'],
                tasks: ['less'],
                options: {
                    livereload: {
                        port: 35729
                    },
                }
            },

            icons: {
                files: ['<%= webfont.icons.src %>'],
                tasks: ['less'],
                options: {
                    livereload: {
                        port: 35729
                    },
                }
            },

            sprites:  {
                files: ['<%= sprite.all.src %>'],
                tasks: ['sprite', 'imagemin', 'less'],
                options: {
                    livereload: {
                        port: 35729
                    },
                }
            },

            images: {
                files: ['<%= imagemin.dynamic.files[0].cwd %><%= imagemin.dynamic.files[0].src %>'],
                tasks: ['imagemin', 'less'],
                options: {
                    livereload: {
                        port: 35729
                    },
                }
            },

            scripts: {
                files: ['<%= jshint.files %>'],
                tasks: ['jshint', 'concat', 'uglify'],
                options: {
                    livereload: {
                        port: 35729
                    },
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-codekit');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-spritesmith');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-webfont');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['codekit', 'jshint', 'concat', 'uglify', 'sprite', 'imagemin', 'webfont', 'copy', 'less']);
};